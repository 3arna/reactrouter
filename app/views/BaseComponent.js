import React from 'react';

class BaseComponent extends React.Component {
  constructor(...args){
    super(...args);
    this._bind('_handleError', '_clearError');
    this.state = {
      isFirstLoad: true
    }
  }

  componentWillUpdate(){
    if(this.state.isFirstLoad){
      this.setState({ isFirstLoad: false });
    }
  }

  _bind(...methods) {
    methods.forEach( (method) => this[method] = this[method].bind(this) );
  }
  _handleError(errorMessage, opts){
    if(this.props.returnErrorMessage){
      this.props.returnerrorMessage(errorMessage);
      return true;
    }

    this.setState({errorMessage: errorMessage});
    this.time = setTimeout(() => this.setState({errorMessage: false}), this.props.errorShowTime || 5000 );
  }
  _clearError(errorMessage){
    clearTimeout(this.timer);
    this.setState({errorMessage: false})
  }
}

export default BaseComponent;
