/*globals document*/

import React from 'react';
import { connectToStores, provideContext } from 'fluxible-addons-react';
import { handleHistory, navigateAction } from 'fluxible-router';
import classNames from 'classnames';
import lodash from 'lodash';
import DocumentTitle from 'react-document-title';
import DocumentMeta from 'react-doc-meta'
import routesConfig from '../routes';

import LoadingPage from './pages/Loading';
import ErrorPage from './pages/ErrorPage';
import NotFoundPage from './pages/NotFound';
import AccessDeniedPage from './pages/AccessDenied';
import Nav from './components/navigations/Nav';
import MobNav from './components/navigations/MobNav';
import UnauthorizedNav from './components/navigations/UnauthorizedNav';
import ToastContainer from './components/toast/ToastContainer';

import { getCategories } from '../actions/categoryActions';
import { getBookmarks } from '../actions/bookmarkActions';
import { getAlerts } from '../actions/alertActions';


@connectToStores(['ApplicationStore', 'UserStore'], (context, props) => {
  const appStore = context.getStore('ApplicationStore');
  const userStore = context.getStore('UserStore');
  return {
    currentPageName: appStore.getCurrentPageName(),
    pageTitle: appStore.getPageTitle(),
    previousUrl: appStore.getPreviousUrl(),
    userLoggedIn: userStore.isLoggedIn(),
    user: userStore.user,
    appInfo: appStore.appInfo,
    alerts: appStore.alerts,
    toastProcess: appStore.processGet('toast'),
  };
})
@handleHistory
@provideContext({
  apiConfig: React.PropTypes.object,
  authenticatedUser: React.PropTypes.object,
  config: React.PropTypes.object,
  translate: React.PropTypes.func,
  getLanguage: React.PropTypes.func,
  getLanguages: React.PropTypes.func,
  userLoggedIn: React.PropTypes.bool,
})
class Application extends React.Component {

  componentDidMount(){
    if(!!this.props.userLoggedIn){
      this.startAlertPolling();
    }
  }

  componentWillUnmount(){
    if(this._pollTimer){
      clearInterval(this._pollTimer);
    }
  }

  startAlertPolling(){
    if(this._pollTimer){
      return;
    }

    const pollInterval = context.getComponentContext().apiConfig.alertsPollFrequency;

    this._pollTimer = setInterval(() => {
      if(!this.props.userLoggedIn){
        return clearInterval(this._pollTimer);
      }
      context.executeAction(getAlerts);
    }, pollInterval);
  }

  render() {
    const { currentRoute, currentNavigateError, isNavigateComplete } = this.props;
    const Handler = currentRoute && currentRoute.get('handler');

    let content;
    let loading;

    if (currentNavigateError && currentNavigateError.statusCode === 404) {
      content = <NotFoundPage />;
    }
    else if (currentNavigateError && (currentNavigateError.statusCode === 403 || currentNavigateError.statusCode === 401)){
      content = <AccessDeniedPage />;
    }
    else if (currentNavigateError) {
      if(currentNavigateError.statusCode === 302){
        content = null;
      }else{
        // Generic error, usually always with statusCode 500
        content = <ErrorPage err={currentNavigateError} userLoggedIn={this.props.userLoggedIn} currentUrl={currentRoute.get('url')} />;
      }
    }
    else if (!Handler) {
      // No handler: this is another case where a route is not found (e.g.
      // is not defined in the routes.js config)
      //content = <NotFoundPage />;
    }
    else if (!isNavigateComplete && currentRoute && currentRoute.get('showLoadingPage')) {
      // Show a loading page while waiting the route's action to finish
      content = <LoadingPage />;
    }
    else {
      // Here you go with the actual page content
      const params = currentRoute.get('params').toJS();
      const extrasForIndividualPages = currentRoute.get && currentRoute.get('isIndividual') && {
        appInfo: this.props.appInfo,
        previousUrl: this.props.previousUrl,
        mobNavHide: currentRoute.get('navHide'),
        isLoggedIn: this.props.userLoggedIn,
      };

      content = (<div
          key={currentRoute.get('url')} className="h-100">
            <Handler {...params} isNavigateComplete={isNavigateComplete} {...extrasForIndividualPages} />
      </div>);
    }

    const translate = this.props.context.translate;
    const defaultMetaTags = [
      { name: 'description', content: translate('phrases.versyLetsYouShareAndConverse') }
    ];

    return (
      <DocumentTitle title={this.props.pageTitle}>
        <div className="h-100">
          <DocumentMeta tags={defaultMetaTags}/>
          {!this.props.userLoggedIn && currentRoute && !currentRoute.get('navHide') && !currentRoute.get('isIndividual') && <UnauthorizedNav/>}

          {currentRoute && !currentRoute.get('mobNavHide') && <MobNav {...this.props}/>}


          {currentRoute && !currentRoute.get('navHide') &&
            <header className="fix-bb fix-clear w-min-60px bg-greyl2 brd-r-white valign-top pos-fixed z-12 h-100 boxshadow-r-5 sm-dsp-none">
              <Nav
                selected={this.props.currentPageName}
                links={routesConfig}
                userLoggedIn={this.props.userLoggedIn}
                user={this.props.user}
                loading={!isNavigateComplete}
                alerts={this.props.alerts} />
            </header>}

          <div className={this._getMainContainerClassNames(currentRoute, this.props.userLoggedIn)}>
            { content }
          </div>
          <ToastContainer toastProcess={this.props.toastProcess}/>
        </div>
      </DocumentTitle>
    )
  }

  _getMainContainerClassNames(currentRoute, userLoggedIn){
    return classNames({
      'w-100 h-100vh fix-bb': true,
      'sm-pdg-t-50px': currentRoute && !currentRoute.get('mobNavHide') && userLoggedIn,
      //'pdg-t-50px pdg-t-100px': !userLoggedIn,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.props.pageTitle !== prevProps.pageTitle) {
      document.title = this.props.pageTitle;
    }
  }
}

export default Application;
