/*globals document*/

import React from 'react';
import { Link } from 'react-router';
import { connectToStores, provideContext } from 'fluxible-addons-react';
import classNames from 'classnames';
import lodash from 'lodash';
import DocumentTitle from 'react-document-title';
import DocumentMeta from 'react-doc-meta'
import routesConfig from '../routes';

import LoadingPage from './pages/Loading';
import ErrorPage from './pages/ErrorPage';
import NotFoundPage from './pages/NotFound';
import AccessDeniedPage from './pages/AccessDenied';
import Nav from './components/navigations/Nav';
import MobNav from './components/navigations/MobNav';
import UnauthorizedNav from './components/navigations/UnauthorizedNav';
import ToastContainer from './components/toast/ToastContainer';

import { getCategories } from '../actions/categoryActions';
import { getBookmarks } from '../actions/bookmarkActions';
import { getAlerts } from '../actions/alertActions';


class Application extends React.Component {

  static contextTypes = {
    getStore: React.PropTypes.func,
    executeAction: React.PropTypes.func,
    config: React.PropTypes.object,
    history: React.PropTypes.object.isRequired,
    location: React.PropTypes.object.isRequired
  };

  constructor(props, context){
    super(props, context);
  }

  render(){
    const { currentRoute, currentNavigateError, isNavigateComplete } = this.props;

    return <DocumentTitle title={this.props.pageTitle}>
      <div className="h-100">
        {false && <DocumentMeta tags={defaultMetaTags}/>}
        {!this.props.userLoggedIn && currentRoute && !currentRoute.get('navHide') && !currentRoute.get('isIndividual') && <UnauthorizedNav/>}

        {currentRoute && !currentRoute.get('mobNavHide') && <MobNav {...this.props}/>}


        {currentRoute && !currentRoute.get('navHide') &&
          <header className="fix-bb fix-clear w-min-60px bg-greyl2 brd-r-white valign-top pos-fixed z-12 h-100 boxshadow-r-5 sm-dsp-none">
            <Nav
              selected={this.props.currentPageName}
              links={routesConfig}
              userLoggedIn={this.props.userLoggedIn}
              user={this.props.user}
              loading={!isNavigateComplete}
              alerts={this.props.alerts} />
          </header>}

        <div className={this._getMainContainerClassNames(currentRoute, this.props.userLoggedIn)}>
          {this.props.children}
        </div>
        {false && <ToastContainer toastProcess={this.props.toastProcess}/>}
      </div>
    </DocumentTitle>
  }

  _getMainContainerClassNames(currentRoute, userLoggedIn){
    return classNames({
      'w-100 h-100vh fix-bb': true,
      'sm-pdg-t-50px': currentRoute && !currentRoute.get('mobNavHide') && userLoggedIn,
      //'pdg-t-50px pdg-t-100px': !userLoggedIn,
    });
  }
}

export default Application;
