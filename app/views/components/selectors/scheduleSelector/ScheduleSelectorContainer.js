import React from 'react';
import moment from 'moment';
import lodash from 'lodash';
import momentTz from 'moment-timezone';

import BaseComponent from '../../../BaseComponent';
import ScheduleSelector from './ScheduleSelector';

import { report } from '../../../../actions/reportActions';
import { toastProcessSuccess, toastProcessError } from '../../../../actions/applicationActions';

export default class ScheduleSelectorContainer extends BaseComponent {

  constructor(){
    super();
    this._bind(
      'handleClose',
      'handleSubmit',
      'handleChangeMonth',
      'handleSelectDate',
      'handleSelectTime',
      'handleSelectTimezone'
    );

    this.state = {
      date: null,
      selectedDate: null,
      selectedHours: 0,
      selectedMinutes: 0,
      selectedTimezone: null,
    };
  }

  componentWillMount(){
    const {publishDate} = this.props;
    const timezone = moment.tz.guess();
    this.setState({
      selectedDate: publishDate || moment().tz(timezone).format(),
      selectedHours: publishDate ? moment(publishDate).tz(timezone).hour() : moment().hour()+1,
      selectedMinutes: publishDate ? moment(publishDate).tz(timezone).minutes() : 30,
      selectedTimezone: timezone,
      date: publishDate || moment().format()
    });
  }

  handleChangeMonth(number){
    this.setState({date: moment(this.state.date).add(number, 'M').format()});
  }

  handleSelectDate(selectedDate){
    const { selectedTimezone } = this.state;
    if(moment(this.state.selectedDate).format('YY-MM-DD') === moment(selectedDate).tz(selectedTimezone).format('YY-MM-DD')){
      selectedDate = null;
    }
    this.setState({selectedDate: selectedDate && moment(selectedDate).tz(selectedTimezone)})
  }

  handleSelectTime(time, type){
    switch(type){
      case 'hours':
        this.setState({selectedHours: time})
      break;
      case 'minutes':
        this.setState({selectedMinutes: time})
      break;
    }
  }

  handleSelectTimezone(timezone){

    const { state } = this;
    const zone = lodash.isArray(timezone) && lodash.first(timezone) || timezone;
    const publishDate = moment(state.selectedDate).hours(state.selectedHours).minute(state.selectedMinutes);

    this.setState({
      selectedDate: publishDate.tz(zone),
      selectedHours: publishDate.tz(zone).hour(),
      selectedMinutes: publishDate.tz(zone).minutes(),
      selectedTimezone: zone,
      date: publishDate
    });
  }

  handleClose(){
    this.props.onSetContextMenuAction(null);
  }

  handleSubmit(){
    const {props, state} = this;

    const timestamp = state.selectedDate
      ? moment(state.selectedDate).tz(state.selectedTimezone).hours(state.selectedHours).minute(state.selectedMinutes)
      : null;

    if(timestamp && moment().diff(timestamp, 'minutes') > -5){
      return this.context.executeAction(toastProcessError, this.context.translate('phrases.postMustBeScheduledInFuture'));
    }

    props.onSubmit(timestamp);
    props.onCancel();
  }

  get modifiedReasons(){
    return this.props.reasons.map((reason) => {
      reason.active = reason.value === this.state.reason;
      reason.text = this.context.translate(reason.text);
      return reason;
    });
  }

  get monthWeeks(){
    const { date, selectedDate } = this.state;

    const startOfMonth = moment(date).startOf('month');
    const weeks = [];

    for(let weekIndex = 0; weekIndex <= 4; weekIndex++){
      const startOfWeek = startOfMonth.clone().add(weekIndex, 'w').day('Sunday');
      const days = [];

      for(let dayIndex = 1; dayIndex <= 7; dayIndex++){
        const day = moment(startOfWeek.clone().add(dayIndex, 'd'));
        days.push({
          date: day,
          dayNumber: day.format('DD'),
          isCurrentMonth: day.month() === startOfMonth.clone().month(),
          isSelected: day.clone().format('YY-DD-MM') === moment(selectedDate).format('YY-DD-MM'),
          isToday: day.clone().format('YY-DD-MM') === moment().format('YY-DD-MM'),
          isDisabled: day.diff(moment().format(), 'days') < 0,
        });
      }
      weeks.push(days);
    }
    return weeks;
  }

  get timezoneList(){
    let timezones = [];
    let namedTimezones = {};

    moment.tz.names().forEach((timezone) => {
      const timezoneGMT = moment.tz(timezone).format('Z');

      namedTimezones[timezoneGMT]
        ? namedTimezones[timezoneGMT].push(timezone)
        : namedTimezones[timezoneGMT] = [timezone];
    });

    for(const timezone in namedTimezones){
      const values = namedTimezones[timezone];
      const timezoneTitle = [];
      values.map((value) =>
        lodash.includes(['Europe', 'America', 'Australia', 'Pacific', 'Asia'], lodash.first(value.split('/')))
        && lodash.last(value.split('/'))
        && timezoneTitle.push(lodash.last(value.split('/')))
      );

      timezones.push({gmt: timezone, value: values, title: `GMT(${timezone}) ${timezoneTitle.join(', ')}`});
    }
    return lodash.sortBy(timezones, (timezone) => parseInt(timezone.gmt));
  }

  render(){
    const {props, state} = this;

    return <ScheduleSelector
      date={state.date}
      isFullScreen={props.isFullScreen}
      onCancel={props.onCancel}
      onSubmit={this.handleSubmit}
      onSelectDate={this.handleSelectDate}
      onSelectTime={this.handleSelectTime}
      onSelectTimezone={this.handleSelectTimezone}
      onChangeMonth={this.handleChangeMonth}
      monthWeeks={this.monthWeeks}
      selectedDate={state.selectedDate}
      selectedHours={state.selectedHours}
      selectedMinutes={state.selectedMinutes}
      selectedTimezone={state.selectedTimezone}
      timezoneList={this.timezoneList}
    />
  }

  static contextTypes = {
    translate: React.PropTypes.func.isRequired,
    executeAction: React.PropTypes.func,
  };

  static defaultProps = {};
}
