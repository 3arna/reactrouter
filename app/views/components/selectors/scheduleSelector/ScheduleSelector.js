import { NavLink } from 'fluxible-router';
import React from 'react';
import lodash from 'lodash';
import moment from 'moment';
import momentTz from 'moment-timezone';
import classNames from 'classnames';
import { translateWeekdays, translateTime } from '../../../../utils/dateUtils';

import DefaultButton from '../../elements/buttons/DefaultButton';
import DateDropdown from '../../elements/dropdowns/DateDropdown';

const _getDayClassNames = (day) => classNames({
  'w-30px h-30px crs-point line-30px mrg-tb-10px dsp-inline brd-rad-50': true,
  'bg-main clr-white': day.isSelected,
  'hover-bg-greyd4 hover-clr-white': !day.isSelected && !day.isToday && !day.isDisabled,
  'bg-white clr-black': day.isToday && !day.isSelected,
  'clr-grey': day.isDisabled,
  'clr-greyd3': !day.isCurrentMonth && !day.isDisabled && !day.isSelected,
});

const _getArrowClassNames = (isNext) => classNames({
  'crs-point hover-clr-white clr-greyl2': true,
  'icon-chevron-back': !isNext,
  'icon-chevron-forward': isNext,
});

const _getContainerClassNames = (isFullScreen) => classNames({
  'fadeIn pos-absolute z-5 top-0 left-0 mrg-auto h-100 pdg-t-100px sm-pdg-t-50px w-100 fix-bb': true,
  'col-8 md-col-12': !isFullScreen,
});

const _generateTime = (time) => (parseInt(time, 10) + 100).toString().substr(1);

const _getTimezoneTitle = (timezoneList, timezone) => {
  const zone = lodash.find(timezoneList, (tz) => lodash.includes(tz.value, timezone));
  return zone && lodash.trunc(zone.title);
}

const ScheduleSelector = (props, { translate, getLanguage }) => {
  console.log('yoyoyo', props.timezoneList)
  return <div className={_getContainerClassNames(props.isFullScreen)}>

      <div onClick={props.onCancel} className="bg-op-05 pos-absolute h-100 w-100 top-0"/>

      <div className="valign-block h-100">
        <div className="txt-center">
          <span className="dsp-inline bg-white w-max-400px pos-relative pdg-20px">

            <div className="txt-left mrg-b-30px">
              <i className="icon-clock dsp-inline mrg-r-10px fnt-md"/>
              <h3 className="fnt-400 dsp-inline mrg-0">{translate('phrases.scheduleYourPost')}</h3>
              <i onClick={props.onCancel} className="crs-point icon-close flt-right fnt-md"/>
            </div>

            <div className="pdg-rl-10">

              <div className="bg-greyd4 clr-white pdg-rl-10px pdg-tb-10px valign-block">
                <i onClick={(e) => props.onChangeMonth(-1)} className={_getArrowClassNames()}/>
                <div className="w-100">
                  <p className="pdg-0 mrg-0 mrg-t-3px txt-cap">
                    {moment(props.date).locale(getLanguage()).format('MMMM')}
                  </p>
                  <div className="fnt-xxxs mrg-0 pdg-0 dsp-block">
                    {moment(props.date).format('YYYY')}
                  </div>
                </div>
                <i onClick={(e) => props.onChangeMonth(+1)} className={_getArrowClassNames(true)}/>
              </div>

              <div className="tb tb-fixed w-100 fnt-xxs">
                <div className="tr clr-white">
                  {translateWeekdays(getLanguage()).map((day) =>
                    <div key={day} className="td bg-greyd2 pdg-tb-10px txt-cap">
                      {day}
                    </div>)}
                </div>
                {props.monthWeeks.map((week, index) =>
                  <div key={index} className="tr">
                    {week.map((day, index) =>
                      <div key={index} className="td bg-greyl1">
                        <span onClick={!day.isDisabled && ((e) => props.onSelectDate(day.date))} className={_getDayClassNames(day)}>
                          {day.dayNumber}
                        </span>
                      </div>)}
                  </div>)}
              </div>

            </div>

            <div className="tb w-100 mrg-t-10px txt-left pdg-rl-10 fnt-sm">
              <DateDropdown
                className="dsp-block"
                defaultValue={_getTimezoneTitle(props.timezoneList, props.selectedTimezone)}
                isDisabled={!props.selectedDate}
                onClick={props.onSelectTimezone}
                items={props.timezoneList}
                title={translateTime(getLanguage(), 'timezone')}
                type="timezones"/>

              <div className="tb w-100 txt-left">

                <div className="td">
                  <DateDropdown
                    type="hours"
                    onClick={props.onSelectTime}
                    isDisabled={!props.selectedDate}
                    className="mrg-r-10px dsp-inline"
                    title={translateTime(getLanguage(), 'hours')}
                    defaultValue={props.selectedHours}
                    items={[...Array(24)]}
                    useIndex
                    formatValueToTime/>

                  <DateDropdown
                    className="mrg-r-10px dsp-inline"
                    defaultValue={props.selectedMinutes}
                    isDisabled={!props.selectedDate}
                    items={[0, 30]}
                    onClick={props.onSelectTime}
                    title={translateTime(getLanguage(), 'minutes')}
                    type="minutes"
                    formatValueToTime/>
                </div>
                <div className="td valign-bot txt-right">
                  <DefaultButton className="valign-bot"
                    onClick={props.onSubmit}
                    text={props.selectedDate && translate('words.schedule') || translate('words.done')}/>
                </div>
              </div>
            </div>

          </span>
        </div>
      </div>
  </div>
}

ScheduleSelector.contextTypes = {
  translate: React.PropTypes.func,
  getLanguage: React.PropTypes.func
}

ScheduleSelector.defaultProps = {
  selectedDate: moment().format(),
  date: moment().format(),
}

export default ScheduleSelector;
