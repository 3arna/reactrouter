import React, { PropTypes } from 'react';
import classNames from 'classnames';
import lodash from 'lodash';

import ErrorAlert from '../../elements/alerts/ErrorAlert';

import MainCategory from '../../elements/categories/MainCategory';
import CategorySearchInput from '../../elements/inputs/CategorySearchInput';

const _getOutOfClassNames = (categories, minCategories) => classNames({
  'fnt-xs pdg-tb-2px': true,
  'clr-redl2': categories.length && categories.length < minCategories,
});

const CategorySelector = (props) => {
  const {
    onChange,
    onChangeDelayed,
    onSubmit,
    delay,
    placeholder,
    inputValue,
    errorMessage,
    onClearErrorMessage,
    maxCategories,
    minCategories,
    selectedCategories,
    categories,
    onRemoveCategory,
    notCategoriesSelectedMessage,
    searchedCategories,
    searchedCategoryExists,
    textCategoryNotExist,
    isSelectedCategory,
    categoryAlreadyAdded,
    messageAlreadyAdded
  } = props;
  
  return <div className="pos-relative">
    <ErrorAlert errorMessage={ errorMessage } />

    <CategorySearchInput
      onChange={onChange}
      onChangeDelayed={onChangeDelayed}
      delay={delay}
      onSubmit={onSubmit}
      placeholder={placeholder}
      inputValue={inputValue}/>

    <div className="pdg-5px fix-bb md-dsp-block md-txt-wrap txt-center md-pdg-tb-10px">
      <div className={_getOutOfClassNames(selectedCategories, minCategories)}>
        {selectedCategories.length} of {maxCategories}
        {(!selectedCategories.length || selectedCategories.length<minCategories) && <div className="mrg-t-10px clr-greyd3" key="empty">
          {notCategoriesSelectedMessage}
        </div>}
      </div>

      <div className="tb w-100 pdg-rl-20px mrg-tb-15px fix-bb">
        <div className="td txt-center clr-grey">
          {!!selectedCategories.length && selectedCategories.map((category) =>
            <MainCategory
              key={category}
              category={category}
              isSelected={true}
              fnClick={onRemoveCategory} />
          )}
        </div>
      </div>
    </div>

    <div className="txt-center">

      {searchedCategories
        && !categoryAlreadyAdded(inputValue)
        && !!inputValue.length
        && !searchedCategoryExists(lodash.get(searchedCategories, 0))
        && <div className="mrg-auto pdg-b-10px">
          <MainCategory
            addClass="dps-inline"
            isGhostCategory={true}
            key="ghost"
            category={inputValue}
            fnClick={onSubmit} />
          <span className="dsp-inline valign-top pdg-tb-15px fnt-sm pdg-l-10px">{textCategoryNotExist}</span>
        </div>}

      {categories.map((category) =>
        isSelectedCategory(category) && 
          <MainCategory
            key={category.id || category}
            category={category}
            fnClick={onSubmit} />
      )}
      {categoryAlreadyAdded(inputValue)
        && <div key="empty" className="pdg-l-15px mrg-tb-20px clr-greyd3 fnt-xs">
        {messageAlreadyAdded}
      </div>}

    </div>

  </div>
}

export default CategorySelector;

