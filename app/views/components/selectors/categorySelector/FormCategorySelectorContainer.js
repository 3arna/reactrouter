import React from 'react';
import lodash from 'lodash';
import { connectToStores, provideContext } from 'fluxible-addons-react';

import Category from '../../../../models/Category';

import BaseComponent from '../../../BaseComponent';

import CategorySelectorContainer from './CategorySelectorContainer';
import OverlayLayout from '../../../layouts/OverlayLayout';

export default class FormCategorySelectorContainer extends BaseComponent {

  constructor(){
    super();
    this._bind('handleCategoriesStore', 'handleSubmit');
    this.state = { selectedCategories: []};
  }

  handleCategoriesStore(selectedCategories){
    this.setState({selectedCategories})
  }

  handleSubmit(){
    this.props.onSubmit && this.props.onSubmit(this.state.selectedCategories);
    this.props.onCancel && this.props.onCancel();
  }

  render(){ 
    const { props, state } = this;

    return <OverlayLayout 
      isFullScreen={props.isFullScreen}
      onCancel={props.onCancel} 
      onSubmit={this.handleSubmit}
      title={this.context.translate('phrases.addInterests')}>
      <CategorySelectorContainer
        minCategories={0}
        maxCategories={3}
        placeholderText={this.context.translate('phrases.searchHobbiesOrInterests')}
        messageMaxReached={this.context.translate('phrases.sorryYouveHitTheMaximumNumberOfInterests')}
        textCategoryNotExist={this.context.translate('phrases.doesNotExistSelectToAddIt')}
        interests={ props.interests }
        returnSelectedCategories={ this.handleCategoriesStore }/>
    </OverlayLayout>
  }

  static contextTypes = {
    translate: React.PropTypes.func
  }
  
  static propTypes = {}
}
