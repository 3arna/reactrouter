import React, { PropTypes } from 'react';
import classNames from 'classnames';
import lodash from 'lodash';

import ErrorAlert from '../../elements/alerts/ErrorAlert';

import FrontCategory from '../../elements/categories/FrontCategory';
import CategorySearchInput from '../../elements/inputs/CategorySearchInput';

const _getOutOfClassNames = (categories, minCategories) => classNames({
  'fnt-sm fnt-second pdg-tb-2px': true,
  'clr-redl2': categories.length && categories.length < minCategories,
});

const FirstCategorySelector = (props) => {
  const {
    onChange,
    onChangeDelayed,
    onSubmit,
    delay,
    placeholder,
    inputValue,
    errorMessage,
    onClearErrorMessage,
    componentTitle,
    maxCategories,
    minCategories,
    selectedCategories,
    categories,
    onRemoveCategory,
    notCategoriesSelectedMessage,
    searchedCategories,
    searchedCategoryExists,
    textCategoryNotExist,
    isSelectedCategory,
    categoryAlreadyAdded,
    messageAlreadyAdded
  } = props;
  
  return <div className="pos-relative">
    <ErrorAlert errorMessage={ errorMessage } />

    <div className="pdg-b-10px fnt-xl fnt-bold fnt-bold">{componentTitle}</div>
    <div className="pdg-rl-10">
      <CategorySearchInput
        theme="big"
        onChange={onChange}
        onChangeDelayed={onChangeDelayed}
        delay={delay}
        onSubmit={onSubmit}
        placeholder={placeholder}
        inputValue={inputValue}/>
    </div>

    <div className="pdg-5px fix-bb md-dsp-block md-txt-wrap txt-center md-pdg-tb-10px">
      <div className={_getOutOfClassNames(selectedCategories, minCategories)}>
        {selectedCategories.length} of {maxCategories}
      </div>
      <div className="tb w-100 pdg-rl-20px fix-bb">
        <div className="td txt-center pdg-t-10px h-100px clr-grey">
          {!!selectedCategories.length && selectedCategories.map((category) =>
            <FrontCategory
              key={category}
              category={category}
              isSelected={true}
              fnClick={onRemoveCategory} />
          )}
          {!selectedCategories.length && <div key="empty">
            {notCategoriesSelectedMessage}
          </div>}
        </div>
      </div>
    </div>

    <div className="pdg-t-10px">

      {searchedCategories
        && !!inputValue.length
        && !categoryAlreadyAdded(inputValue)
        && !searchedCategoryExists(lodash.get(searchedCategories, 0))
        && <div className="mrg-auto pdg-b-10px">
          <FrontCategory
            addClass="dps-inline"
            isGhostCategory={true}
            key="ghost"
            category={inputValue}
            fnClick={onSubmit} />
          <span className="dsp-inline valign-top pdg-tb-15px fnt-sm pdg-l-10px">{textCategoryNotExist}</span>
        </div>}

      {categories.map((category) =>
        isSelectedCategory(category) && <FrontCategory
          key={category.id}
          category={category}
          fnClick={onSubmit} />
      )}
      {categoryAlreadyAdded(inputValue)
        && <div key="empty" className="pdg-l-15px pdg-tb-10px clr-grey">
        {messageAlreadyAdded}
      </div>}

    </div>

  </div>
}

export default FirstCategorySelector;

