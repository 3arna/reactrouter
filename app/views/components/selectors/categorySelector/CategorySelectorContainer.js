import React, { PropTypes } from 'react';
import lodash from 'lodash';
import { connectToStores, provideContext } from 'fluxible-addons-react';

import * as validationUtils from '../../../../utils/validationUtils';

import BaseComponent from '../../../BaseComponent';
import CategorySelector from './CategorySelector';
import FirstCategorySelector from './FirstCategorySelector';
import OverlayLayout from '../../../layouts/OverlayLayout';

import { getSearchedCategories } from '../../../../actions/categoryActions';

@connectToStores(['CategoryStore'], (context, props) => {
  return {
    categories: context.getStore('CategoryStore').categories,
    searchedCategories: context.getStore('CategoryStore').searchedCategories,
  };
})

export default class CategorySelectorContainer extends BaseComponent {

  constructor(){
    super();
    this._bind(
      'handleSubmit', 
      'handleRemoveCategory', 
      'isSelectedCategory', 
      'handleChange', 
      'searchedCategoryExists', 
      'categoryAlreadyAdded',
      'handleDone'
    );
    this.state = {
      errorMessage: false,
      categories: [],
      filteredCategories: false,
      categoryInputValue: ''
    };
  }

  componentDidMount(){
    if(!this.props.interests){
      return false;
    }
    const categories = this.props.interests.map((interest) => interest.name && interest.name.toLowerCase() || interest.toLowerCase());
    this.setState({ categories });
    this.props.returnSelectedCategories && this.props.returnSelectedCategories(categories);
  }

  handleCategoriesSearch(e){
    const value = lodash.isString(e) && e || e.target && e.target.value || '';
    context.executeAction(getSearchedCategories, { query: value })
  }

  handleSubmit(e){
    const categoryName = e.target && e.target.value || e;
    const valid = validationUtils.validInput(categoryName, {
      type: 'text',
      minLength: this.props.minCategoryLength,
      maxLength: this.props.maxCategoryLength,
    });

    if(!valid){
      return this._handleError(this.context.translate('phrases.interestContainsUnsupportedCharacters'));
    }

    if(lodash.contains(this.state.categories, categoryName)){
      this._handleError(this.props.messageAlreadyAdded);
      return false;
    }
    if(this.state.categories.length >= this.props.maxCategories){
      this._handleError(this.props.messageMaxReached);
      return false;
    }

    const categories = lodash.union(this.state.categories, [categoryName.toLowerCase()]);
    this.setState({ categories, categoryInputValue: ''});
    this.handleCategoriesSearch('');
    this.props.returnSelectedCategories && this.props.returnSelectedCategories(categories);
  }
  handleRemoveCategory(categoryName){
    const categories = lodash.remove(this.state.categories, (cat) => cat !== categoryName);
    this.setState({ categories });
    this.props.returnSelectedCategories && this.props.returnSelectedCategories(categories);
  }

  categoryAlreadyAdded(categoryName){
    return lodash.contains(this.state.categories, categoryName);
  }

  isSelectedCategory(category){
    category = category.name || category;
    return !lodash.contains(this.state.categories, category.toLowerCase());
  }
  searchedCategoryExists(category){
    return category
      && this.state.categoryInputValue
      && category.name.toLowerCase() === this.state.categoryInputValue.toLowerCase();
  }
  handleChange(inputEvent){
    if(this.props.maxCategoryLength < inputEvent.target.value.length){
      return this._handleError(this.context.translate('phrases.interestNameTooLong') || this.props.messageToLong);
    }
    this.setState({
      categoryInputValue: inputEvent.target.value && inputEvent.target.value.toLowerCase() || ''
    });
  }

  get notCategoriesSelectedMessage(){
    return !!this.props.minCategories 
      ? this.context.translate('phrases.selectAtLeastInterests', {minLength: this.props.minCategories})
      : this.context.translate('phrases.selectUpToThreeInterests')
  }

  handleDone(e){
    this.props.onSubmit()
  }

  render(){
    const props = {
      onChange: this.handleChange,
      onChangeDelayed: this.handleCategoriesSearch,
      onSubmit: this.handleSubmit,
      delay: this.props.filterDelay,
      placeholder: this.props.placeholderText,
      inputValue: this.state.categoryInputValue,
      errorMessage: this.state.errorMessage,
      onClearErrorMessage: this.handleClearErrorMessage,
      componentTitle: this.props.componentTitle,
      maxCategories: this.props.maxCategories,
      minCategories: this.props.minCategories,
      selectedCategories: this.state.categories,
      categories: this.props.searchedCategories || this.props.categories,
      onRemoveCategory: this.handleRemoveCategory,
      notCategoriesSelectedMessage: this.notCategoriesSelectedMessage,
      searchedCategories: this.props.searchedCategories,
      searchedCategoryExists: this.searchedCategoryExists,
      textCategoryNotExist: this.props.textCategoryNotExist,
      isSelectedCategory: this.isSelectedCategory,
      categoryAlreadyAdded: this.categoryAlreadyAdded,
      messageAlreadyAdded: this.props.messageAlreadyAdded,
    }

    switch(this.props.theme){
      case 'first':
        return <FirstCategorySelector {...props}/>
      default:
        return <CategorySelector {...props}/>
    }
  }

  static contextTypes = {
    translate: React.PropTypes.func.isRequired,
    apiConfig: React.PropTypes.object.isRequired,
  };

  static propTypes = {
    categories: PropTypes.array.isRequired,
    maxCategories: PropTypes.number.isRequired,
    minCategories: PropTypes.number.isRequired,
    minCategoryLength: PropTypes.number.isRequired,
    maxCategoryLength: PropTypes.number.isRequired,
    placeholderText: PropTypes.string.isRequired,
    messageLimitReached: PropTypes.string.isRequired,
    messageAlreadyAdded: PropTypes.string.isRequired,
    //optional
    returnSelectedCategories: PropTypes.func,
    returnClickedCategory: PropTypes.func,
    textCategoryNotExist: PropTypes.string,
  };

  static defaultProps = {
    categories: ['category1', 'category2', 'category3'],
    maxCategories: 25,
    minCategories: 3,
    minCategoryLength: 2,
    maxCategoryLength: 50,
    filterDelay: 500,
    placeholderText: 'What do you like?',
    textCategoryNotExist: 'does not exist - Select or press "Enter" to add it',
    messageLimitReached: 'Max tag amount reached, tag length is incorrect or same tag was specified',
    messageAlreadyAdded: 'This category is already added to your interest list',
    messageToLong: 'This category name is to long',
    messageMaxReached: 'Max amount of categories reached',
  };
}
