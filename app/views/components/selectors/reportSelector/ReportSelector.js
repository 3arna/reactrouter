import { NavLink } from 'fluxible-router';
import React from 'react';
import lodash from 'lodash';

import BackButton from '../../elements/buttons/BackButton';

import SearchResultLink from '../../elements/links/SearchResultLink';
import FollowButtonContainer from '../../elements/buttons/followButton/FollowButtonContainer';
import ListButton from '../../elements/buttons/ListButton';

const ReportSelector = (props, { translate }) =>
  <div className="pos-absolute z-5 top-0 left-0 col-8 md-col-12 mrg-auto h-100 bg-greyl2 pdg-t-100px w-100 md-pdg-l-60px fix-bb">

    <div className="fix-bb pos-absolute z-2 top-0 left-0 md-pdg-l-60px sm-pdg-t-50px w-100">
      <div className="valign-block pdg-tb-10px pdg-rl-3 bg-white boxshadow-b fadeIn fnt-sm">
        <div className="w-min-50px md-w-auto fnt-sm">
          <span onClick={props.onClose} className="clr-black crs-point">{translate('words.cancel')}</span>
          {false && <BackButton onClick={props.onTooglePrivacySelector} />}
        </div>
        <div className="w-100">
          <div className="w-max-700px pdg-rl-3 valign-block mrg-auto">
            <h3 className="pdg-0 pdg-5px fnt-300 txt-center w-100">{translate('words.report')}</h3>
          </div>
        </div>
        <div className="w-min-50px md-w-auto txt-right fnt-sm">
          <span onClick={props.onSubmit} className="clr-main crs-point">{translate('words.done')}</span>
        </div>
      </div>
    </div>

    <div className="txt-left mrg-auto w-max-700px w-100 pdg-rl-3 sm-pdg-0 fnt-sm">

      {props.reasons.map((reason) =>
        <ListButton
          key={reason.value}
          isActive={reason.active}
          onClick={(e) => props.onSelectReason(reason.value)}>
            {reason.text}
        </ListButton>
      )}

    </div>
  </div>

ReportSelector.contextTypes = {
  translate: React.PropTypes.func.isRequired
}

ReportSelector.propTypes = {
};

export default ReportSelector;
