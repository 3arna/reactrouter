import React from 'react';

import BaseComponent from '../../../BaseComponent';
import ReportSelector from './ReportSelector';

import { report } from '../../../../actions/reportActions';

export default class ReportSelectorContainer extends BaseComponent {

  constructor(){
    super();
    this._bind('handleClose', 'handleSubmit', 'handleSelectReason');
    this.state = {
      reason: null,
    };
  }

  handleSelectReason(reason){
    this.setState({ reason: reason.toString() })
  }

  handleClose(){
    this.props.onSetContextMenuAction(null);
  }

  handleSubmit(){
    context.executeAction(report, {
      objectId: this.props.contextMenuActionTarget.id,
      reason: this.state.reason,
    });

    this.props.onSetContextMenuAction(null);
  }

  get modifiedReasons(){
    return this.props.reasons.map((reason) => {
      reason.active = reason.value === this.state.reason;
      reason.text = this.context.translate(reason.text);
      return reason;
    });
  }

  render(){
    const {props, state} = this;

    return <ReportSelector
      onClose={this.handleClose}
      onSubmit={this.handleSubmit}
      onSelectReason={this.handleSelectReason}
      reasons={this.modifiedReasons}
      displayName={props.contextMenuActionTarget.displayName}/>
  }

  static contextTypes = {
    translate: React.PropTypes.func.isRequired
  }

  static propTypes = {
    contextMenuActionObject: React.PropTypes.object,
    onSetContextMenuAction: React.PropTypes.func,
  };

  static defaultProps = {
    reasons: [
      {value: 'inappropriate', text: 'phrases.inappropriateContent'},
      {value: 'abuse', text: 'phrases.abusiveBehaviour'},
      {value: 'spam', text: 'words.spam'},
    ]
  };
}
