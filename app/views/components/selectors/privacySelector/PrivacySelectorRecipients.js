import lodash from 'lodash';
import React from 'react';

import SearchResultLink from '../../elements/links/SearchResultLink';
import FollowButtonContainer from '../../elements/buttons/followButton/FollowButtonContainer';


const PrivacySelectorRecipients = (props) => {
  if(!props.recipients){
    return <div className="txt-center pdg-t-20px">
      <i className="loading-spinner dsp-inline"/>
    </div>
  }
  return <ul className="pdg-0 list-none fix-bb mrg-0 fadeIn">
    {props.recipients && props.recipients.map((recipient) =>
      <SearchResultLink
        className="bg-white pdg-rl-15px mrg-tb-5px"
        key={ recipient.id }
        name={recipient.displayName}
        id={recipient.id}
        size={30}
        avatarUrl={recipient.avatarUrl}>

        {props.isReadOnly
          ? <i className="icon-chevron-forward pdg-5px fix-bb mrg-l-5px"/>
          : <FollowButtonContainer
              theme="round"
              isBookmarked={ !!lodash.find(props.selectedRecipients, (selectedRecipient) => recipient.id === selectedRecipient.id ) }
              id={ recipient.id }
              onToggleBookmark={ props.onToggleUser }
            />
        }
      </SearchResultLink>)
    }
  </ul>
}

export default PrivacySelectorRecipients;
