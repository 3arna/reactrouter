import React from 'react';

import scrollable from '../../../decorators/scrollable';

import SearchResultLink from '../../elements/links/SearchResultLink';
import FollowButtonContainer from '../../elements/buttons/followButton/FollowButtonContainer';
import ListButton from '../../elements/buttons/ListButton';
import PrivacySelectorRecipients from './PrivacySelectorRecipients';
import PrivacySelectorFollowingList from './PrivacySelectorFollowingList';

@scrollable
class PrivacySelectorScroller extends React.Component{
  render(){
    const props = this.props;
    const translate = this.context.translate;

    return <div className="over-auto h-100 fix-bb">
      <div className="txt-left mrg-auto w-max-700px w-100 pdg-t-100px pdg-rl-3 fnt-sm">

        {!props.privateOnly &&
          <ListButton
            onClick={props.onTogglePrivacyType}
            iconClassName="icon-globe"
            isActive={!props.isPrivate}>{translate('phrases.publicPost')}
          </ListButton>
        }

        {!props.privateOnly &&
          <ListButton
            onClick={props.onTogglePrivacyType}
            iconClassName="icon-padlock-closed"
            isExpanded={props.isPrivate}>{translate('phrases.privatePost')}
          </ListButton>
        }

        {props.isPrivate && !props.isReadOnly && !props.showFollowing &&
          <button className="w-100 bg-white brd-none h-50px fnt-md fnt-300 crs-point" onClick={props.onInviteMoreRecipients}>
            <i className="icon-plus pdg-5px fix-bb mrg-l-5px fnt-lg valign-mid"/>
            <span className="valign-mid">
              {translate('phrases.inviteMorePeople')}
            </span>
          </button>
        }

        {props.isPrivate && !props.showFollowing &&
          <PrivacySelectorRecipients
            isReadOnly={ props.isReadOnly }
            onToggleUser={ props.onToggleUser }
            recipients={ props.recipients }
            selectedRecipients={ props.selectedRecipients }
          />
        }

        {props.isPrivate && props.showFollowing &&
          <PrivacySelectorFollowingList
            followingStream={ props.followingStream }
            onToggleUser={ props.onToggleUser }
            selectedRecipients={ props.selectedRecipients }
          />
        }
      </div>
    </div>
  }

  static contextTypes = {
    translate: React.PropTypes.func.isRequired
  }
}

export default PrivacySelectorScroller;
