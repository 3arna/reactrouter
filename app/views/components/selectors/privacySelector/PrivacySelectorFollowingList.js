import lodash from 'lodash';
import React from 'react';

import SearchResultLink from '../../elements/links/SearchResultLink';
import FollowButtonContainer from '../../elements/buttons/followButton/FollowButtonContainer';


const PrivacySelectorRecipients = (props) => {
  if(!props.followingStream){
    return <div className="txt-center pdg-t-20px">
      <i className="loading-spinner dsp-inline"/>
    </div>
  }

  return <ul className="pdg-0 list-none fix-bb mrg-0 fadeIn" style={{ paddingBottom: '20vh'}}>
    {props.followingStream && props.followingStream.map((item) =>
      <SearchResultLink
        className="bg-white pdg-rl-15px mrg-tb-5px"
        key={ item.id }
        name={item.name}
        id={item.id}
        size={30}
        avatarUrl={item.avatarUrl}>
        <FollowButtonContainer
          theme="round"
          isBookmarked={ !!lodash.find(props.selectedRecipients, (recipient) => recipient.id === item.id ) }
          id={ item.id }
          onToggleBookmark={ props.onToggleUser }
        />
      </SearchResultLink>)
    }
  </ul>
}

export default PrivacySelectorRecipients;
