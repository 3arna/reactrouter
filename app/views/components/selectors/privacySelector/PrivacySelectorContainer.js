import React, { PropTypes } from 'react';
import Portal from 'react-portal';
import { connectToStores } from 'fluxible-addons-react';
import lodash from 'lodash';

import * as privacySelectorActions from '../../../../actions/component/privacySelectorActions';
import { updateACL } from '../../../../actions/privateContentActions';
import * as validationUtils from '../../../../utils/validationUtils';
import { toastProcessError } from  '../../../../actions/applicationActions';

import BaseComponent from '../../../BaseComponent';
import PrivacySelector from './PrivacySelector';

@connectToStores(['PrivacySelectorStore', 'ApplicationStore'], (context, props) => {
  const applicationStore = context.getStore('ApplicationStore');
  const privacySelectorStore = context.getStore('PrivacySelectorStore');

  const followingStream = privacySelectorStore.followingStreamPage &&
    privacySelectorStore.followingStreamPage.entries.map((entry) => entry.summary);

  return {
    contentId: privacySelectorStore.contentId,
    currentUrl: applicationStore.getCurrentUrl(),
    followingStream,
    followingStreamNextState: privacySelectorStore.followingStreamPage &&
      privacySelectorStore.followingStreamPage.nextState,
    isOpen: privacySelectorStore.isOpen,
    isPrivate: privacySelectorStore.isPrivate,
    isReadOnly: privacySelectorStore.isReadOnly,
    openedAtUrl: privacySelectorStore.openedAtUrl,
    recipients: privacySelectorStore.recipients,
    selectedRecipients: privacySelectorStore.selectedRecipients,
    showFollowing: privacySelectorStore.showFollowing
  };
})
export default class PrivacySelectorContainer extends BaseComponent {
  static contextTypes = {
    apiConfig: React.PropTypes.object,
    translate: React.PropTypes.func,
  };

  constructor(){
    super();
    this._bind('handleTogglePrivacy', 'handleToggleUser', 'handleSubmit', 'handleScrolledToBottom');
  }

  handleInviteMoreRecipients(){
    context.executeAction(privacySelectorActions.inviteMoreRecipients);
  }

  handleScrolledToBottom(){
    const props = this.props;

    if(props.showFollowing && props.followingStreamNextState){
      return context.executeAction(privacySelectorActions.loadFollowing, { nextState: props.followingStreamNextState });
    }
  }

  handleTogglePrivacy(e){
    e.preventDefault();
    e.stopPropagation();

    if(this.isReadOnly){
      return;
    }

    context.executeAction(privacySelectorActions.togglePrivacy);
  }

  handleToggleUser(userData){
    const props = this.props

    let newRecipients;

    if(!userData.isBookmarked){
      if(props.selectedRecipients.length >= this.context.apiConfig.maximumPrivateRecipients){
        return context.executeAction(toastProcessError, this.context.translate('phrases.sorryYouveHitTheMaximumNumberYouCanSelect'));
      }

      const newRecipient = lodash.find(props.followingStream, (item) => item.id === userData.objectId);
      newRecipients = lodash.union(props.selectedRecipients, [newRecipient]);
    }
    else{
      newRecipients = lodash.remove(props.selectedRecipients, (recipient) => recipient.id !== userData.objectId);
    }

    context.executeAction(privacySelectorActions.setRecipients, { recipients: newRecipients,  });
  }

  handleSubmit(){
    const props = this.props;

    if(props.onUpdatePrivacy){
      props.onUpdatePrivacy({
        isPrivate: props.isPrivate,
        recipients: props.selectedRecipients
      });
    }
    else{
      if(!props.isReadOnly){
        context.executeAction(updateACL, {
          contentId: props.contentId,
          currentAllowedUserIds: props.recipients ? props.recipients.map((recipient) => recipient.id): [],
          allowedUserIds: props.selectedRecipients.map((recipient) => recipient.id)
        });
      }
    }

    context.executeAction(privacySelectorActions.toggleOpen);
  }

  handleCancel(){
    context.executeAction(privacySelectorActions.toggleOpen, { reset: true });
  }

  render(){
    const {props, state} = this;

    return <Portal isOpened={props.isOpen && props.openedAtUrl === props.currentUrl}>
      <PrivacySelector
        followingStream={ props.followingStream}
        isPrivate={ props.isPrivate }
        onTogglePrivacyType={ this.handleTogglePrivacy }
        onToggleUser={ this.handleToggleUser }
        onSubmit={ this.handleSubmit }
        onCancel={ this.handleCancel }
        onScrolledToBottom={ this.handleScrolledToBottom }
        onInviteMoreRecipients={ this.handleInviteMoreRecipients }
        privateOnly={ !!props.contentId }
        isReadOnly={ props.isReadOnly }
        isFullScreen={ props.isFullScreen }
        recipients={ props.recipients }
        selectedRecipients={ props.selectedRecipients }
        showFollowing={ props.showFollowing }
      />
      </Portal>
  }

  static propTypes = {
    following: React.PropTypes.array,
    onTogglePrivacySelector: React.PropTypes.func,
    onUpdatePrivacy: React.PropTypes.func,
    privateOnly: React.PropTypes.bool,
    readOnly: React.PropTypes.bool
  };
}
