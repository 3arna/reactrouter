import { NavLink } from 'fluxible-router';
import React from 'react';
import lodash from 'lodash';

import PrivacySelectorScroller from './PrivacySelectorScroller';

class PrivacySelector extends React.Component {

  render(){
    const props = this.props;
    const translate = this.context.translate;

    return <div className={`pos-absolute z-5 top-0 left-0 mrg-auto md-col-12 h-100 bg-greyl2 md-pdg-l-60px w-100 fix-bb ${!props.isFullScreen && 'col-8'}`}>
      <div className="fix-bb pos-absolute z-2 top-0 left-0 w-100 md-pdg-l-60px sm-pdg-t-50px">
        <div className="valign-block pdg-tb-10px pdg-rl-3 bg-white boxshadow-b fadeIn fnt-sm">
          <div className="w-min-50px md-w-auto fnt-sm">
            <span onClick={props.onCancel} className="clr-black crs-point">{translate('words.cancel')}</span>
          </div>
          <div className="w-100">
            <div className="w-max-700px pdg-rl-3 valign-block mrg-auto">
              <h3 className="pdg-0 pdg-5px fnt-300 txt-center w-100">{props.isReadOnly ? translate('words.participants'): translate('phrases.manageParticipants')}</h3>
            </div>
          </div>
          <div className="w-min-50px md-w-auto txt-right fnt-sm">
            <span onClick={props.onSubmit} className="clr-main crs-point">{this.context.translate('words.done')}</span>
          </div>
        </div>
      </div>

      <PrivacySelectorScroller {...props}/>

    </div>
  }

  static contextTypes = {
    translate: React.PropTypes.func.isRequired
  }
}

export default PrivacySelector;
