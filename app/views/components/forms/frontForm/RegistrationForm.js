import React, { PropTypes } from 'react';
import classNames from 'classnames';

import FrontButton from '../../elements/buttons/FrontButton';
import FrontInput from '../../elements/inputs/FrontInput';

const RegistrationForm = (props, { translate }) =>
  <form noValidate onSubmit={props.onSubmit}>
    <div className="mrg-b-5px fadeIn">
      <div className="fnt-sm">
        <FrontInput
          onClick={props.onErrorClear}
          className="mrg-b-10px"
          type="name"
          name="name"
          placeholder={translate('words.name')}
          errorMessage={props.inputErrors && props.inputErrors.name}/>
        <FrontInput
          onClick={props.onErrorClear}
          className="mrg-b-10px"
          type="email"
          name="email"
          placeholder={translate('phrases.emailAddress')}
          errorMessage={props.inputErrors && props.inputErrors.email}/>
        <FrontInput
          onClick={props.onErrorClear}
          type="password"
          name="password"
          className="mrg-b-10px"
          onChange={props.onPasswordStrengthCheck}
          placeholder={translate('words.password')}
          errorMessage={props.inputErrors && props.inputErrors.password}
          showAsPassword={props.showAsPassword}
          passwordStrength={props.passwordStrength}
          trackPasswordStrength={true}
          togglePasswordType={props.onTogglePasswordType}/>
      </div>
    </div>
    <FrontButton text={translate('words.register')} type="round"/>
  </form>

RegistrationForm.contextTypes = {
  translate: PropTypes.func,
};

RegistrationForm.propTypes = {
  inputErrors: PropTypes.object.isRequired,
  showAsPassword: PropTypes.bool,
  passwordStrength: PropTypes.any,
  onSubmit: PropTypes.func,
  onErrorClear: PropTypes.func,
  onPasswordStrengthCheck: PropTypes.func,
  onTogglePasswordType: PropTypes.func,

};


export default RegistrationForm;
