import React, { PropTypes } from 'react';
import classNames from 'classnames';
import lodash from 'lodash';

import * as validationUtils from '../../../../utils/validationUtils';
import BaseComponent from '../../../BaseComponent';

import RegistrationForm from './RegistrationForm';
import LoginForm from './LoginForm';
import ResetPasswordForm from './ResetPasswordForm';

export default class FrontFormContainer extends BaseComponent {

  static propTypes = {
    onSubmit: PropTypes.func
  };

  static contextTypes = {
    translate: PropTypes.func.isRequired
  }

  static defaultProps = {
    delayError: 2000,
    minLength: 4,
    maxLength: 40,
    minPasswordLength: 8,
    maxPasswordLength: 40,
  }

  constructor(){
    super();
    this._bind(
      'validateForm',
      'handleTogglePasswordType',
      'handlePasswordStrengthCheck',
      'handleSubmitCheck',
      'handleErrorClear'
    );
    this.state = {
      showAsPassword: true,
      passwordStrength: null,
      formValid: null,
      inputErrors: {},
      inputValid: {}
    };
  }

  get errorMessages(){
    const translate = this.context.translate;

    return {
      password: translate('phrases.passwordMustBeAtLeastTwoCharacters'),
      email: translate('phrases.invalidEmailAddress'),
      name: translate('phrases.nameMustBeAtLeastFourCharacters'),
    }
  }

  handleTogglePasswordType(){
    this.setState({showAsPassword: !this.state.showAsPassword});
  }

  handlePasswordStrengthCheck(e){
    const target = e.target || e;
    const strength = validationUtils.validInput(target.value, {
      type: target.type,
      minLength: this.props.minPasswordLength,
      maxLength: this.props.maxPasswordLength,
    }) || null;

    strength !== this.state.passwordStrength && this.setState({passwordStrength: strength});
  }

  handleSubmitCheck(e){
    e.preventDefault();
    const form = e.target;

    var formFields = [];
    form.name && formFields.push({name: 'name', value: form.name.value});
    form.email && formFields.push({name: 'email', value: form.email.value});
    form.password && formFields.push({name: 'password', value: form.password.value, minLength: this.props.minPasswordLength});

    this.validateForm(formFields);

    !lodash.some(formFields, {valid: false}) && this.props.onSubmit(e);
  }

  handleErrorClear(type){
    const inputErrors = this.state.inputErrors;
    if(inputErrors[type]){
      inputErrors[type] = false;
      this.setState(inputErrors);
    }
  }

  validateForm(formFields){
    const inputErrors = this.state.inputErrors;

    formFields.forEach((field) => {
      field.valid = validationUtils.validInput(field.value, {
        type: field.name,
        minLength: field.minLength || this.props.minLength,
        maxLength: field.maxLength || this.props.maxLength,
        invalidEmpty: true,
      });

      if(!field.valid){
        inputErrors[field.name] = this.errorMessages[field.name];
      }
    });

    this.setState({ inputErrors });
  }

  render(){
    const {state, props} = this;

    switch(props.theme){
      case 'registration':
        return <RegistrationForm
          inputErrors={state.inputErrors}
          passwordStrength={state.passwordStrength}
          showAsPassword={state.showAsPassword}
          onErrorClear={this.handleErrorClear}
          onPasswordStrengthCheck={this.handlePasswordStrengthCheck}
          onSubmit={this.handleSubmitCheck}
          onTogglePasswordType={this.handleTogglePasswordType}/>
      case 'login':
        return <LoginForm
          inputErrors={state.inputErrors}
          showAsPassword={state.showAsPassword}
          onErrorClear={this.handleErrorClear}
          onSubmit={this.handleSubmitCheck}
          onTogglePasswordType={this.handleTogglePasswordType}/>
      case 'forgotPassword':
        return <ResetPasswordForm
          inputErrors={state.inputErrors}
          onErrorClear={this.handleErrorClear}
          onSubmit={this.handleSubmitCheck}/>

      default:
        return <div className="pdg-20px bg-black">no form provided</div>
    }


  }
}
