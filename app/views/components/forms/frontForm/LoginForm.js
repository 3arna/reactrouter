import React, { PropTypes } from 'react';
import classNames from 'classnames';

import FrontButton from '../../elements/buttons/FrontButton';
import FrontInput from '../../elements/inputs/FrontInput';
import DefaultLink from '../../elements/links/DefaultLink';

const LoginForm = (props, { translate }) =>
  <form noValidate onSubmit={props.onSubmit}>
    <div className="txt-center mrg-b-10px pos-relative">
      <div className="fnt-sm">

        <FrontInput
          onClick={props.onErrorClear}
          className="mrg-b-10px"
          type="email"
          name="email"
          placeholder={translate('phrases.emailAddress')}
          errorMessage={ props.inputErrors && props.inputErrors.email }/>

        <FrontInput
          onClick={props.onErrorClear}
          type="password"
          name="password"
          placeholder={translate('words.password')}
          errorMessage={ props.inputErrors && props.inputErrors.password }
          showAsPassword={props.showAsPassword}
          togglePasswordType={props.onTogglePasswordType}/>

      </div>
    </div>

    <FrontButton text={translate('phrases.logIn')} type="round"/>

    <DefaultLink className="mrg-t-10px fnt-xs clr-blackl3" href="/forgot-password">{translate('phrases.forgottenPassword')}</DefaultLink>
  </form>

LoginForm.contextTypes = {
  translate: PropTypes.func,
};

LoginForm.propTypes = {
  inputErrors: PropTypes.object.isRequired,
  showAsPassword: PropTypes.bool,
  onSubmit: PropTypes.func,
  onErrorClear: PropTypes.func,
  onTogglePasswordType: PropTypes.func,
};


export default LoginForm;
