import React, { PropTypes } from 'react';
import classNames from 'classnames';

import FrontButton from '../../elements/buttons/FrontButton';
import FrontInput from '../../elements/inputs/FrontInput';
import DefaultLink from '../../elements/links/DefaultLink';

const ResetPasswordForm = (props, { translate }) =>
  <form noValidate onSubmit={props.onSubmit}>
    <div className="mrg-b-15px pos-relative">
      <div className="fnt-sm">
        <FrontInput
          onClick={props.onErrorClear}
          className="mrg-b-10px"
          type="email"
          name="email"
          placeholder={translate('phrases.emailAddress')}
          errorMessage={ props.inputErrors && props.inputErrors.email }/>
      </div>
      <FrontButton text={translate('phrases.resetPassword')} type="round"/>
    </div>
  </form>

ResetPasswordForm.contextTypes = {
  translate: PropTypes.func,
};

ResetPasswordForm.propTypes = {
  inputErrors: PropTypes.object.isRequired,
  onSubmit: PropTypes.func,
  onErrorClear: PropTypes.func,
};


export default ResetPasswordForm;
