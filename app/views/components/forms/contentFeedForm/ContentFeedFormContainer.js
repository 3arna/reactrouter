import { connectToStores } from 'fluxible-addons-react';
import React from 'react';
import lodash from 'lodash';

import uploader from '../../../decorators/uploader';
import Category from '../../../../models/Category';

import formActions from '../../../../actions/component/formActions';
import privacySelectorActions from '../../../../actions/component/privacySelectorActions';
import { apiProcessStart, apiProcessClear, apiProcessError, toastProcessError } from '../../../../actions/applicationActions';

import BaseComponent from '../../../BaseComponent';
import ContentFeedForm from './ContentFeedForm';
import MobileReplyForm from './MobileReplyForm';
import DesktopReplyForm from './DesktopReplyForm';
import PrivacySelectorContainer from '../../selectors/privacySelector/PrivacySelectorContainer';
import FormCategorySelectorContainer from '../../selectors/categorySelector/FormCategorySelectorContainer';

let ScheduleSelectorContainer;

@connectToStores(['ContentFormStore', 'ApplicationStore'], (context, props) => {
  const contentFormStore = context.getStore('ContentFormStore');
  const applicationStore = context.getStore('ApplicationStore');
  const userStore = context.getStore('UserStore');

  return {
    categories: contentFormStore.categories,
    currentUrl: applicationStore.getCurrentUrl(),
    errorMessage: contentFormStore.errorMessage,
    following: userStore.following,
    formVisible: contentFormStore.formVisible,
    formData: contentFormStore.formData,
    formMedia: contentFormStore.formMedia,
    isContent: contentFormStore.isContent,
    isPrivate: props.parentContent && props.parentContent.isPrivate && !contentFormStore.isContent || contentFormStore.isPrivate,
    openedAtUrl: contentFormStore.openedAtUrl,
    posting: contentFormStore.posting,
    processMediaUpload: applicationStore.processGet('processMediaUpload', 0),
    publishDate: contentFormStore.publishDate,
    recipients: props.parentContent && props.parentContent.acl
      && !contentFormStore.isContent && props.parentContent.acl.allowedIds || contentFormStore.recipients,
    tags: contentFormStore.tags,
    defaultCategories: userStore.defaultCategories,
  };
})

@uploader

export default class ContentFeedFormContainer extends BaseComponent {
  constructor(){
    super();

    this._bind(
      'handleSelectMedia',
      'handleTrackUploadProgress',
      'handleRemoveMedia',
      'handleFormSubmit',
      'handleToggleForm',
      'handleTogglePrivacySelector',
      'handleToggleCategorySelector',
      'handleToggleScheduleSelector'
    );

    this.state = {
      media: null,
      uploadProgress: null,
      contentType: null,
      privacySelectorOn: false,
      categorySelectorOn: false,
      scheduleSelectorOn: false,
    };
  }

  componentDidMount(){
    if(this.context.authenticatedUser && this.context.authenticatedUser.specialType){
      require.ensure(['../../selectors/scheduleSelector/ScheduleSelectorContainer'], () => {
        ScheduleSelectorContainer = require('../../selectors/scheduleSelector/ScheduleSelectorContainer');
      }, 'scheduler');
    }
  }

  /*shouldComponentUpdate(nextProps, nextState) {
    return this.props.formVisible !== nextProps.formVisible;
    console.log(nextProps, nextState);
    return false;
  }*/

  handleFormSubmit(e){
    context.executeAction(formActions.postFormContent, { parentContent: this.props.parentContent });
    this.props.onMediaUploadCancel();
    this.setState({uploadProgress: null, media: null});
    e.preventDefault();
    e.stopPropagation();
  }

  handleToggleForm(e){
    const props = this.props;
    context.executeAction(formActions.toggleForm, {
      isContent: false,
      defaultCategories: props.parentContent && props.parentContent.categoryIds
    });
  }

  handleTogglePrivacySelector(e){
    const props = this.props;

    if(props.parentContent && props.parentContent.isPrivate && !props.isContent){
      return;
    }

    context.executeAction(privacySelectorActions.toggleOpen, { isPrivate: props.isPrivate, recipients: props.recipients });
  }

  handleToggleCategorySelector(e){
    this.setState({ categorySelectorOn: !this.state.categorySelectorOn });
  }

  handleToggleScheduleSelector(e){
    this.setState({ scheduleSelectorOn: !this.state.scheduleSelectorOn });
  }

  handleUpdateFormInput(e){
    const elem = e.target;
    context.executeAction(formActions.updateFormInput, {
      field: {
        name: elem.name,
        value: elem.value,
      },
      content: true
    });
  }

  handleUpdateCategories(categories){
    context.executeAction(formActions.updateFormCategories, { categories, content: true });
  }

  handleUpdatePrivacy(privacyData){
    context.executeAction(formActions.updateFormPrivacy, privacyData);
  }

  handleUpdatePublishDate(publishDate){
    context.executeAction(formActions.updatePublishDate, {publishDate});
  }

  handleSelectMedia(event){
    const { props } = this;

    props.onMediaPreview({
      event,
      name: 'media',
      typesAllowed: props.mediaFormatsAllowed
    }, (error, mediaPreviewData) => {

      if(error){
        return context.executeAction(formActions.setErrorMessage, { errorMessage: this.context.translate('phrases.somethingWentWrong')});
      }

      if(!mediaPreviewData){
        return;
      }

      mediaPreviewData.previewUrl && this.setState({media: mediaPreviewData.media});
      context.executeAction(apiProcessStart, props.processMediaUpload);
      mediaPreviewData.onTrackUploadProgress = this.handleTrackUploadProgress;

      props.onMediaUpload(mediaPreviewData, (error, uploadedMediaData) => {
        context.executeAction(apiProcessClear, props.processMediaUpload);

        if(error){
          return context.executeAction(formActions.setErrorMessage, { errorMessage: error });
        }

        context.executeAction(formActions.updateFormMedia, {
          media: uploadedMediaData.media,
          content: true
        });

      });
    });
  }

  handleRemoveMedia(){
    this.state.media = null;
    context.executeAction(formActions.updateFormMedia, { media: null, content: true });
    context.executeAction(apiProcessClear, this.props.processMediaUpload);
    this.props.onMediaUploadCancel();
  }

  handleTrackUploadProgress(e){
    let percent = parseInt(e.percent);
    this.state.uploadProgress !== percent && this.setState({ uploadProgress: percent });
  }

  get categories(){
    const {props} = this;
    return !!props.categories.length && props.categories || [];
  }


  render(){
    const { props, state } = this;

    if(props.isMobile && !props.formVisible){
      return <MobileReplyForm
        errorMessage={ props.errorMessage }
        formData={ props.formData }
        isPrivate={ props.isPrivate }
        onSubmit={ this.handleFormSubmit }
        onToggleForm={ this.handleToggleForm }
        onUpdateFormInput={ this.handleUpdateFormInput }
        posting={ props.posting }/>;
    }

    if(props.isDesktop){
      return !props.formVisible &&
        <DesktopReplyForm
          errorMessage={ props.errorMessage }
          formData={ props.formData }
          isPrivate={ props.isPrivate }
          onSubmit={ this.handleFormSubmit }
          onToggleForm={ this.handleToggleForm }
          onUpdateFormInput={ this.handleUpdateFormInput }
          posting={ props.posting }/>
    }

    if(props.formVisible && props.openedAtUrl === props.currentUrl){

      return <div>
        <ContentFeedForm
          categories={ this.categories }
          className={ props.className }
          errorMessage={ props.errorMessage || props.processMediaUpload && props.processMediaUpload.error }
          formData={ props.formData }
          isContent={ props.isContent }
          isPrivate={ props.isPrivate }
          isFirstLoad={ state.isFirstLoad }
          isFullScreen={ props.isFullScreen }
          media={ !props.processMediaUpload.inProgress && props.formMedia || state.media }
          mediaFormatsAllowed={ props.mediaFormatsAllowed }
          onSelectMedia={ this.handleSelectMedia }
          onSubmit={ this.handleFormSubmit }
          onToggleForm={ this.handleToggleForm }
          onUpdateFormInput={ this.handleUpdateFormInput }
          onRemoveMedia={ this.handleRemoveMedia }
          onTogglePrivacySelector={ this.handleTogglePrivacySelector }
          onToggleCategorySelector={ this.handleToggleCategorySelector }
          onToggleScheduleSelector={  this.handleToggleScheduleSelector }
          posting={ props.posting }
          processMediaUpload={ props.processMediaUpload }
          privacySelectorOn={ state.privacySelectorOn }
          publishDate={ props.publishDate }
          recipients={ props.recipients }
          uploadProgress={ state.uploadProgress }/>

        <PrivacySelectorContainer isFullScreen={ props.isFullScreen } onUpdatePrivacy={ this.handleUpdatePrivacy } />

        {ScheduleSelectorContainer && state.scheduleSelectorOn &&
          <ScheduleSelectorContainer
            onCancel={this.handleToggleScheduleSelector}
            onSubmit={this.handleUpdatePublishDate}
            publishDate={ props.publishDate }
            isFullScreen={ props.isFullScreen }/>
        }


        {state.categorySelectorOn &&
          <FormCategorySelectorContainer
            interests={this.categories}
            onCancel={this.handleToggleCategorySelector}
            onSubmit={this.handleUpdateCategories}
            isFullScreen={ props.isFullScreen }/>
        }
      </div>

    }

    return null;
  }

  static contextTypes = {
    apiConfig: React.PropTypes.object,
    authenticatedUser: React.PropTypes.object,
    translate: React.PropTypes.func,
    executeAction: React.PropTypes.func,
  };

  static propTypes = {
    categories: React.PropTypes.array,
    errorMessage: React.PropTypes.string,
    formData: React.PropTypes.object,
    formVisible: React.PropTypes.bool,
    isFullScreen: React.PropTypes.bool,
    parentContent: React.PropTypes.object,
    posting: React.PropTypes.bool,
    postTo: React.PropTypes.string,
  };

  static defaultProps = {
    maxMediaSize: 50000,
    mediaFormatsAllowed: [
      'image/jpeg',
      'image/png',
      'image/gif',
      'video/mp4',
      //'video/mov',
      'video/quicktime',
      'audio/mp3',
      'audio/mpeg'
    ]
  };
}
