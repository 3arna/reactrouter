import classNames from 'classnames';
import React from 'react';

import Category from '../../../../models/Category';

import ErrorAlert from '../../elements/alerts/ErrorAlert';
import TransparentButton from '../../elements/buttons/TransparentButton';
import PostingFog from '../../elements/fogs/PostingFog';

const _getFormClassNames = () => classNames({
  'md-dsp-none pos-absolute h-min-90px bot-0 w-100 right-0 brd-t-greyl2 z-4 bg-white w-max-inherit': true,
});

const _getInputClassNames = () => classNames({
  'bg-none brd-0 w-100 dsp-block pdg-l-30px brd-l-white focus-brd-main pdg-t-10px pdg-r-10px fnt-main': true,
  'outline-none fnt-300 fix-bb fnt-sm': true,
});

const DesktopReplyForm = (props, { translate }) => {
  const {
    errorMessage,
    formData,
    onSubmit,
    onToggleForm,
    onUpdateFormInput,
    posting
  } = props;

  return(
    <div className={_getFormClassNames()}>
        <PostingFog posting={ posting } />
        {errorMessage &&
          <div className="bg-op-05 clr-white op-05 pdg-10px fnt-xs txt-cap fadeIn">
            {errorMessage}
            <i className="icon-close flt-right fnt-md"/>
          </div>}

        {!posting && <div>
          <textarea
            name="message"
            className={_getInputClassNames()}
            placeholder={translate('words.reply')}
            onChange={onUpdateFormInput}
            defaultValue={formData.message || ''}/>
          <div className="txt-right mrg-b-10px mrg-t-5px mrg-r-10px">
            <i onClick={ onToggleForm } className="icon-attachment crs-point dsp-inline"/>
            <TransparentButton onClick={onSubmit} className="fix-bb dsp-inline" text={translate('words.post')}/>
          </div>
        </div>}

    </div>
  )
};

DesktopReplyForm.contextTypes = {
  translate: React.PropTypes.func.isRequired
}

DesktopReplyForm.propTypes = {
  formData: React.PropTypes.object,
};

export default DesktopReplyForm;
