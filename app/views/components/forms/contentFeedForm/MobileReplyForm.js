import classNames from 'classnames';
import React from 'react';

import Category from '../../../../models/Category';

import ErrorAlert from '../../elements/alerts/ErrorAlert';
import TransparentButton from '../../elements/buttons/TransparentButton';
import PostingFog from '../../elements/fogs/PostingFog';

const _getFormClassNames = () => classNames({
  'md-dsp-block dsp-none h-min-50px pos-fixed left-0 bot-0 z-4 w-100 fix-bb bg-white sm-pdg-0 md-pdg-l-60px': true,
});

const _getInputClassNames = () => classNames({
  'bg-none brd-0 w-100 h-50px pdg-tb-15em pdg-l-50em pdg-r-80px dsp-block': true,
  'outline-none fnt-300 brd-t-greyl1 fix-bb fnt-sm fnt-main': true,
});

/*const _scaleHeight = (e, onChange) => {
  e.target.style.height = 'auto';
  e.target.style.height = (e.target.scrollHeight) + 'px';
  !!onChange && onChange(e);
}*/

const MobileContentForm = (props, { translate }) => {
  const {
    errorMessage,
    formData,
    onSubmit,
    onToggleForm,
    onUpdateFormInput,
    posting
  } = props;

  return(
    <div className={_getFormClassNames()}>
      <PostingFog posting={ posting } />

      {errorMessage &&
          <div className="bg-op-05 clr-white pdg-10px op-05 fnt-xs txt-cap fadeIn">
            {errorMessage}
            <i className="icon-close flt-right fnt-md"/>
          </div>}

      {!posting &&
        <div>
          <i onClick={ onToggleForm } className="icon-attachment pos-absolute crs-point pdg-15px"/>

          <textarea
            name="message"
            className={_getInputClassNames()}
            placeholder={translate('words.reply')}
            onChange={onUpdateFormInput}
            defaultValue={formData.message || ''}/>

          <TransparentButton onClick={onSubmit} className="pos-absolute right-0 bot-0 mrg-10px" text={translate('words.post')}/>
        </div>
      }
    </div>
  )
};

MobileContentForm.contextTypes = {
  translate: React.PropTypes.func.isRequired
}

MobileContentForm.propTypes = {
  formData: React.PropTypes.object,
};

export default MobileContentForm;
