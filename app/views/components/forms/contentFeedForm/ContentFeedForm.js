import classNames from 'classnames';
import React from 'react';

import Category from '../../../../models/Category';

import ErrorAlert from '../../elements/alerts/ErrorAlert';

import BackButton from '../../elements/buttons/BackButton';
import TransparentButton from '../../elements/buttons/TransparentButton';
import IconButton from '../../elements/buttons/IconButton';
import SmallSpinner from '../../elements/spinners/SmallSpinner';
import PostingFog from '../../elements/fogs/PostingFog';

import ProgressBarFog from '../../elements/fogs/ProgressBarFog';
import CloseButton from '../../elements/buttons/CloseButton';

import Media from '../../media/Media';

const getContainerClassNames = (props) => classNames({
  'pos-absolute z-4 top-0 left-0 fadeIn w-100 mrg-auto md-pdg-l-60px bg-greyl2 h-min-100': true,
  'lg-pdg-r-33': !props.isFullScreen,
});

const getButtonClassNames = () => classNames({
  'bg-blackl2 outline-none cursor-pointer brd-rad-15px brd-0 clr-white pdg-tb-10px pdg-rl-50px': true,
  'hover-bg-black trans': true,
});

const _preventDefault = (e) => {
  e.preventDefault();
  e.stopPropagation();
}

const ContentFeedForm = (props, { translate, authenticatedUser }) => {
  const {
    categories,
    errorMessage,
    formData,
    formVisible,
    isFirstLoad,
    onSubmit,
    onToggleForm,
    onUpdateFormInput,
    media,
    posting,
    mediaFormatsAllowed,
    className,
    onSelectMedia,
    parentContent,
    processMediaUpload,
    uploadProgress,
    onRemoveMedia,
    recipients,
    isPrivate,
    onTogglePrivacySelector,
    onToggleCategorySelector,
    onToggleScheduleSelector,
    publishDate,
  } = props;

  return(
    <div className={className + ' ' +  getContainerClassNames(props)}>
      <div className="pos-relative h-min-100vh">

        <PostingFog posting={ posting } />

        {errorMessage &&
          <div className="bg-op-05 clr-white op-05 pdg-10px top-0 sm-top-50px w-100 mrg-t-50px pos-fixed fnt-xs txt-cap fadeIn">
            {errorMessage || 'error'}
            <i className="icon-close flt-right fnt-md"/>
          </div>}

        <div className={`md-pdg-l-60px brd-b-greyl1 pos-fixed sm-top-50px left-0 top-0 z-4 w-100 ${!props.isFullScreen && 'lg-pdg-r-33'}`}>
          <div className="tb pdg-tb-10em bg-white w-100">
            <div className="w-min-50px md-w-auto td pdg-l-10em">
              <BackButton onClick={ onToggleForm } />
            </div>

            <div className="w-100 td">
              <div className="tb w-max-700px w-100 pdg-rl-3 txt-left valign-block mrg-auto">
                <div className="td txt-nowrap">

                  <div className="tb w-100">

                    <div className="td valign-bot">
                      <label
                        className="dsp-inline"
                        onDrop={onSelectMedia}
                        onDragOver={_preventDefault}
                        onDragLeave={_preventDefault}>
                          <IconButton
                            iconClassName="icon-attachment"
                            text={!processMediaUpload.inProgress && !!media ? translate('phrases.mediaAdded') : translate('phrases.addMedia')}
                            isActive={!processMediaUpload.inProgress && !!media}
                            smNameHide/>
                          {!processMediaUpload.inProgress &&
                            <input
                              onChange={onSelectMedia}
                              className="pos-absolute dsp-none"
                              type="file"
                              allow={mediaFormatsAllowed.join(',')}/>}
                      </label>

                      <IconButton
                        iconClassName="icon-star"
                        text={!!categories && !categories.length 
                          ? translate('phrases.addInterests') 
                          : `${categories.length} ${translate('words.interests')}`}
                        onClick={onToggleCategorySelector}
                        smNameHide
                        isActive={!!categories && !!categories.length}
                        isDisabled={isPrivate}/>

                      {!!authenticatedUser.specialType && 
                        <IconButton
                          iconClassName="icon-clock"
                          text={translate('words.schedule')}
                          onClick={onToggleScheduleSelector}
                          isActive={!!publishDate}
                          smNameHide/>}

                    </div>

                    <div className="td txt-right">
                      <IconButton
                        onClick={onTogglePrivacySelector}
                        iconClassName={isPrivate ? 'icon-padlock-closed' : 'icon-globe fnt-md'}
                        isActive={isPrivate}
                        text={!isPrivate ? translate('words.public') : `${recipients.length} ${translate('words.invited')}`}
                        isDisabled={!!publishDate}/>
                      <TransparentButton onClick={onSubmit} text={translate('words.post')} isDisabled={processMediaUpload.inProgress}/>
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div className="td md-dsp-none w-min-50px"/>
          </div>
        </div>

        <div className="h-100 pdg-t-50px sm-pdg-t-100px over-auto">

          <div className="w-max-700px w-100 pdg-rl-3 mrg-auto mrg-tb-30em txt-center">
            <input
              name="name"
              onChange={onUpdateFormInput}
              className="outln-none brd-0 mrg-tb-10em fnt-300 fnt-lg dsp-block clr-black w-100 bg-none focus-brd-grey trans-brd brd-b-greyl2"
              placeholder={translate('phrases.headingOptional')}
              defaultValue={formData.name}/>

            {processMediaUpload.inProgress && !media &&
              <div className="col-10 mrg-auto h-200px pos-relative z-1">
                <CloseButton onClick={onRemoveMedia} className="pos-absolute z-4" style={{right: '-10px', top: '-10px'}}/>
                <ProgressBarFog uploadProgress={uploadProgress}/>
              </div>}

            {media &&
              <Media media={media} uploading={processMediaUpload.inProgress}>
                {processMediaUpload.inProgress &&
                  <ProgressBarFog uploadProgress={uploadProgress}/>}
                  <CloseButton onClick={onRemoveMedia} className="pos-absolute z-3" style={{right: '-10px', top: '-10px'}}/>
              </Media>}

            <textarea
              name="message"
              onChange={onUpdateFormInput}
              className="w-100 outln-none bg-none fnt-main fnt-300 brd-0 clr-black fnt-sm mrg-tb-10em h-min-200px focus-brd-grey trans-brd brd-b-greyl2"
              placeholder={translate('phrases.writeSomething')}
              defaultValue={formData.message}/>
          </div>

        </div>
      </div>
    </div>
  )
};

ContentFeedForm.contextTypes = {
  translate: React.PropTypes.func.isRequired,
  authenticatedUser: React.PropTypes.object,
};

ContentFeedForm.propTypes = {
  formData: React.PropTypes.object,
  onSubmit: React.PropTypes.func.isRequired,
  onToggleForm: React.PropTypes.func.isRequired,
  onUpdateFormInput: React.PropTypes.func.isRequired,
  posting: React.PropTypes.bool.isRequired,
};

export default ContentFeedForm;
