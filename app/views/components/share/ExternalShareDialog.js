import classNames from 'classnames';
import React from 'react';
import { NavLink } from 'fluxible-router';
import lodash from 'lodash';
import { generateSharingUrl } from '../../../utils/contentUtils';
import { catchAuthenticated } from '../../../actions/reportingActions';

const _getShareLinkClassNames = (link) => classNames({
  'crs-point h-100px w-100px line-100px mrg-2px fix-bb bg-white hover-clr-white': true,
  [`clr-${link.color} hover-bg-${link.color}`]: true,
  [link.iconClassName]: true,
});

const _triggerShare = (contentId, platform, executeAction) => {
  executeAction(catchAuthenticated, {contentId, platform});
}

const ExternalShareDialog = (props, {config, apiConfig, translate, authenticatedUser, executeAction}) => {
  return <div className="dsp-inline txt-center w-max-250px fnt-xl">
    {props.shareVia.map((link) => 
      <a 
        key={link.name} 
        target="_blank" 
        onClick={(e) => _triggerShare(props.target.id, link.name, executeAction)}
        href={generateSharingUrl(config, apiConfig, {
          url: props.target.url,
          platform: link.name,
          contentId: props.target.id,
          userId: authenticatedUser.id,
          subject: translate('phrases.checkThisOutOnVersy'),
          body: '',
        })} 
        className={_getShareLinkClassNames(link)}/>)}
  </div>
}

ExternalShareDialog.propTypes = {
  target: React.PropTypes.object,
  url: React.PropTypes.string,
  title: React.PropTypes.string,
  text: React.PropTypes.string,
};

ExternalShareDialog.contextTypes = {
  config: React.PropTypes.object,
  apiConfig: React.PropTypes.object,
  translate: React.PropTypes.func,
  authenticatedUser: React.PropTypes.object,
  executeAction: React.PropTypes.func,
};

ExternalShareDialog.defaultProps = {
  shareVia: [
    {name: 'facebook', iconClassName: 'icon-facebook', color: 'facebook'},
    {name: 'google', iconClassName: 'icon-google', color: 'google'},
    {name: 'twitter', iconClassName: 'icon-twitter', color: 'twitter'},
    {name: 'email', iconClassName: 'icon-email', color: 'main'},
  ],
  url: 'http://versy.com',
  title: 'Check this out on Versy'
};

export default ExternalShareDialog;






