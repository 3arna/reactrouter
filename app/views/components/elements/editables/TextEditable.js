import React from 'react';
import classNames from 'classnames';
import lodash from 'lodash';

const _getInputClassNames = () => classNames({
  'brd-blackl3 brd-dashed fix-bb clr-inherit pdg-5px bg-op-03': true,
  'dsp-block w-100 outline-none fnt-inherit valign-bot ': true,
});

const _scaleHeight = (e, onChange) => {
  e.target.style.height = 'auto';
  e.target.style.height = (e.target.scrollHeight+5) + 'px';
  !!onChange && onChange(e);
}

const _customOnChange = (e, onChange, maxLength) => {
  if(maxLength && maxLength < e.target.value.length){
    return;
  }
  !!onChange && onChange(e);
}

const _customOnKeyDown = (e, maxLength) => {

  if((e.target.value && maxLength && maxLength < e.target.value.length && e.keyCode !== 8) ||
      e.clipboardData && e.clipboardData.getData && e.target.value.length + e.clipboardData.getData('text/plain').length > maxLength){
        e.preventDefault();
        e.target.value = e.target.value.substring(0, maxLength+1);
        return false;
  }
}

const _getInputJsx = (name, placeholder, text, textarea, className, onChange, maxLength) => textarea
  ? <textarea 
      autoFocus
      onFocus={(e) => _scaleHeight(e, null)}
      name={ name } 
      placeholder={ placeholder } 
      onKeyDown={ (e) => _customOnKeyDown(e, maxLength) }
      onPaste={ (e) => _customOnKeyDown(e, maxLength) }
      className={ _getInputClassNames() + className }
      onChange={ (e) => _scaleHeight(e, onChange, maxLength) }
      defaultValue={ text || '' }/>
  : <input 
      name={ name }
      placeholder={ placeholder } 
      defaultValue={ text || '' } 
      onPaste={ (e) => _customOnKeyDown(e, maxLength) }
      className={ _getInputClassNames() + className } 
      onKeyDown={ (e) => _customOnKeyDown(e, maxLength) }
      onChange={ (e) => _customOnChange(e, onChange, maxLength) }/>

const TextEditable = ({editModeOn, name, placeholder, text, className, onChange, children, textarea, maxLength}) => {
  return <div className="pdg-rl-10">
    {editModeOn
      ? _getInputJsx(name, placeholder, text, textarea, className, onChange, maxLength)
      : <div className={ className }>{ children || text }</div>}
  </div>
}

TextEditable.propTypes = {
  placeholder: React.PropTypes.string,
  className: React.PropTypes.string,
  text: React.PropTypes.string,
  editModeOn: React.PropTypes.bool,
  onChange: React.PropTypes.func,
  name: React.PropTypes.string,
  textarea: React.PropTypes.bool,
}

export default TextEditable;