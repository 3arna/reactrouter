import React from 'react';
import { NavLink } from 'fluxible-router';


const SmallSpinner = ({message, className}) =>
  <div className={`txt-center mrg-tb-20px fadeIn ${className}`}>
    <span className="brd-greyl1 bg-white dsp-inline brd-rad-50 valign-bot pdg-5px dropshadow">
      <span className="loading-spinner clr-grey dsp-inline valign-bot w-min-10px h-min-10px"/>
    </span>
    {message && <span className="dsp-inline valign-top mrg-10px">{message}...</span>}
  </div>

export default SmallSpinner;


