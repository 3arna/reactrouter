import React from 'react';
import { NavLink } from 'fluxible-router';
import classNames from 'classnames';
import lodash from 'lodash';

const _getCategoryClassNames = (active) => classNames({
  'content-category valign-block auto': true,
  'content-category--active': active,
});


const ContentCategory = ({category, active}, {translate}) => {
  category = !lodash.includes(category, 'category:') && `category:${category}` || category; 
  return <NavLink href={`/feed/${category}`}>
    <span className={ _getCategoryClassNames(active) }>
      <span>{category && category.replace('category:', '')}</span>
      <i className="icon-tick-big "/>
    </span>
  </NavLink>
}
ContentCategory.propTypes = {
  category: React.PropTypes.string,
  active: React.PropTypes.bool,
};

export default ContentCategory;
