import React from 'react';
import classNames from 'classnames';

const _getContainerClassNames = (isGhostCategory, isSelected) => classNames({
  'tb brd-rad-20px txt-cap pdg-tb-10px pdg-l-20px': true,
  'hover-bg-mainl2 hover-clr-white trans cursor-pointer': true,
  'brd-white': !isGhostCategory,
  'brd-dash-2-white': isGhostCategory,
  'clr-white': !isSelected,
  'bg-white clr-mainl3': isSelected,
});

const _getIconClassNames = (isSelected) => classNames({
  'td pdg-l-10px pdg-r-15px cursor-pointer fa-fw valing-top fnt-xxs': true,
  'icon-plus-big': !isSelected,
  'icon-tick-big': isSelected,
});

const FrontCategory = ({addClass, isGhostCategory, isSelected, fnClick, category}) => 
  <span className={`fadeIn dsp-inline mrg-3px valign-bot ${addClass}`}>
    <div className={_getContainerClassNames(isGhostCategory, isSelected)} 
      onClick={fnClick && fnClick.bind(this, category.name || category)}>
      <span className="td">
        {category.name || category}
      </span>
      <i className={_getIconClassNames(isSelected)}/>
    </div>
  </span>

FrontCategory.propTypes = {
  category: React.PropTypes.any.isRequired,
  fnClick: React.PropTypes.func,
  isSelected: React.PropTypes.bool,
  isGhostCategory: React.PropTypes.bool,
  addClass: React.PropTypes.string
}

FrontCategory.defaultProps = {
  category: {name: 'category name', id: 1234}
}

export default FrontCategory;