import React from 'react';
import classNames from 'classnames';

const _getContainerClassNames = (isGhostCategory, isSelected) => classNames({
  'tb brd-rad-15px txt-cap fnt-bold pdg-tb-5px pdg-l-20px fnt-sm': true,
  'hover-bg-mainl1 hover-clr-white trans cursor-pointer': true,
  'brd-main': !isGhostCategory,
  'brd-dash-2-main': isGhostCategory,
  'clr-main': !isSelected,
  'bg-main clr-white': isSelected,
});

const _getIconClassNames = (isSelected) => classNames({
  'td cursor-pointer fnt-xxs pdg-l-10px pdg-r-15px pdg-tb-5px': true,
  'icon-plus-big': !isSelected,
  'icon-times-big': isSelected,
});

const _clearCategoryName = (category) => category.replace('category:', '');

const FrontCategory = ({addClass, isGhostCategory, isSelected, fnClick, category}) => 
  <span className={`fadeIn dsp-inline mrg-5px valign-bot ${addClass}`}>
    <div className={_getContainerClassNames(isGhostCategory, isSelected)} 
      onClick={fnClick && fnClick.bind(this, category.name || category)}>
      <span className="td">
        {_clearCategoryName(category.name || category)}
      </span>
      <i className={_getIconClassNames(isSelected)}/>
    </div>
  </span>

FrontCategory.propTypes = {
  category: React.PropTypes.any.isRequired,
  fnClick: React.PropTypes.func,
  isSelected: React.PropTypes.bool,
  isGhostCategory: React.PropTypes.bool,
  addClass: React.PropTypes.string
}

FrontCategory.defaultProps = {
  category: {name: 'category name', id: 1234}
}

export default FrontCategory;