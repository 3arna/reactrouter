import React from 'react';
import classNames from 'classnames';

const FrontSuccessAlert = ({children}) => 
  <div className="w-max-500px bg-greyl2 mrg-auto pdg-50px fix-bb">
    {children}
  </div>

FrontSuccessAlert.propTypes = {
  onClick: React.PropTypes.func,
  message: React.PropTypes.string,
}

export default FrontSuccessAlert;