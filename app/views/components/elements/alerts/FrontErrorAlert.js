import React from 'react';
import classNames from 'classnames';

const FrontErrorAlert = ({children}) => 
  <div className="mrg-auto w-max-500px pdg-10px fix-bb">
    <div className="bg-greyl2 pdg-10px fix-bb">
      <div className="brd-redl3 pdg-50px">{children}</div>
    </div>
  </div>

FrontErrorAlert.propTypes = {
  onClick: React.PropTypes.func,
  message: React.PropTypes.string,
}

export default FrontErrorAlert;