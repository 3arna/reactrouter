import React from 'react';
import classNames from 'classnames';

const SmallNotificationAlert = ({className, amount}) => 
  <span className={`txt-center pos-absolute bot-0 bg-second clr-white brd-rad-50 w-10px h-10px pdg-5px fnt-xxs mrg-5px bounce ${className}`}>
    {amount}
  </span>

SmallNotificationAlert.propTypes = {
  className: React.PropTypes.string,
  amount: React.PropTypes.number,
}

export default SmallNotificationAlert;