import React from 'react';
import classNames from 'classnames';

const ConfirmAlert = ({confirmText, onConfirm, title, text}, { translate }) =>
  <div className="dsp-inline w-300px">
    <div className="bg-white pdg-tb-20px pdg-rl-20px brd-b-blackl3">
      <h3 className="fnt-lg">{title}</h3>
      {text && <p className="fnt-sm">{text}</p>}
    </div>
    <button onClick={onConfirm} className="crs-point clr-red flt-left bg-white col-6 fnt-sm outline-none fnt-300 pdg-tb-15px brd-0">
      {confirmText}
    </button>
    <button className="brd-l-blackl3 crs-point flt-right outline-none bg-white col-6 brd-0 fnt-sm fnt-300 pdg-tb-15px">
      { translate('words.cancel')}
    </button>
  </div>

ConfirmAlert.contextTypes = {
  translate: React.PropTypes.func.isRequired
}

ConfirmAlert.propTypes = {
  confirmText: React.PropTypes.string,
  onConfirm: React.PropTypes.func,
  title: React.PropTypes.string,
  text: React.PropTypes.string,
}

export default ConfirmAlert;
