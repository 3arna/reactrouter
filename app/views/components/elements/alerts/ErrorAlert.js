import React from 'react';
import classNames from 'classnames';

const _getErrorContainerClassNames = (errorMessage) => classNames({
  'left-0 right-0 z-2 fix-bb pos-absolute fadeIn fnt-sm mrg-auto fnt-main-reg txt-center w-max-500px': true,
  'dsp-none': !errorMessage,
});

const ErrorAlert = ({errorMessage, onClearErrorMessage}) => 
  <div className={_getErrorContainerClassNames(errorMessage)} style={{marginTop: '-20px'}}>
    <span className="fnt-sm clr-white bg-op-06 pdg-tb-10px pdg-rl-20px brd-rad-15px bg-black boxshadow-b-5 dsp-inline">
      {errorMessage}
      {false && <i onClick={onClearErrorMessage}
        className="cursor-pointer icon-close hover-clr-white pdg-rl-20px clr-white"/>}
    </span>
  </div>

ErrorAlert.propTypes = {
  onClearErrorMessage: React.PropTypes.func,
  errorMessage: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.bool,
  ]),
}

export default ErrorAlert;