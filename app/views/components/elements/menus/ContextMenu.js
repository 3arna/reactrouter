import React, { PropTypes } from 'react';
import classNames from 'classnames';
import { NavLink } from 'fluxible-router';
import Portal from 'react-portal';

const _getItemClassNames = (item) => classNames({
  'w-min-150px dsp-block fix-bb pdg-rl-25px pdg-tb-10px fnt-sm md-pdg-tb-15px': true,
  'crs-point hover-clr-main trans-clr': !item.isDisabled,
  'clr-red': item.isDangerous,
  'clr-main fnt-bold': item.isImportant,
  'op-05': item.isDisabled,
});

const _getContainerClassNames = (isMobile) => classNames({
  'z-15': true,
  'fadeIn top-0 right-0 txt-left triangle triangle-rt mrg-r-40px pos-absolute md-dsp-none boxshadow w-250px': !isMobile,
  'pos-fixed col-12 mrg-0 bot-0 md-dsp-block dsp-none': isMobile,

});

const _getContextMenuJsx = (props, isMobile, pos) => {
  const style = pos && {left: pos.left, top: pos.top};

  return <div onClick={props.onClick} className={_getContainerClassNames(isMobile)} style={style}>
    <ul className="mrg-0 pdg-0 bg-white list-none txt-nowrap clr-blackl3 pdg-tb-5px">
      {props.items.map((item) => 
        <li key={item.text}>
          {item.href && !item.isDisabled
            ? <NavLink className={_getItemClassNames(item)} href={item.href}>
                <span>
                  {item.icon && <i className={`fa fnt-sm fa-fw pdg-r-5px ${item.icon}`}/>}
                  {item.text}
                </span>
              </NavLink>
            : <span className={_getItemClassNames(item)} onClick={!item.isDisabled && item.onClick}>
                {item.icon && <i className={`fa fnt-sm fa-fw pdg-r-5px ${item.icon}`}/>}
                {item.text}
              </span>}
        </li>
      )}
    </ul>
  </div>
}


const ContextMenu = (props) =>
  <div>
    <div className={`pos-relative ${props.className}`} onClick={props.onClick}>
      
      {props.children}

      {props.isActive && <Portal isOpened>{_getContextMenuJsx(props, null, props.pos)}</Portal>}
      {props.isActive && <Portal isOpened>{_getContextMenuJsx(props, true)}</Portal>}

    </div>
    {props.isActive && 
      <Portal isOpened>
        <div onClick={props.onClick} className="fadeIn bg-op-03 pos-absolute w-100 h-min-100 left-0 top-0 z-12"/>
      </Portal>}
  </div>

ContextMenu.propTypes = {
  items: PropTypes.array,
  children: PropTypes.node,
  isActive: PropTypes.bool,
  onClick: PropTypes.func,
}

ContextMenu.defaultProps = {
  items: [
    {text: 'Option 1', isDangerous: null, href: null, isDisabled: null, onClick: null, isImportant: false, },
    {text: 'Option 2', isDangerous: true, isDisabled: true,},
  ]
}

export default ContextMenu;
