import React, { PropTypes } from 'react';
import classNames from 'classnames';

const _getItemClassNames = (itemValue, activeValue) => classNames({
  'td pdg-tb-15px fnt-bold clr-blackl3 crs-point trans': true,
  'brd-b-white': itemValue !== activeValue,
  'brd-b-main': itemValue === activeValue,
  'brd-b-2px': true,
});


const SearchTabsMenu = ({items, active, onClick}) =>
  <nav className="tb tb-fixed sm-tb-auto txt-center w-100">

    {items.map((item) => 
      <span 
        key={item.value} 
        onClick={onClick && onClick.bind(this, item.value)} 
        className={_getItemClassNames(item.value, active)}>
          {item.title}
      </span>
    )}

  </nav>

SearchTabsMenu.propTypes = {
  items: PropTypes.array,
  active: PropTypes.string,
}

SearchTabsMenu.defaultProps = {
  active: 'all',
  items: [
    {title: 'All', value: 'all'},
    {title: 'People', value: 'users'},
    {title: 'Interests', value: 'categories'},
    {title: 'Hashtags', value: 'tags'},
  ],
}

export default SearchTabsMenu;
