import React, { PropTypes } from 'react';
import classNames from 'classnames';
import ReactDom from 'react-dom';
import BaseComponent from '../../../BaseComponent';

import ContextMenu from './ContextMenu';
import ContextMenuMedium from './ContextMenuMedium';

export default class ContextMenuContainer extends BaseComponent {
  
  constructor(){
    super();
    this._bind('handleMenuToggle');
    this.state = {
      isActive: false,
      pos: null,
    };
  }

  handleMenuToggle(e){
    const container = ReactDom.findDOMNode(e.target);
    this.setState({
      isActive: !this.state.isActive, 
      pos: {
        left: container.getBoundingClientRect().left-265, 
        top: container.getBoundingClientRect().top
      }
    });
    e.preventDefault();
    e.stopPropagation();
  }

  render(){
    const {props, state} = this;

    return <ContextMenu
      pos={state.pos}
      items={props.items}
      className={props.className}
      children={props.children}
      onClick={this.handleMenuToggle}
      isActive={state.isActive}/>
  }
}
