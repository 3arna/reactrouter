import React, { PropTypes } from 'react';
import classNames from 'classnames';
import { NavLink } from 'fluxible-router';

const _getItemClassNames = (lowResolution) => classNames({
  'w-min-150px dsp-block brd-greyl1 fix-bb clr-blackl3': true,
  'hover-clr-white hover-bg-black trans cursor-pointer': true,
  'pdg-10px': !lowResolution,
  'pdg-20px': lowResolution
});


const ContextMenu = ({position, items, left, top, onClick}) =>
  <div onClick={onClick} className="md-dsp-block dsp-none h-100 bg-op-05 pos-absolute w-100 left-0 top-0 fadeIn z-3">
    <ul className="pos-absolute w-100 pdg-0 mrg-0 dsp-block bot-0 left-0 bg-greyl2 list-none txt-left clr-blackl3 boxshadow">
      {items.map((item) => 
        <li key={item.title} onClick={item.onClick}>
          {item.routeName 
            ? <NavLink className={_getItemClassNames(true)} routeName={item.routeName}>
                <span><i className={`fa fnt-sm fa-fw pdg-r-5px ${item.icon}`}/>{item.title}</span>
              </NavLink>
            : <span className={_getItemClassNames(true)}><i className={`fa fnt-sm fa-fw pdg-r-5px ${item.icon}`}/>
                {item.title}
              </span>
          }
        </li>
      )}
    </ul>
  </div>

ContextMenu.propTypes = {
  errorMessage: PropTypes.any.isRequired,
  position: PropTypes.string,
  items: PropTypes.array,
  left: PropTypes.number,
  top: PropTypes.number,
}

ContextMenu.defaultProps = {
    errorMessage: 'Basic error message',
    items: [
      {title: 'Copy', icon: 'fa-clone'},
      {title: 'Share', icon: 'fa-share-alt'},
      {title: 'Report', icon: 'fa-exclamation-circle'},
    ],
  }

export default ContextMenu;
