import React from 'react';
import classNames from 'classnames';
import { NavLink } from 'fluxible-router';

import RoundAvatar from '../avatars/RoundAvatar';
import HoverButtonContainer from '../buttons/hoverButton/HoverButtonContainer';
import DefaultFrontButton from '../buttons/DefaultFrontButton';

const FollowingListItem = ({ item, buttonText, onFollowClick, onUnFollowClick }, { translate }) => {

  const textContainerClassNames = classNames({
    'flt-left tb h-min-40px': true,
    'tb': !item.description
  });

  const headingClassNames = classNames({
    'mrg-0': true,
    'td': !item.description
  });

  return(
    <li className="brd-b-grey fix-clear mrg-b-10px">
      {item.imageUrl &&
        <div className="flt-left">
          <NavLink routeName="contentFeed" navParams={{ parentId: item.id }}>
            <RoundAvatar avatarUrl={item.imageUrl} />
          </NavLink>
        </div>
      }
      <div className={textContainerClassNames}>
        <h3 className={headingClassNames}>
          <NavLink routeName="contentFeed" navParams={{ parentId: item.id }} className="clr-black">
            {item.heading}
          </NavLink>
        </h3>

        <p className="mrg-0">
          <NavLink routeName="contentFeed" navParams={{ parentId: item.id }} className="clr-black">
            {item.description}
          </NavLink>
        </p>
      </div>
      <div className="flt-right">
        {item.following
          ? <HoverButtonContainer onClick={() => onUnFollowClick(item.id)} text={translate('words.following')} hoverText={translate('words.unfollow')} />
          : <DefaultFrontButton onClick={() => onFollowClick(item.id)}>{translate('words.follow')}</DefaultFrontButton>
        }

      </div>
    </li>
)};

FollowingListItem.contextTypes = {
  translate: React.PropTypes.func.isRequired
}

FollowingListItem.propTypes = {
  item: React.PropTypes.object
};


export default FollowingListItem;
