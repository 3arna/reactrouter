import React from 'react';
import classNames from 'classnames';
import { NavLink } from 'fluxible-router';

const RoundAvatar = ({size, avatarUrl, redirectUrl}) => {
  const imgJsx = <img className="brd-greyl1 brd-rad-50" height={size} width={size} src={avatarUrl}/>;
  return (
    <div className="dsp-inline valign-bot">
      {redirectUrl
        ? <NavLink href={redirectUrl}>{ imgJsx }</NavLink>
        : imgJsx}
    </div>
  )
}

RoundAvatar.propTypes = {
  avatarUrl: React.PropTypes.string.isRequired,
  size: React.PropTypes.number.isRequired,
  redirectUrl: React.PropTypes.string,
}

RoundAvatar.defaultProps = {
  avatarUrl: '/img/avatar.png',
  size: 40,
}

export default RoundAvatar;
