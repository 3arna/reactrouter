import React from 'react';
import classNames from 'classnames';

const _getBorderClassNames = (editModeOn) => classNames({
  'brd-rad-50 dsp-inline valign-bot': true,
  'brd-white brd-dashed': editModeOn,
  //'brd-white': !editModeOn,
});

const _getIconClassNames = (inProgress) => classNames({
  'fnt-xl': true,
  'icon-camera': !inProgress,
  'icon-settings spin': inProgress,
});

const _getEditModeClassNames = () => classNames({
  'pos-absolute w-100 valign-block bg-op-08 op-07 clr-greyl2 fix-bb h-100 valign-bot brd-rad-50': true,
  'cursor-pointer': true,
});

const _getSpecialIconClassNames = () => classNames({
  'pdg-5px brd-rad-50 pos-absolute fnt-xxxs brd-solid-2-white clr-white': true,
  'icon-crown bg-second top-0 left-0': true,
})

const _preventDefault = (e) => {
  e.preventDefault();
  e.stopPropagation();
}

const EditableAvatar = ({inProgress, editModeOn, avatarUrl, size, text, onChange, isPremium}, {translate}) => 
  <span className={_getBorderClassNames(editModeOn)}>
    <div 
      className="brd-rad-50 pos-relative bg-cover bg-center bg-white"
      style={{ backgroundImage: `url('${avatarUrl}')`, width: size, height: size}}>
      {isPremium && <i className={_getSpecialIconClassNames()}/>}
      {editModeOn && 
        <label 
          className={_getEditModeClassNames()}
          onChange={onChange}
          onDrop={onChange}
          onDragOver={_preventDefault} 
          onDragLeave={_preventDefault}>
            <span>
              <i className={_getIconClassNames(inProgress)}/>
              <p className="mrg-0 fnt-sm">{translate(`words.${inProgress ? 'saving' : 'edit'}`).toLowerCase()}</p>
            </span>
            <input className="pos-absolute dsp-none" type="file"/>
        </label>
      }
    </div>
  </span>

EditableAvatar.contextTypes = {
  translate: React.PropTypes.func.isRequired
};

EditableAvatar.propTypes = {
  avatarUrl: React.PropTypes.string.isRequired,
  size: React.PropTypes.number.isRequired,
  text: React.PropTypes.string,
  inProgress: React.PropTypes.bool,
  onChange: React.PropTypes.func,
}

EditableAvatar.defaultProps = {
  avatarUrl: null,
  size: 80,
  text: 'edit',
}

export default EditableAvatar;
