import React from 'react';
import classNames from 'classnames';

const _getSelectClassNames = (iconClassName) => classNames({
  'brd-0 outline-none dsp-inline w-100 pdg-10px bg-none fnt-xs pos-relative appearance-none fnt-300': true,
  'pdg-l-40px': !!iconClassName,
});

const SettingsDropdown = ({iconClassName, name, placeholder, onChange, items, selected}) => {
  return <label className="pos-relative dsp-block brd-b-grey pdg-tb-5px">
    {iconClassName && <i className={`pos-absolute top-30 left-0 fnt-lg ${iconClassName}`}/>}
    <i className="icon-chevron-down right-0 pos-absolute top-40 fnt-xs mrg-r-10em"/>
    <select 
      onChange={onChange} 
      defaultValue={selected} 
      name={name} 
      className={_getSelectClassNames(iconClassName)} 
      placeholder={placeholder}>
        
        {items.map((item) => 
          <option key={item.value} value={item.value}>
            {item.name}
          </option>)}

    </select>
  </label>
}

SettingsDropdown.propTypes = {
  iconClassName: React.PropTypes.string,
  name: React.PropTypes.string,
  placeholder: React.PropTypes.string,
  onChange: React.PropTypes.func,
  items: React.PropTypes.array,
};

SettingsDropdown.defaultProps = {
  iconClassName: 'icon-gender',
  name: 'gender',
  items: [
    {name: 'Gender', value: false},
    {name: 'Male', value: 'male'},
    {name: 'Female', value: 'female'},
  ]
};

export default SettingsDropdown;