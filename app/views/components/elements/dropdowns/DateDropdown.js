import React from 'react';
import lodash from 'lodash';

import { setLanguage } from '../../../../actions/languageActions';

class DateDropdown extends React.Component {

  constructor(){
    super();
    this.state = {
      dropdownOn: false,
    }
  }

  handleToggle(e){
    this.setState({dropdownOn: !this.state.dropdownOn});
  }

  _generateTime(time, formatValueToTime){
    if(lodash.isObject(time)){
      return lodash.get(time, 'title');
    }
    if(!formatValueToTime){
      return time;
    }
    return (parseInt(time, 10) + 100).toString().substr(1);
  }

  _getValue(time){
    if(lodash.isObject(time)){
      return lodash.get(time, 'value');
    }
    return time;
  }

  render(){
    const {props, state} = this;

    return <span className={`${props.isDisabled && 'op-05'} ${props.className}`}>
      <div className="fnt-xs pdg-5px txt-cap">{props.title}</div>
      <span onClick={!props.isDisabled && this.handleToggle.bind(this)} className="crs-point dsp-block pos-relative left-0 bot-0">
        <div className="brd-grey pdg-rl-20px pdg-tb-10px fnt-sm w-min-60px">
          {this._generateTime(props.defaultValue, props.formatValueToTime)}
        </div>

        <div className={`over-auto pos-absolute bg-white h-max-100px z-1 bg-white w-100 fix-bb boxshadow ${!state.dropdownOn && 'dsp-none'}`}>
          <div style={{width: '4000px'}}>
            {props.items.map((time, index) => 
              <div 
                key={index}
                className="pdg-5px hover-clr-white hover-bg-main crs-point"
                onClick={!props.isDisabled && !!props.onClick && ((e) => props.onClick(props.useIndex ? index : this._getValue(time), props.type))}>
                  <div className="pdg-rl-10px">{this._generateTime(props.useIndex ? index : time, props.formatValueToTime)}</div>
              </div>)}
          </div>
        </div>

      </span>
    </span>
  }
}

export default DateDropdown;
