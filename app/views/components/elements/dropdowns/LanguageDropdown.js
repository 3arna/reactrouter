import React from 'react';
import lodash from 'lodash';

import { setLanguage } from '../../../../actions/languageActions';

class LanguageDropdown extends React.Component {

  static contextTypes = {
    config: React.PropTypes.object,
    executeAction: React.PropTypes.func.isRequired,
    translate: React.PropTypes.func,
    getLanguage: React.PropTypes.func,
    getLanguages: React.PropTypes.func,
  }

  setLanguage(e){
    const language = lodash.filter(e.target.options, (opt) => opt.selected).shift().value;
    context.executeAction(setLanguage, {lang: language});
  }

  get localesJsx(){
    return this.context.getLanguages().map((local) =>
      <option
        key={local.name}
        value={local.prefix}>
          {local.name}
      </option>);
  }

  render(){
    const lang = this.context.getLanguage();
    const translate = this.context.translate;
    const props = this.props;

    return <form className={props.className}>
      <label htmlFor="select" className={`pos-relative dsp-block`}>
        {!props.noIcon && <i className={`icon-language pos-absolute top-30 left-0 fnt-md `}/>}
        {!props.noArrow && <i className={`icon-chevron-down right-0 pos-absolute fnt-xs mrg-r-10px mrg-t-12px top-0`}/>}
        <select
          id="select"
          onChange={this.setLanguage}
          defaultValue={lang} name="locales"
          className={`brd-0 outline-none fnt-xs dsp-block w-100 pos-relative fnt-300 pdg-10px bg-none appearance-none ${!props.noIcon && 'pdg-l-40px'}`}
          placeholder={translate('words.language')}>
            {this.localesJsx}
        </select>
      </label>
    </form>
  }
}

export default LanguageDropdown;
