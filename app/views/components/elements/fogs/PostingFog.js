import React, { PropTypes } from 'react';
import classNames from 'classnames';

import BaseComponent from '../../../BaseComponent';

export default class PostingFog extends BaseComponent {
  static propTypes = {
    posting: PropTypes.bool.isRequired,
  }

  render(){
    return (this.props.posting && 
      <div className="pos-absolute fnt-300 op-05 fadeIn txt-center bg-op-05 z-10 w-100 h-100">
        <div className="valign-block h-100">
          <span className="clr-white fnt-xs">
            <span className="loading-spinner-small"/>
          </span>
        </div>
      </div>);
  }
}
