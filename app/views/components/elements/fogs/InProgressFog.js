import React from 'react';
import classNames from 'classnames';

const InProgressFog = ({onClick, message}) => 
  <div className="pos-absolute h-100 w-100 fadeIn top-0 left-0 z-1" onClick={onClick}>
    <div className="tb w-100 h-100 bg-op-05 txt-center">
      <div className="td clr-grey fnt-sm">
        <span className="bg-main dsp-inline brd-rad-50 valign-bot pdg-5px">
          <span className="loading-spinner clr-grey dsp-inline valign-bot w-min-30px h-min-30px"/>
        </span>
        {message && <div className="mrg-10px">{message}...</div>}
      </div>
    </div>
  </div>

InProgressFog.propTypes = {
  onClick: React.PropTypes.func,
  message: React.PropTypes.string,
}
export default InProgressFog;