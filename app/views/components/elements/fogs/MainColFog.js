import React from 'react';
import classNames from 'classnames';

const MainColFog = ({className, onClick, children, isFullScreen}) => 
  <div onClick={ onClick } 
    className={`fadeIn pos-absolute md-col-12 pdg-l-60px sm-pdg-0 top-0 left-0 w-100 h-100 z-3 bg-op-03 ${!isFullScreen && 'col-8'} ${className}`}>
      <div className="valign-block txt-center h-100">
        <span className="mrg-auto txt-center">
          {children}
        </span>
      </div>
  </div>

MainColFog.propTypes = {
  onClick: React.PropTypes.func,
  className: React.PropTypes.string,
  children: React.PropTypes.node,
}

export default MainColFog;