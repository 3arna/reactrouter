import React from 'react';
import classNames from 'classnames';

const _getFogClassNames = (noFog) => classNames({
  'pos-absolute clr-white txt-center h-100 w-100 z-3 top-0 over-hide': true,
  'bg-op-05': !noFog,
})

const ProgressBarFog = ({uploadProgress, message, noFog}) => 
  <div className={_getFogClassNames(noFog)}>
    <div className="valign-block h-100 pdg-rl-5">
      {uploadProgress < 99 
        ? <div>
            <p>{uploadProgress + '%'} {message}</p>
            <div className="w-max-500px mrg-auto w-100 bg-greyl1 h-5px pos-relative">
              <div className="pos-absolute h-100 bg-second trans-w" style={{width: `${uploadProgress}%`}}></div>
            </div>
          </div>
        : <div>
            <span>
              <i className="icon-settings spin fnt-xxl"/>
              <p className="mrg-0 mrg-t-5px fnt-sm">Processing</p>
            </span>
          </div>
      }
    </div>
  </div>

ProgressBarFog.propTypes = {
  onClick: React.PropTypes.func,
  message: React.PropTypes.string,
}

ProgressBarFog.defaultProps = {
  message: 'Upload'
}
export default ProgressBarFog;