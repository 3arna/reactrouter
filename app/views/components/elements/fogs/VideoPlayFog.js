import React from 'react';
import classNames from 'classnames';
import moment from 'moment';

const formatDuration = (duration) => moment(parseInt(duration)*1000).format('mm:ss')

const VideoPlayFog = ({playVideo, duration, isLoaded, message, children}) => 
  <div className="pos-absolute h-100 w-100 top-0 fadeIn" onClick={playVideo}>
    <div className="tb w-100 h-100 bg-op-05 txt-center">
      
      <div className="td clr-white op-08 hover-op-1">
          <i className="icon-play clr-inherit fnt-xxxl brd-white brd-2px brd-rad-50"/>
          {isLoaded && !!duration && <div className="fnt-300 fnt-xxs pos-absolute bot-0 mrg-15px op-08 pdg-t-5em">{formatDuration(duration)}</div>}
          <div><i className="pos-absolute right-0 bot-0 mrg-15px op-05 pdg-t-5em icon-versy-logo"/></div>
          {children}
      </div>


    </div>
  </div>

VideoPlayFog.propTypes = {
  duration: React.PropTypes.number,
  playVideo: React.PropTypes.func,
  isLoaded: React.PropTypes.bool,
}

VideoPlayFog.defaultProps = {
  isLoaded: true,
  duration: null
}

export default VideoPlayFog;