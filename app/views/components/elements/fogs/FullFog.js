import React from 'react';
import classNames from 'classnames';

const FullFog = ({className, onClick, children}) => 
  <div className={`pos-fixed h-100 w-100 fadeIn top-0 left-0 ${className || 'z-4'}`} onClick={onClick}>
    <div className="tb w-100 h-100 bg-op-05 txt-center">
      <div className="td">
        {children}
      </div>
    </div>
  </div>

FullFog.propTypes = {
  onClick: React.PropTypes.func,
  className: React.PropTypes.string,
  children: React.PropTypes.node,
}

export default FullFog;