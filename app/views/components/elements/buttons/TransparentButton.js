import React from 'react';
import classNames from 'classnames';
import { NavLink } from 'fluxible-router';

const _getButtonClassNames = (isDisabled, className) => classNames({
  'mrg-l-5 fnt-xs pdg-tb-5em pdg-rl-20em dsp-inline brd-main clr-main valign-bot bg-none outline-none': true,
  'hover-bg-main hover-clr-white crs-point trans-bg': !isDisabled,
  'op-05': isDisabled,
  [className]: true,
});

const TransparentButton = ({onClick, text, isDisabled, className}) => 
  <button onClick={onClick} className={_getButtonClassNames(isDisabled, className)} disabled={isDisabled}>{text}</button>

TransparentButton.propTypes = {
  text: React.PropTypes.string.isRequired,
  isDisabled: React.PropTypes.bool,
  onClick: React.PropTypes.func,
}

TransparentButton.defaultProps = {
  text: 'Save'
}

export default TransparentButton;

