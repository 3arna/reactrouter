import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';

const _getButtonClassNames = (isDisabled, className) => classNames({
  'outline-none brd-0 clr-white brd-main pdg-tb-10px pdg-rl-20px dsp-inline bg-main fnt-xs fnt-300': true,
  'hover-clr-main hover-bg-none cursor-pointer': !isDisabled,
  'op-08': isDisabled,
  [className]: true,
});

const DefaultButton = ({isDisabled, onClick, children, text, routeName, href, className}) => {

  let buttonJsx = <button onClick={!routeName && !href && !isDisabled && onClick} className={_getButtonClassNames(isDisabled, className)}>
    {text || children}
  </button>

  return (
    !isDisabled && routeName || href
      ? <Link className="clr-inherit txt-deco-none" to={href || routeName}>{buttonJsx}</Link>
      : buttonJsx
  )
}

DefaultButton.propTypes = {
  isDisabled: React.PropTypes.bool,
  onClick: React.PropTypes.func,
  children: React.PropTypes.node,
  routeName: React.PropTypes.string,
}

export default DefaultButton;

