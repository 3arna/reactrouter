import React from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';

const _getButtonClassNames = () => classNames({
  'clr-black pdg-5px dsp-inline valign-bot fix-bb': true,
  'brd-rad-50 trans-bg hover-clr-white hover-bg-main cursor-pointer': true
});

const _getIconClassNames = (reverse) => classNames({
  'dsp-inline valign-bot': true,
  'icon-chevron-back': !reverse,
  'icon-chevron-forward': reverse,
});

const BackButton = ({ href, routeName, onClick, reverse }) => {

  return href
    ? <Link to={ href || routeName } className={_getButtonClassNames()}>
        <i className={_getIconClassNames(reverse)}/>
      </Link>
    : <span onClick={ onClick } className={_getButtonClassNames()}>
        <i className={_getIconClassNames(reverse)}/>
      </span>
}

BackButton.propTypes = {
  routeName: React.PropTypes.string,
  href: React.PropTypes.string,
  onClick: React.PropTypes.func,
};

export default BackButton;
