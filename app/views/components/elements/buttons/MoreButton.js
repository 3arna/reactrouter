import React from 'react';
import classNames from 'classnames';

const _getMoreButtonClassNames = (active) => classNames({
  'dsp-inline clr-inherit cursor-pointer icon-more-menu': true,
  'clr-white bg-maind1': active
});

const _getWrapperClassNames = (className) => classNames({
  '': !className,
  [className]: className,
}) 

const MoreButton = ({onClick, className}) =>
  <span className={_getWrapperClassNames(className)} onClick={onClick}>
    <i className={_getMoreButtonClassNames()}/>
  </span>

MoreButton.propTypes = {
  active: React.PropTypes.bool,
  onClick: React.PropTypes.func,
};

export default MoreButton;
