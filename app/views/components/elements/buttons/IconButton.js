import React from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';

const _getButtonClassNames = (isActive, isDisabled) => classNames({
  'mrg-rl-1 pdg-rl-5px dsp-inline trans-clr': true,
  'hover-clr-mainl2 clr-main': isActive && !isDisabled,
  'hover-clr-main': !isActive && !isDisabled,
  'crs-point': !isDisabled,
  'op-05': isDisabled
});

const _getIconClassNames = (iconClassName) => classNames({
  'fnt-md mrg-r-5em': true,
  [iconClassName]: true,
});

const _getTextClassNames = (smNameHide) => classNames({
  'valign-top dsp-inline mrg-tb-5em fnt-xs': true,
  'sm-dsp-none': smNameHide,
});

const IconButton = ({ href, routeName, onClick, iconClassName, text, isActive, smNameHide, isDisabled }) => {

  return href && !isDisabled
    ? <Link to={ href || routeName } className={_getButtonClassNames(isActive, isDisabled)}>
        {iconClassName && <i className={_getIconClassNames(iconClassName)}/>}
        <span className={_getTextClassNames(smNameHide)}>{text}</span>
      </Link>
    : <span onClick={ !isDisabled && onClick } className={_getButtonClassNames(isActive, isDisabled)}>
        {iconClassName && <i className={_getIconClassNames(iconClassName)}/>}
        <span className={_getTextClassNames(smNameHide)}>{text}</span>
      </span>
}

IconButton.propTypes = {
  routeName: React.PropTypes.string,
  href: React.PropTypes.string,
  onClick: React.PropTypes.func,
  iconClassName: React.PropTypes.string,
  text: React.PropTypes.string,
  isActive: React.PropTypes.bool,
  smNameHide: React.PropTypes.bool,
};

export default IconButton;
