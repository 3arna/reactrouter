import React from 'react';
import classNames from 'classnames';

const _getButtonClassNames = (isFullScreen) => classNames({
  'crs-point trans bg-main fnt-xxs clr-white brd-rad-50 boxshadow-b-5 hover-bg-mainl2 pdg-15px': true,
  'pos-fixed bot-0 mrg-15px z-1 fadeIn md-r-0': true,
  'right-33': !isFullScreen,
  'right-0': isFullScreen,
})

const RoundPostButton = ({ onClick, isFullScreen }) => 
  <div 
    onClick={ onClick } 
    className={_getButtonClassNames(isFullScreen)}>
      <i className="icon-plus-big valign-bot fnt-xl"/>
  </div>

RoundPostButton.propTypes = {
  onClick: React.PropTypes.func,
};

export default RoundPostButton;
