import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';
import lodash from 'lodash';

const _getButtonClassNames = (type, disabled, onlyIcon) => classNames({
  'mrg-0 fnt-xs bg-none pos-relative trans pdg-rl-20px fix-bb dsp-block w-100 fnt-300 trans-bg': true,
  'clr-inherit outline-none': true,
  'pdg-tb-12px': type !== 'small',
  'pdg-tb-10px': type === 'small',
  'bg-twitter brd-twitter hover-clr-twitter brd-rad-20px': type === 'twitter',
  'bg-facebook brd-facebook hover-clr-facebook brd-rad-20px': type === 'facebook',
  'bg-google brd-google hover-clr-google brd-rad-20px': type === 'google',
  'hover-bg-white brd-solid-2-main': type !== 'submit' || type !== 'round',
  'bg-main hover-bg-maind1 brd-0 brd-solid-2-main clr-white': (lodash.includes(['submit', 'round', 'small'], type) || !type) && !disabled,
  'op-08 bg-greyd2 brd-solid-2-greyd2': disabled,
  'cursor-pointer': !disabled,
  'brd-solid-2-main clr-main brd-rad-20px': type === 'transparent',
  'brd-rad-20px': type === 'round' 
  //'pdg-tb-10px': onlyIcon,
  //'pdg-tb-10px': !onlyIcon
});

const _getIconClassNames = (onlyIcon, type) => classNames({
  'fnt-md': true,
  'flt-left left-0 pdg-r-5px fix-bb sm-dsp-none': !onlyIcon,
  'icon-twitter': type === 'twitter',
  'icon-facebook': type === 'facebook',
  'icon-google': type === 'google',
});


const FrontButton = ({onClick, className, type, disabled, onlyIcon, routeName, text, href}) => {

  const buttonJsx = <div onClick={onClick} className={'mrg-0 fnt-sm ' + className}>
    <button type={type === 'submit' && 'submit'} disabled={disabled} className={_getButtonClassNames(type, disabled, onlyIcon)}>
      {type && <i className={_getIconClassNames(onlyIcon, type)}/>} 
      {!onlyIcon && text} {!onlyIcon && !lodash.includes(['submit', 'transparent', 'round', 'small'], type) && 
        <span className="txt-cap">{type}</span>}
    </button>
  </div>

  return (
    routeName || href
      ? <Link className="clr-inherit txt-deco-none" to={href || routeName}>{buttonJsx}</Link>
      : buttonJsx
  );
}

FrontButton.propTypes = {
  className: React.PropTypes.string,
  text: React.PropTypes.string.isRequired,
  onClick: React.PropTypes.func,
  type: React.PropTypes.any,
  disabled: React.PropTypes.bool,
  onlyIcon: React.PropTypes.bool,
  routeName: React.PropTypes.string,
}

FrontButton.defaultProps = {
  className: '',
  text: 'Sign in with',
  type: false
}

export default FrontButton;

