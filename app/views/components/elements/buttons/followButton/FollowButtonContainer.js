import React from 'react';
import BaseComponent from '../../../../BaseComponent';

import FollowButton from './FollowButton';


export default class FollowButtonContainer extends BaseComponent {
  constructor(){
    super();
    this._bind('handleToggleHover');
    this.state = { isHovered: false }
  }

  handleToggleHover(isHovered){
    this.state.isHovered !== isHovered && this.setState({isHovered});
  }

  render(){
    const { props, state } = this;

    return <FollowButton
      onToggleBookmark={ props.onToggleBookmark }
      onToggleHover={ this.handleToggleHover }
      isBookmarked={ props.isBookmarked }
      isHovered={ state.isHovered }
      id={ props.id }
      className={ props.className }
      theme={ props.theme }/>
  }

  static propTypes = {
    className: React.PropTypes.string,
    isBookmarked: React.PropTypes.any,
    id: React.PropTypes.string.isRequired,
    onToggleBookmark: React.PropTypes.func,
    theme: React.PropTypes.string,
  }
}
