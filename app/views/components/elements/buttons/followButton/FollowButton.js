import React from 'react';
import classNames from 'classnames';

const _getButtonClassNames = (isBookmarked, theme) => classNames({
  'pdg-tb-10em brd-rad-25px pdg-rl-5px dsp-inline fnt-sm fix-bb txt-center': true,
  'cursor-pointer trans-bg': true,

  'w-min-120px': theme !== 'round',

  'clr-white hover-bg-op-05 hover-clr-white brd-grey': !isBookmarked && theme === 'transparent',
  'clr-white bg-op-05 hover-bg-none hover-clr-white brd-grey': isBookmarked && theme === 'transparent',

  'clr-main hover-bg-mainl2 hover-clr-white brd-main': !isBookmarked && theme !== 'transparent',
  'bg-main clr-white hover-bg-none hover-clr-main brd-main': isBookmarked && theme !== 'transparent',
});

const _getIconClassNames = (isHovered, theme, isBookmarked) => classNames({
  'fnt-xs': true,
  'icon-tick-big': !isHovered && isBookmarked,
  'icon-times-big': isHovered && isBookmarked,
  'icon-plus-big': !isBookmarked,
  'mrg-l-10px': theme !== 'round',
  'pdg-rl-5px': theme === 'round',
});

const _generateText = (isBookmarked, theme, isHovered, translate) => {
  if(theme === 'round'){
    return;
  }

  return isBookmarked
    ? !isHovered && translate('words.following') || translate('words.unfollow')
    : translate('words.follow')

};

const FollowButton = ({onToggleBookmark, onToggleHover, isBookmarked, isHovered, id, theme, className}, { translate }) => {
  return <span
    onMouseOver={isBookmarked && onToggleHover.bind(null, true)}
    onMouseLeave={isBookmarked && onToggleHover.bind(null, false)}
    onClick={onToggleBookmark && onToggleBookmark.bind(this, {objectId: id, isBookmarked})}
    className={`${_getButtonClassNames(isBookmarked, theme)} ${className}`}>

      {_generateText(isBookmarked, theme, isHovered, translate)}
      {(isBookmarked || theme==='round') && <i className={_getIconClassNames(isHovered, theme, isBookmarked)}/>}

  </span>
};

FollowButton.contextTypes = {
  translate: React.PropTypes.func.isRequired
}

FollowButton.propTypes = {
  isBookmarked: React.PropTypes.any,
  id: React.PropTypes.string.isRequired,
  toggleBookmark: React.PropTypes.func,
  theme: React.PropTypes.string,
  isHovered: React.PropTypes.bool,
  onToggleBookmark: React.PropTypes.func,
};

FollowButton.defaultProps = {
  isBookmarked: false,
  theme: null,
  id: 'user',
};

export default FollowButton;
