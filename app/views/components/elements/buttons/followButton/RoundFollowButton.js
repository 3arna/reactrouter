import React from 'react';
import classNames from 'classnames';

const _getButtonClassNames = (isBookmarked, theme) => classNames({
  'pdg-tb-10em brd-rad-25px dsp-inline w-min-120px fnt-sm fix-bb txt-center': true,
  'cursor-pointer trans mrg-t-20px': true,
  
  'clr-white hover-bg-white hover-clr-main brd-grey': !isBookmarked && !theme,
  'bg-white clr-main hover-bg-none hover-clr-white brd-grey': isBookmarked && !theme,

  'clr-main hover-bg-main hover-clr-white brd-main': !isBookmarked && theme === 'main',
  'bg-main clr-white hover-bg-none hover-clr-main brd-main': isBookmarked && theme === 'main',
});

const _getIconClassNames = (isHovered, isBookmarked) => classNames({
  'mrg-l-10px fnt-xs': true,
  'icon-tick-big': isBookmarked && !isHovered,
  'icon-times-big': isBookmarked && isHovered,
  'icon-plus-big': !isBookmarked
});

const RoundFollowButton = ({onToggleBookmark, onToggleHover, isBookmarked, isHovered, id, theme}) => {
  return <span 
    onMouseOver={isBookmarked && onToggleHover.bind(null, true)}
    onMouseLeave={isBookmarked && onToggleHover.bind(null, false)}
    onClick={onToggleBookmark.bind(this, {objectId: id, isBookmarked})}
    className={_getButtonClassNames(isBookmarked, theme)}>
      {isBookmarked && <i className={_getIconClassNames(isHovered, isBookmarked)}/>}
  </span>
}

RoundFollowButton.propTypes = {
  isBookmarked: React.PropTypes.any,
  id: React.PropTypes.string.isRequired,
  toggleBookmark: React.PropTypes.func,
  theme: React.PropTypes.string,
  isHovered: React.PropTypes.bool,
  onToggleBookmark: React.PropTypes.func,
};

RoundFollowButton.defaultProps = {
  isBookmarked: false,
  theme: null,
  id: 'user',
}

export default RoundFollowButton;
