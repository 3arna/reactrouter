import React from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';

const _getButtonClassNames = (className) => classNames({
  'icon-times-big pdg-10em brd-rad-50 fnt-sm bg-white boxshadow': true,
  'crs-point hover-clr-main trans-clr': true,
  [className]: true,
});

const CloseButton = ({ href, routeName, onClick, className, style }) => {

  return href
    ? <Link to={ href || routeName} className={className} style={style}>
        <i className={_getButtonClassNames()}/>
      </Link>
    : <i className={_getButtonClassNames(className)} onClick={ onClick } style={style}/>
}

CloseButton.propTypes = {
  routeName: React.PropTypes.string,
  href: React.PropTypes.string,
  onClick: React.PropTypes.func,
};

export default CloseButton;
