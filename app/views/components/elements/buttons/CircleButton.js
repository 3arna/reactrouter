import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';

const _getButtonClassNames = (isDisabled) => classNames({
  'outline-none brd-0 mrg-l-10px pdg-15px brd-rad-50 dsp-inline': true,
  'bg-main clr-white hover-bg-mainl1 trans cursor-pointer': !isDisabled,
  'bg-greyl3 clr-grey': isDisabled,
});

const _getContainerClassNames = (hide) => classNames({
  'dsp-none': hide,
  'fadeIn': !hide,
});

const _getTextClassNames = (isDisabled) => classNames({
  'op-05': isDisabled
});

const CircleButton = ({isDisabled, onClick, hide, text, routeName}) => {

  let buttonJsx = <button onClick={!isDisabled && onClick} className={_getButtonClassNames(isDisabled)}>
    <i className="icon-chevron-forward"/>
  </button>

  if(text){
    buttonJsx = <span className={_getContainerClassNames(hide)}>
      <span className={_getTextClassNames(isDisabled)}>{text}</span>
      {buttonJsx}
    </span>
  }

  return (
    routeName
      ? <Link className="clr-inherit txt-deco-none" to={routeName}>{buttonJsx}</Link>
      : buttonJsx
  )
}

CircleButton.propTypes = {
  text: React.PropTypes.string.isRequired,
  isDisabled: React.PropTypes.bool,
  hide: React.PropTypes.bool,
  routeName: React.PropTypes.string,
  onClick: React.PropTypes.func,
};

CircleButton.defaultProps = {
  text: 'Continue',
};

export default CircleButton;

