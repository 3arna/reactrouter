import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';

const _getSubmitButtonClassNames = (isDisabled) => classNames({
  'outline-none brd-rad-10px brd-0 pdg-tb-10px pdg-rl-30px fnt-sm': true,
  'hover-bg-mainl1 trans cursor-pointer': !isDisabled,
  'bg-blackl2 brd-blackl2 clr-white': true,
  'op-05': isDisabled, 
});

const SubmitButton = ({isDisabled, submit, children, name, className, routeName}) => {
  
  const buttonJsx = <button onClick={!isDisabled && submit} className={_getSubmitButtonClassNames(isDisabled)} type="submit">
    { children || name }
  </button>

  return (
    <div className={className}>
      {!isDisabled && routeName
        ? <Link to={routeName}>{buttonJsx}</Link>
        : buttonJsx
      }
    </div>
  )
}

SubmitButton.propTypes = {
  name: React.PropTypes.string.isRequired,
  isDisabled: React.PropTypes.bool,
  submit: React.PropTypes.func,
  children: React.PropTypes.node,
  className: React.PropTypes.string,
  routeName: React.PropTypes.string,

}

SubmitButton.defaultProps = {
  name: 'Submit',
}

export default SubmitButton;

