import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';

const _getButtonClassNames = (notClickable, isDisabled, isActive, isExpanded) => classNames({
  'mrg-tb-2px pdg-15px dsp-block brd-0 w-100 outline-none txt-left fnt-sm fnt-300': true,
  'bg-white': !isActive && !isExpanded,
  'crs-point': !isDisabled && !isActive && !isExpanded && !notClickable,
  'op-05': isDisabled,
  'bg-main clr-white': isActive || isExpanded,

});

const _getIconClassNames = (iconClassName) => classNames({
  'fnt-xs mrg-r-10px': true,
  [iconClassName]: true,
});

const _getChevronClassNames = (isExpanded) => classNames({
  'flt-right fnt-xs mrg-2px': true,
  'icon-chevron-forward': !isExpanded,
  'icon-chevron-down': isExpanded,
});

const ListButton = ({isDisabled, onClick, routeName, href, children, noForword, iconClassName, isActive, isExpanded}) => {

  const buttonJsx = 
    <button 
      onClick={!isActive && !isExpanded && !routeName && !href && !isDisabled && onClick} 
      className={_getButtonClassNames(!onClick && !routeName && !href, isDisabled, isActive, isExpanded)}>
        {iconClassName && <i className={_getIconClassNames(iconClassName)}/>}
        {children}
        {!noForword && <i className={_getChevronClassNames(isExpanded)}/>}
    </button>

  return (
    routeName || href
      ? <Link className="clr-inherit txt-deco-none" to={href || routeName}>{buttonJsx}</Link>
      : buttonJsx
  )
}

ListButton.propTypes = {
  isDisabled: React.PropTypes.bool,
  onClick: React.PropTypes.func,
  children: React.PropTypes.node,
  routeName: React.PropTypes.string,
  isForword: React.PropTypes.bool,
  iconClassName: React.PropTypes.string,
}

export default ListButton;

