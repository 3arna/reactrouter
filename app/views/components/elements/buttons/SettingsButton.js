import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';

const _getSettingsClassNames = () => classNames({
  'dsp-inline pdg-tb-10px pdg-rl-3 cursor-pointer': true,
  'pos-absolute z-1 right-0 top-0': true,
});

const _getIconClassNames = () => classNames({
  'icon-settings clr-white fnt-xl hover-rotate-45 valign-bot': true,
});

const SettingsButton = ({href, onClick, routeName}) => (href || routeName)
  ? <Link to={ href || routeName } className={_getSettingsClassNames()}>
      <i className={_getIconClassNames()}/>
    </Link>
  : <span className={_getSettingsClassNames()} onClick={onClick}>
      <i className={_getIconClassNames()}/>
    </span>

SettingsButton.propTypes = {
  href: React.PropTypes.string,
  onClick: React.PropTypes.func,
}

export default SettingsButton;