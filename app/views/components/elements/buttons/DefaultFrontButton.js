import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';

const _getButtonClassNames = (isDisabled) => classNames({
  'outline-none brd-0 mrg-l-10px pdg-rl-20px pdg-tb-10px dsp-inline fnt-sm': true,
  'bg-greyl2 clr-black hover-bg-mainl1 hover-clr-white trans cursor-pointer': !isDisabled,
  'bg-greyl3 clr-grey': isDisabled,
});

const _getTextClassNames = (isDisabled) => classNames({
  'op-05': isDisabled,
  'sm-dsp-none': true,
});

const DefaultFrontButton = ({isDisabled, onClick, children, text, routeName}) => {

  let buttonJsx = <button onClick={isDisabled && onClick} className={_getButtonClassNames(isDisabled)}>
    {children}
  </button>

  if(text){
    buttonJsx = <span><span className={_getTextClassNames}>{text}</span>{buttonJsx}</span>
  }

  return (
    routeName && !isDisabled
      ? <Link className="clr-inherit txt-deco-none" to={routeName}>{buttonJsx}</Link>
      : buttonJsx
  )
}

DefaultFrontButton.propTypes = {
  isDisabled: React.PropTypes.bool,
  onClick: React.PropTypes.func,
  children: React.PropTypes.node,
  text: React.PropTypes.string,
  routeName: React.PropTypes.string,
}

export default DefaultFrontButton;

