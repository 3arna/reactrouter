import React from 'react';
import classNames from 'classnames';

const CommentsIndicator = ({showIcon, comments}, { translate }) =>
  <span className="op-08 mrg-r-5px fnt-sm valign-bot">
    {showIcon && <i className="fa fa-comments-o fa-lg fa-fw"/>}
    {comments} {!showIcon && <strong>{translate('words.replies')}</strong>}
  </span>

CommentsIndicator.propTypes = {
  comments: React.PropTypes.number.isRequired,
  showIcon: React.PropTypes.bool.isRequired,
}

CommentsIndicator.defaultProps = {
  comments: 0,
  showIcon: false,
}

export default CommentsIndicator;