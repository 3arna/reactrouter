import React from 'react';
import classNames from 'classnames';

const DownvoteIndicator = ({dislikes}) => 
  <span className="hover-clr-redl2 trans cursor-pointer mrg-r-5px fnt-sm">
    <i className="fa fa-arrow-circle-o-down fa-lg pdg-5px"/>{dislikes}
  </span>

DownvoteIndicator.propTypes = {
  dislikes: React.PropTypes.number.isRequired,
  downvote: React.PropTypes.func,
  isDisabled: React.PropTypes.bool
}

DownvoteIndicator.defaultProps = {
  dislikes: 0
}

export default DownvoteIndicator;