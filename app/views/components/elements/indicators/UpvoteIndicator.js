import React from 'react';
import classNames from 'classnames';

const UpvoteIndicator = ({likes}) => 
  <span className="hover-clr-greenl2 trans cursor-pointer mrg-r-5px fnt-sm">
    <i className="fa fa-arrow-circle-o-up fa-lg mrg-r-2px"/>{likes}
  </span>

UpvoteIndicator.propTypes = {
  likes: React.PropTypes.number.isRequired,
  upvote: React.PropTypes.func,
  isDisabled: React.PropTypes.bool
}

UpvoteIndicator.defaultProps = {
  likes: 0
}

export default UpvoteIndicator;