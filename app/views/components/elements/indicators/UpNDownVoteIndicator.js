import React from 'react';
import classNames from 'classnames';

const _getVoteClassNames = (isLikes, isActive) => classNames({
  'trans cursor-pointer valign-block zoomIn': true,
  'hover-clr-red': !isLikes,
  'hover-clr-green': isLikes,
  'clr-red': !isLikes && isActive,
  'clr-green': isLikes && isActive
});

const _getIconClassNames = (isLikes) => classNames({
  'pdg-5px mrg-l-10px': true,
  'icon-down': !isLikes,
  'icon-up': isLikes,
})

const UpNDownVoteIndicator = ({votes, isLikes, onClick, isActive}) =>
  <span className={_getVoteClassNames(isLikes, isActive)} onClick={onClick}>
    <i className={_getIconClassNames(isLikes)}/>
    <b className="dsp-inline valign-top pdg-t-2px fnt-sm">{votes}</b>
  </span>

UpNDownVoteIndicator.propTypes = {
  votes: React.PropTypes.number.isRequired,
  isLikes: React.PropTypes.bool
}

UpNDownVoteIndicator.defaultProps = {
  votes: 0
}

export default UpNDownVoteIndicator;
