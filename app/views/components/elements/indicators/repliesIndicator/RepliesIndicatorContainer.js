import React, { PropTypes } from 'react';
import BaseComponent from '../../../../BaseComponent';

import RepliesIndicator from './RepliesIndicator';

export default class RepliesIndicatorContainer extends BaseComponent {

  constructor(){
    super();
    this._bind('handleToggleHover');
    this.state = {
      isHovered: false
    };
  }

  handleToggleHover(e){
    this.setState({isHovered: !this.state.isHovered})
  }

  render(){
    return <RepliesIndicator
      isHovered={this.state.isHovered}
      onToggleHover={this.handleToggleHover}
      {...this.props}
    />
  }

  static PropTypes = {
    repliesAmount: React.PropTypes.number,
    contentId: React.PropTypes.string,
    isHovered: React.PropTypes.bool,
    isPrivate: React.PropTypes.bool,
    onToggleHover: React.PropTypes.func,
  };
}

