import React from 'react';
import { NavLink } from 'fluxible-router';
import classNames from 'classnames';

const _getIconClassNames = (isHovered) => classNames({
  'icon-chevron-forward pdg-5px fix-bb mrg-l-2px': true,
  'bg-main clr-white brd-rad-20px': isHovered,
});

const _getRepliesString = (repliesAmount, translate) => {
  if(!repliesAmount){
    return translate('words.reply');
  }

  return !!repliesAmount && repliesAmount > 1
    ? `${repliesAmount} ${translate('words.replies')}`
    : `${repliesAmount} ${translate('words.reply')}`;
}

const Replies = ({repliesAmount, repliesUrl, isPrivate, isHovered, onToggleHover}, { translate }) => {
  return <div onMouseOver={onToggleHover} onMouseOut={onToggleHover} className="valign-block nowrap cursor-pointer">
    {isPrivate && <i className="icon-padlock-closed pdg-5px fix-bb"/>}
    <NavLink preserveScrollPosition={true} href={repliesUrl}>
     <small className="fnt-bold pdg-l-2px">{_getRepliesString(repliesAmount, translate)}</small>
    </NavLink>
    <NavLink preserveScrollPosition={true} href={repliesUrl}>
      <i className={_getIconClassNames(isHovered)}/>
    </NavLink>
  </div>
}

Replies.contextTypes = {
  translate: React.PropTypes.func
};

Replies.propTypes = {
  repliesAmount: React.PropTypes.number,
  contentId: React.PropTypes.string,
  isHovered: React.PropTypes.bool,
  isPrivate: React.PropTypes.bool,
  onToggleHover: React.PropTypes.func,
};

export default Replies;
