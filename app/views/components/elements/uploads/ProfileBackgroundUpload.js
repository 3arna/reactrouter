import React from 'react';
import classNames from 'classnames';
import lodash from 'lodash';

const _preventDefault = (e) => {
  e.preventDefault();
  e.stopPropagation();
};

const _getContainerClassNames = () => classNames({
  'fadeIn bg-op-05 dsp-inline brd-grey brd-dashed brd-1px pdg-tb-20px w-70px clr-white mrg-20px cursor-pointer fnt-md': true,
  'pos-absolute right-0 top-0': true,
  'sm-pos-relative hover-bg-none trans': true,
});

const _getIconClassNames = (inProgress) => classNames({
  'fnt-lg': true,
  'icon-camera': !inProgress,
  'icon-settings spin': inProgress,
});

const ProfileBackgroundUpload = ({inProgress, onChange, text}, {translate}) => 
  <label 
    className={_getContainerClassNames()}
    onChange={onChange} 
    onDrop={onChange} 
    onDragOver={_preventDefault} 
    onDragLeave={_preventDefault}>
      <i className={_getIconClassNames(inProgress)}/>
      <p className="mrg-0 fnt-xs">{translate(`words.${inProgress ? 'saving' : 'edit'}`).toLowerCase()}</p>
      <input className="pos-absolute dsp-none" type="file"/>
  </label>

ProfileBackgroundUpload.contextTypes = {
  translate: React.PropTypes.func.isRequired
};

ProfileBackgroundUpload.propTypes = {
  text: React.PropTypes.string,
  onChange: React.PropTypes.func,
  inProgress: React.PropTypes.bool,
}

ProfileBackgroundUpload.defaultProps = {
  text: 'edit',
}

export default ProfileBackgroundUpload;