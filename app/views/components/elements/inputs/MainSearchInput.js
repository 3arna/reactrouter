import React from 'react';
import classNames from 'classnames';

let _timer = false;

const _getFieldClassNames = () => classNames({
  'tb pdg-rl-10px clr-blackl1 bg-greyl2 fix-bb w-100 mrg-auto pos-relative brd-rad-5px brd-greyl1': true,
});

const _customOnChange = ({onChange, delay}, e) => {
  if(!delay || !onChange){
    return;
  }

  clearTimeout(_timer);
  _timer = setTimeout(() => onChange(e), delay)
};

const _customOnSubmit = ({onChange}, e) => {
  switch(e.keyCode){
    case 13:
      e.preventDefault();
      clearTimeout(_timer);
      onChange(e);
      break;
  }
};

const _customClearInput = (onChange, e) => {
  e.target.parentElement.parentElement.getElementsByTagName('input')[0].value='';
  onChange(e);
};

const MainSearchInput = ({inputValue, placeholder, onChange, delay, defaultValue}) =>
  <div className={_getFieldClassNames()}>
    <div className="td">
      <i className="icon-search valign-bot"/>
    </div>
    <div className="td w-100">
      <input
        defaultValue={defaultValue}
        value={inputValue}
        className="pdg-tb-5px pdg-rl-10px fix-bb brd-0 w-100 mrg-0 bg-none outline-none dsp-block fnt-sm"
        placeholder={placeholder}
        onChange={_customOnChange.bind(this, {onChange, delay})}
        onKeyDown={_customOnSubmit.bind(this, {onChange})}/>
    </div>
    <div className="td">
      <i onClick={_customClearInput.bind(this, onChange)} 
        className="icon-close trans hover-clr-redl2 cursor-pointer valign-bot"/>
    </div>
  </div>

MainSearchInput.propTypes = {
  placeholder: React.PropTypes.string.isRequired,
  onChange: React.PropTypes.func,
  delay: React.PropTypes.number,
}

MainSearchInput.defaultProps = {
  delay: 0,
}

export default MainSearchInput;