import React from 'react';
import classNames from 'classnames';

let _timer = false;

const _getFieldClassNames = () => classNames({
  'tb pdg-rl-20px bg-white fix-bb w-100 mrg-auto mrg-t-30px pos-relative': true,
});

const _getInputClassNames= (theme) => classNames({
  'pdg-rl-15px fnt-300 fix-bb brd-0 w-100 mrg-0 outline-none dsp-block fnt-sm': true,
  'pdg-tb-15px': theme === 'big',
  'pdg-tb-10px': theme !== 'big'
});

const _customOnChange = ({onChange, onChangeDelayed, delay}, e) => {
  onChange && onChange(e);

  if(onChangeDelayed && delay){
    clearTimeout(_timer);
    _timer = setTimeout(() => onChangeDelayed(e), delay)
  } 
};

const _customOnSubmit = ({onSubmit, onChange}, e) => {
  switch(e.keyCode){
    case 13:
      e.preventDefault();
      onSubmit(e);
      break;
  }
};

const _customClearInput = (onChange, e) => {
  e.target.parentElement.parentElement.getElementsByTagName('input')[0].value='';
  onChange(e);
};

const CategorySearchInput = ({inputValue, placeholder, onChange, onChangeDelayed, delay, onSubmit, theme}) =>
  <div className={_getFieldClassNames()}>
    <div className="td">
      <i className="icon-search clr-greyd1 valign-bot"/>
    </div>
    <div className="md-dsp-block td w-100">
      <input
        value={inputValue}
        className={_getInputClassNames(theme)}
        placeholder={placeholder}
        onChange={_customOnChange.bind(this, {onChange, onChangeDelayed, delay})}
        onKeyDown={_customOnSubmit.bind(this, {onSubmit, onChange})}/>
    </div>
    <div className="td">
      <i onClick={_customClearInput.bind(this, onChange)} 
        className="icon-close clr-greyd1 hover-clr-redl2 cursor-pointer valign-bot"/>
    </div>
  </div>

CategorySearchInput.propTypes = {
  placeholder: React.PropTypes.string.isRequired,
  onChangeDelayed: React.PropTypes.func,
  onChange: React.PropTypes.func,
  delay: React.PropTypes.number,
}

CategorySearchInput.defaultProps = {
  placeholder: 'Search hobbies, likes, interests',
  delay: 0,
}

export default CategorySearchInput;