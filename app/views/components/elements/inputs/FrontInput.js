import React from 'react';
import classNames from 'classnames';

let _timer = false;

const _getInputClassNames = (errorMessage, isPassword) => classNames({
  'anim clr-inherit pdg-t-20px pdg-b-5px outline-none': true,
  'mrg-0 fix-bb dsp-block brd-0 fnt-sm w-100 txt-center brd-rad-30px clr-black': true,
  'focus-brd-main focus-clr-main': true,
  'pdg-rl-20px': !isPassword,
  'brd-solid-2-grey': !errorMessage,
  'brd-solid-2-red': errorMessage,
});

const _getIconContainerClassNames = (showAsPassword) => classNames({
  'fix-bb pdg-r-1px clr-grey pos-absolute right-0 pdg-tb-15px crs-point': true,
  'hover-clr-black': !showAsPassword,
  'hover-clr-redl2': showAsPassword,
});

const _getIconClassNames = (showAsPassword) => classNames({
  'icon-view clr-black pdg-rl-10px brd-rad-50': true,
  'op-05': !showAsPassword,
  '': showAsPassword,
});

const _getStrengthClassNames = (passwordStrength) => classNames({
  'fix-bb': true,
  'bg-red col-4 fadeIn': passwordStrength === 'simple',
  'bg-yellow col-8': passwordStrength === 'medium',
  'bg-green col-12': passwordStrength === 'strong',
});

const _getLabelClassNames = (isError) => classNames({
  'clr-red': isError,
  'clr-greyd3': !isError,
});

const _customOnChange = ({onChange, onChangeDelayed, delay}, e) => {
  onChange && onChange(e);

  if(onChangeDelayed && delay){
    clearTimeout(_timer);
    _timer = setTimeout(() => onChangeDelayed(e), delay);
  } 
};

const FrontInput = (props) => {
  const {
    className, 
    errorMessage, 
    name, 
    type, 
    showAsPassword, 
    inputValue, 
    placeholder, 
    togglePasswordType, 
    trackPasswordStrength,
    passwordStrength,
    onChange, 
    onChangeDelayed,
    delay,
    onClick
  } = props;

  return <div className={className}>
    <div className="mrg-0 w-100 pos-relative">
      
      {false && !!errorMessage && 
        <div 
          onClick={onClick && ((e) => onClick(type))} 
          className="fadeIn brd-rad-t-10px pos-relative bg-redl2 clr-white col-10 mrg-auto txt-center">
            <i className="icon-close pdg-tb-3px pdg-r-10px pos-absolute right-0 cursor-pointer"/>
            <div className="txt-cap fnt-xs pdg-rl-20px pdg-tb-5px fnt-300">
              {errorMessage}
            </div>
        </div>}

      {!!togglePasswordType && 
        <div className={_getIconContainerClassNames(showAsPassword)} onClick={togglePasswordType}>
          <i className={_getIconClassNames(showAsPassword)}/>
        </div>}
      <input
        onFocus={onClick && ((e) => onClick(type))}
        name={name}
        type={type === 'password' && showAsPassword && type || 'text'}
        value={inputValue}
        className={_getInputClassNames(errorMessage, type === 'password')}
        onChange={_customOnChange.bind(this, {onChange, onChangeDelayed, delay})}
        valid
        required/>
      <label className={_getLabelClassNames(!!errorMessage)}>{errorMessage || placeholder}</label>
      
    </div>

    {type === 'password' && !!trackPasswordStrength && <div className="bg-white brd-b-greyl1 brd-rad-b-5px col-10 mrg-auto">
      <div className={_getStrengthClassNames(passwordStrength)}></div>
    </div>}
  </div>
}

FrontInput.propTypes = {
  className: React.PropTypes.string,
  placeholder: React.PropTypes.string.isRequired,
  onChange: React.PropTypes.func,
  togglePasswordType: React.PropTypes.func,
  value: React.PropTypes.string,
  type: React.PropTypes.string,
  name: React.PropTypes.string,
  showAsPassword: React.PropTypes.bool,
  passwordStrength: React.PropTypes.any,
  errorMessage: React.PropTypes.any,
  delay: React.PropTypes.number,
}

FrontInput.defaultProps = {
  placeholder: 'custom field',
  delay: 1000,
}

export default FrontInput;