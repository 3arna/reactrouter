import React from 'react';
import classNames from 'classnames';

const _getInputClassNames = (iconClassName, disabled) => classNames({
  'brd-0 outline-none dsp-block w-100 fnt-sm pdg-10px bg-none fnt-300': true,
  'pdg-l-40px': !!iconClassName,
  'op-07': disabled
});

const SettingsInput = ({iconClassName, name, placeholder, disabled, onChange, value}) => {
  return <label className="pos-relative dsp-block brd-b-grey pdg-tb-5px">
    {true && <i className={`pos-absolute top-30 left-0 fnt-lg ${iconClassName}`}/>}
    <input 
      className={_getInputClassNames(iconClassName, disabled)}
      onChange={onChange}
      defaultValue={value}
      name={name}
      placeholder={placeholder} 
      disabled={disabled}/>
  </label>
}

SettingsInput.propTypes = {
  iconClassName: React.PropTypes.string,
  disabled: React.PropTypes.bool,
  value: React.PropTypes.string,
  placeholder: React.PropTypes.string,
  onChange: React.PropTypes.func,
  name: React.PropTypes.string,
};

SettingsInput.defaultProps = {
  iconClassName: 'icon-envelope',
}


export default SettingsInput;