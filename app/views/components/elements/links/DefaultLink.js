import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';

export default ({routeName, href, children, className}) => 
  <Link
    className={`clr-inherit txt-right dsp-inline mrg-l-5px fnt-bold hover-clr-main ${className}`}
    to={routeName || href}>
      {children}
  </Link>