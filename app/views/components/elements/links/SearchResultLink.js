import React from 'react';
import classNames from 'classnames';
import { NavLink } from 'fluxible-router';

const _getContainerClassNames = (type) => classNames({
  'tb clr-blackl2 mrg-tb-5em brd-rad-25px pdg-tb-3em pdg-r-15px pdg-l-3em h-30px': true,
  'hover-clr-blackd1 crs-point': true,
  'trans-pdg': type !== 'user',
});

const _getIconClassNames = (type) => classNames({
  'clr-main mrg-r-10px fnt-sm brd-rad-50 brd-mainl2 pdg-10px': true,
  'icon-star': type === 'category',
  'icon-hash': type === 'tag',
});

const _getTextClassNames = (type) => classNames({
  'td': true,
  'w-min-50px trans-pdg': type === 'user',
});


const SearchResultLink = ({type, name, id, avatarUrl, slug, description, size, children, className}, {translate}) =>
  <div className={`valign-block last-right ${className}`}>
    <div>
      <NavLink className={_getContainerClassNames(type)} href={`/feed/${id}/${slug ? slug : ''}`}>

        {!!avatarUrl &&
          <span className="td">
            <img width={size} height={size} className="brd-rad-50 mrg-r-10px valign-bot brd-white" src={avatarUrl}/>
          </span>}

        <span className={_getTextClassNames(type)}>
          <h1 className="mrg-0 pdg-tb-2px fnt-sm">
            {type !== 'user' && <i className={_getIconClassNames(type)}/>}
            {name}
          </h1>
          {description && <span className="pdg-0 mrg-0 clr-greyd3 fnt-xs sm-dsp-none">{description}</span>}
        </span>
      </NavLink>
    </div>
    <div>
      {children}
    </div>
  </div>

SearchResultLink.propTypes = {
  name: React.PropTypes.string,
  id: React.PropTypes.string,
  avatarUrl: React.PropTypes.string,
  description: React.PropTypes.string,
  created: React.PropTypes.string,
  size: React.PropTypes.number,
};

SearchResultLink.defaultProps = {
  size: 38,
  type: 'user'
}


export default SearchResultLink;
