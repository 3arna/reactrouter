import React from 'react';
import classNames from 'classnames';
import { NavLink } from 'fluxible-router';
import { Link } from 'react-router';

const _getLinkClassNames = (className) => classNames({
  'dsp-inline fnt-xs fnt-main-reg crs-point': true,
  [className]: true,
})

const FrontLink = ({routeName, href, onClick, children, className}) => routeName || href
  ? <Link
      className={_getLinkClassNames(className)}
      to={ href || routeName }>
      { children }
    </Link>
  : <span
      className={_getLinkClassNames(className)}
      onClick={onClick}>
      { children }
    </span>

/*FrontLink.contextTypes = {
  history: React.PropTypes.object.isRequired,
  location: React.PropTypes.object.isRequired
};*/

export default FrontLink;