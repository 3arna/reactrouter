import React from 'react';
import { NavLink } from 'fluxible-router';
import classNames from 'classnames';

import SmallNotificationAlert from '../elements/alerts/SmallNotificationAlert';

class Nav extends React.Component {

  

  _getNavLinkClassNames(pageName){
    return classNames({
      'pdg-tb-15px pos-relative dsp-block clr-inherit brd-b-grey brd-t-white op-08': true,
      'fix-bb trans hover-bg-white': true,
      'pos-absolute left-0 bot-0 w-100 brd-t-grey': pageName === 'logout',
    });
  }

  _generateNavLinkJsx(childrenJsx, routeName){
    return <NavLink 
      key={routeName}
      className={this._getNavLinkClassNames(routeName)} 
      routeName={routeName} 
      activeClass="bg-white op-1 clr-main brd-l-main">
        {childrenJsx}
    </NavLink>
  }

  get navJsx(){
    
    const { 
      selected, 
      links, 
      userLoggedIn, 
      user,
      alerts
    } = this.props;

    return Object.keys(links).map((name) => {
      
      const link = links[name];
      let iconJsx;

      if(!link.onNavigation 
          || (link.onAuthenticated && !userLoggedIn) 
          || (userLoggedIn && link.onAuthenticatedHide)
          || (user && !user.specialType && !!link.onSpecialUser)){
            return true;
      }

      switch(link.page){
        
        case 'broadcastt':
          iconJsx = <img className="brd-rad-50 valign-bot" src={user.avatarUrl} height="30"/>
        break;

        case 'home':
        case 'versyStream':
          iconJsx = <img className="valign-bot" src="/img/versy-rainbow.svg" height="22"/>
        break;

        case 'notifications':
          iconJsx = <span className="dsp-inline">
            <i className={`fnt-lg dsp-block ${link.iconClass}`}/>
            {!!alerts && !!alerts.length && 
              <SmallNotificationAlert className="right-0" amount={this.props.alerts.length} />}
          </span>
        break;
        
        default:
          iconJsx = <i className={`fnt-lg dsp-block ${link.iconClass}`}/>;
        break;
      }

      if(link.page === 'versyStream' && this.props.loading){
        iconJsx = <i className="loading-spinner dsp-inline valign-bot"/>
      }

      return this._generateNavLinkJsx(iconJsx, link.page);
    });
  }

  render(){
    return <div className="mrg-0 pdg-0 h-100 txt-center fix-clear pos-relative"> { this.navJsx } </div>
  }


  static defaultProps = {
    selected: 'home',
    links: {}
  };

  static contextTypes = {
    translate: React.PropTypes.func,
  }
}

export default Nav;
