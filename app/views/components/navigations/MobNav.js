import React from 'react';
import { NavLink } from 'fluxible-router';
import classNames from 'classnames';


import BaseComponent from '../../BaseComponent';
import FullFog from '../elements/fogs/FullFog';
import FrontButton from '../elements/buttons/FrontButton';
import SmallNotificationAlert from '../elements/alerts/SmallNotificationAlert';

export default class MobNav extends BaseComponent {

  static defaultprops = {
    selected: 'home',
    links: {}
  };

  static contextTypes = {
    translate: React.PropTypes.func,
  };

  constructor(){
    super();
    this.state = {
      menuVisible: false,
    }
    this._bind('toggleMenu');
  }

  toggleMenu(){
    this.setState({menuVisible: !this.state.menuVisible});
  }

  navigateMenu(e){
    e.preventDefault();
    e.stopPropagation();
  }

  render(){
    return(
      <div className="dsp-none sm-dsp-block">
        {this.menuJsx}
        <div className="w-100 pos-fixed top-0 bg-white boxshadow-b-5 h-50px fix-bb z-10">
          { this.props.userLoggedIn &&
            <div className="tb h-50px w-100">
              <div onClick={ this.toggleMenu } className="td txt-left valign-mid pos-relative crs-point hover-clr-mainl3 clr-black">
                <i className={ this.navIconClassNames }/>
                {!!this.props.alerts && !!this.props.alerts.length &&
                  <SmallNotificationAlert className="right-0" amount={this.props.alerts.length} />}
              </div>
              <div className="td w-100 txt-center valign-mid">
                <NavLink routeName="versyStream">
                  <img src="/img/versy-rainbow.svg" style={{ width: '28px'}} />
                </NavLink>
              </div>
              <div className="td txt-right valign-mid">
                <NavLink routeName="search">
                  <i className="clr-black trans icon-search hover-clr-mainl3 fnt-lg pdg-10px"/>
                </NavLink>
              </div>
            </div>
          }
          {!this.props.userLoggedIn &&
            <div className="boxshadow valign-block">
              <NavLink routeName="home" className="pdg-rl-20px"><img src="/img/versy-rainbow.svg" style={{ width: '25px'}} /></NavLink>
              <NavLink className={this.navButtnClassNames} routeName="login" activeClass="clr-main">
                {this.context.translate('phrases.logIn')}
              </NavLink>
            </div>
          }
        </div>
      </div>
    )
  }

  get navButtnClassNames(){
    return classNames({
      'txt-center h-50px fix-bb w-100': true,
    });
  }

  get navIconClassNames(){
    return classNames({
      'clr-black fnt-lg pdg-10px icon-menu': true,
    });
  }

  get menuClassNames(){
    return classNames({
      'col-10 h-100 pos-absolute z-11 top-0 over-auto bg-white slideLeft pdg-tb-20em pdg-rl-5 fix-bb clr-black boxshadow-r-5': true,
    })
  }

  get menuBtnClassNames(){
    return classNames({
      'txt-cap dsp-block clr-black bg-white pdg-tb-15px txt-left fix-bb pos-relative': true,
      'hover-clr-main trans-pdg': true
    })
  }

  get menuJsx(){
    return(this.props.userLoggedIn && this.state.menuVisible &&
      <FullFog onClick={ this.toggleMenu } className="z-11 dsp-none sm-dsp-block">
        <div className={this.menuClassNames}>
          <NavLink routeName="broadcast" className="pdg-rl-5 mrg-auto mrg-b-20px dsp-block">
            <img
              className="brd-rad-50 col-5 w-min-50px w-max-70px"
              src={this.props.user.avatarUrl}/>
            <h2 className="mrg-0 pdg-0 mrg-tb-5em txt-cap fnt-400 fnt-lg">{ this.props.user.displayName || this.props.user.userName }</h2>
            <p className="fnt-sm clr-blackl2 mrg-tb-5em">{this.props.user.description}</p>
          </NavLink>
          <div className="brd-t-grey pdg-t-10px pdg-rl-5">
            <NavLink className={this.menuBtnClassNames} routeName="versyStream">
              <i className="icon-versy-logo valign-mid fnt-xl pdg-r-10px"/>
              <span className="valign-mid">{this.context.translate('phrases.versyStream')}</span>
            </NavLink>
            <NavLink className={this.menuBtnClassNames} routeName="broadcast">
              <i className="icon-profile valign-mid fnt-xl pdg-r-10px"/>
              <span className="valign-mid">{this.context.translate('words.profile')}</span>
            </NavLink>
            <NavLink className={this.menuBtnClassNames} routeName="notifications">
              <i className="icon-notifications valign-mid fnt-xl pdg-r-10px"/>
              <span className="valign-mid">{this.context.translate('words.notifications')}</span>
              {!!this.props.alerts && !!this.props.alerts.length &&
                  <SmallNotificationAlert className="left-0" amount={this.props.alerts.length} />}
            </NavLink>
            <NavLink className={this.menuBtnClassNames} routeName="about">
              <i className="icon-info valign-mid fnt-xl pdg-r-10px"/>
              <span className="valign-mid">{this.context.translate('words.about')}</span>
            </NavLink>
            {this.props.user && this.props.user.specialType &&
                <NavLink className={this.menuBtnClassNames} routeName="scheduler">
                <i className="icon-clock valign-mid fnt-xl pdg-r-10px"/>
                <span className="valign-mid">{this.context.translate('phrases.scheduledPosts')}</span>
              </NavLink>}
            <NavLink className={this.menuBtnClassNames} routeName="logout">
              <i className="icon-sign-in-out valign-mid fnt-xl pdg-r-10px"/>
              <span className="valign-mid">{this.context.translate('phrases.logOut')}</span>
            </NavLink>
          </div>
        </div>
      </FullFog>
    )
  }
}
