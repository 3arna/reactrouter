import React from 'react';
import { NavLink } from 'fluxible-router';
import classNames from 'classnames';

import FrontButton from '../elements/buttons/FrontButton';

const UnauthorizedNav = (props, {translate}) =>
  <div className="valign-block bg-white boxshadow-b pos-fixed z-11 txt-center clr-main pdg-r-3 pdg-l-70px sm-pdg-0">
    <h2 className="txt-nowrap fnt-main-reg dsp-inline mrg-0 pdg-rl-5px txt-right">{translate('phrases.joinVersy')}!</h2>
    <NavLink routeName="home" className="pdg-rl-5px dsp-inline pdg-tb-10px"><img src="/img/versy-rainbow.svg" style={{ width: '26px'}} /></NavLink>
    <div className="w-100 txt-left">
      <p className="md-dsp-none dsp-inline mrg-0 pdg-rl-5px">
        {translate('phrases.findTheContentYouLove')}
      </p>
    </div>
    <FrontButton className="w-100px dsp-inline clr-white" routeName="home" text={translate('words.register')}/>
  </div>

UnauthorizedNav.contextTypes = {
  translate: React.PropTypes.func,
}

export default UnauthorizedNav;
