import React from 'react';
import Portal from 'react-portal';

import BaseComponent from '../../BaseComponent';
import Toast from './Toast';

import { toastProcessClear, toastProcessSuccess } from '../../../actions/applicationActions';


export default class ToastContainer extends BaseComponent {
  constructor(){
    super();
    this._bind('handleRemoveToast');
  }

  handleRemoveToast(){
    context.executeAction(toastProcessClear);
  }

  render(){
    const { props, state } = this;

    if(!props.toastProcess.success && !props.toastProcess.error){
      return null;
    }

    false && <Portal isOpened>
    </Portal>

    
    return <Toast onClick={this.handleRemoveToast} isError={!!props.toastProcess.error}>
      {props.toastProcess.success || props.toastProcess.error}
    </Toast>

  }

  static contextTypes = {};

  static propTypes = {};
}
