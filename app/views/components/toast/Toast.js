import React from 'react';
import classNames from 'classnames';

const _getToastClassNames = (isError) => classNames({
  'pdg-rl-50px line-14 fnt-sm fix-bb pdg-tb-10px brd-rad-15px dsp-inline bg-op-07 sm-pdg-rl-10px pos-relative': true,
  '': isError,
});

const Toast = ({onClick, children, isError}) => 
  <div className="txt-center pos-absolute bot-0 left-0 right-0 mrg-auto mrg-b-20px z-12 w-max-400px clr-white sm-pdg-0">
    <span onClick={onClick} className={_getToastClassNames(isError)}>
      {false && <i className="icon-close pos-absolute right-0 top-0 fnt-md"/>}
      {children}
    </span>
  </div>

Toast.propTypes = {
  children: React.PropTypes.node,
  isError: React.PropTypes.bool,
  onClick: React.PropTypes.func,
}

Toast.defaultProps = {
  children: 'success',
}

export default Toast;