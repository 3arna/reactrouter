import classNames from 'classnames';
import React from 'react';
import { NavLink } from 'fluxible-router';

import BackButton from '../elements/buttons/BackButton';
import UserStats from './UserStats';

const _getContainerClassNames = (authenticatedUser) => classNames({
  'brd-b-greyl1 pos-fixed z-1': true,
  'pdg-tb-10em left-0 w-100 bg-white lg-pdg-r-33 md-pdg-l-60px fadeInDown': true,
  'sm-top-50px': authenticatedUser,
  'top-50px sm-pdg-top-100px': !authenticatedUser,

})

const UserFeedHeader = ({user}, {authenticatedUser}) => 
  <div className={_getContainerClassNames(authenticatedUser)}>
      <div className="w-100">
        <div className="w-max-800px pdg-rl-3 txt-left valign-block mrg-auto tb">
          <div className="td w-100">
            <img width="30" height="30" className="brd-rad-50 mrg-r-10px" src={user.avatarUrl}/>
            <h3 className="sm-dsp-none pdg-0 mrg-tb-5em dsp-inline valign-top">{user.displayName}</h3>
          </div>
          <div className="td">
            <UserStats className="dsp-inline valign-bot fnt-sm" user={user} noAvatars={true}/>
          </div>
        </div>
    </div>
  </div>

UserFeedHeader.contextTypes = {
  authenticatedUser: React.PropTypes.object
};

UserFeedHeader.PropTypes = {
  user: React.PropTypes.object
};

export default UserFeedHeader;
