import classNames from 'classnames';
import React from 'react';
import { Link } from 'react-router';

import { getOverThousandString } from '../../../utils/textFormatingUtils';

const _getFollowStatsJsx = (type, user) => {
  type = type || 'followers';

  return user[type].map((entitySummary, index) =>
    <Link key={index} to={`/feed/${entitySummary.id.replace('%3A', ':')}/${entitySummary.slug}`}>
      <img
        className="md-dsp-none brd-rad-50 bg-black pdg-1px hover-bg-blackl3 op-07 hover-op-1 mrg-l-1px dsp-inline valign-bot"
        width="35"
        height="35"
        src={entitySummary.avatarUrl}/>
    </Link>
  )
}

const _getNavLinkClassNames = () => classNames({
  'clr-inherit fnt-inherit op-08 hover-op-1 txt-center fnt-xxs fnt-second dsp-block': true,
});

const UserStats = ({ user, className, noAvatars, onGetHeaderPostion }, { translate }) =>
  <div className={className} onLoad={onGetHeaderPostion}>

    <div className="tb">
      <div className="col-6 txt-right valign-bot td brd-r-blackl1">
        {!noAvatars && <div className="dsp-inline md-dsp-none pdg-r-10px">
          {_getFollowStatsJsx('followers', user)}
        </div>}
        <div className="dsp-inline pdg-r-10px txt-right valign-bot">
          <Link
            to={`/${user.id.replace('%3A', ':')}/followers/`}
            className={_getNavLinkClassNames()}>
              <div className="clr-inherit fnt-xxl">{getOverThousandString(user.followerCount)}</div>
            {translate('words.followers')}
          </Link>
        </div>
      </div>

      <div className="td valign-bot pdg-rl-10px">
        <Link
          to={`/${user.id.replace('%3A', ':')}/interests/`}
          className={_getNavLinkClassNames()}>
            <div className="clr-inherit fnt-xxl">{user.interestCount}</div>
          {translate('words.interests')}
        </Link>
      </div>

      <div className="col-6 td txt-left brd-l-blackl1">
        <div className="dsp-inline pdg-l-10px valign-bot">
          <Link
            to={`/${user.id.replace('%3A', ':')}/following/`}
            className={_getNavLinkClassNames()}
          >
            <div className="clr-inherit fnt-xxl">{getOverThousandString(user.followingCount)}</div>
            {translate('words.following')}
          </Link>
        </div>
        {!noAvatars && <div className="dsp-inline md-dsp-none pdg-l-10px">
          {_getFollowStatsJsx('following', user)}
        </div>}
      </div>
    </div>

  </div>

UserStats.contextTypes = {
  translate: React.PropTypes.func.isRequired
}

export default UserStats;

