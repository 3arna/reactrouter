import React from 'react';
import lodash from 'lodash';
import { connectToStores, provideContext } from 'fluxible-addons-react';

import BaseComponent from '../../../BaseComponent';
import uploader from '../../../decorators/uploader';

import { toggleBookmark } from '../../../../actions/bookmarkActions';
import { toggleForm } from '../../../../actions/component/formActions';

import UserProfile from './UserProfile';

@connectToStores(['ApplicationStore'], (context, props) => ({
  process: context.getStore('ApplicationStore').processGet('updateProfile'),
  processProfileBackground: context.getStore('ApplicationStore').processGet('profileBackground'),
  processProfileAvatar: context.getStore('ApplicationStore').processGet('processProfileAvatar'),
}))

class UserProfileContainer extends BaseComponent {

  static contextTypes = {
    translate: React.PropTypes.func,
    executeAction: React.PropTypes.func,
  }

  constructor(){
    super();
    this.state = {
      headerPosition: 400,
      headerOn: false
    };
    this._bind('handleGetHeaderPosition', 'handleSetContextMenuAction');
  }

  componentWillReceiveProps(nextProps) {

    nextProps.scrollTop > this.state.headerPosition
      && !this.state.headerOn && this.setState({headerOn: true});

    (nextProps.scrollTop <= this.state.headerPosition || nextProps.scrollTop < 10)
      && this.state.headerOn && this.setState({headerOn: false});
  }

  handleToggleForm(){
    context.executeAction(toggleForm, { isContent: true });
  }

  handleToggleBookmark(data){
    context.executeAction(toggleBookmark, data);
  }

  handleSetContextMenuAction(contextMenuActionObject){
    this.setState({ contextMenuActionObject });
  }

  handleGetHeaderPosition(e){
    const pos = this.props.scrollTop + e.currentTarget.getBoundingClientRect().bottom;
    this.state.headerPosition !== pos && this.setState({headerPosition: pos});
  }

  get contextMenuItems(){
    return [
      {text: this.context.translate('phrases.blockUser'), isDangerous: true, 
        onClick: (e) => this.handleSetContextMenuAction({target: this.user, action: 'block'}) },
      {text: this.context.translate('words.report'), isDangerous: true, 
        onClick: (e) => this.handleSetContextMenuAction({target: this.user, action: 'report'}) },
      {text: this.context.translate('words.share'), 
        onClick: (e) => this.handleSetContextMenuAction({target: this.user, action: 'share'}) },
    ];
  }

  get user(){
    return !this.props.isMe && this.props.contentPage.ultimateParent
      || lodash.merge(this.props.contentPage.ultimateParent, this.props.user);
  }

  render() {
    const { props, state } = this;

    return !!props.contentPage &&
      <UserProfile
        isBookmarked={props.isBookmarked}
        user={this.user}
        isMe={props.isMe}
        headerOn={state.headerOn}
        contextMenuItems={this.contextMenuItems}
        contextMenuActionObject={state.contextMenuActionObject}

        onSetContextMenuAction={this.handleSetContextMenuAction}
        onGetHeaderPostion={this.handleGetHeaderPosition}
        onToggleBookmark={this.handleToggleBookmark}
        onToggleForm={this.handleToggleForm}
      />
  }
}

export default UserProfileContainer;
