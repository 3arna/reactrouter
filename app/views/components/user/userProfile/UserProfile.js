import classNames from 'classnames';
import React from 'react';
import { NavLink } from 'fluxible-router';
import lodash from 'lodash';

import FollowButtonContainer from '../../elements/buttons/followButton/FollowButtonContainer';
import SettingsButton from '../../elements/buttons/SettingsButton';
import MoreButton from '../../elements/buttons/MoreButton';
import EditableAvatar from '../../elements/avatars/EditableAvatar';
import ProfileBackgroundUpload from '../../elements/uploads/ProfileBackgroundUpload';
import TextEditable from '../../elements/editables/TextEditable';
import ContextMenuActionsContainer from '../../contentFeed/contentCard/ContextMenuActionsContainer';


import ContextMenuContainer from '../../elements/menus/ContextMenuContainer';

import UserStats from '../UserStats';
import UserFeedHeader from '../UserFeedHeader';

const _getProfileClassNames = (authenticatedUser) => classNames({
  'bg-cover bg-center pdg-b-100px fix-bb h-min-300px pos-relative': true,
  'pdg-t-50px': authenticatedUser,
  'pdg-t-100px': !authenticatedUser
});

const UserProfile = (props, { authenticatedUser }) => {

  const {
    contextMenuItems,
    contextMenuActionObject,
    headerOn,
    isBookmarked,
    isMe,
    onSetContextMenuAction,
    onGetHeaderPostion,
    onToggleBookmark,
    onToggleForm,
    user,
  } = props;

  return <div className="txt-center">

    <ContextMenuActionsContainer
      contextMenuActionObject={contextMenuActionObject}
      onSetContextMenuAction={onSetContextMenuAction}
    />
    {headerOn && <UserFeedHeader user={user}/>}

    <div className={_getProfileClassNames(authenticatedUser)}
      style={{ backgroundImage: `linear-gradient(rgba(0,0,0, .5), rgba(0,0,0, .5)), url('${user.backgroundUrl}')`}}>

      {authenticatedUser &&
        (!!isMe
          ? <SettingsButton routeName="settings"/>
          : <ContextMenuContainer items={contextMenuItems} className="clr-white pos-absolute right-0 top-0 mrg-30px">
              <MoreButton/>
            </ContextMenuContainer>
        )
      }


        <div className="txt-center clr-white txt-center">
          <EditableAvatar
            isPremium={!!user.specialType}
            editModeOn={false}
            avatarUrl={ user.avatarUrl }/>

          <h1 className="fnt-xl clr-white txt-center w-max-500px mrg-auto sm-col-12 fnt-norm mrg-t-10px mrg-b-5px">
            {user.displayName}
          </h1>

          {user.description &&
            <p className="txt-center clr-grey fnt-sm w-max-500px md-col-12 mrg-auto mrg-tb-5px">
              {lodash.trunc(user.description, 250)}
            </p>}

          {user.webSite &&
            <a href={user.webSite} target="_blank" className="fnt-xs hover-clr-secondl2 dsp-block mrg-t-10px clr-inherit txt-deco-none">
              {user.webSite.replace(/(http|https):\/\//, '')}
            </a>}

          {user.location &&
            <span className="fnt-xs w-max-400px mrg-t-5px md-col-12 dsp-block mrg-auto">
              {user.location}
            </span>}

          {authenticatedUser && !isMe &&
            <FollowButtonContainer
              className="mrg-t-20em"
              theme="transparent"
              isBookmarked={ isBookmarked }
              onToggleBookmark={ onToggleBookmark }
              id={ user.id }/>}

        </div>

        <UserStats
          onGetHeaderPostion={onGetHeaderPostion}
          className="bg-op-05 pos-absolute bot-0 w-100 clr-grey pdg-tb-10px pdg-rl-3 fix-bb"
          user={user}/>

    </div>

  </div>
}

UserProfile.contextTypes = {
  authenticatedUser: React.PropTypes.object,
  translate: React.PropTypes.func,
  executeAction: React.PropTypes.func,
};

export default UserProfile;

