import classNames from 'classnames';
import React from 'react';
import { NavLink } from 'fluxible-router';

import BackButton from '../../elements/buttons/BackButton';

const EditProfileHeader = ({ onSaveChanges }, { translate }) =>
  <div className="md-pdg-l-60px fnt-sm col-8 md-col-12 pos-fixed z-1 top-0 left-0">
    <div className="valign-block last-right first-left tb-fixed fix-bb pdg-tb-10px pdg-rl-3 bg-white boxshadow-b-5 fadeIn">
      <div>
        <BackButton href="/feed/user" />
      </div>
      <h3 className="h-30px">{translate('phrases.editProfile')}</h3>
      <div>
        <button
          className="brd-0 bg-none outline-none clr-main fnt-sm dsp-inline valign-bot pdg-rl-10px pdg-tb-5px crs-point hover-clr-mainl2"
          onClick={onSaveChanges}>{translate('words.save')}</button>
      </div>
    </div>
  </div>

EditProfileHeader.contextTypes = {
  translate: React.PropTypes.func.isRequired
}

export default EditProfileHeader;

