import React from 'react';
import { NavLink } from 'fluxible-router';
import classNames from 'classnames';


const UserFeedEmpty = ({isMe, authorName}, {translate}) => {
  if(!isMe){
    return <div></div>
  }
  return <div className="mrg-auto brd-t-greyl1 txt-center pdg-rl-3 pdg-t-20 clr-greyd3">
    <span className="fnt-xxl dsp-inline mrg-tb-5em">
      <i className="icon-streamplus fnt-xxl"/>
    </span>
    <div className="w-max-200px mrg-auto">
      <p className="mrg-tb-10em mrg-b-10em fnt-xs">
        {isMe && translate('phrases.youHaventPostedAnythingYet')}
      </p>
      {isMe && <p className="mrg-tb-10em fnt-xs">{translate('phrases.selectThePlusButton')}</p>}
    </div>
  </div>
}

UserFeedEmpty.contextTypes = {
  translate: React.PropTypes.func
}

export default UserFeedEmpty;
