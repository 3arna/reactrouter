import React from 'react';
import { NavLink } from 'fluxible-router';
import classNames from 'classnames';

import DefaultButton from '../../elements/buttons/DefaultButton';

const VersyFeedEmpty = ({feedFilter}, { translate }) =>
  <div className="mrg-auto brd-t-greyl1 txt-center pdg-t-20 clr-greyd3">
    <span className="fnt-xxl dsp-inline mrg-tb-5em">
      <i className="icon-streamplus fnt-xxl"/>
    </span>
    <div className="mrg-auto">
      <h3 className="mrg-tb-10em pdg-0 fnt-bold">
        {feedFilter && translate('phrases.noNotificationsYet') || translate('phrases.noPostsYet')}
      </h3>
      <p className="mrg-tb-10em mrg-b-10em fnt-xs line-15">
        {feedFilter && translate('phrases.youHaventPostedAnythingYet') || translate('phrases.hereAreSomeThingsYouMightLike')}
      </p>
      {!feedFilter && <DefaultButton className="w-100" routeName="search">{translate('phrases.goToSearch')}</DefaultButton>}
    </div>
  </div>


VersyFeedEmpty.contextTypes = {
  translate: React.PropTypes.func.isRequired
}
export default VersyFeedEmpty;
