import React from 'react';
import { connectToStores, provideContext } from 'fluxible-addons-react';

import BaseComponent from '../../../BaseComponent';
import VersyFeedEmpty from './VersyFeedEmpty';
import SmallSpinner from '../../elements/spinners/SmallSpinner';
import SearchResultLink from '../../elements/links/SearchResultLink';

@connectToStores(['SearchStore'], (context, props) => {
  return {
    trendingResults: context.getStore('SearchStore').trendingResults,
  };
})


export default class VersyFeedEmptyContainer extends BaseComponent {
  constructor(){
    super();
    this._bind();
    this.state = {}
  }

  render(){
    const { props, state } = this;

    return <div className="w-max-400px pdg-rl-3 mrg-auto">
      <VersyFeedEmpty 
          feedFilter={props.feedFilter}/>
      
      <div className="mrg-tb-20px">
        {props.trendingResults && !!props.trendingResults.length && props.trendingResults.map(( result ) => 
          <SearchResultLink 
            key={result.id}
            type={result.type}
            name={result.name}
            id={result.id}
            description={result.description}
            avatarUrl={result.avatarUrl} />)}
      </div>
    </div>
  }

  static propTypes = {
    feedFilter: React.PropTypes.string,
  }
}
