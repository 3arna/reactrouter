import React from 'react';
import { NavLink } from 'fluxible-router';
import classNames from 'classnames';


const RepliesEmpty = (props, { translate }) =>
  <div className="clr-greyd2">
    <span className="fnt-xxl dsp-inline mrg-tb-5em">
      <i className="icon-comments fnt-xxl"/>
    </span>
    <div className="">
      <h3 className="mrg-5em pdg-0 fnt-bold">{translate('phrases.noRepliesYet')}</h3>
      <p className="mrg-0 pdg-0 mrg-b-20px fnt-xs">{translate('phrases.startAConversation')}</p>
    </div>
  </div>

RepliesEmpty.contextTypes = {
  translate: React.PropTypes.func.isRequired,
}

export default RepliesEmpty;

