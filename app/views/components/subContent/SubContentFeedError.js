import { NavLink } from 'fluxible-router';
import classNames from 'classnames';

import React from 'react';

import { getSubcontentData } from '../../../actions/contentActions';

class SubContentFeedError extends React.Component {

  static contextTypes = {
    translate: React.PropTypes.func,
    executeAction: React.PropTypes.func,
  }

  reloadSubContent(selectedContentId){
    context.executeAction(getSubcontentData, {contentId: selectedContentId, needRefresh: true});
  }

  render() {
    const { selectedContentId } = this.props;

    const className = classNames({
      'bg-black pdg-tb-5px pdg-rl-20px brd-rad-20px dsp-inline clr-white fnt-sm txt-deco-none': true,
      'trans hover-clr-second cursor-pointer': true,
    });

    return (<div>
      <i className="fa fa-meh-o dsp-inline fa-3x"/>
      <h1 className="mrg-0 pdg-0">OOPS!</h1>
      <p className="mrg-0 pdg-0 mrg-b-20px">Something has gone wrong</p>
      <span onClick={this.reloadSubContent.bind(this, selectedContentId)} className={className}>
        <i className="fa fa-refresh dsp-inline fnt-sm mrg-r-5px"/> refresh
      </span>
    </div>)
  }
}

export default SubContentFeedError;
