import React from 'react';
import ReactDOM from 'react-dom';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import { connectToStores, provideContext } from 'fluxible-addons-react';
import lodash from 'lodash';

import BaseComponent from  '../../BaseComponent';

import SubContentFeedError from './SubContentFeedError';
import RepliesEmpty from '../states/RepliesEmpty';
import SubContentFeedSeperator from './SubContentFeedSeperator';
import scrollable from '../../decorators/scrollable';

import ContentCardContainer from '../contentFeed/contentCard/ContentCardContainer';

@scrollable
class SubContentFeedScroller extends BaseComponent {

  static contextTypes = {
    translate: React.PropTypes.func,
    executeAction: React.PropTypes.func,
    authenticatedUser: React.PropTypes.object,
  }

  componentDidMount(){
    if(this.props.subContentPage){
      this.startPolling();
      return this.props.onSetScrollPosition({ scrollPosition: ReactDOM.findDOMNode(this).scrollHeight, center: false, suppressScrollEvent: true});
    }
  }

  componentDidUpdate(prevProps){
    if(this.props.newContentAvailable && this._pollTimer || !this.props.subContentPage){
      clearInterval(this._pollTimer);
      this._pollTimer = null;
    }

    if(!this.props.newContentAvailable && !this._pollTimer && this.props.subContentPage){
      this.startPolling();
    }

    if(prevProps.subContentPage && this.props.subContentPage){
      if(prevProps.subContentPage.parentContentId !== this.props.subContentPage.parentContentId && !this.props.loading) {
        return this.props.onSetScrollPosition({ scrollPosition: ReactDOM.findDOMNode(this).scrollHeight, center: false, suppressScrollEvent: true});
      }

      if(prevProps.subContentPage.parentContentId === this.props.subContentPage.parentContentId){
        if(prevProps.subContentPage.pageMin > this.props.subContentPage.pageMin){
          const contentAtTop = this.refs[prevProps.subContentPage.pageMin -1];
          if(!contentAtTop){
            return;
          }
          return this.props.onSetScrollPosition({ scrollPosition: ReactDOM.findDOMNode(contentAtTop).offsetTop, center: false, suppressScrollEvent: true });
        }
        else if(prevProps.subContentPage.pageMin > this.props.subContentPage.pageMin){
          return this.props.onSetScrollPosition({ scrollPosition: ReactDOM.findDOMNode(this).scrollHeight, center: false, suppressScrollEvent: true});
        }
        else if(prevProps.subContentPage.pageMax < this.props.subContentPage.pageMax){
          return this.props.onSetScrollPosition({ scrollPosition: ReactDOM.findDOMNode(this).scrollHeight, center: false, suppressScrollEvent: true});
        }
      }
    }
  }

  componentWillUnmount(){
    if(this._pollTimer){
      clearInterval(this._pollTimer);
      this._pollTimer = null;
    }
  }

  startPolling(){
    if(this._pollTimer || typeof this.props.onCheckForNewContent !== 'function'){
      return;
    }
    //console.log('start subContent poll');

    this._pollTimer = setInterval(() => {
      if(this.props.newContentAvailable){
        return;
      }

      this.props.onCheckForNewContent(this.props.subContentPage && this.props.subContentPage.parentContentId, this.props.subContentPage.pollingURI, true);
    }, 30000);
  }

  render(){
    const props = this.props;
    const translate = this.context.translate;

    if(props.loading){
      return <div className="txt-center tb w-100 h-100vh pdg-rl-40px fix-bb">
        <div className="td clr-greyd1">
          <i className="loading-spinner dsp-inline"/>
        </div>
      </div>
    }

    if(props.subContentError){
      return <div className="txt-center tb w-100 h-100vh pdg-rl-40px fix-bb">
        <div className="td clr-greyd1">
          <SubContentFeedError selectedContentId={props.selectedContentId}/>
        </div>
      </div>
    }

    let subContentJsx = null;
    let joinConversationJsx = null;

    if(props.subContentPage && !props.subContentPage.content.length){
      return <div className="over-auto h-100vh">
        <div key="subContentMessage" className="valign-block txt-center w-100 h-100vh pdg-rl-40px fix-bb">
          <RepliesEmpty/>
        </div>
        {props.newContentAvailable &&
          <div className="pos-absolute txt-center left-0 right-0 mrg-auto" style={{ bottom: 125, zIndex: 999 }}>
            <button
              onClick={props.onLoadNewContent}
              className="fadeInUp clr-main brd-greyd1 pos-relative bg-white pdg-rl-10px pdg-tb-5px brd-rad-15px">
                <i className="icon-down trans fnt-lg"></i>
                <span className="valign-top pdg-l-5px">{translate('phrases.newReplies')}</span>
            </button>
          </div>
        }
      </div>
    }


    if(props.subContentPage && props.subContentPage.content.length) {
      subContentJsx = props.subContentPage.content.map((article, index) => {

        if(article && article.redacted && !article.childCount){
          return false;
        }

        return (<div key={article.id}>
          {false && <SubContentFeedSeperator
            currentDate={article.created}
            previousDate={lodash.get(props.subContentPage.content[index+1], 'created')}
          />}
          <ContentCardContainer
            bookmarks={props.bookmarks}
            content={article}
            ref={article.sequence}
            parentId={props.subContentPage.parentContentId}
            parentFeedId={props.parentFeedId}
            contentContextMenuVisible={props.contextMenuVisible && props.contextMenuActiveContentId === article.id}
            isReply={true}
          />
        </div>)
      }).reverse();
    }


    return (
      <div className="over-auto scrollbar-none h-100vh">
        <div className={`pdg-rl-20px ${!this.context.authenticatedUser ? 'pdg-t-50px' : 'pdg-b-100px'}`}>

          {props.loadingMore &&
            <div className="txt-center pdg-t-20px">
              <i className="loading-spinner dsp-inline"/>
            </div>}

          {subContentJsx}
        </div>

        {props.newContentAvailable &&
          <div className="pos-absolute txt-center left-0 right-0 mrg-auto" style={{ bottom: 125, zIndex: 999 }}>
            <button
              onClick={props.onLoadNewContent}
              className="fadeInUp clr-main brd-greyd1 pos-relative bg-white pdg-rl-10px pdg-tb-5px brd-rad-15px">
                <i className="icon-down trans fnt-lg"></i>
                <span className="valign-top pdg-l-5px">{translate('phrases.newReplies')}</span>
            </button>
          </div>
        }
      </div>
    )
  }
}

export default SubContentFeedScroller;
