import React from 'react';
import lodash from 'lodash';
import moment from 'moment';
import classNames from 'classnames';

import { translatedTimeAgo } from '../../../utils/dateUtils';

export default class ContentDateSeperator extends React.Component {

  static contextTypes = {
    getLanguage: React.PropTypes.func
  }

  render(){

    if(moment(this.props.previousDate).fromNow() === moment(this.props.currentDate).fromNow()){
      return false;
    }
      
    const lineClassName = classNames({
      'brd-0': true,
      'brd-t-greyl1': !this.props.parent,
      'brd-t-blackl2': this.props.parent,
    });

    const dateClassName = classNames({
      'td txt-nowrap pdg-rl-20px fnt-sm': true,
      'clr-greyd3': !this.props.parent,
      'clr-blackl3': this.props.parent,
    });

    return (<div className="tb w-100 mrg-t-20px pdg-l-60px pdg-r-10px fix-bb">
      <div className="td col-6"><hr className={lineClassName}/></div>
      <div className={dateClassName}>{translatedTimeAgo(this.props.currentDate, this.context.getLanguage())}</div>
      <div className="td col-6"><hr className={lineClassName}/></div>
    </div>);
  }
}
