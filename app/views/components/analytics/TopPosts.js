import React from 'react';
import lodash from 'lodash';
import { NavLink } from 'fluxible-router';
import classNames from 'classnames';

const TopPosts = (props) => {
  return <div className="bg-white mrg-10px over-hide brd-greyl1 brd-rad-5px">
    <div className="mrg-20px">
      <h3>{props.title}</h3>
      <table className="w-100 ">
        <tbody>
        {props.posts.map((post, index) => {
          const trClass = classNames({
            'bg-greyl1': index % 2 === 0,
          });

          return <tr key={index} className={trClass}>
            <td className="pdg-5px">{index + 1}</td>
            <td className="pdg-5px"><NavLink className="clr-second txt-under" href={`/content/${post.id}`}>{lodash.trunc(post.title || post.id, 30, '...')}</NavLink></td>
            <td className="txt-right pdg-5px">{post.value} {props.countLabel}</td>
          </tr>
        })}
        </tbody>
      </table>
    </div>
  </div>
};

export default TopPosts;
