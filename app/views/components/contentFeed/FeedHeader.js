import classNames from 'classnames';
import React from 'react';
import { NavLink } from 'fluxible-router';

import FollowButtonContainer from '../elements/buttons/followButton/FollowButtonContainer';

const FeedHeader = ({isBookmarked, onToggleBookmark, id}, {authenticatedUser, translate}) => {

  return <div className="brd-b-greyl1 top-0 sm-top-50px pos-fixed z-2 pdg-tb-10em left-0 w-100 bg-white lg-pdg-r-33 md-pdg-l-60px fadeInDown">
      <div className="w-100">
        <div className="w-max-800px pdg-rl-3 txt-left valign-block mrg-auto tb">
          <div className="td w-100">
            <p className="mrg-0 fnt-xs">{translate('phrases.postsAbout')}</p>
            <h3 className="mrg-0 txt-cap">{id.split(':').pop()}</h3>
          </div>
          {authenticatedUser && id.split(':').shift() !== 'tag' && <div className="td">
            <FollowButtonContainer
              className="fnt-xs pdg-tb-5px"
              isBookmarked={ isBookmarked }
              onToggleBookmark={ onToggleBookmark }
              id={ id }/>
          </div>}
        </div>
    </div>
  </div>
};

FeedHeader.contextTypes = {
  authenticatedUser: React.PropTypes.object,
  translate: React.PropTypes.func,
};

FeedHeader.PropTypes = {
  isBookmarked: React.PropTypes.bool,
  onToggleBookmark: React.PropTypes.func,
  id: React.PropTypes.string,
};

export default FeedHeader;
