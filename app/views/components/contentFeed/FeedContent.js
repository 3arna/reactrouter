import classNames from 'classnames';
import lodash from 'lodash';
import React from 'react';

import { translatedFromNow } from '../../../utils/dateUtils';
import { getFirstVisibleIndex } from '../../../utils/contentUtils';

import BaseComponent from '../../BaseComponent';
import ContentCardContainer from './contentCard/ContentCardContainer';
import ContentFeedEmpty from './ContentFeedEmpty';
import ContentFeedSeperator from './ContentFeedSeperator';
import SmallSpinner from '../elements/spinners/SmallSpinner';
import FeedHeader from './FeedHeader';
import UserFeedEmpty from '../states/UserFeedEmpty';
import VersyFeedEmptyContainer from '../states/versyFeedEmpty/VersyFeedEmptyContainer';

const containerClassNames = (props) => classNames({
  'mrg-auto pdg-rl-20px sm-pdg-rl-10px w-max-800px fix-bb': true,
  'events-none': props.scrolling,
  'pdg-t-50px': props.isBookmarkableFeed,
});

const _noChildrenRedacted = (content) => {
  return content && (content.redacted || content.shareInfo && content.shareInfo.redacted) && !content.childCount; 
}

const getSeperatorDate = (currentArticle, previousArticle, language) => {
  if(!currentArticle){
    return false;
  }

  const previous = previousArticle
    ? translatedFromNow(previousArticle.isNotification
      ? previousArticle.feedContent.created : previousArticle.created, language)
    : false;
  const current =  translatedFromNow(currentArticle.isNotification ? currentArticle.feedContent.created : currentArticle.created, language);

  return current !== previous && current;
};

const FeedContent = (props, { getLanguage }) => {
  if(!props.contentPage.content || !props.contentPage.content.length){
    return props.contentPage.isProfile
      ? <UserFeedEmpty
          isMe={ props.user && props.user.id === props.contentPage.parentContentId }
          authorName={props.contentPage.ultimateParent.displayName}/>
      : <VersyFeedEmptyContainer feedFilter={props.contentPage.filter}/>
  }

  const containerStyle = {
    paddingBottom: props.contentPage.pageMin < 5
      ? 70 + 'vh'
      : 70 + 'vh'
  };

  return <div className={containerClassNames(props)} style={containerStyle}>
    <a id="feedTop"/>

    {props.isBookmarkableFeed &&
      <FeedHeader
        onToggleBookmark={props.onToggleBookmark}
        isBookmarked={props.isBookmarked}
        id={props.contentPage.parentContentId}/>}

    {props.contentPage.content.map((content, index) => {
      const seperatorDate = getSeperatorDate(content, props.contentPage.content[index-1], getLanguage());

      if(_noChildrenRedacted(content)){
        return false;
      }

      return <div key={index}>
        {seperatorDate &&
          <ContentFeedSeperator
            key={index + 'seperator'}
            parent={!!props.contentPage.parent}
            date={seperatorDate}
          />
        }

          <ContentCardContainer
            key={content.id + 'content'}
            content={content}
            parentId={props.contentPage.parentContentId}
            isMe={props.isMe}
            bookmarks={props.bookmarks}
            isOnFeed={true}
            isInViewport={(!props.scrollData.contentInViewport && props.feedScrollPosition && props.feedScrollPosition.contentInViewport &&
              props.feedScrollPosition.contentInViewport.id === content.id) ||
              props.scrollData.contentInViewport && props.scrollData.contentInViewport.id === content.id ||
              (index === 0 && props.scrollTop === 0) }
            onAcceptPrivateConversationInvite={ props.onAcceptPrivateConversationInvite }
            onContentScrolled={props.onContentEnteredViewport}
            onTogglePrivacySelector={props.onTogglePrivacySelector}
            scrollPos={props.scrollTop}
            onSetContextMenuAction={props.onSetContextMenuAction}
            user={props.user}
          />
        </div>
      })
    }

    { props.isLoadingMore && <SmallSpinner /> }
  </div>
};

FeedContent.contextTypes = {
  getLanguage: React.PropTypes.func
};

FeedContent.propTypes = {
  bookmarks: React.PropTypes.array,
  contentPage: React.PropTypes.object.isRequired,
  isMe: React.PropTypes.bool,
  isBookmarkableFeed: React.PropTypes.bool,
  isBookmarked: React.PropTypes.bool,
  onContentEnteredViewport: React.PropTypes.func.isRequired,
  onSetScrollPosition: React.PropTypes.func.isRequired,
  onToggleBookmark: React.PropTypes.func,
  subContentPage: React.PropTypes.object,
  scrollTop: React.PropTypes.number.isRequired,
  scrolling: React.PropTypes.bool.isRequired,
  scrollData: React.PropTypes.object,
  shareContentId: React.PropTypes.string,
  user: React.PropTypes.object,
};

export default FeedContent;
