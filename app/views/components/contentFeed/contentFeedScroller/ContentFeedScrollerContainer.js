import lodash from 'lodash';
import React from 'react';
import ReactDom from 'react-dom';

import { toggleBookmark } from '../../../../actions/bookmarkActions';
import { toggleForm } from '../../../../actions/component/formActions';

import privacySelectorActions from '../../../../actions/component/privacySelectorActions';
import scrollable from '../../../decorators/scrollable';

import BaseComponent from '../../../BaseComponent';
import ContentFeedScroller from './ContentFeedScroller';

@scrollable
class ContentFeedScrollerContainer extends BaseComponent {
  static contextTypes = {
    apiConfig: React.PropTypes.object,
    translate: React.PropTypes.func
  };

  constructor(props, context){
    super(props, context);

    this._bind(
      'checkForNewContent',
      'handleEnterViewport',
      'handleLoadNewContent',
      'handleSetScrollPosition',
      'handleToggleForm',
      'handleToggleSettingsPage',
      'startPolling'
    );
    this.state = {settingsPageOn: false};
  }

  componentDidMount(){
    if(this.props.contentPage){
      this.startPolling();
    }
  }

  componentWillUnmount(){
    if(this._pollTimer){
      clearInterval(this._pollTimer);
      this._pollTimer = null;
    }
  }

  componentDidUpdate(prevProps){

    //console.log(this.props);

    const { contentPage, selectedContent, feedScrollPosition } = this.props;
    const contentPageChanged = prevProps.contentPage !== contentPage;

    const contentClosed = prevProps.selectedContent !== selectedContent && !selectedContent;

    if(this.props.newContentAvailable && this._pollTimer){
      clearInterval(this._pollTimer);
      this._pollTimer = null;
    }

      if(!this.props.newContentAvailable && !this._pollTimer && this.props.contentPage){
        this.startPolling();
      }

    if(contentPageChanged){
      this.props.onSetScrollData({contentInViewport: null});
    }

    if(contentPage && feedScrollPosition && !contentPage.parent){
      const shouldSetScrollPosition = contentPageChanged || contentClosed;

      if(shouldSetScrollPosition){
        this.props.onSetScrollPosition({ scrollPosition: feedScrollPosition.scrollPosition, center: false, supressScrollEvent: true });
      }
    }
  }

  handleLoadNewContent(e){
    this.props.onSetScrollData({ contentInViewport: null })
    this.props.onLoadNewContent(e);
    this.props.onSmoothScrollToTop();
  }

  handleToggleBookmark(data){
    context.executeAction(toggleBookmark, data);
  }

  handleEnterViewport(contentInViewport){
    this.props.onSetScrollData({ contentInViewport });
  }

  handleSetScrollPosition(scrollTop){
    this.props.onSetScrollPosition({ scrollPosition: scrollTop, center: true, supressScrollEvent: true });
  }

  handleToggleForm(){
    const props = this.props;

    let defaultCategories;

    if(props.contentPage && lodash.startsWith(props.contentPage.parentContentId, 'category')){
      defaultCategories = [props.contentPage.parentContentId.replace('category:', '')];
    }

    context.executeAction(toggleForm, { isContent: true, defaultCategories });
  }

  handleTogglePrivacySelector(contentId, isMine){
    context.executeAction(privacySelectorActions.toggleOpen, {
      contentId: contentId,
      isPrivate: true,
      isReadOnly: !isMine
    });
  }

  handleToggleSettingsPage(value){
    this.setState({settingsPageOn: value});
  }

  startPolling(){
    if(this._pollTimer || typeof this.props.onCheckForNewContent !== 'function'){
      return;
    }

    const pollInterval = this.context.apiConfig.contentFeedPollFrequency;

    this._pollTimer = setInterval(() => {
      if(this.props.newContentAvailable){
        return;
      }

      this.checkForNewContent();
    }, pollInterval);
  }

  checkForNewContent(){
    this.props.onCheckForNewContent(this.props.contentPage.parentContentId, this.props.contentPage.pollingURI, false);
  }

  get isBookMarked(){
    const { contentPage, bookmarks } = this.props;

    if(!contentPage){
      return false;
    }

    if(contentPage.parentContentId){
      return !!lodash.some(bookmarks, { id: contentPage.parentContentId });
    }

    if(contentPage.ultimateParent){
      return !!lodash.some(bookmarks, { id: contentPage.ultimateParent.id });
    }

    return false;
  }

  get isBookmarkableFeed(){
    const { contentPage } = this.props;

    return contentPage && contentPage.parentContentId
      && lodash.contains(['category', 'tag'], contentPage.parentContentId.split(':').shift());
  }

  get isLoading(){
    const props = this.props;
    return props.loading ||
      !props.contentPage ||
      (props.contentPage &&
      props.contentPage.parentContentId != props.parentId &&
      !(props.user && props.parentId === 'user' && props.contentPage.parentContentId === props.user.id))
  }

  get isMe(){
    const { user, contentPage } = this.props;

    return user && contentPage && contentPage.ultimateParent && contentPage.ultimateParent.id === user.id
  }

  get settingsContextMenuVisible(){
    return this.isMe && this.props.contextMenuVisible && this.props.contextMenuActiveType === 'settings';
  }

  get postNewContentButtonVisible(){
    const props = this.props;
    if(props.contentPage && (!props.contentPage.isProfile || this.isMe)){
      return true;
    }
  }

  render(){
    const props = this.props;

    return <ContentFeedScroller
      bookmarks={ props.bookmarks }
      contentFormVisible={ props.contentFormVisible }
      contentPage={ props.contentPage }
      feedScrollPosition={ props.feedScrollPosition }
      isBookmarked={ this.isBookMarked }
      isBookmarkableFeed={ this.isBookmarkableFeed }
      isMe={ this.isMe }
      isLoading={this.isLoading}
      isLoadingMore={ props.isLoadingMore }
      newContentAvailable={ props.newContentAvailable }
      onAcceptPrivateConversationInvite={ props.onAcceptPrivateConversationInvite }
      onContentEnteredViewport={ this.handleEnterViewport }
      onSetScrollPosition={ this.handleSetScrollPosition }
      onCheckForNewContent={ props.onCheckForNewContent }
      onLoadNewContent={ this.handleLoadNewContent }
      onToggleForm={ this.handleToggleForm }
      onTogglePrivacySelector={ this.handleTogglePrivacySelector }
      onSetContextAction={ props.onSetContextAction }
      onToggleBookmark={ this.handleToggleBookmark }
      postNewContentButtonVisible={ this.postNewContentButtonVisible }
      previousFeedContentPage={ props.previousFeedContentPage }
      scrolling={ props.scrolling }
      scrollData={ props.scrollData }
      scrollTop={ props.scrollTop }
      selectedContent={ props.selectedContent }
      selectedContentId={ props.selectedContentId }
      shareContentId={ props.shareContentId }
      user={ props.user }
    />
  }

  static propTypes = {
    bookmarks: React.PropTypes.array.isRequired,
    contentPage: React.PropTypes.object,
    contentFormVisible: React.PropTypes.bool.isRequired,
    feedScrollPosition: React.PropTypes.object,
    previousFeedContentPage: React.PropTypes.object,
    scrolling: React.PropTypes.bool.isRequired,
    scrollTop: React.PropTypes.number.isRequired,
    selectedContent: React.PropTypes.object,
    selectedContentId: React.PropTypes.string,
    shareContentId: React.PropTypes.string,
    subContentPage: React.PropTypes.object,
    user: React.PropTypes.object,
    contextMenuVisible: React.PropTypes.bool,
    contextMenuActiveContentId: React.PropTypes.string,
    contextMenuActiveType: React.PropTypes.string,
  }
}

export default ContentFeedScrollerContainer;
