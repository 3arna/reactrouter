import React from 'react';
import ReactDom from 'react-dom';
import Portal from 'react-portal';
import classNames from 'classnames';
import lodash from 'lodash';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import Category from '../../../../models/Category';
import ContentPage from '../../../../models/ContentPage';
import User from '../../../../models/ContentPage';

import PrivacySelectorContainer from '../../selectors/privacySelector/PrivacySelectorContainer';
import UserProfileContainer from '../../user/userProfile/UserProfileContainer';
import FeedContent from '../FeedContent';
import RoundPostButton from '../../elements/buttons/RoundPostButton';


const ContentFeedScroller = (props, { translate }) => {
  if(props.isLoading){
    return <div className="txt-center pos-relative w-100 h-100">
      <i className= "pos-absolute top-45 loading-spinner w-min-60px h-min-60px left-0 right-0 mrg-auto"/>
    </div>
  }

  return <div className="scrollable pos-relative">
      <PrivacySelectorContainer
        recipientLimit={ 25 }
      />

    {props.newContentAvailable &&
      <div
        onClick={props.onLoadNewContent}
        className="pos-fixed txt-center left-0 right-0 mrg-auto md-pdg-l-60px lg-pdg-r-33 sm-pdg-t-50px"
        style={{ top: 10, zIndex: 999 }}>
          <button
            className="fadeInDown clr-main brd-greyd1 pos-relative bg-white pdg-tb-5px outline-none crs-point pdg-rl-10px brd-rad-15px">
              <i className="icon-up trans fnt-lg"/>
              <span className="valign-top pdg-l-5px">{translate('phrases.newContentAvailable')}</span>
          </button>
      </div>
    }


    {props.contentPage.isProfile &&
      <UserProfileContainer
        user={ props.user }
        contentPage={ props.contentPage }
        isBookmarked={ props.isBookmarked }
        isMe={ props.isMe }
        scrollTop={ props.scrollTop }
        onSetContextMenuAction={ props.onSetContextMenuAction }/>}

    {props.postNewContentButtonVisible &&
      <RoundPostButton onClick={props.onToggleForm}/>
    }

    <FeedContent
      bookmarks={props.bookmarks}
      contentPage={props.contentPage}
      feedScrollPosition={props.feedScrollPosition}
      isMe={props.isMe}
      isBookmarked={props.isBookmarked}
      isBookmarkableFeed={props.isBookmarkableFeed}
      isLoadingMore={props.isLoadingMore}
      onAcceptPrivateConversationInvite={ props.onAcceptPrivateConversationInvite }
      onContentEnteredViewport={props.onContentEnteredViewport}
      onSetContextAction={props.onSetContextAction}
      onSetScrollPosition={props.onSetScrollPosition}
      onTogglePrivacySelector={props.onTogglePrivacySelector}
      onToggleBookmark={ props.onToggleBookmark }
      scrolling={props.scrolling}
      scrollTop={props.scrollTop}
      scrollData={props.scrollData}
      selectedContent={props.selectedContent}
      selectedContentId={props.selectedContentId}
      shareContentId={props.shareContentId}
      user={props.user}/>

  </div>
};

ContentFeedScroller.contextTypes = {
  translate: React.PropTypes.func
}

export default ContentFeedScroller;
