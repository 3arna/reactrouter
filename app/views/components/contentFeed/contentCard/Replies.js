import React from 'react';
import lodash from 'lodash';

import ContentCardContainer from './ContentCardContainer';
import SmallSpinner from '../../elements/spinners/SmallSpinner';

const _noChildrenRedacted = (content) => {
  return content && (content.redacted || content.shareInfo && content.shareInfo.redacted) && !content.childCount; 
}

const Replies = (props, { apiConfig, translate }) =>
  <div className="content-card__replies">

    { props.isLoadingOlderReplies
      ? <SmallSpinner/>
      : props.replies && props.replies.length >= apiConfig.numberOfItemsPerPage &&
        <button className="content-card__replies-older-button" onClick={props.onLoadOlderReplies}>
          {translate('phrases.viewOlderReplies')}
        </button>
    }

    {
      props.replies.map((reply) => {
        
        if(_noChildrenRedacted(reply)){
          return false;
        }

        return <ContentCardContainer
          bookmarks={props.bookmarks}
          content={reply}
          key={reply.id}
          parentId={props.content.id}
          isReply={true}
          isInViewport={true}
        />
      }).reverse()
    }
  </div>

Replies.contextTypes = {
  apiConfig: React.PropTypes.object.isRequired,
  translate: React.PropTypes.func.isRequired
};

Replies.propTypes = {
  content: React.PropTypes.object,
  onLoadOlderReplies: React.PropTypes.func,
  replies: React.PropTypes.array,
  showLoadOlderRepliesButton: React.PropTypes.bool
};

export default Replies;
