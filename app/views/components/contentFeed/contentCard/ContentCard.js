import React from 'react';
import moment from 'moment';
import classNames from 'classnames';
import lodash from 'lodash';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';


import { translatedFromNow } from '../../../../utils/dateUtils';
import Author from './Author';
import MoreButton from '../../elements/buttons/MoreButton';
import BackButton from '../../elements/buttons/BackButton';
import ContextMenuContainer from '../../elements/menus/ContextMenuContainer';
import ContextMenuActionsContainer from './ContextMenuActionsContainer';

import Title from './Title';
import Media from '../../media/Media';
import Text from './Text';
import Replies from './Replies';
import Content from './Content';
import NotificationContainer from './notification/NotificationContainer';

import ContentCategory from '../../elements/categories/ContentCategory';
import UpNDownVoteIndicator from '../../elements/indicators/UpNDownVoteIndicator';
import RepliesIndicatorContainer from '../../elements/indicators/repliesIndicator/RepliesIndicatorContainer';
import FollowButtonContainer from '../../elements/buttons/followButton/FollowButtonContainer';
import RepliesEmpty from '../../states/RepliesEmpty';

const _isTaskUneditable = (task) => {
  return task && moment().diff(task.fireDate, 'minutes') > -5;
}

const _isRedacted = (content) => {
  return content.redacted || content.shareInfo && content.shareInfo.redacted;
}

const _getContentWrapperClassNames = (isExtended, isOnFeed, authenticatedUser) => classNames({
  'content-card__wrapper slideLeft': isExtended && !isOnFeed,
  'pdg-t-100px': !authenticatedUser && isExtended,
  'pdg-t-50px': authenticatedUser && isExtended,
});

const _getContentCardClassNames = (isInViewport, isExtended, isReply, isOnFeed) => classNames({
  'content-card': true,
  'content-card--transparent': !isInViewport && !isReply && isOnFeed,
  'content-card--reply': isReply,
  'content-card--extended': isExtended && !isReply && !isOnFeed,
  'hack': !isExtended,
});

const _getContentHeaderClassNames = (authenticatedUser) => classNames({
  'top-50px': !authenticatedUser,
  'top-0 sm-top-50px': authenticatedUser,
  'content-card__wrapper-header': true,
});

const ContentCard = (props, {authenticatedUser, translate, getLanguage}) => {

  return <div className={_getContentWrapperClassNames(props.isExtended, props.isOnFeed, authenticatedUser, _isTaskUneditable)}>
    <ContextMenuActionsContainer
      contextMenuActionObject={props.contextMenuActionObject}
      onSetContextMenuAction={props.onSetContextMenuAction}/>

    {props.isExtended && props.backHref &&
      <div className={_getContentHeaderClassNames(authenticatedUser)}>
        <BackButton href={props.backHref} />
        {authenticatedUser &&
          <FollowButtonContainer
            className="flt-right fnt-xs pdg-tb-5px"
            isBookmarked={ props.isBookmarked }
            onToggleBookmark={ props.onToggleBookmark }
            id={ props.content.id }
          />
        }
      </div>
    }

    <article
      className={_getContentCardClassNames(props.isInViewport, props.isExtended, props.isReply, props.isOnFeed)}
      onClick={!props.isExtended && props.onContentClick}>

      {_isTaskUneditable(props.task) &&
        <div className="pos-absolute z-1 txt-center bg-white-op-07 w-100 h-100">
          <div className="valign-block h-100">
            <div>
              <i className="icon-clock fnt-xl top-40 pos-relative"/><br/>
              <span>{translate('words.processing')}</span>
            </div>
          </div>
        </div>}

      <ReactCSSTransitionGroup
        transitionName="opacity"
        transitionEnter={false}
        transitionEnterTimeout={0}
        transitionLeaveTimeout={300}>

        {props.isOnFeed && props.notificationType &&
          <NotificationContainer
            key="notification-header"
            notificationType={ props.notificationType }
            notificationData={ props.notificationData }
          />
        }

        {props.content.isTemp &&
          <div key="uploading" className="content-notification valign-block last-max">
            <i className="icon-upload"/>
            <small>{translate('words.uploading')}...</small>
          </div>
        }
      </ReactCSSTransitionGroup>

      <header className="valign-block first-max last-top pos-relative">
        {props.content.author &&
          <Author
            author={props.content.shareInfo && props.content.shareInfo.sharer || props.content.author}
            created={translatedFromNow(
              props.task && props.task.fireDate || props.content.shareInfo && props.content.shareInfo.dateShared || props.content.created,
              getLanguage())
            }
            bookmarks={props.bookmarks}
            isReply={props.isReply}/>}
        {!props.isReply

          ? !props.content.isTemp && authenticatedUser && !props.notificationType
            && !_isTaskUneditable(props.task) && !_isRedacted(props.content) &&
              <ContextMenuContainer items={props.contextMenuItems} article={props.content}>
                <MoreButton/>
              </ContextMenuContainer>

          : !props.content.isTemp && <RepliesIndicatorContainer
              contentId={props.content.id}
              repliesAmount={props.content.childCount}
              repliesUrl={`/content/${props.content.id}${props.content.slug ? `/${props.content.slug}` : ''}`}
            />
        }
      </header>

      <Content {...props}/>

      {!props.isReply &&
        <footer className="valign-block first-max last-bot">
          <div>
            {props.content.categoryIds.map((categoryId, index) =>
              <ContentCategory
                key={index}
                category={categoryId}
                active={!!lodash.find(props.bookmarks, {id: categoryId})}/>
            )}
          </div>

          {!props.content.isTemp && !props.showJoinButton && !props.task && (
            props.isExtended
                ? <div className="valign-block nowrap">
                    <UpNDownVoteIndicator
                      isActive={props.content.hasLiked}
                      onClick={authenticatedUser && props.onLikeClick }
                      votes={props.content.likeCount}
                      isLikes={true}/>
                    <UpNDownVoteIndicator
                      isActive={props.content.hasDisliked}
                      onClick={authenticatedUser && props.onDislikeClick }
                      votes={props.content.dislikeCount}/>
                  </div>
                : <RepliesIndicatorContainer
                    contentId={props.content.id}
                    isPrivate={props.content.isPrivate}
                    repliesAmount={props.content.childCount}
                    repliesUrl={`/content/${props.content.id}${props.content.slug ? `/${props.content.slug}` : ''}`}
                  />
            )
          }

          {props.showJoinButton &&
            <div className="valign-block nowrap">
              <a onClick={ props.onAcceptInvite }>
                <small className="fnt-bold">{translate('words.join')}</small> </a>
              <a onClick={ props.onAcceptInvite }><i className="icon-chevron-forward pdg-5px fix-bb mrg-l-5px"/></a>
            </div>
          }

        </footer>
      }
    </article>
    {props.replies && !!props.replies.length
      ? <Replies
          content={props.content}
          replies={props.replies}
          onLoadOlderReplies={props.onLoadOlderReplies}
          isLoadingOlderReplies={props.isLoadingOlderReplies}
          showLoadOlderRepliesButton={props.showLoadOlderRepliesButton}
          bookmarks={props.bookmarks}/>
      : !props.isReply && props.isExtended &&
          <div className="mrg-auto brd-t-greyl1 txt-center pdg-rl-3 pdg-t-10 pdg-b-20 md-dsp-block dsp-none">
            <RepliesEmpty/>
          </div>
    }
  </div>
};

ContentCard.contextTypes = {
  authenticatedUser: React.PropTypes.object,
  getLanguage: React.PropTypes.func.isRequired,
  translate: React.PropTypes.func.isRequired
};

ContentCard.propTypes = {
  content: React.PropTypes.object,
  contentContextMenuVisible: React.PropTypes.bool,
  isExtended: React.PropTypes.bool,
  isInViewport: React.PropTypes.bool,
  isReply: React.PropTypes.bool,
  isOnFeed: React.PropTypes.bool,
  isTruncated: React.PropTypes.bool,
  maxTextLength: React.PropTypes.number,
  maxTitleLength: React.PropTypes.number,
  needTruncation: React.PropTypes.bool,
  onContentClick: React.PropTypes.func,
  onLoadOlderReplies: React.PropTypes.func,
  onTruncationClick: React.PropTypes.func,
  replies: React.PropTypes.array,
  task: React.PropTypes.object,
};

export default ContentCard;
