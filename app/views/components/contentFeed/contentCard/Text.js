import React from 'react';
import Linkify from '../../elements/links/Linkify';
import { NavLink } from 'fluxible-router';
import lodash from 'lodash';
import classNames from 'classnames';

import { generateHypertext } from '../../../../utils/textFormatingUtils';


const _getTextClassNames = (text) => {
  const longestWordLenght = text && lodash.max(text.split(' '), (word) => word.length).length;
  return classNames({
    'content-card__text': true,
    'txt-break': false && text && longestWordLenght && longestWordLenght > 50,
  });
}

const _getTruncatedText = (text, maxTextLength, isTruncated) =>
  isTruncated && lodash.trunc(text, maxTextLength) || text;

const _getMoreOrLessJsx = (isTruncated, onTruncationClick) =>
  <b className="cursor-pointer" onClick={onTruncationClick}>{isTruncated ? 'See More' : 'See Less'}</b>

const _getTransformedText = (text, tags, urls) =>
  ((tags && tags.length) || (urls && urls.length)) && generateHypertext(text, tags, urls) || text;



const Text = ({text, tags, urls, isTruncated, needTruncation, onTruncationClick, maxTextLength}, {translate}) =>
  <p className={_getTextClassNames(text)}>
    <Linkify component={NavLink} properties={{ className: 'content-link'}}>
      {_getTruncatedText(text, maxTextLength, isTruncated)}
    </Linkify>
    {needTruncation && _getMoreOrLessJsx(isTruncated, onTruncationClick)}
  </p>

Text.propTypes = {
  title: React.PropTypes.string,
  tags: React.PropTypes.array,
  urls: React.PropTypes.array,
  isTruncated: React.PropTypes.bool,
  needTruncation: React.PropTypes.bool,
  maxTextLength: React.PropTypes.number,
  onTruncationClick: React.PropTypes.oneOfType([
    React.PropTypes.func,
    React.PropTypes.bool,
  ]),
};

export default Text;
