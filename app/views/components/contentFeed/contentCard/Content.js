import React from 'react';
import lodash from 'lodash';
import classNames from 'classnames';
import moment from 'moment';

import { translatedFromNow } from '../../../../utils/dateUtils';
import Author from './Author';
import Media from '../../media/Media';
import Title from './Title';
import Text from './Text';

const _getWrapperClassNames = (isSharedInfo, isOnFeed) => classNames({
  'pdg-rl-3': isSharedInfo,
  'h-min-120px': isOnFeed,
});

const _getSharedWrapperClassNames = (isSharedInfo) => classNames({
  'brd-grey': isSharedInfo,
});

const Content = (props, { translate, getLanguage }) => {
  return props.content.redacted || (props.content.shareInfo && props.content.shareInfo.redacted)
    ? <div className="txt-center pdg-tb-50px clr-greyd2">
        <h1 className="mrg-0">{ translate('phrases.postRemoved') }</h1>
        <div className="mrg-5px dsp-block fnt-md">{ translate('phrases.thisPostHasBeenRemoved')}</div>
      </div>

    : <div className={_getWrapperClassNames(!!props.content.shareInfo, props.isOnFeed)}>
        <div
          onClick={props.content.shareInfo && ((e) => props.onContentClick(e, props.content.shareInfo.sharedContentId))}
          className={_getSharedWrapperClassNames(!!props.content.shareInfo)}>

          {props.content.author && props.content.shareInfo &&
            <div className="valign-block last-max pdg-rl-3 mrg-tb-10px">
              <Author
                author={props.content.author}
                created={translatedFromNow(props.content.created, getLanguage())}
                isReply={props.isReply}
                bookmarks={props.bookmarks}/>
            </div>}

          {props.content.name &&
            <Title
              isTruncated={ props.isTruncated }
              title={ props.content.name }/>}

          {props.content.media && !!props.content.media.length &&
            <Media
              media={props.content.media}
              hideMedia={!props.mediaVisible}/>}

          {props.content.message &&
            <Text
              text={ props.content.message }
              tags={ props.content.tags }
              urls={ props.content.urls }
              isTruncated={ props.isTruncated }
              maxTextLength={ props.maxTextLength }
              needTruncation={ props.needTruncation }
              onTruncationClick={ props.onTruncationClick }/>
          }
        </div>
      </div>
  }

Content.contextTypes = {
  getLanguage: React.PropTypes.func.isRequired,
  translate: React.PropTypes.func.isRequired
};

Content.propTypes = {
  content: React.PropTypes.object.isRequired,
  contextMenuItems: React.PropTypes.array,
  isReply: React.PropTypes.bool,
  isTruncated: React.PropTypes.bool,
  maxTextLength: React.PropTypes.number,
  needTruncation: React.PropTypes.bool,
  onTruncationClick: React.PropTypes.func,
  bookmarks: React.PropTypes.array,
};

export default Content;
