import React from 'react';
import Portal from 'react-portal';

import BaseComponent from '../../../BaseComponent';


import ReportSelectorContainer from '../../selectors/reportSelector/ReportSelectorContainer';
import ConfirmAlert from '../../elements/alerts/ConfirmAlert';
import ExternalShareDialog from '../../share/ExternalShareDialog';
import MainColFog from '../../elements/fogs/MainColFog';

import { blockUser } from '../../../../actions/userActions';
import { removeContent } from '../../../../actions/removeActions';
import privateContentActions from '../../../../actions/privateContentActions';
import { updateScheduledTask, removeScheduledTask } from '../../../../actions/schedulerActions';

let ScheduleSelectorContainer;

export default class ContextMenuActionsContainer extends BaseComponent {
  constructor(){
    super();
    this._bind(
      'handleBlockUser',
      'handleLeavePrivateConversation',
      'handleRemoveContent',
      'handleTaskDateSubmit'
    );

  }

  componentDidMount(){
    if(this.context.authenticatedUser && this.context.authenticatedUser.specialType){
      require.ensure(['../../selectors/scheduleSelector/ScheduleSelectorContainer'], () => {
        ScheduleSelectorContainer = require('../../selectors/scheduleSelector/ScheduleSelectorContainer');
      }, 'scheduler');
    }
  }

  handleBlockUser(){
    const { contextMenuActionObject } = this.props;
    context.executeAction(blockUser, { userId: contextMenuActionObject.target.id });
  }

  handleRemoveContent(){
    const { contextMenuActionObject, onSetContextMenuAction } = this.props;
    switch(contextMenuActionObject.action){
      case 'removeTask':
        context.executeAction(removeScheduledTask, contextMenuActionObject.target);
        onSetContextMenuAction(null);
      break;
      case 'remove':
        context.executeAction(removeContent, { contentId: contextMenuActionObject.target.id });
      break;
    }
  }

  handleLeavePrivateConversation(){
    const props = this.props;
    context.executeAction(privateContentActions.removeSelf, { contentId: props.contextMenuActionObject.target.id });
  }

  handleTaskDateSubmit(date){
    const { contextMenuActionObject, onSetContextMenuAction } = this.props;

    context.executeAction(updateScheduledTask, {fireDate: date, id: contextMenuActionObject.target.id});
    onSetContextMenuAction(null);
  }

  render(){
    const { props, state } = this;

    //console.log(props);

    if(!props.contextMenuActionObject){
      return null;
    }

    switch(props.contextMenuActionObject.action){
      case 'report':
        return <Portal isOpened>
          <ReportSelectorContainer
            contextMenuActionTarget={props.contextMenuActionObject.target}
            onSetContextMenuAction={props.onSetContextMenuAction}
          />
        </Portal>

      case 'block':
        return <Portal isOpened>
          <MainColFog onClick={(e) => props.onSetContextMenuAction(null)}>
            <ConfirmAlert
              onConfirm={this.handleBlockUser}
              confirmText={this.context.translate('words.ok')}
              title={this.context.translate('phrases.blockUser')}
              text={this.context.translate('phrases.peopleYouBlockCanNoLongerSendYouPrivateMessages')}/>
          </MainColFog>
        </Portal>

      case 'leave':
        return <Portal isOpened>
          <MainColFog onClick={(e) => props.onSetContextMenuAction(null)}>
            <ConfirmAlert
              onConfirm={this.handleLeavePrivateConversation}
              confirmText={this.context.translate('words.ok ')}
              title={this.context.translate('phrases.leaveThisConversation')}
            />
          </MainColFog>
        </Portal>

      case 'remove':
      case 'removeTask':
        return <Portal isOpened>
          <MainColFog isFullScreen={props.contextMenuActionObject.action === 'removeTask'} onClick={(e) => props.onSetContextMenuAction(null)}>
            <ConfirmAlert
              onConfirm={this.handleRemoveContent}
              confirmText={this.context.translate('words.ok')}
              title={this.context.translate('words.delete')}
              text={this.context.translate('phrases.areYouSureYouWantToDeleteThisContent')}
            />
          </MainColFog>
        </Portal>

      case 'share':
        return <Portal isOpened>
          <MainColFog onClick={(e) => props.onSetContextMenuAction(null)}>
            <ExternalShareDialog target={props.contextMenuActionObject.target}/>
          </MainColFog>
        </Portal>

      case 'editTask':
        return <Portal isOpened>
          {ScheduleSelectorContainer &&
            <ScheduleSelectorContainer
              isFullScreen
              onCancel={(e) => props.onSetContextMenuAction(null)}
              onSubmit={this.handleTaskDateSubmit}
              publishDate={ props.contextMenuActionObject.target.fireDate }
            />
          }
        </Portal>
    }

  }

  static contextTypes = {
    authenticatedUser: React.PropTypes.object,
    translate: React.PropTypes.func.isRequired
  };

  static propTypes = {};
}
