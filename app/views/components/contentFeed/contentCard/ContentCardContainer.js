import React from 'react';
import ReactDom from 'react-dom';
import { connectToStores } from 'fluxible-addons-react';
import { navigateAction } from '../../../../actions/routerActions';
import lodash from 'lodash';

import BaseComponent from '../../../BaseComponent';
import ContentCard from './ContentCard';
import ContentType from '../../../../enums/ContentType';
import InviteStatus from '../../../../enums/InviteStatus';

import { toggleForm } from '../../../../actions/component/formActions';
import privateContentActions from '../../../../actions/privateContentActions';
import { like, dislike } from '../../../../actions/likeActions';
import { toggleBookmark } from '../../../../actions/bookmarkActions';
import { repostContent } from '../../../../actions/shareActions';

class ContentCardContainer extends BaseComponent {

  constructor(){
    super();
    this._bind(
      'handleContentClick',
      'handleTruncationClick',
      'handleAcceptInvite',
      'handleLikeClick',
      'handleDislikeClick',
      'handleSetContextMenuAction',
      'handleRepostContent');
    this.state = {
      isTruncated: false,
      mediaVisible: true,
    }
  }

  componentDidMount(){
    if(this.props.isInViewport && this.props.isExtended){
      this.props.setScrollPositionHandler(ReactDom.findDOMNode(this).offsetTop, true);
    }

    const elem = ReactDom.findDOMNode(this);

    this._offsetTop = elem.offsetTop;
    this._offsetHeight = elem.offsetHeight;
  }

  componentDidUpdate(prevProps){

    const { props } = this;
    const contentWrapper = ReactDom.findDOMNode(this);

    /*props.isReply || props.isExtended
      || contentWrapper.offsetTop > props.scrollPos-contentWrapper.offsetHeight
      && contentWrapper.offsetTop < window.innerHeight+props.scrollPos
        ? !this.state.mediaVisible && this.setState({mediaVisible: true})
        : !!this.state.mediaVisible && this.setState({mediaVisible: null});*/


    if(this.props.isExtended && prevProps.replies && this.props.replies){

      if(props.replies.length !== prevProps.replies.length
        && contentWrapper.scrollHeight - contentWrapper.scrollTop - contentWrapper.offsetHeight < 200){

          contentWrapper.scrollTop = contentWrapper.scrollHeight;
      }
    }

  }

  handleAcceptInvite(e){
    e.preventDefault();
    e.stopPropagation();
    const props = this.props;

    if(props.onAcceptPrivateConversationInvite){
      props.onAcceptPrivateConversationInvite(this.props.content.feedContent);
    }
  }

  shouldComponentUpdate(nextProps) {

    const contentElem =  ReactDom.findDOMNode(this);

    const scrollPositionChanged = nextProps.scrollPos !== this.props.scrollPos;

    if(scrollPositionChanged && this.isElementInViewport(nextProps.scrollPos, contentElem)){

      if(!this.props.isInViewport){
        //console.log('viewport changed');
        //this.props.onContentScrolled(this.props.content.id, this.props.content.childCount);
        this.props.onContentScrolled(this.props.content);
      }

      return false;
    }

    return true;
  }

  handleContentClick(e, contentId){
    const props = this.props;

    if(props.content.isTemp || props.task){
      return;
    }

    if(lodash.includes(contentId, 'content:')){
      e.preventDefault();
      e.stopPropagation();
      return context.executeAction(navigateAction, {
        method: 'GET',
        url: '/content/' + contentId + (props.content.slug ? `/${props.content.slug}` : '')
      });
    }

    if(!lodash.includes([e.target.parentNode.tagName, e.target.tagName, e.target.parentNode.parentNode.tagName], 'A')){
      e.preventDefault();
      e.stopPropagation();
      if(props.content.isNotification){
        if(props.content.type === ContentType.UserInvitedToPrivateConversation &&
          props.content.data !== InviteStatus.Accepted){
          return this.handleAcceptInvite(e);
        }

        contentId = props.content.feedContent.id;
      }
      else{
        contentId = props.content.id;
      }

      context.executeAction(navigateAction, {
        method: 'GET',
        url: '/content/' + contentId + (props.content.slug ? `/${props.content.slug}` : '')
      });
    }
  }

  handleToggleBookmark(data){
    context.executeAction(toggleBookmark, data);
  }

  handleLikeClick(e){
    context.executeAction(like, {objectId: this.props.content.id});
  }

  handleDislikeClick(e){
    context.executeAction(dislike, {objectId: this.props.content.id})
  }

  handleSetContextMenuAction(contextMenuActionObject){
    this.setState({ contextMenuActionObject });
  }

  handleTruncationClick(e){
    e.stopPropagation();
    this.setState({isTruncated: !this.state.isTruncated});
  }


  handleRepostContent(){
    const {props} = this;
    const content = props.content.isNotification ? props.content.feedContent : props.content;

    context.executeAction(repostContent, { parentId: props.user.id, contentId: content.id});
  }

  isElementInViewport(scrollPosition, element){
    const elemTop = this._offsetTop -250;
    const elemBottom = elemTop + this._offsetHeight;

    return scrollPosition > elemTop && scrollPosition < elemBottom;
  }

  get isMine(){
    const {props} = this;
    let content = props.content;

    if(!content || !props.user){
      return;
    }

    if(content.isNotification){
      content = content.feedContent;
    }
    return content.author
      && content.author.id === props.user.id;
  }


  get isBookmarked(){
    return this.props.bookmarks && this.content.id
      && lodash.find(this.props.bookmarks, {id: this.content.id}) || false;
  }

  get content(){
    return this.props.content.isNotification
      ? this.props.content.feedContent
      : this.props.content
  }

  get needTruncation(){
    const props = this.props;
    return !!(props.content.message && props.content.message.length > props.maxTextLength
        || props.content.name && props.content.name.length > props.maxTitleLength)
  }

  get contextMenuItems(){
    const props = this.props;
    const translate = this.context.translate;

    const content = props.content.isNotification ? props.content.feedContent : props.content;

    if(props.content.feedContent && props.content.feedContent.isPrivate || props.content.isPrivate){
      if(props.isMe || this.isMine){
        return [
          {
            text: translate('words.delete'), isDangerous: true,
            onClick: (e) =>
              this.handleSetContextMenuAction({
                target: props.content,
                action: 'remove'
              })
          },
          {
            text: translate('phrases.manageParticipants'),
            onClick: (e) =>
              props.onTogglePrivacySelector(props.content.isNotification
                ? props.content.feedContent.id
                : props.content.id, true
              )
          }
        ]
      }

      return [
        {
          text: translate('words.report'), isDangerous: true,
          onClick: (e) =>
            this.handleSetContextMenuAction({target: props.content, action: 'report'})
        },
        {
          text: translate('words.leave'), isDangerous: true,
          onClick: (e) =>
            this.handleSetContextMenuAction({target: content, action: 'leave'})
        },
        {
          text: translate('phrases.viewParticipants'),
          onClick: (e) =>
            props.onTogglePrivacySelector(props.content.isNotification
              ? props.content.feedContent.id
              : props.content.id, false
            )
        }
      ]
    }

    if(props.isMe || this.isMine){

      if(props.task && props.task.fireDate){
        return [
          {
            text: translate('words.delete'), isDangerous: true,
            onClick: (e) => this.handleSetContextMenuAction({target: props.task, action: 'removeTask'})
          },
          {
            text: translate('words.reschedule'),
            onClick: (e) => this.handleSetContextMenuAction({target: props.task, action: 'editTask'})
          }
        ]
      }

      if(props.content.shareInfo){
        return [
          {
            text: translate('words.share'),
            onClick: (e) => this.handleSetContextMenuAction({target: {id: props.content.shareInfo.sharedContentId}, action: 'share'})
          },
          {
            text: translate('words.delete'), isDangerous: true,
            onClick: (e) => this.handleSetContextMenuAction({target: props.content, action: 'remove'})
          }
        ]
      }

      return [
        {
          text: translate('words.delete'), isDangerous: true,
          onClick: (e) => this.handleSetContextMenuAction({target: props.content, action: 'remove'})
        },
        {
          text: translate('words.share'),
          onClick: (e) => this.handleSetContextMenuAction({target: props.content, action: 'share'})
        }
      ]
    }

    return [
      {
        text: translate('phrases.blockUser'), isDangerous: true,
        onClick: (e) => this.handleSetContextMenuAction({target: content.author, action: 'block'})
      },
      {
        text: translate('words.report'), isDangerous: true,
        onClick: (e) => this.handleSetContextMenuAction({target: content, action: 'report'})
      },
      {
        text: !this.isBookmarked ? translate('phrases.followThisConversation') : translate('phrases.unfollowThisConversation'), isImportant: true,
        onClick: (e) => this.handleToggleBookmark({isBookmarked: this.isBookmarked, objectId: content.id})
      },
      {
        text: translate('words.repost'),
        onClick: this.handleRepostContent
      },
      {
        text: translate('words.share'),
        onClick: (e) => this.handleSetContextMenuAction({target: content, action: 'share'})
      }
    ];
  }

  render(){
    const { props, state } = this;

    //this.content.shareInfo && console.log(props, this.content.shareInfo.sharer);

    return <ContentCard
      backHref={ props.backHref }
      bookmarks={ props.bookmarks }
      content={ this.content }
      parentFeed={ this.parentFeed }
      contextMenuItems={ this.contextMenuItems }
      contextMenuActionObject={ state.contextMenuActionObject }
      isExtended={ props.isExtended }
      isInViewport={ props.isInViewport }
      isReply={ props.isReply }
      isOnFeed={ props.isOnFeed }
      isBookmarked= { this.isBookmarked }
      isTruncated={ props.isExtended && state.isTruncated || !props.isExtended }
      maxTextLength={ props.maxTextLength }
      maxTitleLength={ props.maxTitleLength }
      mediaVisible={ state.mediaVisible }
      notificationType={ props.content.isNotification && props.content.type }
      notificationData={ props.content.isNotification && props.content.data }
      needTruncation={ this.needTruncation }
      onAcceptInvite={ this.handleAcceptInvite }
      onContentClick={ this.handleContentClick }
      onDislikeClick={ this.handleDislikeClick }
      onLikeClick={ this.handleLikeClick }
      onLoadOlderReplies={ props.onLoadOlderReplies }
      onSetContextMenuAction={ this.handleSetContextMenuAction }
      onTruncationClick= { props.isExtended && this.handleTruncationClick }
      onToggleBookmark={ this.handleToggleBookmark }
      replies={ props.replies }
      showLoadOlderRepliesButton={props.showLoadOlderRepliesButton}
      showJoinButton={ props.content.isNotification && !props.content.feedContentRepliesEnabled }
      task={ props.task }
    />
  }

  static contextTypes = {
    translate: React.PropTypes.func.isRequired
  }

  static propTypes = {
    backHref: React.PropTypes.string,
    bookmarks: React.PropTypes.array,
    content: React.PropTypes.object.isRequired,
    contentContextMenuVisible: React.PropTypes.bool,
    isExtended: React.PropTypes.bool,
    isMine: React.PropTypes.bool,
    isInViewport: React.PropTypes.bool,
    isReply: React.PropTypes.bool,
    isOnFeed: React.PropTypes.bool,
    maxTextLength: React.PropTypes.number,
    maxTitleLength: React.PropTypes.number,
    notificationType: React.PropTypes.string,
    onLoadOlderReplies: React.PropTypes.func,
    onSetScrollPosition: React.PropTypes.func,
    parentId: React.PropTypes.string,
    replies: React.PropTypes.array,
    scrollPosition: React.PropTypes.number,
    user: React.PropTypes.object,
    task: React.PropTypes.object,
  };

  static defaultProps = {
    maxTextLength: 300,
    maxTitleLength: 70,
  }
}

export default ContentCardContainer;

