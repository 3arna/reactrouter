import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';
import lodash from 'lodash';

const _getAuthorImageSize = (isReply) => isReply ? 40 : 42;

const _getIconClassNames = (isPrime, isFollowed) => classNames({
  'pdg-3px brd-rad-50 pos-absolute fnt-xxxs brd-solid-2-white brd-w-2px clr-white': true,
  'icon-crown bg-second': isPrime,
  'icon-tick-big bg-main': isFollowed,
});

const _isFollowed = (bookmarks, author) =>
  !!lodash.some(bookmarks, {id: author.id});

const Author = ({author, created, isReply, bookmarks}, {translate}) => {
  return <div className="content-author valign-block">
    <Link to={`/feed/${author.id}/${author.slug}`} className="dsp-block pos-relative">
      <i className={_getIconClassNames(!!author.specialType)} style={{top: '-4px', left: '-4px'}}/>
      <i className={_getIconClassNames(null, _isFollowed(bookmarks, author))} style={{right: '-4px', bottom: '-4px'}}/>
      <img
        className="content-card__author-avatar"
        width={_getAuthorImageSize(isReply)}
        height={_getAuthorImageSize(isReply)}
        src={author.avatarUrl}/>
    </Link>
    <Link to={`/feed/${author.id}/${author.slug}`}>
      <h3 className="content-card__author-name">{author.displayName}</h3>
      <time>{created}</time>
    </Link>

  </div>
}

Author.propTypes = {
  title: React.PropTypes.string,
  created: React.PropTypes.string,
};

export default Author;
