import React from 'react';
import lodash from 'lodash';

const Title = ({title, isTruncated, maxTitleLength}, {translate}) =>
  <h2 className="content-card__title">
    {isTruncated && lodash.trunc(title, maxTitleLength) || title}
  </h2>

Title.propTypes = {
  title: React.PropTypes.string,
  isTruncated: React.PropTypes.bool,
  maxTitleLength: React.PropTypes.number,
};

export default Title;
