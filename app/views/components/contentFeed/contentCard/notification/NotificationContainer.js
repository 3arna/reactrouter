import React from 'react';

import BaseComponent from '../../../../BaseComponent';
import ContentType from '../../../../../enums/ContentType';
import InviteStatus from '../../../../../enums/InviteStatus';
import Notification from './Notification';
import debugLib from 'debug';

const debug = debugLib('versy.notifications');

class NotificationContainer extends BaseComponent {
  constructor(){
    super();
  }

  get notificationText(){
    const props = this.props;
    const translate = this.context.translate;

    switch(props.notificationType){
      case ContentType.UserInvitedToPrivateConversation:
        const inviteStatus = props.notificationData;
        return inviteStatus === InviteStatus.Accepted
          ? translate('phrases.youAreFollowingThisConversation')
          : translate('phrases.youHaveBeenInvitedToJoinThisConversation');
      case ContentType.PrivateContentActivityNotification:
        return translate('phrases.thereAreNewReplies');
      case ContentType.PublicContentActivityNotification:
        return translate('phrases.thereAreNewReplies');
      case ContentType.UserStartedPrivateConversation:
        return translate('phrases.youStartedAConversation');
      case ContentType.UserFollowedContentNotification:
        return translate('phrases.youAreFollowingThisConversation');
      default:
        debug('unknown notification type', props.notificationType);
        return 'this is a notification we havent implemented yet';
    }
  }


  get notificationIconClass(){
    const props = this.props;

    switch(props.notificationType){
      case ContentType.UserInvitedToPrivateConversation:
      case ContentType.PrivateContentActivityNotification:
      case ContentType.UserStartedPrivateConversation:
        return 'icon-padlock-closed';
      case ContentType.UserFollowedContentNotification:
      case ContentType.PublicContentActivityNotification:
        return 'icon-globe';
      default:
        return 'icon-info';
    }
  }

  render(){
    return <Notification
      text={ this.notificationText }
      iconClass={ this.notificationIconClass }
      type={this.props.notificationType} />
  }

  static contextTypes = {
    translate: React.PropTypes.func.isRequired
  }

  static propTypes = {

  }
}

export default NotificationContainer;
