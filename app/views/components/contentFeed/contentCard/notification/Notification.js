import React from 'react';
import lodash from 'lodash';

const Notification = ({text, iconClass, type}) =>
  <div className="content-notification valign-block last-max">
    <i className={iconClass || 'icon-padlock-closed'}/>
    <small>{text}</small>
  </div>

Notification.propTypes = {
  notificationType: React.PropTypes.string,
};

export default Notification;
