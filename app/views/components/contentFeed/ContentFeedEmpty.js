import React from 'react';
import { NavLink } from 'fluxible-router';
import classNames from 'classnames';
import { connectToStores, provideContext } from 'fluxible-addons-react';

import { toggleForm } from '../../../actions/component/formActions';

@connectToStores(['ContentFormStore'], (context, props) => { 
  return {formVisible: context.getStore('ContentFormStore').formVisible}
})

class ContentFeedEmpty extends React.Component {

  static contextTypes = {
    translate: React.PropTypes.func,
    executeAction: React.PropTypes.func,
  }

  render(){
    return <div className="txt-center fix-bb clr-greyd1 txt-center mrg-30px fnt-xs">empty feed</div>
  }
}

export default ContentFeedEmpty;
