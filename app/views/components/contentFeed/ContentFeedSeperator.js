import React from 'react';
import lodash from 'lodash';
import moment from 'moment';
import classNames from 'classnames';

import { getAgo } from '../../../utils/dateUtils';

const getDateClassName = (props) => classNames({
  'td txt-nowrap pdg-rl-40px ': true,
  'clr-greyd3': !props.parent,
  'clr-blackl3': props.parent,
});

const getWrapperClassName = (props) => classNames({
  'tb w-100 fix-bb mrg-tb-3 pdg-rl-3 fnt-sm': true,
  'pdg-l-60px sm-pdg-rl-0px': props.isExtended
});

const ContentDateSeperator = (props) => {
  return <div className={getWrapperClassName(props)}>
    <div className="td col-6">
      <hr className="brd-0 brd-t-grey"/>
    </div>
    <div className={getDateClassName(props)}>
      {props.date}
    </div>
    <div className="td col-6">
      <hr className="brd-0 brd-t-grey"/>
    </div>
  </div>;
};

export default ContentDateSeperator;
