import React from 'react';
import ReactDom from 'react-dom';
import moment from 'moment';
import classNames from 'classnames';
import lodash from 'lodash';

import BaseComponent from '../../BaseComponent';

import VideoPlayFog from '../elements/fogs/VideoPlayFog';

import placeholder from '../../decorators/placeholder';

//@placeholder

export default class VideoPlayer extends BaseComponent {

  constructor(){
    super();
    this._bind(
      'playVideo', 
      'dataLoaded', 
      'updateTimeline', 
      'muteMedia', 
      'setMediaCurrentTime', 
      'setFullScreen', 
      'exitFullScreen',
      'setMediaVolume');
    this.state = {
      isPlaying: false,
      isLoaded: false,
      isMuted: false,
      isSoundSliderVisible: false,
      mediaTimeline: 0,
      mediaCurrentTime: '00:00',
      mediaDuration: 0,
      mediaVolume: 50,
    }
  }

  componentDidMount(){
    this.handleMount();
  }

  componentWillUnmount(){
    this.handleUnmount();
  }

  componentWillReceiveProps(nextProps){
    const {props, state} = this;

    if(nextProps.startedMediaId !== props.media.id && state.isPlaying){
      this.playVideo.bind(this, true)();
    }
  }

  handleMount(){
    const elem = this.refs.media;
    if(!elem){
      return;
    }
    this._isMounted = true;
    elem.volume = this.state.mediaVolume / 100;
    //elem.addEventListener('loadstart', this.dataLoaded);
    elem.addEventListener('loadeddata', this.dataLoaded);
    elem.addEventListener('timeupdate', this.updateTimeline);
    this._timer = setTimeout(() => this.dataLoaded, 5000);
  }

  handleUnmount(){
    const elem = this.refs.media;
    this.setState({
      mediaTimeLine: 0,
      mediaCurrentTime: '00:00',
      mediaStarted: false,
      isLoaded: false,
      isPlaying: false,
    });
    if(!elem){
      return;
    }
    this._isMounted && elem.removeEventListener('loadeddata', this.dataLoaded);
    this._isMounted && elem.removeEventListener('timeupdate', this.updateTimeline);
    clearTimeout(this._timer);
  }

  updateTimeline(){
    const elem = this.refs.media;

    if(!elem){
      return;
    }

    if(elem.currentTime === elem.duration){
      return this.setState({
        mediaTimeLine: 0,
        mediaCurrentTime: '00:00',
        mediaStarted: false,
        isLoaded: true,
        isPlaying: false,
      });
    }

    this._isMounted && this.setState({
      mediaTimeline: elem.currentTime / elem.duration * 100,
      mediaCurrentTime: moment(parseInt(elem.currentTime)*1000).format('mm:ss'),
    });
  }

  muteMedia(e){
    //const elem = ReactDom.findDOMNode(this.refs.media);
    //!this.state.isMuted ? elem.muted = true : elem.muted = false;
    //this.setState({ isMuted: !this.state.isMuted });
    this.setState({isSoundSliderVisible: !this.state.isSoundSliderVisible});
    e.preventDefault();
    e.stopPropagation();
  }

  dataLoaded(e){
    const elem = this.refs.media;
    if(!elem){
      return;
    }
    !this.state.isLoaded && this.setState({isLoaded: true, mediaDuration: elem.duration});
  }

  playVideo(isPlaying, e){
    const elem = this.refs.media;
    if(this.state.isPlaying !== !isPlaying){
      !isPlaying ? elem.play() : elem.pause();
      this.setState({ isPlaying: !isPlaying });
      !isPlaying && this.props.onStartMedia(this.props.media.id);
    }
    e && e.preventDefault();
    e && e.stopPropagation();
  }

  setMediaVolume(e){
    const position = e.currentTarget.getBoundingClientRect().bottom - e.clientY;
    const percentage = position / e.currentTarget.getBoundingClientRect().height;
    const elem = this.refs.media;
    elem.volume = percentage;
    this.setState({mediaVolume: percentage*100});
    e.preventDefault();
    e.stopPropagation();
  }

  setMediaCurrentTime(e){
    const position = e.clientX - e.currentTarget.getBoundingClientRect().left;
    const percentage = position / e.currentTarget.getBoundingClientRect().width;
    const elem = this.refs.media;
    elem.currentTime = this.state.mediaDuration * percentage;
    e.preventDefault();
    e.stopPropagation();
  }

  setFullScreen(e){
    e.preventDefault();
    e.stopPropagation();
    const elem = this.refs.media;
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) {
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
      elem.webkitRequestFullscreen();
    }
  }

  exitFullScreen(e){
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    }
  }

  preventDefault(e){
    e.preventDefault();
    e.stopPropagation();
  }

  get muteButtonClassNames(){
    return classNames({
      'pdg-rl-10px fnt-md icon-volume': true,
      'clr-mainl3': this.state.isMuted,
    });
  }


  render(){

    const { props, state} = this;

    //console.log(props.startedMediaId, props.media.id, state.isPlaying);

    return <div className="pos-relative mrg-auto"
      style={{
        maxWidth: `${props.dimensions.width}px`,
        maxHeight: `${props.dimensions.height}px`}}>
          
          {props.children}

          {state.isPlaying && 
            <div onClick={this.preventDefault} className="pos-absolute bot-0 left-0 bg-op-05 w-100 z-1 pdg-rl-10px fadeIn clr-grey">
              <div className="valign-block">
                <div className="w-100 pdg-tb-15px" onClick={this.setMediaCurrentTime}>
                  <div className="pdg-2px bg-blackd3">
                    <div className="bg-greyd3 pdg-tb-1px top-0 trans-w" style={{width: state.mediaTimeline+'%'}}/>
                  </div>
                </div>
                <div className="fnt-xs pdg-l-10px w-min-60px txt-center sm-dsp-none">{state.mediaCurrentTime}</div>
                <div>
                  <i className={this.muteButtonClassNames} onClick={this.muteMedia}/>
                  {state.isSoundSliderVisible && 
                    <div className="bg-op-05 h-80px pos-absolute pdg-tb-15px" style={{bottom: '35px'}}>
                      <div onClick={this.setMediaVolume} className="h-100 fix-bb pdg-rl-15px">
                        <div className="h-100 fix-bb pdg-2px bg-blackd3 pos-relative">
                          <div 
                            className="bg-greyd3 pdg-rl-1px pos-absolute bot-0 left-0 mrg-l-1px trans-w" 
                            style={{height: state.mediaVolume+'%'}}/>
                        </div>
                      </div>
                    </div>}
                </div>
                <div onClick={this.setFullScreen}>
                  <i className="pdg-l-10px icon-increase"/>
                </div>
              </div>
            </div>}

          <div 
            className="pos-relative mrg-auto over-auto fix-bb w-auto h-auto bg-blackd3"
            style={{ paddingBottom: `${props.dimensions.padding}%` }}>
            
            {!props.hideMedia && 
              <video
                poster={props.media.poster}
                //controls={state.isPlaying}
                muted={ state.isMuted }
                className="pos-absolute h-100 w-100 top-0 left-0 dsp-block"
                ref="media"
                onDoubleClick={this.exitFullScreen}
                onClick={this.playVideo.bind(this, true)}>
                <source 
                  src={props.media.mediaUrl} 
                  type="video/mp4"/>
              </video>}

            {!props.uploading && !state.isPlaying &&
              <VideoPlayFog
                isLoaded={state.isLoaded}
                duration={state.mediaDuration}
                playVideo={this.playVideo.bind(this, false)}>
                  {false &&
                    <button className="brd-0 mrg-0 pdg-10px bg-main clr-white brd-rad-50 fnt-sm">
                      <i className="icon-alert"/>
                    </button>}
              </VideoPlayFog>}
          </div>
    </div>
  }

  static propTypes = {
    media: React.PropTypes.object,
    maxMediaWidth: React.PropTypes.number,
    maxMediaHeight: React.PropTypes.number,
  }
}
