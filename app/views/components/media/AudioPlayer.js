import React from 'react';
import ReactDom from 'react-dom';
import moment from 'moment';
import classNames from 'classnames';
import lodash from 'lodash';

import BaseComponent from '../../BaseComponent';

import Media from './Media';

export default class AudioPlayer extends BaseComponent {

  constructor(){
    super();
    this._bind(
      'playMedia', 
      'muteMedia', 
      'setMediaCurrentTime', 
      'handleAudioLoad', 
      'resetMedia', 
      'updateTimeline'
    );
    this.state = {
      mediaStarted: false,
      mediaMuted: false,
      mediaTimeline: 0,
      mediaCurrentTime: '00:00',
      mediaDuration: 0,
      mediaVolume: 0.3,
    };
  }

  componentDidMount() {
    this._isMounted = true;
    const elem = this.refs.media;

    if(!elem){
      return;
    }

    elem.volume = this.state.mediaVolume;
    elem.addEventListener('loadedmetadata', this.handleAudioLoad);
    elem.addEventListener('ended', this.resetMedia);

    elem.addEventListener('timeupdate', this.updateTimeline);
  }

  componentWillUnmount(){
    this._isMounted = false;
    if(!this.refs.media){
      return;
    }
    
    this.refs.media.removeEventListener('loadedmetadata', this.handleAudioLoad);
    this.refs.media.removeEventListener('ended', this.resetMedia);
    this.refs.media.removeEventListener('timeupdate', this.updateTimeline);
  }

  componentWillReceiveProps(nextProps){
    const {props, state} = this;

    if(nextProps.startedMediaId !== props.media.id && state.mediaStarted){
      this.playMedia.bind(this, true)();
    }
  }

  handleAudioLoad(){
    this._isMounted && this.setState({
      mediaDuration: this.refs.media.duration,
    });
  }

  updateTimeline(){

    const elem = this.refs.media;
    this._isMounted && this.setState({
      mediaTimeline: elem.currentTime / elem.duration * 100,
      mediaCurrentTime: moment(parseInt(elem.currentTime)*1000).format('mm:ss'),
    })
  }

  resetMedia(e){
    if(!this._isMounted){
      return
    }
    const elem = this.refs.media;
    elem.currentTime = 0;
    this.setState({
      mediaTimeLine: 0,
      mediaCurrentTime: '00:00',
      mediaStarted: false
    });
  }

  playMedia(mediaStarted, e){
    const elem = this.refs.media;
    if(this.state.mediaStarted !== !mediaStarted){
      !mediaStarted ? elem.play() : elem.pause();
      this.setState({ mediaStarted: !mediaStarted });
      !mediaStarted && this.props.onStartMedia(this.props.media.id);
    }
    e && e.preventDefault();
    e && e.stopPropagation();
  }

  /*playMedia(e){
    const elem = ReactDom.findDOMNode(this.refs.media);
    !this.state.mediaStarted ? elem.play() : elem.pause();
    this.setState({ mediaStarted: !this.state.mediaStarted });
    e && e.preventDefault();
    e && e.stopPropagation();
  }*/

  muteMedia(e){
    const elem = ReactDom.findDOMNode(this.refs.media);
    !this.state.mediaMuted ? elem.muted = true : elem.muted = false;
    this.setState({ mediaMuted: !this.state.mediaMuted });
    e.preventDefault();
    e.stopPropagation();
  }

  setMediaCurrentTime(e){
    const position = e.clientX - e.currentTarget.getBoundingClientRect().left;
    const percentage = position / e.currentTarget.getBoundingClientRect().width;
    const elem = ReactDom.findDOMNode(this.refs.media);
    elem.currentTime = this.state.mediaDuration * percentage;
    e.preventDefault();
    e.stopPropagation();
  }

  get audioClassNames(){
    return classNames({
      'txt-center pdg-tb-15px w-100 pos-relative fix-bb bg-white': true,
      'col-max-8 mrg-auto': this.props.isSelected,
    });
  }

  get playButtonClassNames(){
    return classNames({
      'cursor-pointer td fnt-xxl brd-rad-50': true,
      'clr-greyd2 icon-play brd-grey': !this.state.mediaStarted && this.state.mediaDuration,
      'clr-main icon-play brd-main': this.state.mediaStarted,
      'icon-settings brd-white spin': !this.state.mediaDuration,
    });
  }

  get muteButtonClassNames(){
    return classNames({
      'icon-volume cursor-pointer fnt-lg fadeIn': true,
      'clr-main': !this.state.mediaMuted && this.state.mediaStarted,
      'clr-grey': this.state.mediaMuted,
      'dsp-none': !this.state.mediaDuration
    });
  }

  render(){

    const {props, state} = this;

    return <div className={this.audioClassNames}>
      
      {this.props.children}

      <div className="pdg-rl-3">
 
        {this.props.childMedia && 
          <div className="pdg-rl-3">
            <Media media={props.childMedia} />
          </div>}

        <div className="mrg-b-5px pdg-tb-5px cursor-pointer" onClick={this.setMediaCurrentTime}>
          <div className="brd-greyl1">
            <div className="brd-white bg-greyl2 pdg-1px">
              <div className="pdg-tb-1px bg-main op-08 trans-w" style={{width: state.mediaTimeline+'%'}}/>
            </div>
          </div>
        </div>

        <div className="tb w-100 clr-grey">
          <i className="td sm-dsp-none w-min-50px"/>
          
          <div className="td w-100 txt-center">  
            <div className="tb w-100">
              <small className="pdg-r-20px td col-6 fnt-xs fix-bb txt-right">{moment(parseInt(state.mediaDuration)*1000).format('mm:ss')}</small>
              <i className={this.playButtonClassNames} onClick={this.playMedia.bind(this, state.mediaStarted)}/>
              <small className="pdg-l-20px td col-6 fix-bb txt-left">
                <span className="w-100px dsp-block fnt-xs">{state.mediaCurrentTime}</span>
              </small>
            </div>
          </div>
          
          <div className="w-min-50px td sm-dsp-none txt-right fnt-sm">
            <i className={this.muteButtonClassNames} onClick={this.muteMedia}/>
          </div>

        </div>
        <audio ref="media"><source src={props.media.mediaUrl} type={props.media.mimeType}/></audio>
      </div>
    </div>
  }
}
