import React from 'react';
import ReactDom from 'react-dom';
import classNames from 'classnames';
import lodash from 'lodash';

import BaseComponent from '../../BaseComponent';
import VideoPlayFog from '../elements/fogs/VideoPlayFog';

import placeholder from '../../decorators/placeholder';

//@placeholder

export default class ImageViewer extends BaseComponent {

  constructor(){
    super();
    this._bind('handleImageLoad', 'handleTogglePlay', 'handleImageError', 'handleMount');
    this.state = {
      isLoading: true,
      isPlaying: null,
      mediaUrl: null,
      isError: null,
    };
  }

  componentDidMount(){
    this.handleMount();
  }

  componentDidUpdate(newProps){
    if(newProps.hideMedia !== this.props.hideMedia){
      newProps.hideMedia && this.handleMount();
    }
  }

  componentWillUnmount(){
    this.handleUnmount();
  }

  handleMount(){
    const elem = this.refs.image;
    if(!elem){
      return;
    }

    this._isMounted = true;
    elem.addEventListener('load', this.handleImageLoad);
    elem.addEventListener('error', this.handleImageError);
    this._timer = setTimeout(() => this._isMounted && this.state.isLoading && this.setState({isLoading: false}), 2000);
  }

  handleUnmount(){
    const elem = this.refs.image;
    if(!elem){
      return;
    }

    this._isMounted = false;
    clearTimeout(this._timer);
    
    elem.removeEventListener('load', this.handleImageLoad);
    elem.removeEventListener('error', this.handleImageError);
  }

  handleImageLoad(){
    this._isMounted && this.setState({isLoading: false});
  }
  handleImageError(e){
    this._isMounted && this.setState({isLoading: false, isError: true});
  }

  handleTogglePlay(isPlaying, e){
    this.state.isPlaying !== !isPlaying && this.setState({ isPlaying: !isPlaying });
    e.preventDefault();
    e.stopPropagation();
  }

  get mediaUrl(){
    const { dimensions, media, maxMediaHeight, maxMediaWidth } = this.props;
    const imageResizeEndpoint = this.context.apiConfig.imageResizeEndpoint;

    return !lodash.contains(media.mediaUrl, 'blob:')
      ? `${imageResizeEndpoint}${dimensions.width}x${dimensions.height}/${media.mediaUrl}`
      : media.mediaUrl;
  }

  get placeholderUrl(){
    const { media, isAnimated } = this.props;
    const imageResizeEndpoint = this.context.apiConfig.imageResizeEndpoint;
    
    const placeholderWidth = isAnimated && media.attributes.width 
      || parseInt(media.attributes.width * 0.05);
    const placeholderHeight = isAnimated && (media.attributes.height-1) 
      || parseInt(media.attributes.height * 0.05);

    return !lodash.contains(media.mediaUrl, 'blob:')
      ? `${imageResizeEndpoint}${placeholderWidth}x${placeholderHeight}/${media.mediaUrl}`
      : media.mediaUrl;
  }

  get imageClassNames(){
    const {props, state} = this;
    return classNames({
      'w-100 valign-top pos-absolute h-100 top-0 left-0 fadeIn': true,
      'dsp-none': state.isLoading || (props.isAnimated && !state.isPlaying) || state.isError,
      'dsp-block': !state.isLoading || (props.isAnimated && state.isPlaying)
    })
  }

  get placeholderClassNames(){
    return classNames({
      'pos-relative mrg-auto over-auto fix-bb w-auto h-auto bg-cover bg-grey': true,
      'blur-10px': !this.state.isLoading ? false : true,
    })
  }

  render(){
    const {props, state} = this;

    return <span
      className="dsp-inline pos-relative w-100 valign-bot"
      style={{ 
        maxWidth: props.dimensions.width, 
        maxHeight: props.dimensions.maxHeight
      }}>
        {props.children}

        {state.isError && 
          <div className="w-100 h-100 pos-absolute z-1 bg-white">
            <span className="top-40 dsp-block pos-relative clr-greyd3 fnt-lg"> 
              <h1 className="mrg-0 pdg-0 fnt-xxxl clr-greyl1">OOPS</h1>
              <p className="mrg-0 pdg-0 fnt-xs mrg-t-15px pos-absolute top-0 fnt-bold w-100">Unsupported Image Type!</p>
            </span>
          </div>}

        {false &&
          <div className="w-100 h-100 pos-absolute z-1 clr-grey">
            <span className="top-40 dsp-block pos-relative clr-white fnt-lg">
              <i className="icon-camera fnt-lg"/>
            </span>
          </div>}

        <div
          className={this.placeholderClassNames}
          style={{
            paddingBottom: `${props.dimensions.padding}%`,
            backgroundImage: `url('${this.placeholderUrl}')`
          }}>
        </div>

        {!props.hideMedia && 
          <img
            ref="image"
            className={ this.imageClassNames }
            src={ this.mediaUrl }
            onClick={ props.isAnimated && this.handleTogglePlay.bind(this, true)}
            onMouseLeave={ props.isAnimated && this.handleTogglePlay.bind(this, true)}/>
        }

        {props.isAnimated && !state.isPlaying && !props.uploading && !state.isLoading && 
          <VideoPlayFog isLoaded={true} playVideo={this.handleTogglePlay.bind(this, false)}/>}

        
    </span>
  }

  static contextTypes = {
    apiConfig: React.PropTypes.object,
  };

  static propTypes = {
    media: React.PropTypes.object,
    maxMediaWidth: React.PropTypes.number,
    maxMediaHeight: React.PropTypes.number,
  }
}
