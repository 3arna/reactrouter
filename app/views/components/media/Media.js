import React from 'react';
import ReactDom from 'react-dom';
import classNames from 'classnames';
import lodash from 'lodash';
import { connectToStores } from 'fluxible-addons-react';

import { mediaSetStarted } from '../../../actions/mediaActions';

import BaseComponent from '../../BaseComponent';

import VideoPlayer from './VideoPlayer';
import AudioPlayer from './AudioPlayer';
import ImageViewer from './ImageViewer';

//@placeholder

@connectToStores(['MediaPlayerStore'], (context, props) => {
  return {
    startedMediaId: context.getStore('MediaPlayerStore').startedMediaId
  };
})

export default class Media extends BaseComponent {

  constructor(){
    super();
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.hideMedia !== this.props.hideMedia 
      || (this.props.media.mediaUrl !== nextProps.media.mediaUrl) 
      || (!!nextProps.uploading) 
      || (!!nextProps.children)
      || (nextProps.startedMediaId !== this.props.startedMediaId);
  }

  handleStartedMedia(mediaId){
    context.executeAction(mediaSetStarted, {mediaId});
  }

  get mainMedia(){
    return lodash.isArray(this.props.media) && lodash.first(this.props.media) || this.props.media;
  }

  get childMedia(){
    const mainMedia = this.mainMedia;
    return lodash.isArray(mainMedia.childMedia) && lodash.first(mainMedia.childMedia) || false;
  }

  _getDimensions(media){

    const {height, width} = media.attributes;
    const {maxMediaHeight, maxMediaWidth} = this.props;

    const isWidthBigger = width > maxMediaWidth;
    const isHeightBigger = height > maxMediaHeight;

    const wRatio = width / height;
    const hRatio = height / width;
    const maxRatio = maxMediaHeight / maxMediaWidth;

    if(!isWidthBigger && !isHeightBigger){
      return {
        width: width, 
        height: height, 
        padding: (hRatio*100)
      }
    }

    let scaledHeight;
    let scaledWidth;

    if(wRatio <= 1 && isHeightBigger){
      scaledHeight = maxMediaHeight;
      scaledWidth = wRatio * scaledHeight;
    } else
    if(wRatio > 1 && isWidthBigger){
      scaledWidth = maxMediaWidth;
      scaledHeight = hRatio * scaledWidth;
    } else {
      scaledWidth = maxMediaWidth;
      scaledHeight = maxMediaHeight;
    }


    return {
      width: parseInt(scaledWidth), 
      height: parseInt(scaledHeight), 
      padding: (hRatio*100)
    }
  }

  get mediaJsx(){
    
    const {props} = this;
    const mimeType = this.mainMedia.mimeType.split('/');

    const childMedia = this.childMedia;
    const dimensions = this._getDimensions(this.mainMedia);

    //console.log(this.mainMedia);

    switch(lodash.first(mimeType)) {
      case 'image': 
        return <ImageViewer 
          media={this.mainMedia} 
          dimensions={dimensions}
          children={props.children}
          isAnimated={lodash.last(mimeType) === 'gif'}
          uploading={props.uploading}
          onStartMedia={this.handleStartedMedia}
          startedMediaId={props.startedMediaId}
          hideMedia={props.hideMedia}/>;
      case 'video':
        return <VideoPlayer 
          media={this.mainMedia}
          childMedia={childMedia}
          dimensions={dimensions}
          children={props.children}
          uploading={props.uploading}
          onStartMedia={this.handleStartedMedia}
          startedMediaId={props.startedMediaId}
          hideMedia={props.hideMedia}
          />;
      case 'audio':
        return <AudioPlayer 
          children={props.children} 
          uploading={props.uploading} 
          media={this.mainMedia}
          childMedia={childMedia}
          onStartMedia={this.handleStartedMedia}
          startedMediaId={props.startedMediaId}
          hideMedia={props.hideMedia}/>
      default:
        return <div>{this.mainMedia.mimeType} {this.mainMedia.mediaUrl}</div>;
    }
  }

  render(){
    return <div className="txt-center">
      {this.mediaJsx}
    </div>
  }

  static propTypes = {
    media: React.PropTypes.any,
    maxMediaWidth: React.PropTypes.number,
    maxMediaHeight: React.PropTypes.number,
  };

  static defaultProps = {
    maxMediaWidth: 800,
    maxMediaHeight: 500,
  }

}
