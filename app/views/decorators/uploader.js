import React from 'react';
import ReactDom from 'react-dom';
import lodash from 'lodash';
import request from 'superagent';
import MobileDetect from 'mobile-detect';
import exif from 'exif-js';

import BaseComponent from  '../BaseComponent';

function uploader(Component){
  return class Uploader extends BaseComponent {

    static contextTypes = {
      apiConfig: React.PropTypes.object
    };

    constructor(){
      super();
      this.state = {
        default: {
          name: 'default',
          previewUrl: false,
          media: false,
        }
      };

      this.uploadRequest = null;

      this._bind(
        'handleMediaPreview',
        'handleMediaUpload',
        'handleMediaAttributes',
        'getMedia',
        'handleMediaUploadCancel',
        '_uploadFile'
      );
    }

    componentWillUnmount(){
      this.previewUrl && this._revokeObjectUrl(this.previewUrl);
      this.previewUrl = null;
    }

    handleMediaAttributes(params, cb){

      if(params.media && params.media.attributes){
        return cb(null, params.media.attributes);
      }

      let mediaType = params.media && params.media.mimeType || params.file.type;
      mediaType = mediaType.split('/').shift();
      const url = params.previewUrl || params.media && params.media.mediaUrl;

      let element = params.element;

      const timer = setTimeout(() =>
        cb(new Error('Error can not extract attributes from media')), 10000);

      switch(mediaType){
        case 'image':
          if(!element){
            element = new Image();
            element.src = url;
          }
          element.onload = () => {
            //TODO revoke object url, seems to cause error probably need to do it in componentWillUnmount and after posting or removing media
            // (URL || webkitURL).revokeObjectUrl(element.src);
            clearTimeout(timer);
            return cb(null, {
              width: element.naturalWidth,
              height: element.naturalHeight
            })
          };

          break;
        case 'video':

          if(!element){
            element = document.createElement('video');
            const elementSrc = document.createElement('source');
            elementSrc.type = 'video/mp4';
            elementSrc.src = url;
            element.appendChild(elementSrc);
          }

          element.addEventListener('loadedmetadata', () => {
            clearTimeout(timer);
            return cb(null, {
              width: element.videoWidth,
              height: element.videoHeight,
              duration: parseInt(element.duration)
            })
          });

          break;
        case 'audio':

          if(!element){
            element = document.createElement('audio');
            const elementSrc = document.createElement('source');
            elementSrc.type = params.file.type;
            elementSrc.src = url;
            element.appendChild(elementSrc);
          }

          element.addEventListener('loadedmetadata', () => {
            clearTimeout(timer);
            return cb(null, {
              duration: parseInt(element.duration)
            })
          });

          break;
      }
    }

    _readFile(file, cb){
      const reader = new FileReader();

      reader.onloadend = (e) => {

        if(file.type.indexOf('image/jpeg') > -1 && this._getExifOrientation(e.target.result)){
          return cb(null, null, true);
        }

        return cb(null, this._createObjectUrl(e.target.result, file.type));
      };

      file && reader.readAsArrayBuffer(file);
    }

    _createObjectUrl(arrayBuffer, mimeType){
      const blob = new Blob([arrayBuffer], {type: mimeType});
      return (URL || webkitURL).createObjectURL(blob);
    }

    _revokeObjectUrl(url){
      (URL || webkitURL).revokeObjectURL(url);
    }

    _getExifOrientation(arrayBuffer){
      if(!arrayBuffer){
        return null;
      }

      var parser = require('exif-parser').create(arrayBuffer);

      var result = parser.parse();

      return result && result.tags.Orientation;
    }

    _uploadFile(params, cb){
      const formData = new FormData();
      formData.append('file', params.file);
      if(params.autoOrient){
        formData.append('autoOrientImage', params.autoOrient);
      }

      this.uploadRequest = request
        .post('/media')
        .send(formData)
        .on('progress', (e) => params.onTrackUploadProgress && params.onTrackUploadProgress(e))
        .end(cb);
    }

    handleMediaPreview(params, cb){

      const apiConfig = this.context.apiConfig;

      // if we already have the preview
      if(params && params.file){
        return cb(null, params);
      }


      const files = params.event.dataTransfer && params.event.dataTransfer.files || params.event.target.files;
      params.file = lodash.first(files);
      params.event.preventDefault();
      params.event.stopPropagation();

      if(!params.file){
        return cb();
      }


      // max file size checker
      const fileType = params.file.type.split('/').shift();

      switch(fileType){
        case 'video':
          if(params.file.size > apiConfig.maxVideoUploadSizeBytes){
            return cb(`Max video size exceeded "${apiConfig.maxVideoUploadSizeBytes / 1024 / 1024}mb"`);
          }
          break;
        case 'image':
          if(params.file.size > apiConfig.maxImageUploadSizeBytes){
            return cb(`Max image size exceeded "${apiConfig.maxImageUploadSizeBytes / 1024 / 1024}mb"`);
          }
          break;
        case 'audio':
          if(params.file.size > apiConfig.maxAudioUploadSizeBytes){
            return cb(`Max audio size exceeded "${apiConfig.maxAudioUploadSizeBytes / 1024 / 1024}mb"`);
          }
          break;
      }

      // file type checker
      if(params.file.type && params.typesAllowed
        && !lodash.contains(params.typesAllowed, params.file.type) || params.file.type.length < 3){
        return cb(`Type of data "${params.file.type}" is not allowed`);
      }

      if(params.file.name.split('.').pop().toLowerCase() === 'mov'){
        return cb(null, params);
      }

      // stops preview getter for mobile browsers
      const isMobile = !!new MobileDetect(navigator.userAgent).mobile();
      if(isMobile && ['video', 'audio'].indexOf(params.file.type.split('/').shift()) > -1 ){
          return cb(null, params);
      }

      this._readFile(params.file, (error, previewUrl, autoOrient) => {
        if(!previewUrl){
          params.autoOrient = autoOrient;

          return cb(null, params);
        }

        params.previewUrl = previewUrl;
        this.previewUrl = previewUrl;

        this.handleMediaAttributes(params, (err, attributes) => {
          if(err){
            return cb(err);
          }

          params.media = {
            attributes,
            mimeType: params.file.type,
            mediaUrl: previewUrl
          };

          cb && cb(null, params);
        });
      });
    }

    handleMediaUpload(params, cb){
      this.handleMediaPreview(params, ( err, params ) => {

        if(err){
          return cb(error);
        }

        this._uploadFile(params, ( err, uploadedMedia ) => {
          if(err){
            return cb(error);
          }

          params.media = uploadedMedia.body;

          this.handleMediaAttributes(params, (err, attributes) => {
            if(err){
              return cb(err);
            }
            params.media.attributes = attributes;


            cb && cb(null, params);
          });
        });
      });
    }

    handleMediaUploadCancel(){
      this.previewUrl && this._revokeObjectUrl(this.previewUrl);
      this.previewUrl = null;

      this.uploadRequest && this.uploadRequest.abort();
      this.uploadRequest = null;
    }

    getMedia(nameString){
      return lodash.get(this.state, nameString);
    }

    render(){
      return (
        <Component
          {...this.props}
          getMedia={ this.getMedia }
          onMediaPreview={ this.handleMediaPreview }
          onMediaUpload={ this.handleMediaUpload }
          onMediaAttributes={ this.handleMediaAttributes }
          onMediaUploadCancel= { this.handleMediaUploadCancel }
        />
      )
    }
  }
}

export default uploader;
