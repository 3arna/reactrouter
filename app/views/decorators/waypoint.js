import React from 'react';
import ReactDom from 'react-dom';
import ExecutionEnvironment from 'react/lib/ExecutionEnvironment';

import BaseComponent from  '../BaseComponent';

function waypoint(Component){
  return class Waypoint extends BaseComponent {

    constructor(){
      super();
      this.state = {
        isInViewport: false
      };

      this._bind('_findScrollableAncestor', '_handleScroll');
    }


    componentDidMount() {
      this.waypointElement =  ReactDom.findDOMNode(this);
      this.scrollableAncestor = this._findScrollableAncestor();
      this.scrollableAncestor.addEventListener('scroll', this._handleScroll);
      window.addEventListener('resize', this._handleScroll);
    }

    componentWillUnmount(){
      const scrollable = this.scrollableAncestor;
      scrollable.removeEventListener('scroll', this.handleScroll);
      scrollable.removeEventListener('resize', this._handleScroll);
    }

    _findScrollableAncestor() {
      let node = ReactDom.findDOMNode(this);

      while (node.parentNode) {
        node = node.parentNode;

        if (node === document) {
          // This particular node does not have a computed style.
          continue;
        }

        const style = window.getComputedStyle(node);

        const overflowY = style.overflowY ||
          style.overflow;

        if (overflowY === 'auto' || overflowY === 'scroll') {
          return node;
        }
      }

      // A scrollable ancestor element was not found, which means that we need to
      // do stuff on window.
      return window;
    }


    _handleScroll(e){
      console.log('scroll')
      const scrollTop = e.target.scrollTop;

      if(this.scrollTimer){
        const isInViewport = this._isInViewport(scrollTop, this.waypointElement);

        if(isInViewport !== this.state.isInViewport){
          this.setState({isInViewport: this._isInViewport(scrollTop, this.waypointElement)});
        }

        clearTimeout(this.scrollTimer);
      }

      this.scrollTimer = setTimeout(() => {
        //console.log('finish scroll');
        const isInViewport = this._isInViewport(scrollTop, this.waypointElement);

        if(isInViewport != this.state.isInViewport){
          this.setState({isInViewport: this._isInViewport(scrollTop, this.waypointElement)});
        }

      }, 50);
    }

    _isInViewport(scrollPosition, element){
      const elemTop = element.offsetTop -200;
      const elemBottom = elemTop + element.offsetHeight;

      return scrollPosition > elemTop && scrollPosition < elemBottom;
    }

    render(){
      return <Component {...this.props} isInViewport={this.state.isInViewport} />;
    }
  }
}

export default waypoint;
