import React from 'react';
import ReactDom from 'react-dom';
import classNames from 'classnames';
import lodash from 'lodash';

import BaseComponent from '../BaseComponent';

function placeholder(MediaComponent){
  return class Placeholder extends BaseComponent {

    constructor(){
      super();
      this._bind('handleLoad', 'handleError', 'handlePlaceholderLoad');
      this.state = {
        isLoaded: false,
        placeholderUrl: false,
      };
    }

    handlePlaceholderLoad(){

      const { 
        media,

        maxMediaWidth,
        maxMediaHeight,

        placeholderIconUrl,
        placeholderIconSize,
        placeholderBgColor

      } = this.props;

      const ratio = media.attributes.width / media.attributes.height;

      // set canvas height and width
      const canvas = document.createElement('canvas');
      
      canvas.width = parseInt(maxMediaHeight*ratio) < media.attributes.width 
        ? parseInt(maxMediaHeight*ratio)
        : media.attributes.width;

      canvas.height = maxMediaHeight >= media.attributes.height
        ? this.props.media.attributes.height
        : this.props.maxMediaHeight;

      // fills the canva with background color
      const context = canvas.getContext("2d");
      context.fillStyle = placeholderBgColor;
      context.fillRect(0, 0, canvas.width, canvas.height);
      
      // loads and sets state of new placeholder image url
      const img = new Image();
      
      img.onload = () => {
        context.drawImage(
          img, 
          (canvas.width/2)-(placeholderIconSize/2), 
          (canvas.height/2)-(placeholderIconSize/2), 
          placeholderIconSize, 
          placeholderIconSize
        );
        
        return this.setState({placeholderUrl: canvas.toDataURL()})
      }
      
      img.src = placeholderIconUrl;
    }

    handleLoad(){
      this.setState({isLoaded: true});
    }

    handleError(){
      console.log('image loading error');
    }

    render(){
      return <MediaComponent 
        onLoadPlaceholder={ this.handlePlaceholderLoad }
        onLoad={ this.handleLoad } 
        onError={ this.handleError }
        isLoaded={ this.state.isLoaded }
        placeholderUrl={ this.state.placeholderUrl }
        {...this.props} />
    }

    static defaultProps = {
      maxMediaHeight: 500,
      maxMediaWidth: 800,
      placeholderIconSize: 300,
      placeholderIconUrl: '/img/placeholder-image.png',
      placeholderBgColor: '#f2f2f2',
    }
  }
}

export default placeholder;
