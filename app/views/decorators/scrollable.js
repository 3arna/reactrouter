import React from 'react';
import ReactDom from 'react-dom';
import lodash from 'lodash';

function scrollable(Component){
  return class Scrollable extends React.Component {

    constructor(){
      super();
      this.handleScroll = this.handleScroll.bind(this);
      this.handleSetScrollData = this.handleSetScrollData.bind(this);
      this.handleSetScrollPosition = this.handleSetScrollPosition.bind(this);
      this.handleSmoothScrollToTop = this.handleSmoothScrollToTop.bind(this);

      this.state = {
        scrollTop: 0,
        scrolling: false,
        scrollData: {}
      };

      Component.prototype.setScrollData = this.setScrollData;
      Component.propTypes = {
        onScroll: React.PropTypes.func,
        onScrollEnd: React.PropTypes.func
      }
    }

    componentDidMount(){
      this.attachScrollListener();
    }

    componentWillUnmount(){
      this.removeScrollListener();

      if(this.scrollTimer){
        clearTimeout(this.scrollTimer);
        this.scrollTimer = null;
      }
    }

    attachScrollListener(){
      const scrollable = ReactDom.findDOMNode(this);
      scrollable.addEventListener('scroll', lodash.throttle(this.handleScroll, 10 ));
      this._scrollListenerAttached = true;
    }

    removeScrollListener(){
      const scrollable = ReactDom.findDOMNode(this);
      scrollable.removeEventListener('scroll', this.handleScroll);
      this._scrollListenerAttached = false;
    }

    handleScroll(e){
      //console.log('handle scroll')
      const scrollTop = e.target.scrollTop;

      if(this.scrollTimer){
        if(typeof this.props.onScroll === 'function') {
          this.props.onScroll(scrollTop, this.state.scrollData);
        }

        if(scrollTop === 0 && this.state.scrollTop > 0){
          //console.log('back to top')
          if(typeof this.props.onScrolledToTop === 'function') {
            this.props.onScrolledToTop();
          }
        }

        if( scrollTop > (e.target.scrollHeight - e.target.offsetHeight - 150))
        {
          if(typeof this.props.onScrolledToBottom === 'function' && !this._scrolledToBottom) {
            this.props.onScrolledToBottom();
            this._scrolledToBottom = true;
          }
        }
        else{
          this._scrolledToBottom = false;
        }

        !this.supressNextScrollEvent && this.setState({ scrollTop: scrollTop });

        clearTimeout(this.scrollTimer);
      }
      else{
        //console.log('scroll start');
        !this.supressNextScrollEvent && this.setState({ scrolling: true })
        if(!this.supressNextScrollEvent && typeof this.props.onScrollStart === 'function') {
          this.props.onScrollStart(scrollTop, this.state.scrollData);
        }
      }

      this.scrollTimer = setTimeout(() => {

        //console.log('finish scroll');
        !this.supressNextScrollEvent && this.setState({ scrollTop, scrolling: false });
        this.scrollTimer = null;
        //console.log('finish scroll', scrollTop)

        if(!this.supressNextScrollEvent && typeof this.props.onScrollEnd === 'function') {
          //console.log('on scrollend', this.supressNextScrollEvent)
          this.props.onScrollEnd(scrollTop, this.state.scrollData);
        }
        //console.log('scroll event reactivated');
        this.supressNextScrollEvent = false;
      }, 100);
    }

    handleSetScrollPosition(options){

      const { scrollPosition, center, supressScrollEvent } = options;

      //console.log('scrollable setting scroll position', options);

      if(!scrollPosition && scrollPosition !== 0){
        return;
      }

      console.log('set scroll pos')

      if(supressScrollEvent){
        this.supressNextScrollEvent = true;
      }

      const scrollable = ReactDom.findDOMNode(this);
      const scrollTop = center ? scrollPosition - (scrollable.clientHeight / 3) : scrollPosition;
      //console.log('setting scrolltop', scrollTop);
      scrollable.scrollTop = scrollTop;
      this.setState({ scrollTop, scrolling: false });
    }

    handleSmoothScrollToTop(){
      const scrollable = ReactDom.findDOMNode(this);
      let timeOut;

      this.supressNextScrollEvent = true;

      scrollable.scrollTop = 0;
    }

    handleSetScrollData(data){
      //console.log('setting scroll data', data);
      this.setState({ scrollData: data });
    }

    render(){
      return <Component
        { ...this.props }
        scrollTop={ this.state.scrollTop }
        scrolling={ this.state.scrolling }
        scrollData={ this.state.scrollData }
        onSetScrollPosition={ this.handleSetScrollPosition }
        onSetScrollData={ this.handleSetScrollData }
        onSmoothScrollToTop={ this.handleSmoothScrollToTop }
      />;
    }
  }
}

export default scrollable;
