import React from 'react';
import { IndexRoute, Route } from 'react-router';
import Application from './Application';

import loginActions from '../actions/loginActions';
import contentActions from '../actions/contentActions';

import HomeContainer from './pages/home/HomeContainer';
import LoginContainer from './pages/login/LoginContainer';
import RegisterContainer from './pages/register/RegisterContainer';
import ForgotPasswordContainer from './pages/forgotPassword/ForgotPasswordContainer';
import ChangePasswordContainer from './pages/changePassword/ChangePasswordContainer';

import About from './pages/About';
import PrivacyPolicy from './pages/privacyPolicy/PrivacyPolicy';
import Terms from './pages/terms/Terms';
import WhatIsVersy from './pages/WhatIsVersy';

import Loading from './pages/Loading';
import ErrorPage from './pages/ErrorPage';
import AccessDenied from './pages/AccessDenied';
import NotFound from './pages/NotFound';
import BrowserNotSupportedPage from './pages/BrowserNotSupportedPage';
import VersyStreamContainer from './pages/versyStream/VersyStreamContainer';
import ContentContainer from './pages/content/ContentContainer';
import ContentFeedContainer from './pages/contentFeed/ContentFeedContainer';

export default function getRoutes(context){
  const routes = (
    <Route name="app" path="/" component={Application}>
      <IndexRoute name="home" component={HomeContainer} />
      <Route name="login" path="login" component={LoginContainer} />
      <Route name="register" path="register" component={RegisterContainer} />
      <Route name="forgotPassword" path="forgot-password" component={ForgotPasswordContainer} />
      <Route name="changePassword" path="reset-password" component={ChangePasswordContainer} />

      <Route name="about" path="about" component={About} />
      <Route name="privacyPolicy" path="privacy-policy" component={PrivacyPolicy} />
      <Route name="terms" path="terms" component={Terms} />
      <Route name="whatIsVersy" path="what-is-versy" component={WhatIsVersy} />
      <Route name="error" path="error" component={ErrorPage} />
      <Route name="accessDenied" path="access-denied" component={AccessDenied} />
      <Route name="notFound" path="not-found" component={NotFound} />

      <Route name="versyStream" path="/feed/versy" component={VersyStreamContainer} onEnter={function(nextState, redirect, callback){
          context.executeAction(contentActions.getVersyStream, nextState, callback);
        }}
      />

      <Route name="content" path="/content/:parentId(/:title)" component={ContentContainer} onEnter={function(nextState, redirect, callback){
          context.executeAction(contentActions.getContent, nextState, callback);
        }}
      />

      <Route name="contentFeed" path="/feed/:parentId(/:title)" component={ContentFeedContainer} onEnter={function(nextState, redirect, callback){
          console.log('wtf', nextState);
          context.executeAction(contentActions.getContentFeed, nextState, callback);
        }}
      />

      <Route name="loginFacebook" path="/login/facebook" component={Loading} onEnter={function(nextState, redirect, callback){
          context.executeAction(loginActions.getFacebookCode, {}, callback);
        }}
      />
      <Route name="loginGoogle" path="/login/google" component={Loading} onEnter={false && loginActions.getGoogleCode}/>
      <Route name="loginTwitter" path="/login/twitter" component={Loading} onEnter={false && loginActions.getTwitterCode}/>

      <Route name="authFacebook" path="/auth/facebook" component={Loading} onEnter={function(nextState, redirect, callback){
          context.executeAction(loginActions.getFacebookToken, nextState.location, callback);
        }}
      />
      <Route name="authGoogle" path="/auth/google" component={Loading} onEnter={false && loginActions.getGoogleToken}/>
      <Route name="authTwitter" path="/auth/twitter" component={Loading} onEnter={false && loginActions.getTwitterToken}/>

      <Route name="logout" path="/logout" component={Loading} onEnter={false && loginActions.logout}/>

      <Route path="*" component={NotFound}/>

    </Route>
  );

  return routes;
}

