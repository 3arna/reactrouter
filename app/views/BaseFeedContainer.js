import React from 'react';
import { connectToStores, provideContext } from 'fluxible-addons-react';

import { checkForNewContent, getSubcontentData } from '../actions/contentActions';
import { setFeedScrollPosition, clearReplies } from '../actions/component/contentFeedActions';

import BaseComponent from './BaseComponent';
import ContentType from '../enums/ContentType';

class BaseFeedContainer extends BaseComponent {
  constructor(){
    super();

    this._bind(
      'handleContentFeedScrolled',
      'handleLoadOlderReplies',
      'handleLoadNewReplies',
      'handleCheckForNewContent',
      'handleSetContextAction'
    );
  }

  handleContentFeedScrolled(scrollPosition, { contentInViewport }){
    const props = this.props;

    context.executeAction(setFeedScrollPosition, { scrollPosition, contentInViewport: contentInViewport });

    if(!contentInViewport){
      return;
    }

    if(contentInViewport && contentInViewport.isNotification && !contentInViewport.feedContentRepliesEnabled){
      return context.executeAction(clearReplies);
    }

    const actualContentId = contentInViewport.isNotification
      ? contentInViewport.feedContent.id
      : contentInViewport.id;


    const shouldUpdateSubcontent =
      !this.isMobile &&
      (!props.subContentPage || (props.subContentPage && props.subContentPage.parentContentId != actualContentId)) &&
      !props.feedLoadingMore &&
      contentInViewport;

    if(shouldUpdateSubcontent){
      context.executeAction(getSubcontentData, { parentContentId: actualContentId});
    }
  }

  handleCheckForNewContent(parentContentId, pollToken, isSubContent){
    context.executeAction(checkForNewContent, { parentContentId, pollToken, isSubContent });
  }

  handleLoadNewReplies(e){
    e.preventDefault();
    context.executeAction(getSubcontentData, { parentContentId: this.props.subContentPage.parentContentId, newContent: true });
  }

  handleLoadOlderReplies(){
    const subContentPage = this.props.subContentPage;
    if(!subContentPage){
      return;
    }

    if(subContentPage.pageMin === 1 || this.props.subContentLoadingMore){
      return;
    }

    context.executeAction(getSubcontentData, {
      parentContentId: subContentPage.parentContentId,
      needRefresh: true,
      sequence: subContentPage.pageMin -1
    });
  }

  handleSetContextAction(contextMenuActionObject){
    this.setState({ contextMenuActionObject });
  }

  get isMobile(){
    return window && window.innerWidth <= 768;
  }

  get selectedContentId(){
    return this.props.selectedContentId && this.props.selectedContentId.replace('%3A', ':');
  }

  static propTypes = {
    bookmarks: React.PropTypes.array,
    contentFormVisible: React.PropTypes.bool,
    feedLoading: React.PropTypes.bool,
    feedLoadingMore: React.PropTypes.bool,
    feedContentPage: React.PropTypes.object,
    feedScrollPosition: React.PropTypes.object,
    interests: React.PropTypes.array,
    previousFeedContentPage: React.PropTypes.object,
    selectedContent: React.PropTypes.object,
    selectedContentId: React.PropTypes.string,
    subContentLoading: React.PropTypes.bool,
    subContentLoadingMore: React.PropTypes.bool,
    subContentError: React.PropTypes.string,
    subContentPage: React.PropTypes.object,
    subContentReplyTo: React.PropTypes.string,
    user: React.PropTypes.object,
  };

  static stores = [
    'ContentFeedStore',
    'ContentFormStore',
    'ApplicationStore',
    'UserStore',
  ];

  static initProps = (context, props) => {
    const ContentFeedStore = context.getStore('ContentFeedStore');
    const ContentFormStore = context.getStore('ContentFormStore');
    const ApplicationStore = context.getStore('ApplicationStore');
    const UserStore = context.getStore('UserStore');

    return {
      bookmarks: UserStore.bookmarks,
      contentFormVisible: ContentFormStore.formVisible,
      feedLoading: ContentFeedStore.feedLoading,
      feedLoadingMore: ContentFeedStore.feedLoadingMore,
      feedContentPage: ContentFeedStore.getFeedContentPage(),
      feedScrollPosition: ContentFeedStore.getFeedScrollPosition(),
      interests: UserStore.interests,
      newFeedContentAvailable: ContentFeedStore.newFeedContentAvailable,
      newSubContentAvailable: ContentFeedStore.newSubContentAvailable,
      previousFeedContentPage: ContentFeedStore.getPreviousFeedContentPage(),
      feedParentContentId: ContentFeedStore.feedParentContentId,
      selectedContent: ContentFeedStore.selectedContent,
      subContentLoading: ContentFeedStore.subContentLoading,
      subContentLoadingMore: ContentFeedStore.subContentLoadingMore,
      subContentError: ContentFeedStore.subContentError,
      subContentPage: ContentFeedStore.getSubContentPage(),
      subContentReplyTo: ContentFeedStore.subContentReplyTo,
      user: UserStore.user,
    };

  }
}

export default BaseFeedContainer;
