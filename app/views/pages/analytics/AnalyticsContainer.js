import React from 'react';
import { connectToStores } from 'fluxible-addons-react';
import moment from 'moment';

import * as analyticsUtils from '../../../utils/analyticsUtils';

import Analytics from './Analytics';
import BaseComponent from '../../BaseComponent';

@connectToStores(['AnalyticsStore', 'UserStore', 'ApplicationStore'], (context) => {
  const analyticsStore = context.getStore('AnalyticsStore');
  const userStore = context.getStore('UserStore');
  const applicationStore = context.getStore('ApplicationStore');

  return {
    user: userStore.user,
    userAnalytics: analyticsStore.userAnalytics
  };
})
class AnalyticsContainer extends BaseComponent {
  constructor(){
    super();
  }

  get chartData(){
    const props = this.props;

    if(!props.userAnalytics){
      return null;
    }

    const colours = {
      purple: '#602E8D',
      purpleOpacity: 'rgba(96, 46, 141, 0.2)',
      green: '#44c19f',
      greenOpacity: 'rgba(68, 193, 159, 0.2)',
      salmon: '#f2808b',
      salmonOpacity: 'rgba(242, 128, 139, 0.2)'
    }

    const followersChartType = {
      key: 'followers',
      config: {
        aggregate: true,
        fillColor: colours.purpleOpacity,
        strokeColor: colours.purple,
        pointColor: colours.purple,
        pointStrokeColor: colours.purple
      }
    };

    const repliesChartType = {
      key: 'feed_post_replies',
      config: {
        label: 'replies',
        fillColor: colours.purpleOpacity,
        strokeColor: colours.purple,
        pointColor: colours.purple,
        pointStrokeColor: colours.purple
      }
    };

    const averageRepliesChartType = {
      key: 'feed_avg_replies',
      config: {
        label: 'average replies per post',
        fillColor: colours.salmonOpacity,
        strokeColor: colours.salmon,
        pointColor: colours.salmon,
        pointStrokeColor: colours.salmon
      }
    };

    const postsChartType = {
      key: 'feed_posts',
      config: {
        label: 'posts',
        fillColor: colours.greenOpacity,
        strokeColor: colours.green,
        pointColor: colours.green,
        pointStrokeColor: colours.green
      }
    };

    const upVotesChartType = {
      key: 'up_votes',
      config: {
        label: 'up votes',
        fillColor: colours.purple,
        strokeColor: colours.purple
      }
    };

    const downVotesChartType = {
      key: 'down_votes',
      config: {
        label: 'down votes',
        fillColor: colours.green,
        strokeColor: colours.green
      }
    };

    const sharesChartType = {
      key: 'shares',
      config: {
        fillColor: "#f3ecf9",
        strokeColor: "#602E8D",
        pointColor: "#602E8D",
        pointStrokeColor: "#602E8D"
      }
    }

    const data = {
      followers: analyticsUtils.getChartData(props.userAnalytics, [followersChartType]),
      postsAndReplies: analyticsUtils.getChartData(props.userAnalytics, [repliesChartType, postsChartType, averageRepliesChartType]),
      sentiment: analyticsUtils.getChartData(props.userAnalytics, [upVotesChartType, downVotesChartType]),
      shares: analyticsUtils.getChartData(props.userAnalytics, [sharesChartType])
    };

    return data;

  }

  get lastUpdatedDate(){
    return moment(this.props.userAnalytics.date).format('MMMM Do YYYY, h:mm:ss a');
  }

  render(){
    return <Analytics
      chartData={this.chartData}
      lastUpdated={this.lastUpdatedDate}
      topPosts={this.props.userAnalytics.posts}
      user={this.props.user}
    />
  }
}

export default AnalyticsContainer;
