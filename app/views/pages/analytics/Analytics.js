import React from 'react';

import OneColLayout from '../../layouts/OneColLayout';
import RoundAvatar from '../../components/elements/avatars/RoundAvatar';
import TopPosts from '../../components/analytics/TopPosts';

let LineChart, BarChart;

class Analytics extends React.Component {

  componentDidMount(){
    require.ensure(['react-chartjs'], () => {
      const reactChartJs = require('react-chartjs');
      LineChart = reactChartJs.Line;
      BarChart = reactChartJs.Bar;
      this.forceUpdate();
    }, 'analytics');
  }

  render() {

    const props = this.props;

    const lineChartOptions = {
      scaleLabel: ` <%= Number(value)%>`,
      multiTooltipTemplate: ' <%= datasetLabel %>: <%= value %>',
      pointDotRadius: 0,
      pointHitDetectionRadius: 5,
      responsive: true,
      scaleShowGridLines: false,
      showTooltips: true,
    };

    const barChartOptions = {
      scaleLabel: ` <%= Number(value)%>`,
      multiTooltipTemplate: ' <%= datasetLabel %>: <%= value %>',
      pointHitDetectionRadius: 5,
      responsive: true,
      scaleShowGridLines: false,
    };

    return <OneColLayout maxWidth>
      <div className="w-max-900px mrg-auto over-hide">
        <div className="txt-center w-100">
          <RoundAvatar size={100} avatarUrl={props.user.avatarUrl}/>
          <h2 className="mrg-t-0">{props.user.displayName}</h2>
          <h3>Versy Statistics</h3>
          <p>Last updated: {props.lastUpdated}</p>
        </div>
        <div className="over-hide">
          <div className="flt-left col-6 md-col-12">
            <div className="bg-white mrg-10px over-hide brd-greyl1 brd-rad-5px">
              <div className="mrg-20px txt-center">
                <h3>Followers</h3>
                {LineChart &&
                  <LineChart data={props.chartData.followers} options={lineChartOptions}/>
                }
              </div>
            </div>
          </div>
          <div className="flt-left col-6 md-col-12">
            <div className="bg-white mrg-10px over-hide brd-greyl1 brd-rad-5px">
              <div className="mrg-20px txt-center">
                <h3>Posts and Replies</h3>
                {LineChart &&
                  <LineChart data={props.chartData.postsAndReplies} options={lineChartOptions}/>
                }
              </div>
            </div>
          </div>
        </div>
        <div className="over-hide">
          <div className="flt-left  col-6 md-col-12">
            <div className="bg-white mrg-10px over-hide brd-greyl1 brd-rad-5px">
              <div className="mrg-20px txt-center">
                <h3>Sentiment</h3>
                {BarChart &&
                  <BarChart data={props.chartData.sentiment} options={barChartOptions}/>
                }
              </div>
            </div>
          </div>
          <div className="flt-left col-6 md-col-12">
            <div className="bg-white mrg-10px over-hide brd-greyl1 brd-rad-5px">
              <div className="mrg-20px txt-center">
                <h3>Shares</h3>
                {LineChart &&
                  <LineChart data={props.chartData.shares} options={lineChartOptions}/>
                }
              </div>
            </div>
          </div>
        </div>
        <div className="mrg-t-40px">
          <h3 className="mrg-b-10px txt-center">Top Performing Posts</h3>
          <div>
            <div className="flt-left col-6 md-col-12">
              <TopPosts posts={props.topPosts.replies} title="By Replies" countLabel="replies"/>
            </div>
            <div className="flt-left col-6 md-col-12">
              <TopPosts posts={props.topPosts.shares} title="By Shares" countLabel="shares"/>
            </div>
          </div>
          <div className="mrg-t-20px">
            <div className="flt-left col-6 md-col-12">
              <TopPosts posts={props.topPosts.likes} title="Most Liked" countLabel="likes"/>
            </div>
            <div className="flt-left col-6 md-col-12">
              <TopPosts posts={props.topPosts.dislikes} title="Most Disliked" countLabel="dislikes"/>
            </div>
          </div>
        </div>
      </div>

    </OneColLayout>
  }
};

export default Analytics;
