import { connectToStores, provideContext } from 'fluxible-addons-react';
import classNames from 'classnames';
import lodash from 'lodash';

import { NavLink } from 'fluxible-router';
import React from 'react';

import { addBookmark, removeBookmark, toggleBookmark } from '../../../actions/bookmarkActions';
import { getFollowingStream, getFollowersStream, getInterestsStream } from '../../../actions/bookmarkStreamActions';

import BaseComponent from '../../BaseComponent';
import BookmarkStream from './BookmarkStream';
import BookMarkStreamType from '../../../enums/BookmarkStreamType';

@connectToStores(['BookmarkStreamStore', 'UserStore', 'ApplicationStore'], (context) => {
  const bookmarkStreamStore = context.getStore('BookmarkStreamStore');
  const userStore = context.getStore('UserStore');
  const applicationStore = context.getStore('ApplicationStore');

  return {
    authenticatedUser: userStore.user,
    authenticatedUserBookmarks: userStore.bookmarks,
    streamPage: bookmarkStreamStore.streamPage,
    streamType: bookmarkStreamStore.streamType,
    isLoadingMore: bookmarkStreamStore.isLoadingMore
  };
})
class BookmarkStreamContainer extends BaseComponent {
  constructor(){
    super();

    this._bind('isFollowing', 'handleScrolledToBottom');
  }

  static contextTypes = {
    translate: React.PropTypes.func,
    executeAction: React.PropTypes.func
  };

  get heading(){
    const translate = this.context.translate;

    switch(this.props.streamType){
      case BookMarkStreamType.FOLLOWERS:
        return translate('words.followers');
      case BookMarkStreamType.FOLLOWING:
        return translate('words.following');
      case BookMarkStreamType.INTERESTS:
        return translate('words.interests');
      default:
        return null;
  }
}

  get streamItems(){

    return this.props.streamPage &&
      this.props.streamPage.entries.map(entry => ({
        id: entry.summary.id,
        name: entry.summary.name,
        description: entry.summary.description,
        avatarUrl: entry.summary.avatarUrl,
        following: this.isFollowing(entry.summary.id),
        slug: entry.summary.slug ? entry.summary.slug : '',
        type: lodash.contains(entry.summary.id, 'category') ? 'category' : 'user',
        isMe: this.props.authenticatedUser &&
          this.props.authenticatedUser.id === entry.summary.id,
      }));
  }

  get userId(){
    return this.props.streamPage && this.props.streamPage.sourceUser
      && this.props.streamPage.sourceUser.id;
  }

  isFollowing(userId){
    const { authenticatedUserBookmarks } = this.props;
    return lodash.find(authenticatedUserBookmarks, (bookmark) => bookmark.id === userId);
  }

  handleScrolledToBottom(){
    const props = this.props;

    if(props.isLoadingMore || !props.streamPage.nextState){
      return;
    }

    let action;

    switch(props.streamType){
      case BookMarkStreamType.FOLLOWERS:
        action = getFollowersStream;
        break;
      case BookMarkStreamType.FOLLOWING:
        action = getFollowingStream;
        break;
      case BookMarkStreamType.INTERESTS:
        action = getInterestsStream;
        break;
    }

    context.executeAction(action, { userId: props.userId, nextState: props.streamPage.nextState });
  }

  handleToggleBookmark(data){
    context.executeAction(toggleBookmark, data)
  }

  render(){
    const { props } = this;

    return <BookmarkStream
      user={ props.streamPage && props.streamPage.sourceUser }
      heading={ this.heading }
      streamItems={ this.streamItems }
      onScrolledToBottom={ this.handleScrolledToBottom }
      onToggleBookmark={ this.handleToggleBookmark }
      streamType={ props.streamType }
      userId={ this.userId }
      isMe={ this.userId === props.authenticatedUser.id }
    />
  }
}

export default BookmarkStreamContainer;
