import { NavLink } from 'fluxible-router';
import React from 'react';

import scrollable from '../../decorators/scrollable';

import BackButton from '../../components/elements/buttons/BackButton';
import BookMarkStreamType from '../../../enums/BookmarkStreamType';

import SearchResultLink from '../../components/elements/links/SearchResultLink';
import FollowButtonContainer from '../../components/elements/buttons/followButton/FollowButtonContainer';

@scrollable
class BookmarkStream extends React.Component {
  render(){
    const props = this.props;

    return <div className="txt-center pdg-tb-50px h-100vh over-auto over-touch">

      <div className="md-pdg-l-60px pos-fixed top-0 left-0 z-1 w-100">
        <div className="valign-block pdg-tb-10px pdg-rl-3 bg-white boxshadow-b fadeIn">
          <div className="w-min-50px md-w-auto">
            <BackButton href={`/feed/${props.userId}/${props.user.slug}`} />
          </div>
          <div className="w-100">
            <div className="w-max-700px pdg-rl-3 valign-block mrg-auto">
              <img width="30" height="30" className="brd-rad-50 mrg-r-10px" src={props.user.avatarUrl}/>
              <h3 className="pdg-0 mrg-3em txt-left w-100">{props.user.name}</h3>
            </div>
          </div>
          <div className="w-min-50px md-dsp-none"/>
        </div>
      </div>

      <div className="md-pdg-l-60px">
        <div className="txt-left mrg-auto w-max-700px w-100 pdg-rl-3">
          <h3 className="brd-b-grey pdg-b-5px mrg-t-30px">
            {props.heading}
            {props.isMe && props.streamType === BookMarkStreamType.INTERESTS &&
            <NavLink href="/interests-selector" className="flt-right clr-main fnt-xs mrg-t-5px">edit</NavLink>}
          </h3>
          <ul className="pdg-0 list-none fix-bb mrg-0" style={{ paddingBottom: '20vh'}}>
            {props.streamItems.map((item) =>
              <SearchResultLink key={ item.id } {...item}>
                {!item.isMe && <FollowButtonContainer
                  theme={item.type === 'category' && 'round' || null}
                  isBookmarked={ item.following }
                  id={ item.id }
                  onToggleBookmark={ props.onToggleBookmark }/>}
              </SearchResultLink>)}
          </ul>
        </div>
      </div>
    </div>
  }
}


BookmarkStream.propTypes = {
  heading: React.PropTypes.string.isRequired,
  streamItems: React.PropTypes.array.isRequired,
  userId: React.PropTypes.string.isRequired,
  user: React.PropTypes.object,
  onToggleBookmark: React.PropTypes.func
};

export default BookmarkStream;
