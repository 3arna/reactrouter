import React from 'react';
import lodash from 'lodash';
import { connectToStores } from 'fluxible-addons-react';
import ReactDom from 'react-dom';

//import scrollable from '../../decorators/scrollable';

import { checkForNewContent, getSubcontentData } from '../../../actions/contentActions';
import privacySelectorActions from '../../../actions/component/privacySelectorActions';

import { updateACL } from '../../../actions/privateContentActions';
import * as contentUtils from '../../../utils/contentUtils';

import BaseComponent from '../../BaseComponent';
import Content from './Content';

@connectToStores(
  ['ContentFeedStore', 'ContentFormStore', 'ApplicationStore', 'UserStore'],
  (context, props) => {

    const ApplicationStore = context.getStore('ApplicationStore');
    const ContentFeedStore = context.getStore('ContentFeedStore');
    const UserStore = context.getStore('UserStore');

    const subContentPage = ContentFeedStore.getSubContentPage();

    return {
      bookmarks: UserStore.bookmarks,
      content: ContentFeedStore.selectedContent,
      feedLoading: ContentFeedStore.feedLoading,
      feedContentPage: ContentFeedStore.getFeedContentPage(),
      feedScrollPosition: ContentFeedStore.getFeedScrollPosition(),
      following: UserStore.following,
      interests: UserStore.interests,
      previousPage: ApplicationStore.previousPage,
      previousFeedContentPage: ContentFeedStore.getPreviousFeedContentPage(),
      previousUrl: ApplicationStore.getPreviousUrl(),
      newSubContentAvailable: ContentFeedStore.newSubContentAvailable,
      subContentLoading: ContentFeedStore.subContentLoading,
      subContentLoadingMore: ContentFeedStore.subContentLoadingMore,
      subContentError: ContentFeedStore.subContentError,
      subContentPage: subContentPage,
      subContentReplyTo: ContentFeedStore.subContentReplyTo,
      user: UserStore.user,
    };

  }
)

//@scrollable

class ContentContainer extends BaseComponent{
  constructor(){
    super();
    this._bind('handleLoadOlderReplies', 'handleLoadNewReplies', 'handleTogglePrivacySelector');

    this.state = {
      privacySelectorOpen: false
    }
  }

  /*componentDidUpdate(prevProps){

    const contentWrapper = ReactDom.findDOMNode(this);

    if(prevProps.subContentPage && this.props.subContentPage
      && prevProps.subContentPage.content.length !== this.props.subContentPage.content.length
      && contentWrapper.scrollHeight - window.scrollY - window.outerHeight < 100){
      window.scroll(0, contentWrapper.scrollHeight);
    }

  }*/

  get backHref(){
    const props = this.props;
    if(!props.subContentPage || !props.content){
      return;
    }

    if(props.previousPage && lodash.includes(props.previousPage, 'analytics')){
      return props.previousPage;
    }

    //TODO Refactor this

    if(props.subContentPage.ancestorId && (!props.previousUrl || (props.previousUrl && props.previousUrl.indexOf('/feed')))){
      return `/content/${props.subContentPage.ancestorId}/${props.subContentPage.ancestor.slug}`;
    }

    if(props.previousFeedContentPage){
      if(props.previousFeedContentPage.isProfile){
        return `/feed/${props.previousFeedContentPage.parentContentId}/${props.previousFeedContentPage.ultimateParent.slug}`;
      }

      let parentFeedIdentifier = props.previousFeedContentPage.parentContentId;
      if(lodash.contains(parentFeedIdentifier, 'versy')){
        parentFeedIdentifier = 'versy';
      }

      return `/feed/${parentFeedIdentifier}/${props.previousFeedContentPage.filter ? props.previousFeedContentPage.filter : ''}`
    }

    return `/feed/${props.content.author.id}/${props.content.author.slug}`;
  }

  handleCheckForNewContent(parentContentId, pollToken, isSubContent){
    context.executeAction(checkForNewContent, { parentContentId, pollToken, isSubContent });
  }

  handleLoadNewReplies(e){
    e.preventDefault();
    //console.log('load new replies');
    context.executeAction(getSubcontentData, { parentContentId: this.props.subContentPage.parentContentId, newContent: true });
  }

  handleLoadOlderReplies(){
    const subContentPage = this.props.subContentPage;
    if(subContentPage.pageMin === 1 || this.props.subContentLoadingMore){
      return;
    }

    context.executeAction(getSubcontentData, {
      parentContentId: subContentPage.parentContentId,
      needRefresh: true,
      sequence: subContentPage.pageMin -1
    });
  }

  handleTogglePrivacySelector(contentId, isMine){
    context.executeAction(privacySelectorActions.toggleOpen, {
      contentId: contentId,
      isPrivate: true,
      isReadOnly: !isMine
    });
  }


  get isMine(){
    const { subContentPage, user} = this.props;

    if(!subContentPage || !user){
      return false;
    }

    if(user && user.id === subContentPage.ultimateParent.id){
      return true;
    }

    return false;
  }

  get metaTags(){
    return contentUtils.getContentMetaTags(this.props.content, this.context.apiConfig.imageResizeEndpoint);
  }

  get pageTitle(){
    return contentUtils.getPageTitle(this.props.content);
  }

  render(){
    const props = this.props;
    const parentId = props.params.parentId.replace('%3A', ':');

    return <Content
      backHref={ this.backHref }
      bookmarks={ props.bookmarks }
      content={ props.content }
      contextMenuVisible={ props.contextMenuVisible }
      contextMenuActiveContentId={ props.contextMenuActiveContentId }
      contextMenuActiveType={ props.contextMenuActiveType }
      following={ props.following }
      isMine={ this.isMine }
      loading={ !props.content || props.content && props.content.id !== parentId }
      metaTags={ this.metaTags }
      newSubContentAvailable={ props.newSubContentAvailable }
      onCheckForNewContent={ this.handleCheckForNewContent }
      onLoadNewReplies={ this.handleLoadNewReplies }
      onLoadOlderReplies={ this.handleLoadOlderReplies }
      onRepliesScrolledToBottom={ this.handleRepliesScrolledToBottom }
      onTogglePrivacySelector={ this.handleTogglePrivacySelector }
      pageTitle={ this.pageTitle }
      parentId={ parentId }
      privacySelectorOpen={ this.state.privacySelectorOpen }
      previousFeedContentPage={ props.previousFeedContentPage }
      subContentError={ props.subContentError }
      subContentLoading={ props.subContentLoading }
      subContentLoadingMore={ props.subContentLoadingMore }
      subContentPage={ props.subContentPage }
      subContentReplyTo={ props.subContentReplyTo }
      user={ props.user }
    />
  }

  static contextTypes = {
    apiConfig: React.PropTypes.object
  }
}

export default ContentContainer;
