import React from 'react';
import Portal from 'react-portal';
import DocMeta from 'react-doc-meta';
import DocTitle from 'react-document-title';

import PrivacySelectorContainer from '../../components/selectors/privacySelector/PrivacySelectorContainer';
import ContentCardContainer from '../../components/contentFeed/contentCard/ContentCardContainer';
import SubContentFeedScroller from '../../components/subContent/SubContentFeedScroller';


import TwoColLayout from '../../layouts/TwoColLayout';

const Content = (props) =>
  <DocTitle title={props.pageTitle}>
    <TwoColLayout parentContent={props.content} isMobile isDesktop={!!props.subContentPage} >

    {props.loading &&
      <div className="txt-center pos-relative w-100 h-100">
        <i className="pos-absolute top-45 loading-spinner w-min-60px h-min-60px" style={{marginLeft: '-30px'}}/>
      </div>
    }

    <PrivacySelectorContainer
      recipientLimit={ 25 }
    />

    {!props.loading &&
      <DocMeta tags={props.metaTags} />
    }

    {!props.loading &&
      <ContentCardContainer
        backHref={props.backHref}
        bookmarks={props.bookmarks}
        content={props.content}
        parentId={props.parentId}
        isMe={props.isMe}
        categories={props.categories}
        isExtended={true}
        isMine={ props.isMine }
        onLoadOlderReplies={props.onLoadOlderReplies}
        onTogglePrivacySelector={props.onTogglePrivacySelector}
        isLoadingOlderReplies={props.subContentLoadingMore}
        replies={props.subContentPage && props.subContentPage.content}
        showLoadOlderRepliesButton={props.subContentPage && props.subContentPage.pageMin > 1}
        contentContextMenuVisible={props.contextMenuVisible && props.contextMenuActiveContentId === content.id}
        user={props.user}
      />
    }

    <SubContentFeedScroller
      allowPosting={ true }
      bookmarks={ props.bookmarks }
      loading={ props.subContentLoading }
      loadingMore={ props.subContentLoadingMore }
      newContentAvailable={ props.newSubContentAvailable }
      onCheckForNewContent={ props.onCheckForNewContent }
      onLoadNewContent={ props.onLoadNewReplies }
      onScrolledToTop={ props.onLoadOlderReplies }
      parentFeedId={ props.parentId }
      replyTo={ props.subContentReplyTo }
      subContentError={ props.subContentError }
      subContentPage={ props.subContentPage }
      selectedContentId={ props.subContentPage && props.subContentPage.parentContentId || props.selectedContentId }
      user={ props.user }
      contextMenuVisible={ props.contextMenuVisible }
      contextMenuActiveContentId={ props.contextMenuActiveContentId }
      contextMenuActiveType={ props.contextMenuActiveType }
    />

  </TwoColLayout>
</DocTitle>

Content.propTypes = {
  bookmarks: React.PropTypes.array,
  feedContentPage: React.PropTypes.object,
  feedScrollPosition: React.PropTypes.object,
  isMine: React.PropTypes.bool,
  onScrolledToBottom: React.PropTypes.func,
  onRepliesScrolledToBottom: React.PropTypes.func,
  parentId: React.PropTypes.string.isRequired,
  previousFeedContentPage: React.PropTypes.object,
  selectedContent: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.object,
  ]),
  selectedContentId: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.string,
  ]),
  subContentLoading: React.PropTypes.bool,
  subContentPage: React.PropTypes.object,
  subContentError: React.PropTypes.oneOfType([
    React.PropTypes.object,
    React.PropTypes.string,
  ]),
  subContentReplyTo: React.PropTypes.string,
  contextMenuVisible: React.PropTypes.bool,
  contextMenuActiveContentId: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.string,
  ]),
  contextMenuActiveType: React.PropTypes.string,
  user: React.PropTypes.object,
};

export default Content;
