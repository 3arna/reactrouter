import React from 'react';
import { NavLink, navigateAction } from 'fluxible-router';

import Process from '../../../models/Process';
import Category from '../../../models/Category';

import LanguageDropdown from '../../components/elements/dropdowns/LanguageDropdown';
import CategorySelectorContainer from '../../components/selectors/categorySelector/CategorySelectorContainer';
import DefaultFrontButton from '../../components/elements/buttons/SubmitButton';
import InProgressFog from '../../components/elements/fogs/InProgressFog';

import BackButton from '../../components/elements/buttons/BackButton';

const Settings = (props, {translate}) => {
  const {
    process,
    interests,
    searchedCategories,
    categories,
    categoriesChanged,
    selectedCategories,
    onCategoriesStore,
    onCategoriesSearch,
    onSubmit
  } = props;

  return <div className="clr-blackl1 pdg-t-50px bg-greyl2 h-min-100vh">
    <div className="md-pdg-l-60px pos-fixed top-0 left-0 z-1 w-100">
      <div className="valign-block fix-bb pdg-tb-10px pdg-rl-3 w-100 bg-white boxshadow-b fadeIn">
        <div className="w-min-50px md-w-auto">
          <BackButton href={`/${props.user.id}/interests`} />
        </div>
        <div className="w-100">
          <div className="w-max-700px pdg-rl-3 txt-left valign-block mrg-auto">
            <img width="30" height="30" className="brd-rad-50 mrg-r-10px dsp-inline" src={props.user.avatarUrl}/>
            <h3 className="pdg-0 mrg-3em w-100">{props.user.displayName}</h3>
          </div>
        </div>
        <div className="w-min-50px">
          <span className="clr-main crs-point dsp-inline" isDisabled={!categoriesChanged} onClick={onSubmit}>
            {translate('words.save')}
          </span>
        </div>
      </div>
    </div>

    {process.inProgress && <InProgressFog message={translate('phrases.savingInterests')}/>}
    <div className="md-pdg-l-60px">
      <div className="w-max-700px mrg-auto pdg-rl-3">

        <h3 className="pdg-b-5px mrg-0 mrg-t-30px">
          Interests
        </h3>

         <CategorySelectorContainer
          interests={interests}
          searchedCategories={searchedCategories}
          categories={categories}
          selectedCategories={selectedCategories}
          searchCategories={onCategoriesSearch}
          returnSelectedCategories={onCategoriesStore}
          placeholderText={translate('phrases.whatDoYouLike')}
          componentTitle={translate('phrases.selectInterests')}
          textCategoryNotExist={translate('phrases.categoryDoesNotExists')}
          messageMaxReached={translate('phrases.categoryMaxLimitReached')}
          messageAlreadyAdded={translate('phrases.categoryAlreadyAdded')}/>
      </div>
    </div>

  </div>
}

Settings.contextTypes = {
  translate: React.PropTypes.func
};

Settings.propTypes = {
  process: Process.propTypes.isRequired,

  interests: React.PropTypes.arrayOf(Category.propTypes),
  searchedCategories: React.PropTypes.oneOfType([
    React.PropTypes.arrayOf(Category.propTypes),
    React.PropTypes.bool,
  ]),
  categories: React.PropTypes.arrayOf(Category.propTypes),
  selectedCategories: React.PropTypes.arrayOf(React.PropTypes.string),
  categoriesChanged: React.PropTypes.bool,
  onSubmit: React.PropTypes.func,
  onCategoriesStore: React.PropTypes.func,
  onCategoriesSearch: React.PropTypes.func,
};

export default Settings;
