import React from 'react';
import lodash from 'lodash';
import { connectToStores, provideContext } from 'fluxible-addons-react';

import Process from '../../../models/Process';
import Category from '../../../models/Category';

import BaseComponent from '../../BaseComponent';
import InterestsSelector from './InterestsSelector';

import { toastProcessError } from '../../../actions/applicationActions';
import { getSearchedCategories, bookmarkCategories } from '../../../actions/categoryActions';


@connectToStores(['ApplicationStore', 'UserStore', 'CategoryStore'], (context, props) => {
  const applicationStore = context.getStore('ApplicationStore');
  const userStore = context.getStore('UserStore');
  const categoryStore = context.getStore('CategoryStore');
  return {
    categories: categoryStore.categories,
    searchedCategories: categoryStore.searchedCategories,
    interests: userStore.interests,
    process: applicationStore.processGet('interestsSelector'),
    user: userStore.user,
  };
})

export default class InterestsSelectorContainer extends BaseComponent {

  constructor(){
    super();
    this._bind('handleCategoriesStore', 'handleCategoriesSearch', 'handleSubmit');
    this.state = { selectedCategories: [] };
  }

  handleCategoriesStore(selectedCategories){
    this.setState({selectedCategories})
  }

  handleCategoriesSearch(e){
    context.executeAction(getSearchedCategories, { query: e.target.value })
  }

  handleSubmit(){
    const componentContext = context.getComponentContext();
    if(this.state.selectedCategories.length < componentContext.apiConfig.minimumFirstUseCategories){
      return this.context.executeAction(toastProcessError, 
        context.getComponentContext().translate('phrases.selectAtLeastInterests', {
          minLength: componentContext.apiConfig.minimumFirstUseCategories
        }));
    }

    context.executeAction(bookmarkCategories, {
      process: this.props.process,
      selectedCategories: this.state.selectedCategories,
      redirectTo: `/${this.props.user.id}/interests`,
    })
  }

  get categoriesChanged(){
    return true;
    /*const interests = this.props.interests.map((interest) => interest.name);
    return !lodash.isEqual(interests.sort(), this.state.selectedCategories.sort());*/
  }

  render(){
    
    const { props, state } = this;
    
    return <InterestsSelector
      user={props.user}
      process={props.process}
      interests={props.interests}
      searchedCategories={props.searchedCategories}
      categories={props.categories}
      categoriesChanged={this.categoriesChanged}
      selectedCategories={state.selectedCategories}
      onCategoriesStore={this.handleCategoriesStore}
      onCategoriesSearch={this.handleCategoriesSearch}
      onSubmit={this.handleSubmit}/>
  }

  static contextTypes = {
    apiConfig: React.PropTypes.object,
    translate: React.PropTypes.func,
    executeAction: React.PropTypes.func,
  };
  
  static propTypes = {
    process: Process.propTypes.isRequired,

    interests: React.PropTypes.arrayOf(Category.propTypes),
    searchedCategories: React.PropTypes.oneOfType([
      React.PropTypes.arrayOf(Category.propTypes),
      React.PropTypes.bool,
    ]),
    categories: React.PropTypes.arrayOf(Category.propTypes),
  };
}
