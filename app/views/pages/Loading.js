import React from 'react';

const Loading = () =>
  <div className="tb txt-center w-100 h-100vh">
    <div className="td">
      <i className="dsp-inline loading-spinner w-min-60px h-min-60px"/>
    </div>
  </div>

export default Loading;

