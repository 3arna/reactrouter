import React, { PropTypes } from "react";
import DefaultButton from '../components/elements/buttons/DefaultButton';

const BrowserNotSupportedPage = ({err, currentUrl}, {authenticatedUser, translate }) =>
  <div className="txt-center clr-red md-pdg-l-60px h-min-100 valign-block">
    <div>
      <div className="pos-relative clr-greyd3 fnt-lg">
        <div className="fnt-xxxl">
          <h1 className="mrg-0 pdg-0 fnt-xxxl clr-greyl1 pos-relative">{ translate('words.oops')}</h1>
        </div>
        <p className="mrg-0 pdg-0 mrg-b-20px pos-absolute top-40 fnt-bold w-100">{ translate('phrases.versyDoesNotSupportThisBrowser') }</p>
        { process.env.NODE_ENV !== "production" && err && <pre align="center">{ err.message }</pre>}
      </div>
    </div>
  </div>

BrowserNotSupportedPage.contextTypes = {
  authenticatedUser: React.PropTypes.object,
  translate: React.PropTypes.func
};

export default BrowserNotSupportedPage;
