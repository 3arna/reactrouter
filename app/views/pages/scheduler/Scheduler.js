import React from 'react';
import DocMeta from 'react-doc-meta';

import OneColLayout from '../../layouts/OneColLayout';
import ContentCard from '../../components/contentFeed/contentCard/ContentCardContainer';
import RoundPostButton from '../../components/elements/buttons/RoundPostButton';
import ContentFeedFormContainer from '../../components/forms/contentFeedForm/ContentFeedFormContainer';

const Scheduler = (props, {translate, authenticatedUser}) => {

  return <OneColLayout noHeader maxWidth>
    <div className="h-100vh over-auto scrollbar-none pdg-b-10">
      <div className="w-max-700px mrg-auto">
        <h3 className="mrg-0 pdg-t-40px pdg-b-10px clr-blackl2">
          <i className="icon-clock mrg-r-10px valign-bot"/>{translate('phrases.scheduledPosts')}
        </h3>

        {props.tasks.map((task) => 
          <ContentCard 
            task={task}
            content={task.content}
            categories={props.categories}
            key={task.id}
            user={authenticatedUser} />)}
      </div>
    </div>

    
    <ContentFeedFormContainer isFullScreen isMobile={false} parentContent={props.parentContent}/>
    <RoundPostButton isFullScreen onClick={props.onToggleForm}/>

  </OneColLayout>
}

Scheduler.contextTypes = {
  translate: React.PropTypes.func,
  authenticatedUser: React.PropTypes.object,
};

Scheduler.propTypes = {};

export default Scheduler;
