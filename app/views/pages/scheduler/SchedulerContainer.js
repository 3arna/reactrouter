import React from 'react';
import lodash from 'lodash';
import { connectToStores, provideContext } from 'fluxible-addons-react';

//import Process from '../../../models/Process';
//import SearchResult from '../../../models/SearchResult';

import BaseComponent from '../../BaseComponent';
import Scheduler from './Scheduler';

import { getResults } from '../../../actions/searchActions';
import { toggleForm } from '../../../actions/component/formActions';

@connectToStores(['ApplicationStore', 'SchedulerStore', 'ContentFormStore'], (context, props) => {
  const applicationStore = context.getStore('ApplicationStore');
  const schedulerStore = context.getStore('SchedulerStore');
  const contentFormStore = context.getStore('ContentFormStore');
  return {
    tasks: schedulerStore.tasks,
    contentFormVisible: contentFormStore.formVisible,
    //nextState: searchStore.nextState,
    //process: applicationStore.processGet(''),
  };
})

class SchedulerContainer extends BaseComponent {

  constructor(){
    super();
    //this._bind('handleToggleForm');
    //this.state = { typeOfSearch: null };
  }

  handleToggleForm(){
    context.executeAction(toggleForm, { isContent: true });
  }

  render(){
    const { props, state } = this;
    return <Scheduler 
      contentFormVisible={props.contentFormVisible}
      onToggleForm={this.handleToggleForm}
      tasks={props.tasks}/>
      
  }

  static contextTypes = {
    translate: React.PropTypes.func.isRequired,
    apiConfig: React.PropTypes.object.isRequired,
  };
  
  static propTypes = {};

  static defaultProps = {};
}

export default SchedulerContainer;
