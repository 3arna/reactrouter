import React from 'react';
import { NavLink } from 'fluxible-router';
import classNames from 'classnames';

import scrollable from '../../decorators/scrollable';

import Process from '../../../models/Process';
import SearchResult from '../../../models/SearchResult';

import MainSearchInput from '../../components/elements/inputs/MainSearchInput';
import SearchTabsMenu from '../../components/elements/menus/SearchTabsMenu';

import SearchResultLink from '../../components/elements/links/SearchResultLink';
import SmallSpinner from '../../components/elements/spinners/SmallSpinner';

@scrollable
class Search extends React.Component {
  render(){

    const {
      process,
      searchResults,
      typeOfSearch,
      searchQuery,
      onChange,
      onNavClick,
      onTabClick
    } = this.props;

    const translate = this.context.translate;

    return <div className="search-page bg-greyl2 h-100vh over-auto over-touch">

      <div className="search-page__header-wrapper">

        <header className="valign-block">
          <MainSearchInput
            onChange={onChange}
            delay={500}
            defaultValue={searchQuery}
            placeholder={translate('words.search')}
          />
          <NavLink routeName="versyStream">{translate('words.cancel')}</NavLink>
        </header>

        {!!typeOfSearch && <div className="search-page__nav-wrapper fadeIn">
          <SearchTabsMenu
            onClick={onTabClick}
            active={typeOfSearch}
            items={[
              {title: translate('words.all'), value: 'all'},
              {title: translate('words.people'), value: 'users'},
              {title: translate('words.interests'), value: 'categories'},
              {title: translate('words.hashtags'), value: 'tags'},
            ]}
          />
        </div>}

      </div>

      <div className="md-pdg-l-60px">
        <div className={classNames('search-page__results-container fadeIn', {'search-page__results-container--tabs-on': !!typeOfSearch})}>

          {!typeOfSearch && <h3>{translate('words.trending')}</h3>}

          {!process.inProgress && searchResults && !searchResults.length && <p>{translate('phrases.noResultsFound')}</p>}
          {searchResults && !!searchResults.length && searchResults.map(( result ) => 
            <SearchResultLink key={result.id} {...result} />)}

          { process.inProgress && <SmallSpinner message={false && translate('words.searching')}/>}

        </div>
      </div>
    </div>
  }

  static contextTypes = {
    translate: React.PropTypes.func
  };

  static propTypes = {
    process: Process.propTypes.isRequired,
    searchResults: React.PropTypes.arrayOf(SearchResult.propTypes),
    searchQuery: React.PropTypes.string,
    typeOfSearch: React.PropTypes.string,

    onChange: React.PropTypes.func,
    onTabClick: React.PropTypes.func,
  };
}

export default Search;
