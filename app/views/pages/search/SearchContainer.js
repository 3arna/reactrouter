import React from 'react';
import lodash from 'lodash';
import { connectToStores, provideContext } from 'fluxible-addons-react';

import Process from '../../../models/Process';
import SearchResult from '../../../models/SearchResult';

import BaseComponent from '../../BaseComponent';
import Search from './Search';

import { getResults } from '../../../actions/searchActions';

@connectToStores(['ApplicationStore', 'UserStore', 'SearchStore'], (context, props) => {
  const applicationStore = context.getStore('ApplicationStore');
  const userStore = context.getStore('UserStore');
  const searchStore = context.getStore('SearchStore');
  return {
    searchResults: searchStore.searchResults,
    trendingResults: searchStore.trendingResults,
    typeOfSearch: searchStore.typeOfSearch,
    searchQuery: searchStore.searchQuery,
    nextState: searchStore.nextState,
    process: applicationStore.processGet('search'),
  };
})

class SearchContainer extends BaseComponent {

  constructor(){
    super();
    this._bind('handleChange', 'handleTabClick', 'handleScrolledToBottom');
    this.state = { typeOfSearch: null };
  }

  componentWillMount(){
    this.setState({ typeOfSearch: this.props.typeOfSearch });
  }

  handleTabClick(searchType, e){
    if(this.props.process.inProgress || !lodash.includes(['all', 'users', 'tags', 'categories'], searchType)){
      return;
    }

    this.setState({typeOfSearch: searchType});

    context.executeAction(getResults, {
      process: this.props.process,
      typeOfSearch: searchType,
      searchQuery: this.props.searchQuery,
    });
  }

  handleChange(e){
    const value = e.target.value || '';
    
    const minSearchTextLength = context.getComponentContext().apiConfig 
      && context.getComponentContext().apiConfig.minimumRequiredSearchChars
      || this.props.minSearchTextLength;

    if(!!value.length && value.length < minSearchTextLength){
      return;
    }

    let typeOfSearch = !value.length ? null : this.props.typeOfSearch || 'all';
    typeOfSearch = lodash.contains(value, '#') && 'tags' || typeOfSearch;

    this.setState({ typeOfSearch });

    context.executeAction(getResults, {
      process: this.props.process,
      typeOfSearch,
      searchQuery: value,
    });
  }

  handleScrolledToBottom(){
    const props = this.props;

    if(props.process.inProgress || !props.nextState || !props.typeOfSearch){
      return;
    }

    context.executeAction(getResults, {
      process: props.process,
      typeOfSearch: props.typeOfSearch,
      searchQuery: props.searchQuery,
      nextState: props.nextState,
      ignoreResultClearing: true,
    });
  }

  render(){
    const { props, state } = this;

    return <Search
      process={props.process}
      searchResults={!!state.typeOfSearch ? props.searchResults : props.trendingResults}
      searchQuery={props.searchQuery}

      typeOfSearch={state.typeOfSearch}

      onChange={this.handleChange}
      onTabClick={this.handleTabClick}
      onScrolledToBottom={this.handleScrolledToBottom}
    />

  }

  static contextTypes = {
    translate: React.PropTypes.func.isRequired,
    apiConfig: React.PropTypes.object.isRequired,
  };
  
  static propTypes = {
    minSearchTextLength: React.PropTypes.number.isRequired,

    process: Process.propTypes.isRequired,
    searchResults: React.PropTypes.arrayOf(SearchResult.propTypes),
    typeOfSearch: React.PropTypes.string,
    searchQuery: React.PropTypes.string,
  };

  static defaultProps = {
    minSearchTextLength: 3,
  };
}

export default SearchContainer;
