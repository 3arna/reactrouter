import React from "react";
import DefaultButton from '../components/elements/buttons/DefaultButton';

const AccessDenied = (props, { translate }) =>
  <div className="txt-center clr-red md-pdg-l-60px h-min-100 valign-block">
    <div>
      <div className="mrg-auto txt-center pdg-rl-3 clr-greyd3">
        <span className="fnt-xxl dsp-inline mrg-tb-5em">
          <i className="icon-sad fnt-xxl"/>
        </span>
        <div className="w-max-200px mrg-auto">
          <h3 className="mrg-tb-10em pdg-0 fnt-bold">
            { translate('phrases.accessDenied') }
          </h3>
          <p className="mrg-tb-10em mrg-b-10em fnt-xs line-15">
            { translate('phrases.youCannotAccessThisContent') }
          </p>
          <DefaultButton routeName="versyStream">{ translate('phrases.goToYourVersyStream') }</DefaultButton>
        </div>
  </div>
      </div>
  </div>

AccessDenied.contextTypes = {
  translate: React.PropTypes.func
}

export default AccessDenied;
