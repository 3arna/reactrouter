import React from 'react';
import DocMeta from 'react-doc-meta';
import DocTitle from 'react-document-title';

import ContentFeedScrollerContainer from '../../components/contentFeed/contentFeedScroller/ContentFeedScrollerContainer';
import SubContentFeedScroller from '../../components/subContent/SubContentFeedScroller';
import ContextMenuActionsContainer from '../../components/contentFeed/contentCard/ContextMenuActionsContainer';

import TwoColLayout from '../../layouts/TwoColLayout';

const ContentFeed = (props) => {

  return <DocTitle title={props.pageTitle}>
    <TwoColLayout
      parentContent={props.subContentPage && props.subContentPage.parent}
      isDesktop={!!props.subContentPage}
      contentFormOn={true}>

      <DocMeta tags={props.metaTags} />

      <ContextMenuActionsContainer
        contextMenuActionObject={props.contextMenuActionObject}
        onSetContextMenuAction={props.onSetContextMenuAction}
        contextMenuActionProcess={props.contextMenuActionProcess}/>

      <ContentFeedScrollerContainer
        bookmarks={ props.bookmarks }
        contentFormVisible={ props.contentFormVisible }
        contentPage={ props.feedContentPage }
        feedScrollPosition={ props.feedScrollPosition }
        isLoading={ props.feedLoading }
        isLoadingMore={ props.feedLoadingMore }
        newContentAvailable={ props.newFeedContentAvailable }
        onAcceptPrivateConversationInvite={ props.onAcceptPrivateConversationInvite }
        onCheckForNewContent={ props.onCheckForNewContent }
        onLoadNewContent={ props.onLoadNewFeedContent}
        onScrollEnd={ props.onContentFeedScrolled }
        onScrolledToBottom={ props.onLoadOlderFeedContent }
        parentId={ props.parentId }
        previousFeedContentPage={ props.previousFeedContentPage }
        returnToFeedParentContentId={ props.returnToFeedParentContentId }
        selectedContent={ props.selectedContent }
        selectedContentId={ props.selectedContentId }
        subContentPage={ props.subContentPage }
        user={ props.user }
        onSetContextMenuAction={props.onSetContextMenuAction}
      />

      <SubContentFeedScroller
        allowPosting={ true }
        bookmarks={ props.bookmarks }
        contextMenuVisible={ props.contextMenuVisible }
        contextMenuActiveContentId={ props.contextMenuActiveContentId }
        contextMenuActiveType={ props.contextMenuActiveType }
        loading={ props.subContentLoading }
        loadingMore={ props.subContentLoadingMore }
        newContentAvailable={ props.newSubContentAvailable }
        onCheckForNewContent={ props.onCheckForNewContent }
        onLoadNewContent={ props.onLoadNewReplies }
        onScrolledToTop={ props.onLoadOlderReplies }
        parentFeedId={ props.parentId }
        replyTo={ props.subContentReplyTo }
        subContentError={ props.subContentError }
        subContentPage={ props.subContentPage }
        selectedContentId={ props.subContentPage && props.subContentPage.parentContentId || props.selectedContentId }
        user={ props.user }
      />

    </TwoColLayout>
  </DocTitle>
}

ContentFeed.propTypes = {
  bookmarks: React.PropTypes.array,
  feedContentPage: React.PropTypes.object,
  feedScrollPosition: React.PropTypes.object,
  feedLoading: React.PropTypes.bool.isRequired,
  onScrolledToBottom: React.PropTypes.func,
  onLoadMoreReplies: React.PropTypes.func,
  parentId: React.PropTypes.string,
  previousFeedContentPage: React.PropTypes.object,
  selectedContent: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.object,
  ]),
  selectedContentId: React.PropTypes.oneOfType([
    React.PropTypes.bool,
    React.PropTypes.string,
  ]),
  subContentLoading: React.PropTypes.bool,
  subContentPage: React.PropTypes.object,
  subContentError: React.PropTypes.oneOfType([
    React.PropTypes.object,
    React.PropTypes.string,
  ]),
  subContentReplyTo: React.PropTypes.string,
  contextMenuVisible: React.PropTypes.bool,
  contextMenuActiveContentId: React.PropTypes.oneOfType([
      React.PropTypes.bool,
      React.PropTypes.string,
  ]),
  contextMenuActiveType: React.PropTypes.string,
};

export default ContentFeed;
