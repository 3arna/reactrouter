import React from 'react';
import lodash from 'lodash';
import { connectToStores, provideContext } from 'fluxible-addons-react';

import { getContentFeedData, getSubcontentData, getStreamData } from '../../../actions/contentActions';
import { setFeedScrollPosition } from '../../../actions/component/contentFeedActions';
import * as contentFeedUtils from '../../../utils/contentFeedUtils';

import BaseFeedContainer from '../../BaseFeedContainer';
import ContentFeed from './ContentFeed';

@connectToStores(
  BaseFeedContainer.stores,
  BaseFeedContainer.initProps
)
class ContentFeedContainer extends BaseFeedContainer{
  static contextTypes = {
    apiConfig: React.PropTypes.object,
    translate: React.PropTypes.func,
  };

  constructor(){
    super();
    this._bind('handleLoadOlderFeedContent', 'handleLoadNewFeedContent');
  }

  handleLoadNewFeedContent(){
    const parentId = this.props.parentId;
    let getDataAction = getContentFeedData;
    let actionParams = { parentId, newContent: true, needRefresh: true };

    if(lodash.startsWith(parentId, 'category') || lodash.startsWith(parentId, 'tag')){
      getDataAction = getStreamData;
      actionParams = { feedIdentifier: parentId, newContent: true };
    }

    context.executeAction(getDataAction, actionParams);
  }

  handleLoadOlderFeedContent(){
    const parentId = this.props.parentId;

    if(lodash.startsWith(parentId, 'category') || lodash.startsWith(parentId, 'tag')){
      return this.handleLoadOlderStreamContent();
    }

    const feedContentPage = this.props.feedContentPage;
    if(feedContentPage.content.length < this.context.apiConfig.numberOfItemsPerPage || feedContentPage.pageMin === 1 || this.props.feedLoadingMore){
      return;
    }

    context.executeAction(getContentFeedData, { parentId: feedContentPage.parentContentId, needRefresh: true, sequence: feedContentPage.pageMin -1 });
  }

  handleLoadOlderStreamContent(){
    const feedContentPage = this.props.feedContentPage;

    if(this.props.feedLoadingMore || !feedContentPage.nextState){
      return;
    }

    context.executeAction(getStreamData, { feedIdentifier: feedContentPage.parentContentId, needRefresh: true, nextState: feedContentPage.nextState });
  }

  get metaTags(){
    const props = this.props;
    if(!props.feedContentPage){
      return [];
    }

    return contentFeedUtils.getFeedMetaTags(props.feedContentPage, this.context.translate);
  }

  get pageTitle(){
    const props = this.props;
    if(!props.feedContentPage){
      return '';
    }

    return contentFeedUtils.getPageTitle(props.feedContentPage, this.context.translate);
  }

  render(){
    const {props, state} = this;
    const parentId = decodeURIComponent(props.params.parentId);

    return <ContentFeed
      bookmarks={ props.bookmarks }
      contentFormVisible={ props.contentFormVisible }
      contextMenuVisible={ props.contextMenuVisible }
      contextMenuActiveContentId={ props.contextMenuActiveContentId }
      contextMenuActiveType={ props.contextMenuActiveType }
      feedContentPage={ props.feedContentPage }
      feedLoading={ props.feedLoading }
      feedLoadingMore={ props.feedLoadingMore }
      feedScrollPosition={ props.feedScrollPosition }
      metaTags={ this.metaTags }
      newFeedContentAvailable={ props.newFeedContentAvailable }
      newSubContentAvailable={ props.newSubContentAvailable }
      onCheckForNewContent={ this.handleCheckForNewContent }
      onContentFeedScrolled={ this.handleContentFeedScrolled }
      onLoadNewFeedContent={ this.handleLoadNewFeedContent }
      onLoadNewReplies={ this.handleLoadNewReplies }
      onLoadOlderFeedContent={ this.handleLoadOlderFeedContent }
      onLoadOlderReplies={ this.handleLoadOlderReplies }
      parentId={ parentId }
      isMe={ parentId === 'user' || props.user && props.user.id === parentId }
      pageTitle={ this.pageTitle }
      previousFeedContentPage={ props.previousFeedContentPage }
      selectedContent={ props.selectedContent }
      selectedContentId={ this.selectedContentId }
      subContentError={ props.subContentError }
      subContentLoading={ props.subContentLoading }
      subContentLoadingMore={ props.subContentLoadingMore }
      subContentPage={ props.subContentPage }
      subContentReplyTo={ props.subContentReplyTo }
      user={ props.user }
    />
  }
}

export default ContentFeedContainer;
