import React from 'react';
import { connectToStores } from 'fluxible-addons-react';

import Process from '../../../models/Process';

import BaseComponent from '../../BaseComponent';
import ForgotPassword from './ForgotPassword';

import { passwordReset } from '../../../actions/emailActions';
import { apiProcessClear } from '../../../actions/applicationActions';

@connectToStores(['ApplicationStore'], (context, props) =>
  ({process: context.getStore('ApplicationStore').processGet('forgotPassword')}))

class ForgotPasswordContainer extends BaseComponent {

  constructor(){
    super();
    this._bind('handleSubmit', 'handleCloseAlert');
    this.state = {};
  }

  handleSubmit(e){
    context.executeAction(passwordReset, {
      email: e.target.email.value,
      process: this.props.process,
    });
    e.preventDefault();
  }

  handleCloseAlert(e){
    context.executeAction(apiProcessClear, this.props.process);
  }

  render(){
    return <ForgotPassword
      process={this.props.process}
      onCloseAlert={this.handleCloseAlert}
      onSubmit={this.handleSubmit}/>
  }

  static propTypes = {
    process: Process.propTypes.isRequired,
  };
}


export default ForgotPasswordContainer;
