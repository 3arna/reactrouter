import React from 'react';
import classNames from 'classnames';

import Process from '../../../models/Process';

import FrontLayout from '../../layouts/FrontLayout';
import FrontInput from '../../components/elements/inputs/FrontInput';
import FrontButton from '../../components/elements/buttons/FrontButton';
import DefaultFrontButton from '../../components/elements/buttons/DefaultFrontButton';
import DefaultLink from '../../components/elements/links/DefaultLink';

import FrontFormContainer from '../../components/forms/frontForm/FrontFormContainer';

const ForgotPassword = ({process, onCloseAlert, onSubmit}, {translate}) =>
  <FrontLayout
    onCloseAlert={onCloseAlert}
    process={process}
    loadingMessage={translate('phrases.sendingEmail')}
    className="clr-white w-max-800px w-100 fix-bb mrg-auto"
    title={translate('phrases.forgottenPassword')}
    description={translate('phrases.noWorriesEnterYourEmailBelow')}>

    {!process.success && 
      <div className="w-max-300px mrg-auto clr-white fnt-sm mrg-b-30px">
        <FrontFormContainer theme="forgotPassword" onSubmit={onSubmit}/>
      </div>}

  </FrontLayout>

ForgotPassword.contextTypes = {
  translate: React.PropTypes.func
}

ForgotPassword.propTypes = {
  process: Process.propTypes.isRequired,
  onSubmit: React.PropTypes.func,
  onCloseAlert: React.PropTypes.func,
}

export default ForgotPassword;
