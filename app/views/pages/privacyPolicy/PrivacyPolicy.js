import React from "react";

import OneColLayout from '../../layouts/OneColLayout';
import PrivacyEn from './PrivacyEn';
import PrivacyEs from './PrivacyEs';
import PrivacyPt from './PrivacyPt';

const getTranslatedPrivacyPolicy = (language) =>{
  switch(language){
    case 'en':
      return <PrivacyEn />;
    case 'es':
      return <PrivacyEs />;
    case 'pt':
      return <PrivacyPt />;
    default:
      return <PrivacyEn />;
  }
};

const PrivacyPolicy = (props, { translate, getLanguage }) =>
  <OneColLayout {...props} title={translate('phrases.privacyPolicy')}>
    { getTranslatedPrivacyPolicy(getLanguage()) }
  </OneColLayout>

PrivacyPolicy.contextTypes = {
  getLanguage: React.PropTypes.func,
  translate: React.PropTypes.func
};

export default PrivacyPolicy;
