import React from 'react';
import lodash from 'lodash';
import { connectToStores, provideContext } from 'fluxible-addons-react';

import User from '../../../models/User';
import Process from '../../../models/Process';

import BaseComponent from '../../BaseComponent';
import uploader from '../../decorators/uploader';

import { apiProcessStart, apiProcessClear, apiProcessError } from '../../../actions/applicationActions';
import { saveUserProfileChanges, saveUserProfileBackground, saveUserProfileAvatar, toggleEditMode }
  from '../../../actions/userActions';

import { validInput } from '../../../utils/validationUtils';

import Settings from './Settings';

@connectToStores(['ApplicationStore', 'UserStore'], (context, props) => ({
  process: context.getStore('ApplicationStore').processGet('updateProfile'),
  processProfileBackground: context.getStore('ApplicationStore').processGet('profileBackground', 0),
  processProfileAvatar: context.getStore('ApplicationStore').processGet('processProfileAvatar', 0),
  user: context.getStore('UserStore').user
}))

@uploader

class SettingsContainer extends BaseComponent {

  constructor(){
    super();
    this.state = {
      bgTempMedia: null,
      tempAvatarMedia: null,
      edited: {
        /*
        displayName: false,
        description: false,
        website: false,
        gender: false,
        location: false,
        */
      }
    }

    this._bind(
      'handleInputChange',
      'handleSaveChanges',
      'handleChangeProfileBackground',
      'handleChangeProfileAvatar',
      'handleClearProcess')
  }

  handleChangeProfileBackground(event){

    const { props } = this;

    context.executeAction(apiProcessStart, props.processProfileBackground);
    props.onMediaPreview({
      event,
      name: 'background',
      typesAllowed: ['image/jpeg', 'image/png']
    }, (error, mediaPreviewData) => {

      if(error){
        return context.executeAction(apiProcessError, {
          process: props.processProfileBackground,
          error,
          maxTime: 3000,
        });
      }

      if(!mediaPreviewData){
        return context.executeAction(apiProcessClear, props.processProfileBackground);
      }

      mediaPreviewData.previewUrl && this.setState({tempBgMedia: mediaPreviewData.media});

      props.onMediaUpload(mediaPreviewData, (error, data) => {

        context.executeAction(saveUserProfileBackground, {
          error: error,
          data: data && data.media,
          process: props.processProfileBackground,
        });
      });
    });
  }

  handleChangeProfileAvatar(event){

    const { props } = this;

    context.executeAction(apiProcessStart, props.processProfileAvatar);
    props.onMediaPreview({
      event,
      name: 'background',
      typesAllowed: ['image/jpeg', 'image/png']
    }, (error, mediaPreviewData) => {

      if(error){
        return context.executeAction(apiProcessError, {
          process: props.processProfileAvatar,
          error: this.context.translate('phrases.somethingWentWrong'),
          maxTime: 3000,
        });
      }

      if(!mediaPreviewData){
        return context.executeAction(apiProcessClear, props.processProfileAvatar);
      }

      mediaPreviewData.previewUrl && this.setState({tempAvatarMedia: mediaPreviewData.media});

      props.onMediaUpload(mediaPreviewData, (error, data) => {

        context.executeAction(saveUserProfileAvatar, {
          error: error,
          data: data && data.media,
          process: props.processProfileAvatar,
        });
      });
    });
  }

  handleInputChange(e){
    const edited = this.state.edited;
    edited[e.target.name] = e.target.value;
    this.setState({edited});
  }

  handleSaveChanges(){
    context.executeAction(saveUserProfileChanges, {
      data: lodash.merge(this.props.user, this.state.edited),
      process: this.props.process
    });
  }

  handleClearProcess = (e) => {
    context.executeAction(apiProcessClear, this.props.processProfileBackground);
    context.executeAction(apiProcessClear, this.props.processProfileAvatar);
  }

  render() {
    const props = this.props;
    const state = this.state;

    //console.log(props.isMe, props.user, props.contentPage.ultimateParent);

    return <Settings
      process={props.process}
      processProfileBackground={props.processProfileBackground}
      processProfileAvatar={props.processProfileAvatar}
      tempBgMedia={state.tempBgMedia}
      tempAvatarMedia={state.tempAvatarMedia}

      user={props.user}
      editModeOn={true}

      onClearProcess={this.handleClearProcess}
      onChangeProfileBackground={this.handleChangeProfileBackground}
      onChangeProfileAvatar={this.handleChangeProfileAvatar}
      onInputChange={this.handleInputChange}
      onSaveChanges={this.handleSaveChanges}/>
  }

  static contextTypes = {
    translate: React.PropTypes.func,
    executeAction: React.PropTypes.func,
  }

  static propTypes = {
    process: Process.propTypes.isRequired,
    processProfileBackground: Process.propTypes.isRequired,
    processProfileAvatar: Process.propTypes.isRequired,
    user: User.propTypes.isRequired,
  }
}

export default SettingsContainer;
