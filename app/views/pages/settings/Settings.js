import classNames from 'classnames';
import React from 'react';
import { NavLink } from 'fluxible-router';
import lodash from 'lodash';

import ErrorAlert from '../../components/elements/alerts/ErrorAlert';
import EditableAvatar from '../../components/elements/avatars/EditableAvatar';
import ProfileBackgroundUpload from '../../components/elements/uploads/ProfileBackgroundUpload';
import TextEditable from '../../components/elements/editables/TextEditable';
import InProgressFog from '../../components/elements/fogs/InProgressFog';

import LanguageDropdown from '../../components/elements/dropdowns/LanguageDropdown';
import SettingsDropdown from '../../components/elements/dropdowns/SettingsDropdown';
import SettingsInput from '../../components/elements/inputs/SettingsInput';

import EditProfileHeader from '../../components/user/userProfile/EditProfileHeader';
import UserStats from '../../components/user/UserStats';

import TwoColLayout from '../../layouts/TwoColLayout';

const Settings = (props, { translate }) => {

  const {
    process,
    processProfileBackground,
    processProfileAvatar,
    tempBgMedia,
    tempAvatarMedia,

    user,
    editModeOn,

    onClearProcess,
    onChangeProfileBackground,
    onChangeProfileAvatar,
    onInputChange,
    onSaveChanges,
  } = props;

  return <TwoColLayout>
    <div className="txt-center pdg-t-50px h-100vh over-auto over-touch">

      <EditProfileHeader onSaveChanges={onSaveChanges}/>

      <div className="bg-cover pos-relative bg-center pdg-b-100px pdg-t-50px fix-bb h-min-300px"
        style={{ backgroundImage: `linear-gradient(rgba(0,0,0, .5), rgba(0,0,0, .5)), url('${tempBgMedia && tempBgMedia.mediaUrl || user.backgroundUrl}')`}}>

        {!!process.inProgress && <InProgressFog message={translate('words.saving')}/>}

        {(processProfileBackground.error || processProfileAvatar.error || process.error) &&
          <ErrorAlert
            errorMessage={processProfileBackground.error || processProfileAvatar.error || process.error}
            clearErrorMessageDelay={1500}
            onClearErrorMessage={onClearProcess}/>}

        <ProfileBackgroundUpload
          inProgress={ processProfileBackground.inProgress }
          onChange={ onChangeProfileBackground }/>

        <div className="txt-center clr-white txt-center">
          <EditableAvatar
            editModeOn={editModeOn}
            inProgress={ processProfileAvatar.inProgress }
            onChange={ onChangeProfileAvatar }
            avatarUrl={ tempAvatarMedia && tempAvatarMedia.mediaUrl || user.avatarUrl }/>

          <TextEditable
            editModeOn={editModeOn}
            placeholder={translate('words.name')}
            name="displayName"
            maxLength={40}
            text={user.displayName}
            className="fnt-xl txt-center w-max-400px mrg-auto sm-col-12 mrg-t-10px"
            onChange={onInputChange}/>

          <TextEditable
            textarea={true}
            editModeOn={editModeOn}
            placeholder={translate('words.bio')}
            name="description"
            maxLength={500}
            text={user.description}
            className="fnt-sm txt-center w-max-400px md-col-12 mrg-auto mrg-t-5px"
            onChange={onInputChange} />

          <TextEditable
            editModeOn={editModeOn}
            placeholder={translate('words.website')}
            name="webSite"
            maxLength={200}
            text={user.webSite}
            className="fnt-xs txt-center w-max-400px md-col-12 mrg-auto mrg-t-10px"
            onChange={onInputChange}>

            {user.webSite &&
              <NavLink
                href={user.webSite.indexOf('http') === -1 && `http://${user.webSite}` || user.webSite}
                target="_blank" className="hover-clr-secondl2 mrg-t-15px clr-white txt-deco-none">
                  {user.webSite.replace(/(http|https):\/\//, '')}
              </NavLink>}

          </TextEditable>

          <TextEditable
            editModeOn={editModeOn}
            placeholder={translate('words.location')}
            maxLength={40}
            name="location"
            text={user.location}
            className="fnt-xs txt-center w-max-400px md-col-12 mrg-auto mrg-t-5px"
            onChange={onInputChange} />

        </div>

        {false && <UserStats user={user} isMe={true}/>}

      </div>

      <div className="pdg-tb-30px clr-black pdg-rl-3 w-max-800px mrg-auto">
        <h4 className="txt-left pdg-0 mrg-tb-2 fnt-md">{translate('phrases.privateInformation')}</h4>
        <div className="bg-white pdg-rl-3 pdg-tb-5px">

          <SettingsInput
            name="emailAddress"
            placeholder={translate('phrases.emailAddress')}
            onChange={onInputChange}
            disabled={!!user.emailAddress}
            value={user.emailAddress}/>

          <SettingsDropdown 
            onChange={onInputChange} 
            selected={user.gender}
            name="gender"
            items={[
              {name: translate('words.gender'), value: false},
              {name: translate('words.male'), value: 'male'},
              {name: translate('words.female'), value: 'female'},
            ]}/>

          <LanguageDropdown className="pdg-tb-5px"/>

        </div>
      </div>

    </div>
  </TwoColLayout>
}

Settings.contextTypes = {
  translate: React.PropTypes.func,
  executeAction: React.PropTypes.func,
};

export default Settings;
