import React, { PropTypes } from "react";
import { Link } from 'react-router';

import OneColLayout from '../layouts/OneColLayout';
import ListButton from '../components/elements/buttons/ListButton';
import LanguageDropdown from '../components/elements/dropdowns/LanguageDropdown';
import FrontLink from '../components/elements/links/FrontLink';
import FrontButton from '../components/elements/buttons/FrontButton';
import Media from '../components/media/Media';

const _getDescription = (language) => {
  switch(language){
    case 'en': 
      return 'Versy is the social app that lets you explore your passions and create a world of your interests with like-minds. \
        It\’s a universe filled with content, where you can connect, chat and express opinions about all of the topics you love. \
        Start posting now on mobile, tablet or web';

    case 'es': 
      return 'Versy es un app que te permite encontrar el contenido que te apasiona y donde podrás crear un mundo de intereses con gente que piensa como tu. \
        Es un universo lleno de contenido donde puedes conectarte, chatear y expresar tus opiniones acerca de los temas que mas te interesan. \
        Empieza a publicar ya a través de tu celular, tableta o web';

    case 'pt': 
      return 'Versy é uma rede social que te permite explorar suas paixōes e criar um novo mundo com as pessoas que tem os mesmos interesses que você. \
        É um universo recheado de conteúdos, onde voce pode se conectar, conversar e expressar suas opiniōes sobre tudo aquilo que você curte. \
        Comece a postar agora através do seu celular, tablet ou pela internet';
  }
}

const _getMedia = (language) => {
  
  const media = { 
    mimeType: 'video/mp4',
    attributes: { 
      height: 720,
      width: 1280,
      category: 'uploaded' 
    },
    poster: '/img/poster.jpg',
    childMedia: [] 
  };

  media.mediaUrl = `https://d1d0j16bwc1zaw.cloudfront.net/us-east-1/live-marketing-media/versy-marketing-${language}.mp4`;
  
  return media;
}

const WhatIsVersy = (props, { translate, getLanguage }) =>
  <div className="txt-center pdg-t-50px pdg-rl-3">
    <div className="pos-absolute left-0 pdg-rl-3 dsp-inline dsp-block w-100 fix-clear md-dsp-none" >
      <LanguageDropdown className="brd-grey w-150px flt-left md-flt-none fnt-xs" noIcon/>
      <FrontButton href="/register" className="flt-right" type="small" text={translate('words.register')}/>
    </div>

    <div className="w-max-700px mrg-auto">
      <Link to="/" className="dsp-inline"><img src="/img/versy-rainbow.svg" className="md-w-50px dsp-inline" width="80"/></Link>
      <h1 className="mrg-0 mrg-t-20px mrg-b-10px fnt-xxl fnt-main-reg fnt-600 clr-main">{translate('phrases.whatIsVersy')} </h1>
      <h4 className="pdg-0 mrg-auto txt-center mrg-b-40px fnt-100 line-15 clr-blackl2">
        {_getDescription(getLanguage())}.
      </h4>

      <Media media={_getMedia(getLanguage())}/>
      <FrontLink className="mrg-b-15px dsp-none md-dsp-block mrg-t-30px clr-main" href="/register">
        {`${translate('phrases.newToVersy')} ${translate('words.register')}`}
      </FrontLink>

      <LanguageDropdown className="brd-grey w-150px fnt-xs dsp-none md-dsp-block mrg-auto" noIcon/>
    </div>
  </div>

WhatIsVersy.contextTypes = {
  translate: React.PropTypes.func,
  getLanguage: React.PropTypes.func,
};

export default WhatIsVersy;
