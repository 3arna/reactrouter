import React from 'react';
import lodash from 'lodash';
import { connectToStores, provideContext } from 'fluxible-addons-react';

import Category from '../../../models/Category';
import Process from '../../../models/Process';

import BaseComponent from '../../BaseComponent';
import First from './First';

import { getSearchedCategories, bookmarkCategories } from '../../../actions/categoryActions';
import { toastProcessError } from '../../../actions/applicationActions';

@connectToStores(['ApplicationStore', 'UserStore', 'CategoryStore'], (context, props) => {
  const applicationStore = context.getStore('ApplicationStore');
  const userStore = context.getStore('UserStore');
  const categoryStore = context.getStore('CategoryStore');
  return {
    categories: categoryStore.categories,
    searchedCategories: categoryStore.searchedCategories,
    interests: userStore.interests,
    process: applicationStore.processGet('firstUse')
  };
})

export default class FirstContainer extends BaseComponent {

  constructor(){
    super();
    this._bind('handleCategoriesStore', 'handleCategoriesSearch', 'handleSubmit');
    this.state = { selectedCategories: []};
  }

  handleCategoriesStore(selectedCategories){
    this.setState({selectedCategories})
  }

  handleCategoriesSearch(e){
    context.executeAction(getSearchedCategories, { query: e.target.value })
  }

  handleSubmit(){
    const componentContext = context.getComponentContext();
    if(this.state.selectedCategories.length < componentContext.apiConfig.minimumFirstUseCategories){
      return context.executeAction(
        toastProcessError, 
        context.getComponentContext().translate('phrases.selectAtLeastInterests', {
          minLength: componentContext.apiConfig.minimumFirstUseCategories
        })
      );
    }

    context.executeAction(bookmarkCategories, {
      process: this.props.process,
      selectedCategories: this.state.selectedCategories
    })
  }

  render(){
    return <First
      process={this.props.process}
      interests={this.props.interests}
      searchedCategories={this.props.searchedCategories}
      categories={this.props.categories}
      selectedCategories={this.state.selectedCategories}
      onCategoriesStore={this.handleCategoriesStore}
      onCategoriesSearch={this.handleCategoriesSearch}
      onSubmit={this.handleSubmit}
    />
  }

  static contextTypes = {
    translate: React.PropTypes.func.isRequired,
    apiConfig: React.PropTypes.object.isRequired,
  };

  static propTypes = {
    process: Process.propTypes.isRequired,

    interests: React.PropTypes.arrayOf(Category.propTypes),
    searchedCategories: React.PropTypes.oneOfType([
      React.PropTypes.arrayOf(Category.propTypes),
      React.PropTypes.bool,
    ]),
    categories: React.PropTypes.arrayOf(Category.propTypes),
  };
}
