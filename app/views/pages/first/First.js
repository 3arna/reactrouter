import React from 'react';

import Category from '../../../models/Category';
import Process from '../../../models/Process';

import CategorySelectorContainer from '../../components/selectors/categorySelector/CategorySelectorContainer.js';
import CircleButton from '../../components/elements/buttons/CircleButton';
import BackButton from '../../components/elements/buttons/BackButton';
import InProgressFog from '../../components/elements/fogs/InProgressFog';

const First = (props, {translate, apiConfig}) => {

  const {
    process,
    interests,
    searchedCategories,
    categories,
    selectedCategories,
    onSubmit,
    onCategoriesStore,
    onCategoriesSearch
  } = props;

  return <div className="clr-blackl1 pdg-t-50px bg-greyl2 h-min-100vh">
    <div className="pos-fixed top-0 left-0 z-1 w-100">
      <div className="valign-block fix-bb pdg-rl-3 w-100 bg-white boxshadow-b fadeIn">
        <div className="w-min-50px md-w-auto">
          <img className="dsp-none md-dsp-inline" src="/img/versy-rainbow.svg" width="20"/>
        </div>
        <div className="w-100">
          <div className="w-max-700px pdg-rl-3 txt-left valign-block mrg-auto">
          </div>
        </div>
        <div className="w-min-50px pdg-tb-15px">
          <span className="clr-main crs-point dsp-inline" onClick={onSubmit}>
            {translate('words.continue')}
          </span>
        </div>
      </div>
    </div>

    {process.inProgress && <InProgressFog message={translate('phrases.savingInterests')}/>}
    <div className="md-pdg-l-60px">
      <div className="w-max-700px mrg-auto pdg-rl-3 clr-main txt-center">

        <div className="md-dsp-none mrg-t-30px">
          <img src="/img/versy-rainbow.svg" className="md-w-50px" width="90"/>
          <h1 className="mrg-0 mrg-t-30px mrg-b-10px fnt-xxl fnt-main-reg fnt-600">{translate('phrases.selectInterests')} </h1>
          <h4 className="pdg-0 mrg-auto clr-main txt-center mrg-b-40px fnt-100 fnt-md md-dsp-none">
            {translate('phrases.findTheContentYouLove')}.
          </h4>
        </div>

         <CategorySelectorContainer
          interests={interests}
          searchedCategories={searchedCategories}
          categories={categories}
          selectedCategories={selectedCategories}
          searchCategories={onCategoriesSearch}
          returnSelectedCategories={onCategoriesStore}
          placeholderText={translate('phrases.whatDoYouLike')}
          componentTitle={translate('phrases.selectInterests')}
          textCategoryNotExist={translate('phrases.doesNotExistSelectToAddIt')}
          messageMaxReached={translate('phrases.categoryMaxLimitReached')}
          messageAlreadyAdded={translate('phrases.categoryAlreadyAdded')}/>
      </div>
    </div>

  </div>
}

First.contextTypes = {
  translate: React.PropTypes.func.isRequired,
  apiConfig: React.PropTypes.object,
};

First.propTypes = {
  process: Process.propTypes.isRequired,

  interests: React.PropTypes.arrayOf(Category.propTypes),
  searchedCategories: React.PropTypes.oneOfType([
    React.PropTypes.arrayOf(Category.propTypes),
    React.PropTypes.bool,
  ]),
  categories: React.PropTypes.arrayOf(Category.propTypes),
  selectedCategories: React.PropTypes.arrayOf(React.PropTypes.string),
  onSubmit: React.PropTypes.func,
  onCategoriesStore: React.PropTypes.func,
  onCategoriesSearch: React.PropTypes.func,
};

export default First;
