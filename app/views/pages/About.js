import React, { PropTypes } from "react";

import OneColLayout from '../layouts/OneColLayout';
import ListButton from '../components/elements/buttons/ListButton';

const About = (props, { translate }) =>
  <OneColLayout {...props} previousUrl={false} title={ translate('words.about') }>

      <ListButton noForword>
        <strong className="fnt-400">App version:</strong> {props.appInfo && props.appInfo.version}
      </ListButton>
      <ListButton iconClassName="icon-privacypolicy" href="/privacy-policy">{ translate('phrases.privacyPolicy')}</ListButton>
      <ListButton iconClassName="icon-termsofuse" href='/terms'>{ translate('phrases.termsAndConditions') }</ListButton>

  </OneColLayout>

About.contextTypes = {
  translate: React.PropTypes.func
};

export default About;
