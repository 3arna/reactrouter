import React from 'react';
import classNames from 'classnames';

import Process from '../../../models/Process';

import FrontLayout from '../../layouts/FrontLayout';
import FrontButton from '../../components/elements/buttons/FrontButton';
import DefaultFrontButton from '../../components/elements/buttons/DefaultFrontButton';
import FrontFormContainer from '../../components/forms/frontForm/FrontFormContainer';
//import LoginForm from '../../components/forms/frontForm/LoginForm';

const Login = ({process, onSubmit, onCloseAlert}, {translate, apiConfig}) =>
  <FrontLayout
    process={process}
    onCloseAlert={onCloseAlert}
    loadingMessage={translate('phrases.useEmail')}
    title={translate('phrases.useEmail')}
    description={translate('phrases.useEmailToLogIn')}
    showExtras>

      <div className="w-max-250px mrg-auto clr-white fnt-sm mrg-b-30px">

        <FrontFormContainer
          theme="login"
          onSubmit={onSubmit}
          minPasswordLength={apiConfig.minPasswordChars}/>

        <p className="fnt-sm mrg-t-30px clr-main line-13 op-08 clr-black dsp-inline">
          {translate('phrases.joiningFromVersyChatSelectRegisterButton')}
        </p>

        <FrontButton href="/register" text={translate('words.register')} type="transparent"/>

      </div>

  </FrontLayout>

Login.contextTypes = {
  translate: React.PropTypes.func.isRequired,
  apiConfig: React.PropTypes.object.isRequired,
}

Login.propTypes = {
  process: Process.propTypes.isRequired,
  onSubmit: React.PropTypes.func,
  onCloseAlert: React.PropTypes.func,
}

export default Login;
