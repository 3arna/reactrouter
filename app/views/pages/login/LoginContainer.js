import React from 'react';
import { connectToStores } from 'fluxible-addons-react';

import Process from '../../../models/Process';

import BaseComponent from '../../BaseComponent';
import Login from './Login';

import { loginUser } from '../../../actions/emailActions';
import { apiProcessClear } from '../../../actions/applicationActions';

@connectToStores(['ApplicationStore'], (context, props) =>
  ({process: context.getStore('ApplicationStore').processGet('login')}))

export default class LoginContainer extends BaseComponent {

  constructor(){
    super();
    this._bind('handleSubmit', 'handleCloseAlert');
  }
  
  handleSubmit(e){
    context.executeAction(loginUser, {
      process: this.props.process,
      email: e.target.email.value,
      password: e.target.password.value
    });
    e.preventDefault();
  }

  handleCloseAlert(e){
    context.executeAction(apiProcessClear, this.props.process);
  }

  render(){
    return <Login
      process={this.props.process}
      onSubmit={this.handleSubmit}
      onCloseAlert={this.handleCloseAlert}/>
  }

  static propTypes = {
    process: Process.propTypes.isRequired,
  };
}
