import React from 'react';

import Process from '../../../models/Process';

import FrontLayout from '../../layouts/FrontLayout';
import DefaultFrontButton from '../../components/elements/buttons/DefaultFrontButton';
import FrontButton from '../../components/elements/buttons/FrontButton';
import FrontFormContainer from '../../components/forms/frontForm/FrontFormContainer';
import FrontLink from '../../components/elements/links/FrontLink';

const Home = ({process, onSubmit, onCloseAlert, onToggleSignUpWithEmail, signUpWithEmail}, {translate, apiConfig, getLanguage}) =>
  <FrontLayout
    process={process}
    onCloseAlert={onCloseAlert}
    loadingMessage={translate('words.registering')}
    showExtras>

      <div className="w-max-350px mrg-auto clr-white fnt-sm pdg-t-20px">
        <div className="w-max-200px mrg-auto">
          <FrontButton className="mrg-b-15px" type="facebook" text={translate('phrases.logInWith')} href="/login/facebook"/>
          <FrontButton className="mrg-b-15px" type="google" text={translate('phrases.logInWith')} href="/login/google"/>
          <FrontButton className="mrg-b-15px" type="twitter" text={translate('phrases.logInWith')} href="/login/twitter"/>          
        </div>
      </div>

      <div className="fnt-xs mrg-t-15px mrg-b-5px clr-black">{translate('words.or').toLowerCase()}</div>

      <FrontLink className="mrg-b-40px" href="login">
        {translate('phrases.useEmail')}
      </FrontLink>

  </FrontLayout>

Home.contextTypes = {
  translate: React.PropTypes.func.isRequired,
  apiConfig: React.PropTypes.object.isRequired,
  getLanguage: React.PropTypes.func,
};

Home.propTypes = {
  process: Process.propTypes.isRequired,
  onSubmit: React.PropTypes.func,
  onCloseAlert: React.PropTypes.func,
}

export default Home;
