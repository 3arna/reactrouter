import React from 'react';
import classNames from 'classnames';

import DefaultButton from '../components/elements/buttons/DefaultButton';

const ConfirmEmail = (props, {translate}) => {
  const {
  } = props;

  return <div className="txt-center clr-red md-pdg-l-60px h-min-100 valign-block">
    <div>
      <div className="pos-relative clr-greyd3 fnt-lg">
        <div className="fnt-xxxl">
          <h1 className="mrg-0 pdg-0 fnt-xxxl clr-greyl1 pos-relative">{ translate('words.thanks')}</h1>
        </div>
        <p className="mrg-0 pdg-0 mrg-b-20px pos-absolute top-40 fnt-bold w-100">{ translate('phrases.yourEmailHasBeenConfirmed')}</p>
      </div>
      <DefaultButton className="fnt-sm" href="/">{ translate('words.done')}</DefaultButton>
    </div>
  </div>
}

ConfirmEmail.contextTypes = {
  translate: React.PropTypes.func
}

ConfirmEmail.propTypes = {
}

export default ConfirmEmail;
