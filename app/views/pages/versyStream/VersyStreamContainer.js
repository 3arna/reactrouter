import React from 'react';
import lodash from 'lodash';
import { connectToStores, provideContext } from 'fluxible-addons-react';

import { getStreamData, getSubcontentData } from '../../../actions/contentActions';
import { setFeedScrollPosition } from '../../../actions/component/contentFeedActions';
import { acceptInvite } from '../../../actions/privateContentActions';

import BaseFeedContainer from '../../BaseFeedContainer';
import ContentFeed from '../contentFeed/ContentFeed';

@connectToStores(
  BaseFeedContainer.stores,
  BaseFeedContainer.initProps
)
class VersyStreamContainer extends BaseFeedContainer {
  constructor(){
    super();

    this._bind('handleLoadOlderFeedContent', 'handleLoadNewFeedContent', 'handleAcceptPrivateConversationInvite');
  }

  handleAcceptPrivateConversationInvite(content){
    context.executeAction(acceptInvite, { content, feedIdentifier: this.feedIdentifier });
  }

  handleLoadNewFeedContent(e){
    e.preventDefault();
    context.executeAction(getStreamData, { feedIdentifier: this.feedIdentifier, newContent: true });
  }

  handleLoadOlderFeedContent(){
    const props = this.props;

    if(props.feedLoadingMore){
      return;
    }

    context.executeAction(getStreamData, { feedIdentifier: this.feedIdentifier, nextState: props.feedContentPage.nextState });
  }

  get feedIdentifier(){
    return `versy${this.props.filterName || ''}`;
  }

  get pageTitle(){
    return 'Versy';
  }

  render(){
    const {props, state} = this;

    return <ContentFeed
      bookmarks={ props.bookmarks }
      contentFormVisible={props.contentFormVisible}
      contextMenuVisible={ props.contextMenuVisible }
      contextMenuActiveContentId={ props.contextMenuActiveContentId }
      contextMenuActiveType={ props.contextMenuActiveType }
      feedContentPage={ props.feedContentPage }
      feedLoading={ props.feedLoading }
      feedLoadingMore={ props.feedLoadingMore }
      feedScrollPosition={ props.feedScrollPosition }
      newFeedContentAvailable={ props.newFeedContentAvailable }
      newSubContentAvailable={ props.newSubContentAvailable }
      onAcceptPrivateConversationInvite={ this.handleAcceptPrivateConversationInvite }
      onCheckForNewContent={ this.handleCheckForNewContent }
      onContentFeedScrolled={ this.handleContentFeedScrolled }
      onLoadNewFeedContent={ this.handleLoadNewFeedContent }
      onLoadNewReplies={ this.handleLoadNewReplies }
      onLoadOlderReplies={ this.handleLoadOlderReplies }
      onLoadOlderFeedContent={ this.handleLoadOlderFeedContent }
      pageTitle={ this.pageTitle }
      previousFeedParentContentId={ props.previousFeedParentContentId }
      parentId={ this.feedIdentifier }
      selectedContent={ props.selectedContent }
      selectedContentId={ props.selectedContentId }
      subContentError={ props.subContentError }
      subContentLoading={ props.subContentLoading }
      subContentLoadingMore={ props.subContentLoadingMore }
      subContentPage={ props.subContentPage }
      subContentReplyTo={ props.subContentReplyTo }
      user={ props.user }
      toastProcess={ props.toastProcess }
    />
  }
}

export default VersyStreamContainer;
