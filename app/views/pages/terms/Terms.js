import React, { PropTypes } from "react";

import OneColLayout from '../../layouts/OneColLayout';
import TermsEn from './TermsEn';
import TermsEs from './TermsEs';
import TermsPt from './TermsPt';

const getTranslatedTerms = (language) =>{
  switch(language){
    case 'en':
      return <TermsEn />;
    case 'es':
      return <TermsEs />;
    case 'pt':
      return <TermsPt />;
    default:
      return <TermsEn />;
  }
};

const Terms = (props, { translate, getLanguage }) => {
  return <OneColLayout {...props} title={translate('phrases.termsAndConditions')}>
    { getTranslatedTerms(getLanguage()) }
  </OneColLayout>
}

Terms.contextTypes = {
  getLanguage: React.PropTypes.func,
  translate: React.PropTypes.func
};

export default Terms;
