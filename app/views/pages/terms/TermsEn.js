import React from 'react';

const TermsEn = (props) => <div className="mrg-tb-10em">
  <p>THESE TERMS GOVERN YOUR USE OF OUR VERSY APP AND SERVICE (COLLECTIVELY “Versy”). BY DOWNLOADING, ACCESSING OR USING VERSY, YOU ARE CONCLUDING AND CONFIRMING A LEGALLY BINDING CONTRACT BASED ON THESE TERMS WITH US, MSNGR AG, THE OWNER OF THE VERSY APP AND SERVICE.</p>
  <p>Permission to use Versy</p>
  <p>We grant you a charge-free, limited, freely revocable, personal, non-assignable, non-exclusive permission to use Versy for your own personal and private household purposes. You may also use Versy for your own job or professional activities if you can and do agree to these Terms also on behalf of your employer or other organization (and, in that case, references in these Terms to “you” means you personally and your organization). You are not permitted to resell or distribute Versy. We grant this permission on the condition that you are at least 13 years old, that you have obtained parental consent if you are not yet an adult and that you fully comply with these Terms and all applicable laws. If you use Versy in violation of these Terms or any applicable law, you breach our contract, access our computers without permission and infringe our intellectual property rights and other rights. We reserve all rights not expressly granted herein, including, without limitation, title, ownership and all intellectual property rights to Versy, our technologies and any other tangible and intangible items we own or make available.</p>
  <p>Restrictions</p>
  <p>You must comply with all of our DO&#8217;s and DON&#8217;Ts and you must not access or use Versy in violation of any laws, regulations or third party rights. We reserve the right, to the greatest extent permitted by law, to cancel, terminate and remove any messages, other content, communication history and user accounts at our sole discretion, at any time, including in cases of violations of these Terms and our DO&#8217;s and DON&#8217;Ts. If you have questions, concerns or complaints, please visit http://support.versy.com.</p>
  <p>Changes</p>
  <p>We may change Versy or these Terms, or discontinue Versy at any time, without advance notice, at our sole discretion, for any or no reason. We may also assign our contract to an affiliated entity or an acquirer of all or substantially all our assets, with notice to you. If you do not wish to accept the changes an assignment, your sole remedy is to terminate our contract and discontinue using Versy.</p>
  <p>Warranty Disclaimer</p>
  <p>We provide Versy &#8216;as is,&#8217; without any express or implied warranties or representations. Your use of Versy is at your sole risk. We disclaim any warranties to the maximum extent permitted under applicable law. In particular, we do not warrant that Versy will meet all of your requirements or expectations, that Versy will function uninterruptedly, timely, secure or error-free or that all errors in Versy will be corrected.</p>
  <p>Limitation of Liability</p>
  <p>To the maximum extent permitted by applicable law, we disclaim and exclude all liability of Msngr AG and its directors, officers, employees, affiliates and subcontractors for any direct, indirect, foreseeable or unforeseeable, typical or non-typical or consequential damages or loss of profits, whether derived from torts, breaches of contract, culpa in contrahendo, positive breach of duty or any other legal theory, except damages caused with wilful intent or gross negligence.</p>
  <p>Indemnity</p>
  <p>You represent and warrant that any content and information you post, communicate or transmit to or via Versy is legal, accurate, not defamatory and owned or licensed by you. You are responsible for any such content and other information. You agree to defend us, indemnify us and hold us harmless from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney&#8217;s fees) arising from: (i) your use of and access to Versy; (ii) any violation of these Terms or applicable law; (iii) your violation of any third party right, including without limitation any copyright, property, or privacy right; or (iv) any claim that one of your submissions caused damage to a third party. This defense and indemnification obligation will still apply even if these Terms change or you stop using Versy.</p>
  <p>Choice of Law and Arbitration</p>
  <p>These Terms, our contract and any dispute arising out of or related to these Terms, our contract or Versy, shall be governed by the laws of Switzerland. Any dispute shall be finally settled by arbitration under the rules and auspices of the ICC in Zurich, Switzerland, except that either party may seek injunctive relief in any court of competent jurisdiction. If and to the extent you are using Versy for your own private household purposes, you can request that we (1) conduct arbitration in the capital of your home country, and (2) advance that portion of the arbitration fees that exceed the amount you would have to pay to litigate the case in court (as substantiated in a written application by you). The party that ultimately prevails in arbitration or courts shall be entitled to reimbursement for arbitration and court fees as well as reasonable attorneys fees.</p>
  <p>&nbsp;</p>
  <h4 className="txt-center">DO&#8217;s and DON&#8217;Ts</h4>
  <p>DO</p>
  <ul>
    <li>comply with our Versy Terms and all applicable laws and regulations</li>
    <li>protect your User ID and password and keep them confidential from all third parties</li>
  </ul>
  <p>DO NOT</p>
  <ul>
    <li>share User IDs and passwords with third parties</li>
    <li>submit, store or transfer:
      <ul>
        <li>any material that is copyrighted, protected by trade secret or otherwise subject to third party intellectual property rights</li>
        <li>false information or misrepresent information in any ways that could damage us or any third party</li>
        <li>material that is unlawful, obscene, defamatory, libelous, threatening, harassing, hateful, racially or ethnically offensive, or encourages conduct that would be considered a criminal offense, give rise to civil liability or violate any law</li>
        <li>material containing software viruses, worms, Trojan horses or other harmful computer code, files, scripts, agents or programs</li>
      </ul>
    </li>
    <li>use Versy to send unsolicited marketing messages, unless you obtained prior consent from the recipient or otherwise complied with applicable law</li>
    <li>use Versy to bully, taunt or harass another user</li>
    <li>use another person&#8217;s account or impersonate another person</li>
    <li>interfere with or disrupt the integrity or performance of Versy or the data contained therein</li>
    <li>attempt to gain unauthorized access to Versy or any of our systems, networks or accounts</li>
    <li>resell or charge others for use of or access to the Service</li>
    <li>copy or distribute any part of Versy in any medium.</li>
  </ul>
</div>

export default TermsEn;