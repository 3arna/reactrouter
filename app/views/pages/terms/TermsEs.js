import React from 'react';

const TermsEs = (props) => <div className="mrg-tb-10em">
  <p>ESTOS TÉRMINOS RIGEN SU USO DE NUESTRA APLICACIÓN Y NUESTRO SERVICIO DE VERSY (CONJUNTAMENTE “Versy”). CUANDO DESCARGA, ACCEDE O USA VERSY, USTED ESTÁ PACTANDO Y CONFIRMANDO UN CONTRATO LEGALMENTE VINCULANTE CONFORME A ESTOS TÉRMINOS CON NOSOTROS, MSNGR AG, EL PROPIETARIO DE LA APLICACIÓN Y DEL SERVICIO DE VERSY.</p>
  <p>Permiso para usar Versy</p>
  <p>Le otorgamos un permiso libre de cargos, libremente revocable, personal, intransferible, no exclusive para usar Versy para propósitos de su propio uso personal y de domicilio privado. Usted también puede usar Versy para su propio empleo o actividades profesionales si usted puede y en efecto acepta estos Términos también en nombre de su empleador u otra organización (y, en dicho caso, las referencias en estos Términos a “usted” significa usted personalmente y su organización). Usted no tiene permitido revender o distribuir Versy. Otorgamos este permiso bajo la condición de que usted tenga al menos 13 años de edad, que usted ha obtenido consentimiento de sus padres si usted aún no es un adulto y que usted cumple completamente con estos Términos y todas las leyes pertinentes. Si usted usa Versy en violación de estos Términos o cualquier ley pertinente, usted incumpla nuestro contrato, acceda a nuestros computadores sin permiso e infrinja nuestros derechos de propiedad intelectual y otros derechos. Nos reservamos todos los derechos no otorgados aquí, incluidos, sin limitación, título, titularidad y todos los derechos de propiedad intelectual sobre Versy, nuestras tecnologías y cualesquiera otros artículos tangibles e intangibles que tengamos o que pongamos a disposición.</p>
  <p>Restricciones</p>
  <p>Usted debe cumplir con todos nuestros HAGA y NO HAGA y usted no debería acceder o usar Versy en violación de cualesquiera leyes, regulaciones o derechos de terceros. Nos reservamos el derecho, hasta el máximo permitido por la ley, a cancelar, terminar y eliminar cualesquiera mensajes, otro contenido, historial de comunicación y cuentas de usuario a nuestra sola discreción, en cualquier momento, inclusive en casos de violaciones de estos Términos y nuestros HAGA y NO HAGA. Si tiene preguntas, inquietudes o quejas, visite http://support.versy.com.</p>
  <p>Cambios</p>
  <p>Podremos modificar Versy o estos Términos, o discontinuar Versy en cualquier momento, sin aviso anticipado y a nuestra entera discreción por cualquier o ninguna razón. También podremos transferir nuestro contrato a una entidad filial o a un adquirente de todos o sustancialmente todos nuestros activos, avisándole a usted. Si usted no desea aceptar los cambios de una asignación, su única solución es dar por concluido nuestro contrato y discontinuar usando Versy.</p>
  <p>Renuncia de Garantía</p>
  <p>Suministramos Versy “tal cual es” sin ninguna garantía o representación expresa o implícita. Su uso de Versy es bajo su propio riesgo. Renunciamos a cualesquiera garantías hasta por el máximo permitido bajo las leyes pertinentes. En particular, no garantizamos que Versy cumpla con todos sus requerimientos o expectativas, que Versy funcionará ininterrumpidamente, oportunamente, de forma segura o libre de errores, o que todos los errores en Versy sean corregidos.</p>
  <p>Limitación de Responsabilidad</p>
  <p>Por el máximo permitido por las leyes pertinentes, renunciamos y excluimos toda la responsabilidad de msngr AG y sus directores, oficiales, empleados, filiales y subcontratistas para cualesquiera daños directos, indirectos, previsibles o imprevisibles, típicos o atípicos o consecuenciales o lucro cesante, ya sea derivado de responsabilidad extracontractual, incumplimientos de contratos, culpa in contrahendo, incumplimiento afirmativo de deberes o cualquier otra teoría legal, excepto daños causados por culpa grave.</p>
  <p>Indemnidad</p>
  <p>Usted declara y garantiza que cualquier contenido e información que usted publique, comunique o transmita a o a través de Versy es legal, precisa, no difamatoria y de su propiedad o licenciada por usted. Usted es responsable por cualquier tal contenido u otra información. Usted se obliga a defendernos, indemnizarnos y mantenernos indemnes desde y contra cualesquiera y todas las reclamaciones, daños, obligaciones, pérdidas, responsabilidades, costos o deudas, y gastos (incluidos pero sin limitarse a honorarios de abogados) que surjan de: (i) su uso de o acceso a Versy; (ii) cualquier violación de estos Términos o las leyes pertinentes; (iii) su violación de cualquier derecho de terceros, inclusive, entre otros, cualquier derecho de autor, propiedad, o derecho de privacidad; o (iv) cualquier reclamación de que cualquiera de sus envíos causaron daño a un tercero. Esta obligación de defensa e indemnización continuará aplicando incluso si estos Términos cambian o usted deja de usar Versy.</p>
  <p>Foro y Arbitraje</p>
  <p>Estos Términos, nuestro contrato y cualquier controversia que surja de o se relaciones con estos Términos, nuestro contrato o Versy, se regirán por las leyes de Suiza. Cualquier controversia será resuelta de manera definitiva por arbitraje bajo las reglas y auspicios de la Cámara de Comercio Internacional en Zúrich, Suiza, excepto que cualquiera parte solicite medida cautelar en cualquier corte de jurisdicción competente. Sí y en tanto usted esté usando Versy para sus propios propósitos domésticos, usted puede solicitar que nosotros (1) realicemos el arbitraje en la capital de su país de origen, y (2) avancemos aquella porción de las tasas de arbitraje que exceden la cantidad que usted tendría que pagar para litigar el caso en la corte (según se sustancie en una solicitud suya por escrito). La parte que finalmente prevalezca en arbitraje y las cortes deberá estar legitimado a un reembolso por las tasas de arbitraje y corte así como honorarios de abogados razonables.</p>
  <p>&nbsp;</p>
  <h4 className="txt-center">HAGA y NO HAGA</h4>
  <p>HAGA</p>
  <ul>
    <li>Cumpla con nuestros Términos de Versy y todas las leyes pertinentes y regulaciones.</li>
    <li>Proteja su identificación (ID) de usuario y contraseña y manténgalos bajo confidencialidad frente a terceros.</li>
  </ul>
  <p>NO HAGA</p>
  <ul>
    <li>Comparta identificaciones (ID) de usuario y claves de acceso con terceros.</li>
    <li>Presente, almacene o transfiera:
      <ul>
        <li>Ningún material que tenga derecho de autor, protegido por secretos comerciales, o de cualquier otra forma sujetos a derechos de propiedad intelectual de terceros.</li>
        <li>Información falsa o falsifique información de cualesquiera maneras que pudieran perjudicar a nosotros o a un tercero.</li>
        <li>Material que sea ilegal, obsceno, difamatorio, amenazante, acosador, odioso, racial o étnicamente ofensivo, o promueva conductas que puedan considerarse infracciones penales, den lugar a responsabilidad civil o violen cualquier ley.</li>
        <li>Material que contenga virus de software, gusanos, caballos troyanos u otros códigos de ordenador, archivos, scripts, agentes o programas maliciosos.</li>
      </ul>
    </li>
    <li>Use Versy para enviar mensajes de mercadeo no solicitados, salvo que usted obtenga el consentimiento previo del receptor o de otra manera haya cumplido con las leyes pertinentes.</li>
    <li>Use Versy para matonear, tentar o acosar a otro usuario.</li>
    <li>Use la cuenta de otra persona o hágase pasar por otra persona.</li>
    <li>Interfiera con o irrumpa la integridad o el desempeño de Versy o de los datos allí contenidos.</li>
    <li>Intente obtener acceso no autorizado a Versy o a cualquiera de nuestros sistemas, redes o cuentas.</li>
    <li>Revenda o le cobre a otros por el uso de o el acceso al Servicio.</li>
    <li>Copie o distribuya cualquier parte de Versy en cualquier medio.</li>
  </ul>
</div>

export default TermsEs;