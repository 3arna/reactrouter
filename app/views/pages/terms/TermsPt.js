import React from 'react';

const TermsPt = (props) => <div className="mrg-tb-10em">
  <p>ESTES TERMOS REGEM O SEU USO DO NOSSO APLICATIVO E SERVIÇO VERSY (COLETIVAMENTE &#8220;Versy&#8221;). AO FAZER O DOWNLOAD, ACESSAR OU USAR O VERSY, VOCÊ ESTÁ CONCLUINDO E CONFIRMANDO CONOSCO, MSNGR AG, PROPRIETÁRIOS DO APLICATIVO E SERVIÇO VERSY, UM CONTRATO LEGALMENTE VINCULANTE COM BASE NESTES TERMOS.</p>
  <p>Permissão para usar o Versy</p>
  <p>Nós concedemos a você uma permissão gratuita, limitada, livremente revogável, pessoal, intransferível, não exclusiva para usar o Versy para seus próprios fins domésticos pessoais e privados. Você também poderá usar o Versy para seu próprio trabalho ou atividades profissionais se você puder concordar e de fato concordar com estes Termos também em nome de seu empregador ou outra organização (e, nesse caso, referências nestes Termos a &#8220;você&#8221; significam você pessoalmente e a sua organização). Não é permitido revender ou distribuir o Versy. Nós concedemos esta permissão sob a condição de que você tenha ao menos 13 anos, que tenha obtido consentimento dos seus pais caso ainda não seja um adulto e que tenha cumprido integralmente estes Termos e todas as leis aplicáveis. Caso você use o Versy violando estes Termos ou qualquer lei aplicável, você violará o nosso contrato, acessará nossos computadores sem permissão e infringirá nossos direitos de propriedade intelectual e demais direitos. Nós reservamos todos os direitos que não tenham sido expressamente concedidos neste instrumento, incluindo, sem limitação, titularidade, propriedade e todos os direitos de propriedade intelectual sobre o Versy, nossas tecnologias e quaisquer outros itens tangíveis e intangíveis que possuirmos ou tornarmos disponíveis.</p>
  <p>Restrições</p>
  <p>Você deverá cumprir com todos os nossos SIM e NÃO e não deverá acessar ou usar o Versy violando quaisquer leis, regulamentos ou direitos de terceiros. Reservamos o direito, na máxima extensão permitida pela legislação, de cancelar, rescindir e remover quaisquer mensagens, outros conteúdos, histórico de comunicação e contas de usuários, unicamente a nosso critério, a qualquer momento, incluindo casos de violações destes Termos e nossos SIM e NÃO. Se você tiver dúvidas, preocupações ou reclamações, por favor, visite: http://support.versy.com.</p>
  <p>Alterações</p>
  <p>Nós poderemos alterar o Versy ou estes Termos, ou descontinuar o Versy a qualquer momento, sem aviso prévio, unicamente a nosso critério, por qualquer ou nenhuma razão. Nós também poderemos ceder nosso contrato a uma empresa afiliada ou uma adquirente de todos ou substancialmente todos os nossos ativos, com notificação a você. Se você não desejar aceitar as alterações, uma cessão, o seu único recurso será rescindir nosso contrato e descontinuar o uso do Versy.</p>
  <p>Isenção de Garantia</p>
  <p>Nós fornecemos o Versy &#8220;no estado em que se encontra&#8221;, sem quaisquer garantias ou declarações expressas ou implícitas. O seu uso do Versy é a seu próprio risco. Nos isentamos de qualquer garantia na máxima extensão permitida pela legislação aplicável. Em particular, nós não garantimos que o Versy atenderá todas as suas exigências ou expectativas, que o Versy funcionará ininterruptamente, tempestivamente, de forma segura ou livre de erros ou que todos os erros no Versy serão corrigidos.</p>
  <p>Limitação de Responsabilidade</p>
  <p>Na máxima extensão permitida pela legislação aplicável, nos isentamos e excluímos qualquer responsabilidade da msngr AG e seus conselheiros, diretores, empregados, afiliadas e subcontratadas por quaisquer danos ou lucros cessantes diretos, indiretos, previsíveis ou não, típicos ou não, ou consequenciais, derivados de ilícitos, inadimplemento de contrato, culpa in contrahendo, violação positiva de dever ou qualquer outra teoria legal, exceto danos causados por dolo ou culpa grave.</p>
  <p>Indenização</p>
  <p>Você declara e garante que qualquer conteúdo ou informação que você publicar, comunicar ou transmitir para ou por meio do Versy é legal, precisa, não difamatória e de propriedade ou licenciada por você. Você é responsável por qualquer referido conteúdo e outras informações. Você concorda em nos defender, indenizar e manter indenes de e contra todas e quaisquer reclamações, danos, obrigações, perdas, responsabilidades, custos ou débitos, e despesas (incluindo, mas sem limitação, honorários advocatícios) decorrentes de (i) seu uso ou acesso ao Versy; (ii) qualquer violação destes Termos ou legislação aplicável; (iii) sua violação de qualquer direito de terceiro, incluindo, sem limitação, qualquer direito autoral, de propriedade ou de privacidade; ou (iv) qualquer reclamação de que uma de suas submissões causou danos a um terceiro. Essa obrigação de defesa e indenização se aplicará ainda que estes Termos mudem ou você pare de usar o Versy.</p>
  <p>Escolha da lei e Arbitragem</p>
  <p>Estes Termos, nosso contrato e qualquer disputa oriunda ou relacionada a estes Termos, nosso contrato ou ao Versy serão regidas pelas leis da Suíça. Qualquer disputa será finalmente resolvida por arbitragem sob as regras e égide do ICC em Zurique, Suíça, mas qualquer uma das partes poderá ajuizar medidas cautelares em qualquer tribunal de jurisdição competente. Se e na medida em que você esteja usando o Versy para seus próprios usos domésticos privados, você poderá solicitar que nós (1) realizemos a arbitragem na capital de seu país de residência e (2) adiantemos a porção das taxas de arbitragem que excederem a quantia que você teria que pagar para litigar o caso em juízo (conforme comprovado em um pedido seu por escrito). A parte que finalmente prevalecer na arbitragem ou tribunal terá o direito ao reembolso das taxas de arbitragem e judiciais, assim como de honorários advocatícios razoáveis.</p>
  <p>&nbsp;</p>
  <h4 className="txt-center">SIM e NÃO</h4>
  <p>SIM</p>
  <ul>
    <li>cumpra nossos Termos do Versy e todas as leis e regulamentos aplicáveis</li>
    <li>proteja seu ID de Usuário e senha e os mantenha em sigilo de quaisquer terceiros</li>
  </ul>
  <p>NÃO</p>
  <ul>
    <li>compartilhe seu ID de Usuário e senhas com terceiros</li>
    <li>submeta, armazene ou transfira:
      <ul>
        <li>qualquer material que seja objeto de proteção autoral, protegido por segredo de negócio ou de outra forma sujeito a direitos de propriedade intelectual de terceiros</li>
        <li>informações falsas ou informações incorretas de qualquer forma que possa causar dano a nós ou a qualquer terceiro</li>
        <li>material que seja ilegal, obsceno, difamatório, calunioso, ameaçador, de assédio, de ódio, racial ou etnicamente ofensivo, ou que incentive conduta que possa ser considerada como crime, resulte em responsabilidade civil ou viole qualquer lei</li>
        <li>material contendo vírus, worms, cavalos de Troia, ou outro código, arquivos, scripts, agentes ou programas de computador nocivos</li>
      </ul>
    </li>
    <li>use o Versy para enviar mensagens de marketing não solicitadas, a menos que você tenha obtido consentimento prévio do destinatário ou de outra forma cumprido a legislação aplicável</li>
    <li>use o Versy para fazer bullying, insultar ou assediar outros usuários</li>
    <li>use a conta de outra pessoa ou se passe por outra pessoa</li>
    <li>interfira ou interrompa a integridade ou desempenho do Versy ou dos dados ali contidos</li>
    <li>tente conseguir acesso não autorizado ao Versy ou qualquer de nossos sistemas, redes ou contas</li>
    <li>revenda ou cobre de outros pelo uso ou acesso ao Serviço</li>
    <li>copie ou distribua qualquer parte do Versy em qualquer mídia.</li>
  </ul>
</div>

export default TermsPt