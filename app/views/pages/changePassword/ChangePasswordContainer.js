import React from 'react';
import { connectToStores } from 'fluxible-addons-react';

import Process from '../../../models/Process';

import BaseComponent from '../../BaseComponent';
import ChangePassword from './ChangePassword';

import { passwordChange } from '../../../actions/emailActions';
import { apiProcessClear } from '../../../actions/applicationActions';
import * as validationUtils from '../../../utils/validationUtils';

@connectToStores(['ApplicationStore'], (context, props) =>
  ({process: context.getStore('ApplicationStore').processGet('changePassword')}))

export default class ChangePasswordContainer extends BaseComponent {

  constructor(){
    super();
    this._bind('handleValidateInput', 'submitEnabled', 'handleTogglePasswordType', 'handleSubmit', 'handleCloseAlert');
    this.state = {
      showAsPassword: true,
      email: false,
      token: false,
      suitable: {
        password: false,
        passwordRepeat: false,
      }
    };
  }

  componentDidMount(){
    var qs = {};
    window.location.search.replace('?', '').split('&').forEach((param) => {
      param = param.split('=');
      qs[param.shift()] = param.pop();
    });
    this.setState({token: qs.token, email: unescape(qs.email)});
  }

  handleValidateInput(e){
    const state = this.state;
    state.suitable[e.target.name] = validationUtils.validInput(e.target.value, {
      type: e.target.type,
      minLength: this.context.apiConfig.minPasswordChars || 8,
      maxLength: 40,
    });

    state[e.target.name] = e.target.value;
    this.setState(state);
  }

  submitEnabled(){
    return this.state.suitable.password
      && this.state.suitable.passwordRepeat
      && this.state.password === this.state.passwordRepeat
      /*|| !this.state.email
      || !this.state.token;*/
  }

  handleTogglePasswordType(){
    this.setState({showAsPassword: !this.state.showAsPassword});
  }

  handleSubmit(e){
    context.executeAction(passwordChange, {
      process: this.props.process,
      password: e.target.password.value,
      email: this.state.email,
      token: this.state.token,
    });
    e.preventDefault();
  }

  handleCloseAlert(e){
    context.executeAction(apiProcessClear, this.props.process);
  }

  render(){
    return <ChangePassword
      process={this.props.process}
      showAsPassword={this.state.showAsPassword}
      submitEnabled={this.submitEnabled()}
      warrning={!this.state.email || !this.state.token}
      onValidateInput={this.handleValidateInput}
      onTogglePasswordType={this.handleTogglePasswordType}
      onCloseAlert={this.handleCloseAlert}
      onSubmit={this.handleSubmit}
    />
  }

  static propTypes = {
    process: Process.propTypes.isRequired,
  };

  static contextTypes = {
    apiConfig: React.PropTypes.object,
  }
}
