import React from 'react';
import classNames from 'classnames';

import Process from '../../../models/Process';

import FrontLayout from '../../layouts/FrontLayout';
import FrontInput from '../../components/elements/inputs/FrontInput';
import FrontButton from '../../components/elements/buttons/FrontButton';
import DefaultFrontButton from '../../components/elements/buttons/DefaultFrontButton';
import DefaultLink from '../../components/elements/links/DefaultLink';

const ChangePassword = (props, {translate}) => {
  const {
    process,
    warrning,
    showAsPassword,
    submitEnabled,
    onTogglePasswordType,
    onSubmit,
    onValidateInput,
    onCloseAlert
  } = props;

  return <FrontLayout
    onCloseAlert={onCloseAlert}
    process={process}
    loadingMessage={translate('phrases.changingPassword')}
    title={translate('phrases.changePassword')}
    description={false}
    className="clr-white w-max-800px w-100 fix-bb mrg-auto">

    <div className="w-max-300px mrg-auto clr-white fnt-sm pdg-b-40px">
      {warrning && 
        <div className="clr-white brd-rad-30px bg-redl2 pdg-10px mrg-b-10px w-max-500px pdg-rl-30px fnt-sm">
          {translate('phrases.invalidEmailAddress')}
        </div>}

      <form onSubmit={onSubmit}>
        <div className="fnt-sm mrg-b-10px">
          <FrontInput
          type="password"
          name="password"
          placeholder={translate('words.password')}
          onChange={onValidateInput}
          showAsPassword={showAsPassword}
          togglePasswordType={onTogglePasswordType}/>
        </div>
        <div className="fnt-sm mrg-b-10px">
          <FrontInput
          type="password"
          name="passwordRepeat"
          placeholder={translate('phrases.repeatPassword')}
          onChange={onValidateInput}
          showAsPassword={showAsPassword}
          togglePasswordType={onTogglePasswordType}/>
        </div>
        <FrontButton text={translate('phrases.changePassword')} disabled={!submitEnabled}/>
      </form>
    </div>

  </FrontLayout>
}

ChangePassword.contextTypes = {
  translate: React.PropTypes.func
}

ChangePassword.propTypes = {
  process: Process.propTypes.isRequired,
  warrning: React.PropTypes.bool.isRequired,

  showAsPassword: React.PropTypes.bool,
  submitEnabled: React.PropTypes.bool,
  onTogglePasswordType: React.PropTypes.func,
  onValidateInput: React.PropTypes.func,
  onSubmit: React.PropTypes.func,
  onCloseAlert: React.PropTypes.func,
}

export default ChangePassword;
