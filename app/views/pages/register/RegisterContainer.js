import React from 'react';
import { connectToStores } from 'fluxible-addons-react';

import Process from '../../../models/Process';

import BaseComponent from '../../BaseComponent';
import Register from './Register';
import { registerUser } from '../../../actions/emailActions';
import { apiProcessClear } from '../../../actions/applicationActions';

@connectToStores(['ApplicationStore'], (context, props) =>
  ({process: context.getStore('ApplicationStore').processGet('registration')}))

export default class RegisterContainer extends BaseComponent {

  constructor(){
    super();
    this.state = { signUpWithEmail: false };
    this._bind('handleSubmit', 'handleCloseAlert', 'hanldeToggleSignUpWithEmail');
  }

  handleSubmit(e){
    e.preventDefault();
    context.executeAction(registerUser, {
      process: this.props.process,
      name: e.target.name.value,
      email: e.target.email.value,
      password: e.target.password.value
    });
  }

  handleCloseAlert(e){
    context.executeAction(apiProcessClear, this.props.process);
  }

  hanldeToggleSignUpWithEmail(e){
    this.setState({signUpWithEmail: !this.state.signUpWithEmail});
  }

  render(){
    return <Register
      onSubmit={this.handleSubmit}
      onCloseAlert={this.handleCloseAlert}
      process={this.props.process}
      signUpWithEmail={this.state.signUpWithEmail}
      onToggleSignUpWithEmail={this.hanldeToggleSignUpWithEmail}/>
  }

  static propTypes = {
    process: Process.propTypes.isRequired,
  };

}
