import React from 'react';

import Process from '../../../models/Process';

import FrontLayout from '../../layouts/FrontLayout';
import DefaultFrontButton from '../../components/elements/buttons/DefaultFrontButton';
import FrontButton from '../../components/elements/buttons/FrontButton';
import FrontFormContainer from '../../components/forms/frontForm/FrontFormContainer';
import FrontLink from '../../components/elements/links/FrontLink';

const Register = ({process, onSubmit, onCloseAlert, onToggleSignUpWithEmail, signUpWithEmail}, {translate, apiConfig, getLanguage}) =>
  <FrontLayout
    process={process}
    onCloseAlert={onCloseAlert}
    description={translate('phrases.useEmailToRegister')}
    loadingMessage={translate('words.registering')}
    showExtras
    title={translate('words.register')}>

      <div className="w-max-350px mrg-auto clr-white fnt-sm">
        <div className="w-max-250px mrg-auto">
          <FrontFormContainer
            theme="registration"
            onSubmit={onSubmit}
            minPasswordLength={apiConfig.minPasswordChars}/>
        </div>
      </div>

      <div className="fnt-xs mrg-t-15px mrg-b-10px clr-black">{translate('words.or').toLowerCase()}</div>

      <FrontLink className="mrg-b-15px" href="/">
        {translate('phrases.useMySocialAccount')}
      </FrontLink>

  </FrontLayout>

Register.contextTypes = {
  translate: React.PropTypes.func.isRequired,
  apiConfig: React.PropTypes.object.isRequired,
  getLanguage: React.PropTypes.func,
};

Register.propTypes = {
  process: Process.propTypes.isRequired,
  onSubmit: React.PropTypes.func,
  onCloseAlert: React.PropTypes.func,
}

export default Register;
