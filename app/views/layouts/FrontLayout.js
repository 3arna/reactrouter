import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';

import Process from '../../models/Process';

import FrontButton from '../components/elements/buttons/FrontButton';
import InProgressFog from '../components/elements/fogs/InProgressFog';
import FullFog from '../components/elements/fogs/FullFog';
import FrontErrorAlert from '../components/elements/alerts/FrontErrorAlert';
import FrontSuccessAlert from '../components/elements/alerts/FrontSuccessAlert';
import LanguageDropdown from '../components/elements/dropdowns/LanguageDropdown';
import FrontLink from '../components/elements/links/FrontLink';
import DefaultLink from '../components/elements/links/DefaultLink';

const _handleParallax = (e) => {
  var top = -(2000/2 - e.clientY)/50;
  var left = -(2000/2 - e.clientX)/50;

  const img = document.getElementById('img');
  img.style.transform = `translate(${left}px,${top}px)`;

  //onMouseMove={_handleParallax}
}

const FrontLayout = (props, {translate, apiConfig, getLanguage}) => {
  const {
    children, 
    className, 
    description,
    loadingMessage, 
    onCloseAlert,
    process,
    returnLinkHref,
    returnLinkText,
    showExtras,
    title,
  } = props;
  //
  return <div className="valign-block txt-center pos-relative h-100">

    <div className="pos-absolute h-100 over-hide w-100 bg-op-05 md-bg-none">
      <img 
        id="img" 
        src="/img/cards-long-bg-low.jpg" 
        className="pos-relative z-negative md-dsp-none" 
        width="100%"
        style={{maxWidth: '1400px'}}/>
      {false && <div id="img" className="pos-relative z-negative bg-cover bg-center trans-transform md-bg-none" 
        style={{
          backgroundImage: "url('/img/cards-bg.jpg')", 
          top: 0, 
          left: 0,
          marginLeft: '-2.5%',
          marginTop: '-2.5%',
          width: '105%',
          height: '110%'}}/>}
    </div>
    
    <div className="pos-relative">

      <div className={`clr-white w-max-700px w-100 fix-bb mrg-auto pdg-tb-20px ${props.className}`}>

        <div className="bg-white-op-095 clr-main pos-relative pdg-t-3 pdg-b-3">

          <div className="pos-absolute left-0 pdg-rl-3 dsp-inline">
            <LanguageDropdown className="brd-grey w-150px flt-left fnt-xs md-dsp-none" noIcon/>
            {false && <FrontLink className="mrg-b-30px flt-right md-dsp-none" href={returnLinkHref || '/login'}>
              {returnLinkText || `${translate('phrases.alreadyHaveAnAccount')} ${translate('phrases.logInHere')}`}
            </FrontLink>}
          </div>

          <Link to="/" className="dsp-inline"><img src="/img/versy-rainbow.svg" className="md-w-50px dsp-inline" width="80"/></Link>
          <h1 className="mrg-0 mrg-t-20px mrg-b-10px fnt-xxl fnt-main-reg fnt-600">{title || translate('words.join') + ' Versy!'} </h1>
          <h4 className="pdg-0 mrg-auto clr-main txt-center mrg-b-40px fnt-100 md-dsp-none">
            {description || translate('phrases.findTheContentYouLove')}.
          </h4>

          {false && <FrontLink className="mrg-b-15px dsp-none md-dsp-block" href={returnLinkHref || '/login'}>
            {returnLinkText || `${translate('phrases.alreadyHaveAnAccount')} ${translate('phrases.logInHere')}`}
          </FrontLink>}

          <div className="pdg-rl-3">
            {children}
          </div>

          {showExtras && <p className="clr-blackl3 fnt-xxs w-150px mrg-auto mrg-b-30px md-w-auto line-15 md-fnt-xs">
            {translate('phrases.byContinuingYouAgree')}
            <DefaultLink className="txt-deco-under fnt-main-reg" href="/terms">{translate('phrases.termsAndConditions')}</DefaultLink> &
            <DefaultLink className="txt-deco-under fnt-main-reg" href="/privacy-policy">{translate('phrases.privacyPolicy')}</DefaultLink>
          </p>}

          {showExtras && <div className="mrg-tb-10px">
            <a href={apiConfig.appStoreAndroid}>
              <img className="dsp-inline mrg-3px" src={`/img/stores/play-badge-${getLanguage()}.png`} height="30"/>
            </a>
            <a href={apiConfig.appStoreIOS}>
              <img className="dsp-inline mrg-3px" src={`/img/stores/app-store-badge-${getLanguage()}.svg`} height="30"/>
            </a>
          </div>}

          <FrontLink href="/what-is-versy" className="mrg-b-10px">{translate('phrases.whatIsVersy')}</FrontLink>

          <LanguageDropdown className="brd-grey w-150px fnt-xs dsp-none md-dsp-block mrg-auto" noIcon/>

        </div>

        {process.inProgress &&
          <InProgressFog message={loadingMessage}/>
        }

        {(process.error || process.success) &&
          <FullFog>
            {process.success &&
              <FrontSuccessAlert>
                <p className="clr-black">
                  {process.success}<br/>
                </p>
                <FrontButton onClick={onCloseAlert} text={translate('words.ok')}/>
              </FrontSuccessAlert>
            }
            {process.error &&
              <FrontErrorAlert>
                <p className="clr-blackl3">
                  {process.error}<br/>
                </p>
                <FrontButton onClick={onCloseAlert} text={translate('words.close')}/>
              </FrontErrorAlert>
            }
          </FullFog>
        }

      </div>

    </div>

  </div>
}

FrontLayout.contextTypes = {
  translate: React.PropTypes.func.isRequired,
  apiConfig: React.PropTypes.object.isRequired,
  getLanguage: React.PropTypes.func,
};

FrontLayout.propTypes = {
  process: Process.propTypes.isRequired,
  loadingMessage: React.PropTypes.string.isRequired,

  children: React.PropTypes.node,
  className: React.PropTypes.string,
  buttonJsx: React.PropTypes.node,
  onCloseAlert: React.PropTypes.func,
}

export default FrontLayout;
