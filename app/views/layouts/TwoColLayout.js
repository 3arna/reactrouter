import React from 'react';
import classNames from 'classnames';
import lodash from 'lodash';

import Process from '../../models/Process';

import FrontButton from '../components/elements/buttons/FrontButton';
import InProgressFog from '../components/elements/fogs/InProgressFog';
import FullFog from '../components/elements/fogs/FullFog';
import FrontErrorAlert from '../components/elements/alerts/FrontErrorAlert';
import FrontSuccessAlert from '../components/elements/alerts/FrontSuccessAlert';
import ContentFeedFormContainer from '../components/forms/contentFeedForm/ContentFeedFormContainer';


const TwoColLayout = (props, { authenticatedUser }) => {

  const lastChild = !!props.children.length && props.children.pop();

  return <div className="fix-clear w-100 bg-greyl1 h-100">
    <div className="md-pdg-l-60px lg-pdg-r-33 h-100">

      {(props.contentFormOn || props.isMobile || props.isDesktop) &&
        <ContentFeedFormContainer isMobile={props.isMobile} parentContent={props.parentContent}/>}

      {props.children}
    </div>
    <div className="valign-top bg-white col-4 top-0 right-0 md-dsp-none bg-white pos-fixed z-10 boxshadow-l-5 h-100">
      {lastChild}
      {authenticatedUser && props.isDesktop && <ContentFeedFormContainer parentContent={props.parentContent} isDesktop={props.isDesktop} />}
    </div>
  </div>
}

TwoColLayout.contextTypes = {
  authenticatedUser: React.PropTypes.object
};

TwoColLayout.propTypes = {
  children: React.PropTypes.node,
  isDesktop: React.PropTypes.bool,
  isMobile: React.PropTypes.bool,
}

export default TwoColLayout;
