import React from 'react';
import classNames from 'classnames';
import lodash from 'lodash';

const OverlayLayout = ({onSubmit, onCancel, title, children, isFullScreen}, {translate}) => 
  <div className={`pos-absolute z-5 top-0 left-0 md-col-12 mrg-auto h-100 bg-greyl2 pdg-t-100px md-pdg-l-60px fix-bb w-100 ${!isFullScreen && 'col-8'}`}>

    <div className="fix-bb pos-absolute z-2 top-0 left-0 w-100 md-pdg-l-60px sm-pdg-t-50px">
      <div className="valign-block pdg-tb-10px pdg-rl-3 bg-white boxshadow-b fadeIn fnt-sm">
        <div className="w-min-50px md-w-auto fnt-sm">
          <span onClick={onCancel} className="clr-black crs-point">{translate('words.cancel')}</span>
        </div>
        <div className="w-100">
          <div className="w-max-700px pdg-rl-3 valign-block mrg-auto">
            <h3 className="pdg-0 pdg-5px fnt-300 txt-center w-100">{title}</h3>
          </div>
        </div>
        <div className="w-min-50px md-w-auto txt-right fnt-sm">
          <span onClick={onSubmit} className="clr-main crs-point">{translate('words.done')}</span>
        </div>
      </div>
    </div>

    <div className="w-max-600px mrg-auto pdg-rl-3">
      {children}
    </div>

  </div>

OverlayLayout.contextTypes = {
  translate: React.PropTypes.func
}

export default OverlayLayout;