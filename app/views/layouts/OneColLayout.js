import React from 'react';
import classNames from 'classnames';
import lodash from 'lodash';
import BackButton from '../components/elements/buttons/BackButton';

const _getPageClassNames = (mobNavHide, isLoggedIn, noHeader) => classNames({
  'bg-greyl2 fix-bb h-min-100 md-pdg-l-60px': true,
  //'pdg-t-50px': !mobNavHide && isLoggedIn,
  'pdg-t-50px': !mobNavHide && !noHeader,
  //'pdg-t-100px': !isLoggedIn
});

const _getHeaderClassNames = (mobNavHide, isLoggedIn) => {
  return classNames({
    'top-0 fix-bb pos-fixed z-2 left-0 w-100 md-pdg-l-60px': true,
    'sm-pdg-t-50px': !mobNavHide && isLoggedIn,
    //'': isLoggedIn,
    //'top-50px': !isLoggedIn
  });
}

const OneColLayout = ({previousUrl, title, children, isLoggedIn, mobNavHide, appInfo, noHeader, maxWidth}) => {

  previousUrl = !!previousUrl ? previousUrl : isLoggedIn && '/feed/user' || '/';

  return <div className={_getPageClassNames(mobNavHide, isLoggedIn, noHeader)}>
  
    {!noHeader && <div className={_getHeaderClassNames(mobNavHide, isLoggedIn)}>
      <div className="valign-block pdg-tb-10px pdg-rl-3 bg-white boxshadow-b fadeIn fnt-sm">
        <div className="w-min-50px fnt-sm">
          <BackButton href={previousUrl} />
        </div>
        <div className="w-100">
          <div className="w-max-700px pdg-rl-3 valign-block mrg-auto">
            <h3 className="pdg-0 pdg-5px txt-center w-100">{title}</h3>
          </div>
        </div>
        <div className="w-min-50px txt-right fnt-sm"/>
      </div>
    </div>}

    <div className={`txt-left mrg-auto w-100 pdg-rl-3 ${!maxWidth && 'w-max-700px'}`}>
      {children}
    </div>
  </div>
}

OneColLayout.propTypes = {
  children: React.PropTypes.node,
  previousUrl: React.PropTypes.any,
  title: React.PropTypes.string,
}

export default OneColLayout;
