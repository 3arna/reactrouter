import React from 'react';
import DocumentTitle from 'react-document-title';
import DocumentMeta from 'react-doc-meta';
import ApplicationStore from '../stores/ApplicationStore';

class Html extends React.Component {

  static defaultProps = {
    script: [],
    css: []
  }

  render() {

    //console.log('tadam', this.props.context);

    const {scripts, styles, version} = this.props;
    const applicationStore = this.props.context.getStore(ApplicationStore);
    let metaTags = DocumentMeta.rewind();

    return (
      <html>
        <head>
          <meta charSet="utf-8" />
          <title>{DocumentTitle.rewind()}</title>
          <meta name="viewport" content="width=device-width, user-scalable=no" />
          { styles.map((href, k) => <link key={k} rel="stylesheet" type="text/css" href={`/css/${href}?v=${version}`} />) }
          <script dangerouslySetInnerHTML={{__html: this.props.state}}/>
          { scripts.map((href, k) => <script key={k} src={`/js/${href}?v=${version}`} />) }
          {
            metaTags.map((tag, index) =>
              <meta data-doc-meta="true" key={index} {...tag} />)
          }
        </head>
        <body>
          <div id="app" dangerouslySetInnerHTML={{__html: this.props.markup}}/>
        </body>
      </html>
    );
  }
}

export default Html;
