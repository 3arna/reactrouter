import MediaContainer from './MediaContainer';
import EntitySummary from './EntitySummary';
import { PropTypes } from 'react';
import { clientConfigs } from '../configs';

class User extends MediaContainer {
  constructor(properties){
    super(properties);

    if(!properties){
      return {};
    }

    this.emailAddress = properties.emailAddress;
    this.id = properties.id;
    this.displayName = properties.displayName;
    this.description = properties.description;
    this.userName = properties.username;
    this.updated = properties.updated;
    this.url = properties.url;
    this.followerCount = properties.followerCount;
    this.interestCount = properties.interestCount;
    this.followers = properties.followers && properties.followers.map((follower) => new EntitySummary(follower)) || [];
    this.followingCount = properties.followingCount;
    this.following = properties.following && properties.following.map((followed) => new EntitySummary(followed))|| [];
    this.slug = properties.slug;
    this.webSite = this.generateWebSite(properties.webSite);
    this.gender = properties.gender;
    this.specialType = properties.specialType;
    this.location = properties.location;
    this.url = properties.url;
    this.avatarUrl = this.getAvatarUrl();
    this.backgroundUrl = this.getBackgroundUrl();
  }

  getAvatarUrl(){
    var avatarMedia = this.findMedia('icon') || this.findMedia('uploaded');
    return avatarMedia && avatarMedia.mediaUrl && this.generateImageUrl(avatarMedia.mediaUrl)  || '/img/avatar.png';
  }

  generateWebSite(webSite){
    return webSite && webSite.indexOf('http') === -1
      ? `http://${properties.webSite}`
      : webSite
  }

  generateImageUrl(avatarUrl, size){
    size = size || 100;
    return `${clientConfigs.resizeImagesBaseUrl}/resize/${size}x${size}/${avatarUrl}`
  }

  getBackgroundUrl(){
    const backgroundMedia = this.findMedia('background');
    return backgroundMedia && backgroundMedia.mediaUrl && this.generateImageUrl(backgroundMedia.mediaUrl, 1200)
      || '/img/default-bg.jpg'
  }

  static propTypes = PropTypes.shape({
    emailAddress: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    description: PropTypes.string,
    userName: PropTypes.string,
    url: PropTypes.string,
    followerCount: PropTypes.number,
    followingCount: PropTypes.number,
    webSite: PropTypes.string,
    gender: PropTypes.string,
    specialType: PropTypes.string,
    location: PropTypes.string,
    avatarUrl: PropTypes.string,
    backgroundUrl: PropTypes.string,
    updated: PropTypes.number
  });

}

export default User;
