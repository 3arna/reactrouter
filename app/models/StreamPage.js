import lodash from 'lodash';

import Content from './Content';
import EntitySummary from './EntitySummary';
import Category from './Category';
import Notification from './Notification';
import ContentType from '../enums/ContentType';

class StreamPage {
  constructor(properties){
    this.entries = properties.entries && properties.entries.map(this.generateEntries);
    this.categories = properties.categories && properties.categories.map((category) => new Category(category));
    this.nextState = properties.nextState;
    this.sourceUser = new EntitySummary(properties.sourceUser);
    this.pollingURI = properties.pollingURI;
    this.fetchedAt = new Date();
  }

  generateEntries(entry){

    entry.summary
      ? entry.summary = lodash.contains(entry.summary.id, 'category')
        ? new Category(entry.summary)
        : new EntitySummary(entry.summary)
      : false;
    return entry;
  }
}

export default StreamPage;
