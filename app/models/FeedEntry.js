import EntitySummary from './EntitySummary';

class FeedEntry{
  constructor(properties){
    this.summary = new EntitySummary(properties.summary);
    this.type = properties.type;
  }
}

export default FeedEntry;
