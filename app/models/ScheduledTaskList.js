import Media from './Media';
import { PropTypes } from 'react';
import lodash from 'lodash';
import ScheduledTask from './ScheduledTask';

class ScheduledTaskList {
  constructor(properties){
    console.log(properties);
    //this.list = properties.map((task) => new ScheduledTask(task));
  }

  static propTypes = PropTypes.shape({
    list: PropTypes.arrayOf(ScheduledTask)
  });
}

export default ScheduledTaskList;
