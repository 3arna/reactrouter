class Url {
  constructor(properties){
    this.title = properties.title;
    this.description = properties.description;
    this.originalUrl = properties.originalUrl;
    this.shortenedUrl = properties.shortenedUrl;
  }
}

export default Url;
