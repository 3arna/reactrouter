import Datapoint from './Datapoint';

class UserAnalytics {
  constructor(properties){
    this.datapoints = properties.datapoints.map(datapoint => new Datapoint(datapoint));
    this.posts = properties.posts;
  }
}

export default UserAnalytics;
