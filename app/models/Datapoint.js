class Datapoint {
  constructor(properties){
    this.date = properties.date;
    this.totals = properties.totals;
    this.deltas = properties.deltas;
  }
}

export default Datapoint;
