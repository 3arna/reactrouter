import lodash from 'lodash';
import User from './User';
import Media from './Media';
import MediaContainer from './MediaContainer';
import Url from './Url';
import ShareInfo from './ShareInfo';

class Content {
  constructor(properties){
    this.acl = properties.acl;
    this.author = properties.author && new User(properties.author);
    this.categoryIds = properties.categoryIds || [];
    this.childCount = properties.childCount;
    this.created = properties.created;
    this.creatorId = properties.creatorId;
    this.dislikeCount = properties.dislikeCount;
    this.followerCount = properties.followerCount;
    this.hasLiked = properties.hasLiked;
    this.hasDisliked = properties.hasDisliked;
    this.id = properties.id;
    this.isPrivate = this.id.indexOf('private') === 0;
    this.isTemp = properties.isTemp;
    this.likeCount = properties.likeCount;
    this.media = properties.media
      ? lodash.isArray(properties.media)
        ? properties.media.map((media) => new Media(media))
        : new Media(properties.media)
      : false;
    this.message = properties.message;
    this.name = properties.name;
    this.parentId = properties.parentId;
    this.recentActivityCount = properties.recentActivityCount;
    this.redacted = properties.redacted;
    this.sequence = properties.sequence;
    this.shareCount = properties.shareCount;
    this.shareInfo = properties.shareInfo && new ShareInfo(properties.shareInfo);
    this.slug = properties.slug;
    this.tags = properties.tags || [];
    this.type = properties.type;
    this.urls = properties.urls ? properties.urls.map((url) => new Url(url)) : [];
    this.url = properties.url;
    this.updated = properties.updated;


    //this.description = properties.description;
    //this.avatarUrl = properties.avatarUrl || 'http://img.faceyourmanga.com/mangatars/0/770/770390/large_809337.png';
    //this.backgroundImageUrl = properties.backgroundImageUrl;
    //this.displayName = properties.displayName;

  }

  generateMessageHtml(properties){
    let message = properties.message;
    message = this.generateTags(message, properties);
    message = this.generateMessageLinks(message, properties);
    return message;
  }

  generateTags(message, properties){
    properties.tags && properties.tags.forEach((tag) => {
      message = message.replace(tag, '');
    });
    return message;
  }

  generateMessageLinks(message, properties){
    properties.urls && properties.urls.forEach((url) => {
      message = message.replace(url.originalUrl, '');
      //message = message.replace(url.originalUrl, `<a target="_blank" href="${url.originalUrl}">${url.originalUrl}</a>`);
    });
    return message;
  }
}

export default Content;
