import Media from './Media';
import { PropTypes } from 'react';
import lodash from 'lodash';

class Process {
  constructor(properties){
    this.name = properties.name;
    this.error = properties.error;
    this.success = properties.sucess;
    this.inProgress = properties.inProgress;
  }

  static propTypes = PropTypes.shape({
    name: PropTypes.string.isRequired,
    error: PropTypes.string,
    success: PropTypes.oneOfType([
      PropTypes.bool, 
      PropTypes.string
    ]),
    inProgress: PropTypes.bool,
  });
}

export default Process;
