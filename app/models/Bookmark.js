import EntityIdentifier from './EntityIdentifier';
import EntitySummary from './EntitySummary';

class Bookmark {
  constructor(properties){
    //console.log(properties);
    this.id = properties.id;
    this.type = properties.type;
    this.created = properties.created;
    this.name = properties.summary && properties.summary.name || properties.name || properties.id.split(':').pop();
    this.summary = properties.summary && new EntitySummary(properties.summary);
    //this.entityId = new EntityIdentifier(properties.entityId);
    this.userId = properties.userId;
    this.visibility = properties.visibility;
  }
}

export default Bookmark;
