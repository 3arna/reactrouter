import User from './User';

class ShareInfo {
  constructor(properties){
    this.dateShared = properties.dateShared;
    this.sharedContentId = properties.sharedContentId;
    this.sharer = properties.sharer && new User(properties.sharer);
    this.redacted = properties.redacted;
  }
}

export default ShareInfo;
