class EntityIdentifier {
  constructor(properties){
    this.entityType = properties.entityType;
    this.entityId = properties.entityId;
  }
}
