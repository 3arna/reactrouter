class MediaUpload {
  constructor(properties){
    this.mimeType = properties.mimeType;
    this.url = properties.url;
    this.id = properties.id;
    this.downloadUrl = properties.downloadUrl;
  }
}

export default MediaUpload;
