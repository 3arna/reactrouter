import lodash from 'lodash';

class ApiErrorInfo {
  constructor(properties, statusCode){
    if(properties && properties.serverErrorMsg){
      //this.created = properties.created;
      this.clientErrorMsg = properties.clientErrorMsg;
      //this.serverErrorMsg = properties.serverErrorMsg;
      //this.source = properties.source;
      //this.stack = properties.stack;
    }else{
      //hack for api calls
      this.clientErrorMsg = properties;
    }

    this.statusCode = statusCode;

    this.errorCode = properties && properties.errorCodeMap
      && (lodash.get(properties.errorCodeMap, 'general.0.errorCode'));
  }
}

export default ApiErrorInfo;
