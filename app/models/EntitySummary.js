import MediaContainer from './MediaContainer';
import { clientConfigs } from '../configs';

class EntitySummary extends MediaContainer {
  constructor(properties){
    super(properties);
    this.id = properties.id;
    this.name = properties.name;
    this.displayName = properties.name;
    this.description = properties.description;
    this.avatarUrl = properties.avatarUrl || this.getAvatarUrl();
    this.slug = properties.slug;
  }

  getAvatarUrl(){
    var avatarMedia = this.findMedia('icon') || this.findMedia('uploaded');
    return avatarMedia && `${clientConfigs.resizeImagesBaseUrl}/resize/100x100/${avatarMedia.mediaUrl}` || '/img/avatar.png';
  }

  /*generateAvatarUrl(avatarUrl){
    const size = 100;
    return `https://images-dev.msngr.com/resize/${size}x${size}/${avatarUrl}`
  }*/
}

export default EntitySummary;
