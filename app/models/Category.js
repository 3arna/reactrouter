import Media from './Media';
import { PropTypes } from 'react';
import lodash from 'lodash';

class Category {
  constructor(properties){
    if(!properties){
      return false;
    }
    this.attributes = properties.attributes || {};
    this.created = properties.created;
    this.id = properties.id;
    this.ranking = properties.ranking;
    this.name = properties.name;
    this.updated = properties.updated;
  }

  static propTypes = PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    attributes: PropTypes.object,
    created: PropTypes.number,
    ranking: PropTypes.number,
    updated: PropTypes.number
  });
}

export default Category;
