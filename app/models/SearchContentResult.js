import Content from './Content';

class SearchContentResult {
  constructor(properties){
    this.content = properties.results.map((contentItem) => new Content(contentItem));
    this.nextState = properties.nextState;
    this.pollingURI = properties.pollingURI;
  }
}

export default SearchContentResult;
