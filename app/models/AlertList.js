import Media from './Media';
import { PropTypes } from 'react';
import lodash from 'lodash';
import Alert from './Alert';

class AlertList {
  constructor(properties){
    this.list = properties.list.map((alert) => new Alert(alert));
  }

  static propTypes = PropTypes.shape({
    list: PropTypes.arrayOf(Alert)
  });
}

export default AlertList;
