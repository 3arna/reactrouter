import Media from './Media';
import { PropTypes } from 'react';
import lodash from 'lodash';

class Alert {
  constructor(properties){
    this.created = properties.created;
    this.contentId = properties.contentId;
    this.creatorId = properties.creatorId;
    this.alertKey = properties.alertKey;
  }

  static propTypes = PropTypes.shape({
    created: PropTypes.string,
    contentId: PropTypes.string,
    creatorId: PropTypes.string,
    alertKey: PropTypes.string,
  });
}

export default Alert;
