import User from './User';

class ACLUserList {
  constructor(properties){
    this.users = properties.users.map((user) => new User(user));
  }
}

export default ACLUserList;
