import Media from './Media';
import { PropTypes } from 'react';
import lodash from 'lodash';
import Bookmark from './Bookmark';

class BookmarkList {
  constructor(properties){
    this.bookmarks = properties.bookmarks.map((bookmark) => new Bookmark(bookmark));
  }

  static propTypes = PropTypes.shape({
    bookmarks: PropTypes.arrayOf(Bookmark)
  });
}

export default BookmarkList;
