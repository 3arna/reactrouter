import lodash from 'lodash';

import User from './User';
import Content from './Content';

//IContentPage {
//  total (long): The total number of children residing at this level of the feed,
//  max (long): The largest sequence number returned in this payload. If located at the head of the feed, this will invariably equal the total attribute,
//  ultimateParent (IMetadata): The root of this content feed, which will represent the parent publishers users' metadata,
//  parent (IFeedContent, optional): The immediate parent of this feed,
//  content (Array[IFeedContent]): The child content items of this feed. Bounded by the requests max size and sequencing constraints
//}

class ContentPage {
  constructor(properties){
    this.max = properties.max;
    this.pageMax = properties.pageMax;
    this.pageMin = properties.pageMin;
    this.pollingURI = properties.pollingURI;
    this.feedMax = properties.feedMax;
    this.lineage = properties.lineage && properties.lineage.map((ancestor) => new Content(ancestor));
    this.categories = properties.categories;
    this.ultimateParent = properties.ultimateParent && new User(properties.ultimateParent);

    if(this.lineage && this.lineage.length){
      this.parent = this.lineage[0];

      if(this.lineage.length > 1){
        this.ancestor = this.lineage[1];
        this.ancestorId = this.ancestor.id;
      }
    }

    let content = properties.entries || properties.content;
    lodash.remove(content,
      (contentItem) => contentItem.type === 'com.myriadgroup.versy.notification.types.UserPostedBroadcastFeedContentNotification');

    this.content = content && content.map((contentItem) => new Content(contentItem));

    //custom
    this.parentContentId = this.parent && this.parent.id || this.ultimateParent && this.ultimateParent.id || 'versy';
    this.ancestorId = properties.lineage && properties.lineage.length > 1 && properties.lineage.shift().parentId;
    this.isProfile = !this.parent && !!this.ultimateParent;
    this.fetchedAt = new Date();
  }
}

export default ContentPage;
