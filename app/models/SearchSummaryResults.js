import SearchResult from './SearchResult';

class SearchSummaryResults {
  constructor(properties){
    this.results = properties.results.map((result) => new SearchResult(result));
    this.nextState = properties.nextState;
    this.pollingURI = properties.pollingURI;
  }
}

export default SearchSummaryResults;
