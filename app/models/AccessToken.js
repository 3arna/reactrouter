class AccessToken {
  constructor(properties){
    this.userId = properties.userId;
    this.token = properties.token;
  }
}

export default AccessToken;
