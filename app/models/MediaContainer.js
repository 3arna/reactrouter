import Media from './Media';
import lodash from 'lodash';

class MediaContainer {
  constructor(properties){
    this.media = properties && properties.media && properties.media.map((media) => new Media(media));
  }

  findMedia(category){
    if(!this.media){
      return null;
    }

    return this.media.find((media) => {
      return media.attributes && lodash.includes(category, media.attributes.category);
    });
  }
}

export default MediaContainer;
