import { PropTypes } from 'react';
import lodash from 'lodash';
import { clientConfigs } from '../configs';

import MediaContainer from './MediaContainer';
import Media from './Media';

class SearchResult extends MediaContainer{
  constructor(properties){
    super(properties);

    this.name = properties.name;
    this.media = properties.media;
    this.id = properties.id;

    this.description = properties.description;
    this.type = lodash.get(properties.id && properties.id.split(':'), 0);
    //this.media = properties.media && properties.media.map((media) => new Media(media));
    this.avatarUrl = this.type === 'user'
      ? properties.media && this.getAvatarUrl() || '/img/avatar.png'
      : null;
    this.slug = this.type === 'user' ? properties.slug : '';
  }

  getAvatarUrl(){
    const avatarMedia = this.findMedia(['icon', 'uploaded']);
    return avatarMedia && `${clientConfigs.resizeImagesBaseUrl}/resize/100x100/${avatarMedia.mediaUrl}` || '/img/avatar.png';
  }

  static propTypes = PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
    //media: PropTypes.objectOf(Media),
    avatarUrl: PropTypes.string,
  });
}

export default SearchResult;
