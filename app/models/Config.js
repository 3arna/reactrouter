import lodash from 'lodash';

/*'private.message.poll.frequency': '30',
     'min.firstuse.categories': '3',
     'number.of.items.per.page': '20',
     'minimum.required.search.characters': '3',
     'my.test.prop1': 'time:1455556237384',
     'category.refresh.time.seconds': '300',
     'contentfeed.poll.frequency': '60',
     'number.users.per.page': '20',
     'facebook.app.id': '1635147640058724',
     'max.categories.per.content': '3',
     'maximum.private.recipients': '25',
     'min.password.chars': '2',
     'configuration.refresh.time.seconds': '60',
     'maximum.number.of.categories': '25',
     'image.resize.endpoint': 'http://image-dev.versy.com/',
     'service.pid': 'com.myriadgroup.versy.service.client',
     'versy.stream.poll.frequency': '60',
     'external.share.campaigncode.format': '?campaignCode=s~${clientType}~${platform}~~${contentId}~~${userId}'*/

class Config {
  constructor(properties){
    this.appStoreAndroid = properties.map['app.store.android'];
    this.appStoreIOS = properties.map['app.store.ios'];
    this.alertsPollFrequency = parseInt(properties.map['alert.poll.frequency']) * 1000;
    this.privateMessagePollFrequency = parseInt(properties.map['private.message.poll.frequency']) * 1000;
    this.minimumFirstUseCategories = parseInt(properties.map['min.firstuse.categories']);
    this.numberOfItemsPerPage = parseInt(properties.map['number.of.items.per.page']);
    this.minimumRequiredSearchChars = parseInt(properties.map['minimum.required.search.characters']);
    this.categoryRefreshTimeSeconds = parseInt(properties.map['category.refresh.time.seconds']) * 1000;
    this.contentFeedPollFrequency = parseInt(properties.map['contentfeed.poll.frequency']) * 1000;
    this.facebookAppId = properties.map['facebook.app.id'];
    this.maximumCategoriesPerContent = parseInt(properties.map['max.categories.per.content']);
    this.maximumPrivateRecipients = parseInt(properties.map['maximum.private.recipients']);
    this.maximumNumberOfCategories = parseInt(properties.map['maximum.number.of.categories']);
    this.imageResizeEndpoint = properties.map['image.resize.endpoint'] + 'resize/';
    this.versyStreamPollFrequency = parseInt(properties.map['versy.stream.poll.frequency']) * 1000;
    this.configurationRefreshTime = parseInt(properties.map['configuration.refresh.time.seconds']) * 1000;
    this.externalShareCampaignCodeFormat = properties.map['external.share.campaigncode.format'];
    this.minPasswordChars = properties.map['min.password.chars'];
    this.maxVideoUploadSizeBytes = parseInt(properties.map['max.video.megabytes']) * 1024 * 1024 ;
    this.maxImageUploadSizeBytes = parseInt(properties.map['max.image.megabytes']) * 1024 * 1024 ;
    this.maxAudioUploadSizeBytes = parseInt(properties.map['max.video.megabytes']) * 1024 * 1024 ; // done on purpose as we dont have a value for audio
  }
}

export default Config;
