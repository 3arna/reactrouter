import Content from './Content';
import InviteStatus from '../enums/InviteStatus';
import ContentType from '../enums/ContentType';

class Notification {
  constructor(properties){
    this.data = properties.data;
    this.id = properties.id;
    this.feedContent = new Content(properties.feedContent);
    this.type = properties.type;
    this.isNotification = true;
    this.feedContentRepliesEnabled = true;

    if(this.type === ContentType.UserInvitedToPrivateConversation && this.data !== InviteStatus.Accepted){
      this.feedContentRepliesEnabled = false;
    }
  }
}

export default Notification;
