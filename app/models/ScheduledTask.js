import Media from './Media';
import { PropTypes } from 'react';
import lodash from 'lodash';

import Content from './Content';

class ScheduledTask {
  constructor(properties){
    this.fireDate = properties.fireDate;
    this.taskType = properties.taskType;
    this.command = properties.command;
    this.arguments = properties.arguments;
    this.content = this.arguments && this._generateContentFromArguments(this.arguments);
    this.jsonpayload = properties.jsonpayload;
    this.name = properties.name;
    this.updated = properties.updated;
    this.created = properties.created;
    this.creatorId = properties.ceatorId;
    this.id = properties.id;
  }

  _generateContentFromArguments(args){
    args = JSON.parse(args);
    const content = args.content;
    content.author = args.user;
    return new Content(content);
  }

  /*static propTypes = PropTypes.shape({
    command: PropTypes.string,
  });*/
}

export default ScheduledTask;
