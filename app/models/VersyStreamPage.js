import lodash from 'lodash';

import Content from './Content';
import EntitySummary from './EntitySummary';
import Category from './Category';
import Notification from './Notification';
import ContentType from '../enums/ContentType';

class VersyStreamPage {
  constructor(properties){
    this.content = properties.entries && properties.entries.map((entry) => {
        //console.log('entry type', entry);
        switch(entry.type){
          case ContentType.UserFeedContent:
            return new Content(entry);
            break;
          case ContentType.FeedBookmark:
            return new Content(entry);
          case ContentType.UserInvitedToPrivateConversation:
            return new Notification(entry);
            break;
          default:
            return new Notification(entry);
          break;
        }
      });

    this.nextState = properties.nextState;
    this.sourceUser = new EntitySummary(properties.sourceUser);
    this.pollingURI = properties.pollingURI;

    this.contributors = properties.contributors;
    this.filter = this.defineFilter(properties.contributors);
    this.parentContentId = `versy${this.filter || ''}`;
    this.fetchedAt = new Date();
  }

  generateEntries(entry){

    entry.summary
      ? entry.summary = lodash.contains(entry.summary.id, 'category')
        ? new Category(entry.summary)
        : new EntitySummary(entry.summary)
      : false;
    return entry;
  }

  defineFilter(contributors){
    if(lodash.contains(contributors, 'search')){
      return null;
    }else
    if(lodash.contains(contributors, 'followers')){
      return 'followers';
    }else
    if(lodash.contains(contributors, 'following')){
      return 'following';
    }else
    if(lodash.contains(contributors, 'categories')){
      return 'categories';
    }else
    if(lodash.contains(contributors, 'simpleNotification')){
      return 'notifications';
    }else{
      return null;
    }
  }
}

export default VersyStreamPage;
