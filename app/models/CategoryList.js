import Media from './Media';
import { PropTypes } from 'react';
import lodash from 'lodash';
import Category from './Category';

class CategoryList {
  constructor(properties){
    this.categories = properties.categories.map((category) => new Category(category));
  }

  static propTypes = PropTypes.shape({
    categories: PropTypes.arrayOf(Category)
  });
}

export default CategoryList;
