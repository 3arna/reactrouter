class Media {
  constructor(properties){
    this.mimeType = properties.mimeType;
    this.mediaUrl = properties.mediaUrl && properties.mediaUrl.replace('http://', 'https://');
    //this.thumbnailUrl = properties.thumbnailUrl || false;
    this.id = properties.id;
    this.attributes = false;
    this.childMedia = [];

    properties.childMedia && properties.childMedia.forEach((childMedia) => {
      this.childMedia.push(new Media(childMedia));
    });

    if(properties.attributes && Object.keys(properties.attributes).length){
      this.attributes = {
        extension: properties.attributes.extension,
        height: properties.attributes.height && parseInt(properties.attributes.height),
        size: properties.attributes.size && parseInt(properties.attributes.size),
        width: properties.attributes.width && parseInt(properties.attributes.width),
        category: properties.attributes.category
      }
    }
  }
}

export default Media;
