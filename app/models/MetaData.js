class MetaData {
  constructor(properties){
    this.avatarUrl = properties.avatarUrl || 'http://img.faceyourmanga.com/mangatars/0/770/770390/large_809337.png' ;
    this.description = properties.description;
    this.displayName = properties.displayName;
    this.backgroundImageUrl = properties.backgroundImageUrl || 'https://images-dev.msngr.com/resize/800x800/http://versy-stack-data.s3.amazonaws.com/us-west-2/stage-versy-media/0a00010a-3000-0023-0000-014fff5e8547';
  }
}

export default MetaData;
