const searchActions = {};

searchActions.getResults = (context, payload, callback) => {

  if(!payload.process && context.getStore('SearchStore').searchResults){
    return callback();
  }

  //console.log(payload.typeOfSearch);

  payload.process && context.dispatch('API_PROCESS_START', payload.process);
  !payload.ignoreResultClearing && context.dispatch('SEARCH_CLEAR_RESULTS');

  let typeOfSearch = payload.typeOfSearch;
  let serviceName;
  let serviceParams = {};

  switch(typeOfSearch){
    case 'all':
      serviceName = 'searchService_all';
      serviceParams.query = payload.searchQuery;
    break;
    case 'users':
    case 'categories':
    case 'tags':
      serviceName = 'searchService_bySearchType';
      serviceParams.query = payload.searchQuery;
      serviceParams.typeOfSearch = typeOfSearch;
    break;
    default:
      serviceName = 'searchService_trending';
    break;
  }

  payload.nextState 
    ? serviceParams.state = payload.nextState
    : false;

  context.service
    .read(serviceName)
    .params(serviceParams)
    .end((error, apiResponse) => {
      if(error || apiResponse.error){

        payload.process && context.dispatch('API_PROCESS_ERROR', {
          error: error || apiResponse.error,
          process: payload.process,
        });

        return callback();
      }

      context.dispatch('SEARCH_GET_RESULTS_SUCCESS', {
        searchResults: apiResponse.result.results,
        nextState: apiResponse.result.nextState,
        typeOfSearch,
        searchQuery: payload.searchQuery,
      });

      payload.process && context.dispatch('API_PROCESS_CLEAR', payload.process);

      return callback();
    });
};

export default searchActions;
