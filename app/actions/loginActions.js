import lodash from 'lodash';
import routes from '../routes';
import async from 'async';
import { navigateAction } from 'fluxible-router';

export default {

  logout(context, payload, done){
    context.dispatch('USER_LOGOUT_SUCCESS');
    context.clearCookie('token', {path: '/'});
    context.service.delete('identityService_user', {}, {}, (err) => {
      context.executeAction(navigateAction, { method: 'GET', url: '/'});
      return done();
    });
  },

  login(context, payload, done){
    context.service.read('identityService_user', { token: payload.username, userId: payload.password }, {}, loginDone.bind(context));
  },

  //Facebook login section
  getFacebookCode(context, payload, done){
    console.log('its the context', context.history);
    const { client_id, redirect_uri, codeUrl} = context.config.facebook;
    const { baseUrl } = context.config;
    const returnUrl = baseUrl + redirect_uri;

    return context.redirectTo(`${codeUrl}?client_id=${client_id}&redirect_uri=${returnUrl}&scope=public_profile,email`);
    done(null, true);
  },

  getFacebookToken(context, payload, done){
    //code returned from facebook auth request
    console.log('payload', payload.query);
    const code = payload.query.code;

    if(!code){
      return context.redirectTo('/error');
    }

    socialLogin('facebook', { code }, context, done);
  },


  //Google login section
  getGoogleCode(context, payload, done){
    const { codeUrl, clientId, redirectUri} = context.config.google;
    const { baseUrl } = context.config;
    return context.redirectTo(`${codeUrl}?client_id=${clientId}&response_type=code&redirect_uri=${baseUrl}${redirectUri}&scope=openid email profile`);
  },

  getGoogleToken(context, payload, done){
    const code = payload.get('query').get('code');

    if(!code){
      return context.redirectTo('/error');
    }

    socialLogin('google', { code }, context, done);
  },

  //Twitter login section
  getTwitterCode(context, payload, done){
    context.service.read('authenticationService_twitterRequestToken', {}, {}, (error, requestTokenData) => {
      if(error){
        return context.redirectTo('/error');
      }

      const { authenticateUrl } = context.config.twitter;
      return context.redirectTo(`${authenticateUrl}?oauth_token=${requestTokenData.requestToken}`);
    });
  },

  getTwitterToken(context, payload, done){
    const options = {
      token: payload.get('query').get('oauth_token'),
      verifier: payload.get('query').get('oauth_verifier')
    };

    socialLogin('twitter', options, context, done);
  }
};

function socialLogin(provider, options, context, done){
  console.log('social options', options);
  const serviceName = {
    facebook: 'authenticationService_facebookAccessToken',
    google: 'authenticationService_googleAccessToken',
    twitter: 'authenticationService_twitterAccessToken'
  };

  context.service
    .read(serviceName[provider])
    .params(options)
    .end((error, token) => {

      if(error){
        console.log(error);
        return context.redirectTo('/error');
      }

      console.log('yoyo its the token', token);

      context.service
        .create('identityService_user')
        .params({
          language: context.getLanguage(),
          provider: provider,
          providerIdentity: token
        })
        .end(loginDone.bind(context));
    });
}

function loginDone(error, apiResponse){
  var context = this;
  if (error || apiResponse.error) {
    console.log(error || apiResponse.error);
    return context.redirectTo('/error');
  }

  context.setCookie('token', apiResponse.result.token, {path: '/'});

  return context.redirectTo('/feed/versy', {forceRedirect: true});
}
