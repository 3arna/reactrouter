import { navigateAction } from 'fluxible-router';

export default {
  
  apiProcessClear: (context, proc, callback) =>
    context.dispatch('API_PROCESS_CLEAR', proc),

  apiProcessStart: (context, proc, callback) =>
    context.dispatch('API_PROCESS_START', proc),

  apiProcessError: (context, payload, callback) =>
    context.dispatch('API_PROCESS_ERROR', payload),


  toastProcessSuccess: (context, successMessage, callback) => {
    const proc = {
      name: 'toast', 
      success: successMessage,
      maxTime: 5000,
    };
    context.dispatch('API_PROCESS_SUCCESS', proc);
  },

  toastProcessError: (context, errorMessage, callback) => {
    const proc = {
      name: 'toast', 
      error: errorMessage,
      maxTime: 5000,
    };
    context.dispatch('API_PROCESS_ERROR', proc);
  },

  toastProcessClear: (context, proc, callback) =>
    context.dispatch('API_PROCESS_CLEAR', 'toast'),

}
