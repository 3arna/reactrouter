const privacySelectorActions = {};

privacySelectorActions.inviteMoreRecipients = (context, payload, callback) => {
  context.dispatch('PRIVACY_SELECTOR_TOGGLE_FOLLOWING');
  context.executeAction(privacySelectorActions.loadFollowing, {}, callback);
};

privacySelectorActions.loadRecipients = (context, { contentId }, callback) => {
  context.service
    .read('privateConversationService_getRecipients')
    .params({ contentId })
    .end((error, apiResponse) => {

      if(error || apiResponse.error){
        //context.executeAction(navigateAction, { method: 'GET', url: '/error'});
        return callback(error || apiResponse.error);
      }

      context.dispatch('PRIVACY_SELECTOR_RECIPIENTS_READ_SUCCESS', { recipients: apiResponse.result.users });

      return callback();
    });
};

privacySelectorActions.loadFollowing = (context, payload, callback) => {
  const userStore = context.getStore('UserStore');

  context.service
    .read('versyStreamService_followingStream')
    .params({ userId: userStore.user.id, nextState: payload.nextState })
    .end((error, apiResponse) => {
      if(error || apiResponse.error){
        return callback(error || apiResponse.error);
      }

      context.dispatch('PRIVACY_SELECTOR_FOLLOWING_STREAM_READ_SUCCESS', { streamPage: apiResponse.result, isNextState: payload.nextState });

      return callback();
    });
};

privacySelectorActions.setRecipients = (context, { recipients }, callback) => {
  context.dispatch('PRIVACY_SELECTOR_RECIPIENTS_READ_SUCCESS', { recipients, selectedOnly: true });
  return callback();
};

privacySelectorActions.toggleOpen = (context, { contentId, isPrivate, recipients, isReadOnly }, callback) => {
  const applicationStore = context.getStore('ApplicationStore');
  const privacySelectorStore = context.getStore('PrivacySelectorStore');
  const isOpen = privacySelectorStore.isOpen;

  context.dispatch('PRIVACY_SELECTOR_TOGGLE_OPEN', {
    contentId,
    currentUrl: applicationStore.getCurrentUrl(),
    recipients,
    isPrivate,
    isReadOnly
  });

  if(!isOpen){
    if(contentId){
      context.executeAction(privacySelectorActions.loadRecipients(context, { contentId }, callback))
    }
    else{
      context.executeAction(privacySelectorActions.loadFollowing(context, {}, callback));
    }
  }else{
    return callback();
  }
};

privacySelectorActions.togglePrivacy = (context, payload, callback) => {
  context.dispatch('PRIVACY_SELECTOR_TOGGLE_PRIVACY');
  return callback();
};

export default privacySelectorActions;
