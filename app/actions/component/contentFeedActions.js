export default {
  setFeedScrollPosition: (context, payload, callback) => {
    //console.log('set scroll pos', payload.scrollPosition)
    context.dispatch('CONTENT_PAGE_SET_FEED_SCROLL_POSITION', payload);
    callback();
  },


  clearReplies: (context, payload, callback) => {
    context.dispatch('CONTENT_PAGE_SUBCONTENT_CLEAR', payload);
    callback();
  }
}
