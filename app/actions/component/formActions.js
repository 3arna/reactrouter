import lodash from 'lodash';
import { navigateAction } from 'fluxible-router';

import { getContentFeedData, getSubcontentData } from '../contentActions';
import { toastProcessSuccess, toastProcessError } from '../applicationActions';
import { getScheduledTasks } from '../schedulerActions';

const formActions = {};

formActions.toggleForm = (context, payload, done) => {
  const applicationStore = context.getStore('ApplicationStore');

  context.dispatch('CONTENT_FORM_TOGGLE', {
    isContent: payload.isContent,
    currentUrl: applicationStore.getCurrentUrl() ,
    defaultCategories: payload.defaultCategories
  });

  return done();
}

formActions.updateFormInput = (context, payload, done) => {
  payload && context.dispatch('CONTENT_FORM_UPDATE_INPUT', payload.field)
  return done();
}

formActions.updateFormMedia = (context, payload, done) => {
  payload && context.dispatch('CONTENT_FORM_UPDATE_MEDIA', payload.media)
  return done();
}

formActions.updateFormPrivacy = (context, payload, done) => {
  payload && context.dispatch('CONTENT_FORM_UPDATE_PRIVACY', payload);
  return done();
}

formActions.updateFormCategories = (context, payload, done) => {
  payload && context.dispatch('CONTENT_FORM_UPDATE_CATEGORIES', payload.categories)
  return done();
}

formActions.updatePublishDate = (context, payload, done) => {
  payload && context.dispatch('CONTENT_FORM_UPDATE_PUBLISH_DATE', payload.publishDate);
  return done();
}

formActions.setErrorMessage = (context, { errorMessage }, done) => {
  errorMessage && context.dispatch('CONTENT_FORM_ERROR_SET', errorMessage);
  return done();
}

formActions.postFormContent = (context, payload, done) => {
  let postTo;
  let formData;

  const { parentContent } = payload;

  const ContentFeedStore = context.getStore('ContentFeedStore');
  const UserStore = context.getStore('UserStore');
  const contentFormStore = context.getStore('ContentFormStore');

  if(!UserStore.isLoggedIn()){
    return context.executeAction(navigateAction, { method: 'GET', url: '/'});
  }

  const isPrivate = context.getStore('ContentFormStore').isPrivate;

  let currentPageMax;

  const isContent = contentFormStore.isContent;

  formData = contentFormStore.returnFormData();

  if(!formData.name && !formData.message && !formData.media){
    context.dispatch('CONTENT_FORM_ERROR_SET', context.translate('phrases.pleaseAddTextOrAnAttachment'));
    return done();
  }

  if(parentContent && !isContent){
    postTo = parentContent.id;
    currentPageMax = ContentFeedStore.findContentPage(postTo).pageMax;
    formData.categoryIds = !isPrivate && !formData.categoryIds && parentContent.categoryIds || formData.categoryIds;
  }
  else{
    postTo = 'user';
    const userContentPage = ContentFeedStore.findContentPage(UserStore.user.id);
    currentPageMax = userContentPage ? userContentPage.pageMax : 0;
  }

  if(!postTo || !formData){
    return done(new Error('content id or form data is not specified'), true);
  }

  context.dispatch('CONTENT_FORM_POSTING_START');

  context.service.create('contentService_publish', { postTo, currentPageMax, isPrivate, user: UserStore.user }, formData, {}, (error, apiResponse) => {
    if(error || apiResponse.error){
      context.dispatch('CONTENT_FORM_ERROR_SET', context.translate('phrases.somethingWentWrong'));
      return done(apiResponse.error.clientErrorMsg || error);
    }

    
    //scheduled post scheduled task refresh is delayed by 2s cause of async nature of content posting
    if(formData.publishDate){ 
      return setTimeout(() => 
        context.executeAction(getScheduledTasks, null, () => {
          context.dispatch('CONTENT_FORM_POSTING_SUCCESS');
          context.executeAction(toastProcessSuccess, context.translate('phrases.yourPostHasBeenScheduled'));
          done(); 
        }), 2000);
    }

    context.dispatch('CONTENT_FORM_POSTING_SUCCESS');

    if(parentContent && !isContent){
      context.executeAction(getSubcontentData, { parentContentId: postTo, needRefresh: true, quiet: true, parentalLineage: 2 });
    }
    else{
      if(ContentFeedStore.feedParentContentId === UserStore.user.id){
        context.dispatch('CONTENT_PAGE_SUBCONTENT_CLEAR');
      }

      context.executeAction(getContentFeedData, { parentId: postTo, needRefresh: true });
    }
    return done();
  });
}

export default formActions;
