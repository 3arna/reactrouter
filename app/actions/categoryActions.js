import async from 'async';
import { navigateAction } from 'fluxible-router';
import lodash from 'lodash';
import { getBookmarks } from './bookmarkActions';
import { getMe } from './userActions';
import { getContentFeedData } from './contentActions';

export default {

  getCategories: (context, payload, callback) => {
    context.service.read('categoryService_firstUse', { language: context.getLanguage() }, {}, (error, apiResponse) => {
      if(error || apiResponse.error){
        context.executeAction(navigateAction, { method: 'GET', url: '/error'});
        return callback(error || apiResponse.error, true);
      }

      context.dispatch('CATEGORY_GET_ALL_SUCCESS', apiResponse.result.categories);

      callback();
    });
  },

  getSearchedCategories: (context, payload, callback) => {
    if(!payload.query || (payload.query && payload.query.length < 2)){
      return context.dispatch('CATEGORY_GET_SEARCHED_SUCCESS', false);
    }

    context.service.read('categoryService_search', payload, {}, (error, apiResponse) => {
      if(error || apiResponse.error){
        context.executeAction(navigateAction, { method: 'GET', url: '/error'});
        return callback(error || apiResponse.error, true);
      }

      context.dispatch('CATEGORY_GET_SEARCHED_SUCCESS', apiResponse.result.categories);

      callback();
    });
  },

  bookmarkCategories: (context, payload, callback) => {
    //checks if changes was made
    const interests = context.getStore('UserStore').interests.map((interest) => interest.name);
    if(lodash.isEqual(interests.sort(), payload.selectedCategories.sort())){
      context.executeAction(navigateAction, { method: 'GET', url: payload.redirectTo || '/feed/versy'});
      return callback(null, true);
    }

    context.dispatch('API_PROCESS_START', payload.process);

    context.service.create('categoryService_bookmark')
      .params(payload)
      .end((error, apiResponse) => {

        if(error){
          return callback(error);
        }

        if(apiResponse.error){
          context.dispatch('API_PROCESS_ERROR', {
            error: apiResponse.error,
            process: payload.process
          });
          return callback();
        }

        context.executeAction(getMe, {}, (error) => {
          if(error){
            return callback(error);
          }

          context.executeAction(getBookmarks, null, () => {
            if(payload.noRedirect){
              return callback();
            }

            context.dispatch('API_PROCESS_CLEAR', payload.process);
            context.executeAction(getContentFeedData, { parentId: 'user', needRefresh: true });
            context.executeAction(navigateAction, { method: 'GET', url: payload.redirectTo || '/feed/versy'});
            callback(null, true);
          });
        });
      });
  }
}
