import { navigateAction } from 'fluxible-router';
import { toastProcessSuccess, toastProcessError } from './applicationActions';

export default {
  
  registerUser: (context, payload, callback) => {
    payload.language = context.getLanguage();

    context.dispatch('API_PROCESS_START', payload.process);
    context.service.create('registrationService_user')
      .params(payload)
      .end((error, apiResponse) => {

        if(error){
          return context.redirectTo('/error');
        }

        if(apiResponse.error){
          context.dispatch('API_PROCESS_ERROR', {
            error: apiResponse.error,
            process: payload.process,
          });

          return callback();
        }

        context.setCookie('token', apiResponse.result.token, {path: '/'});
        //const redirectUrl = context.getStore('UserStore').interests.length >= 3 ? '/feed/user' : '/first';

        return context.redirectTo('/feed/versy', { forceRedirect: true });

        //context.dispatch('USER_READ_SUCCESS', apiResponse.result);

        //callback();
      });
  },
  loginUser: (context, payload, callback) => {
    context.dispatch('API_PROCESS_START', payload.process);
    context.service
      .read('registrationService_user')
      .params(payload)
      .end((error, apiResponse) => {

        if(error){
          return context.redirectTo('/error');
        }

        if(apiResponse.error){
          context.dispatch('API_PROCESS_ERROR', {
            error: context.translate('phrases.incorrectEmailOrPassword'),
            process: payload.process,
          });

          return callback();
        }

        context.setCookie('token', apiResponse.result.token, {path: '/'});
        //const redirectUrl = context.getStore('UserStore').interests.length >= 3 ? '/feed/user' : '/first';

        return context.redirectTo('/feed/versy', { forceRedirect: true });

        //callback();
      });
  },
  passwordReset: (context, payload, callback) => {
    payload.language = context.getLanguage();
    
    context.dispatch('API_PROCESS_START', payload.process);
    context.service
      .read('passwordService_reset')
      .params(payload)
      .end((error, apiResponse) => {

        if(error){
          return context.redirectTo('/error');
        }

        if(apiResponse.error){
          context.dispatch('API_PROCESS_ERROR', {
            error: apiResponse.error,
            process: payload.process,
          });

          return callback();
        }

        context.dispatch('API_PROCESS_SUCCESS', {
          success: context.translate('phrases.PleaseCheckYourEmail'),
          process: payload.process
        });
        callback();
      });
  },
  passwordChange: (context, payload, callback) => {
    context.dispatch('API_PROCESS_START', payload.process);
    context.service
      .create('passwordService_reset')
      .params(payload)
      .end((error, apiResponse) => {
        if(error){
          return context.redirectTo('/error');
        }

        if(apiResponse.error){
          context.dispatch('API_PROCESS_ERROR', {
            error: apiResponse.error,
            process: payload.process,
          });

          return callback();
        }

        context.setCookie('token', apiResponse.result.token, {path: '/'});

        return context.redirectTo('/feed/versy', { forceRedirect: true });

        //callback();
      });
  },
  confirmEmail: (context, payload, callback) => {

    const token = payload.get('query').get('token') || '';
    const email = payload.get('query').get('email') || '';

    if(!token.length || !email.length){
      return context.redirectTo('/not-found');
    }

    context.service
      .read('emailService_confirm')
      .params({token, email})
      .end((error, apiResponse) => {
        if(error || apiResponse.error){
          return context.redirectTo('/error');
        }
        return callback();
      });
  },
}
