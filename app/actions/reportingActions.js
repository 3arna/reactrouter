import { navigateAction } from 'fluxible-router';
import lodash from 'lodash';
import { toastProcessSuccess, toastProcessError } from './applicationActions';

export default {
  catchAuthenticated: (context, payload, callback) => {

    context.service.create('reportingService_catchAuthenticated')
      .params(payload)
      .end((error, apiResponse) => {
        if(error){
          console.log('Reporting error', error);
        }
      });
  },
}
