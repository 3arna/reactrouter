import { navigateAction } from './routerActions';
import async from 'async';
import lodash from 'lodash';
import moment from 'moment';
import debugLib from 'debug';

import * as contentUtils from '../utils/contentUtils';

const debug = debugLib('versy');
const contentActions = {};

contentActions.checkForNewContent = (context, { parentContentId, pollToken, isSubContent }, callback) => {
  let serviceName = 'contentFeedService_poll';

  if(lodash.contains(parentContentId, 'versy')){
    serviceName = 'versyStreamService_poll';
  }

  if(lodash.startsWith(parentContentId, 'category') || lodash.startsWith(parentContentId, 'tag')){
    serviceName = 'searchContentService_poll';
  }

  debug('polling %s', serviceName);

  context.service
    .read(serviceName)
    .params({ parentContentId, pollToken })
    .end((error, result) => {
      if(error || result.error){
        return callback();
      }

      if(result.newContentAvailable){
        debug('new content available');
        if(result.forceReload && serviceName === 'contentFeedService_poll'){
          debug('forcing feed reload');
          return isSubContent
            ? contentActions.getSubcontentData(context, { parentContentId, needRefresh: true, quiet: true }, callback)
            : contentActions.getContentFeedData(context, { parentId: parentContentId, needRefresh: true }, callback);
        }
        else{
          debug('dispatch CONTENT_PAGE_NEW_CONTENT_AVAILABLE');
          context.dispatch('CONTENT_PAGE_NEW_CONTENT_AVAILABLE', isSubContent);
        }
      }
      else{
        debug('new content issues');
      }

      return callback();
    });
};

contentActions.clearFeeds = (context, payload, callback) => {
  context.dispatch('CONTENT_PAGE_FEED_CLEAR');
  context.dispatch('CONTENT_PAGE_SUBCONTENT_CLEAR');
  if(callback){
    return callback();
  }
};

contentActions.getSubcontentData = (context, payload, callback) => {
  const contentFeedStore = context.getStore('ContentFeedStore');
  const currentSubContentPage = contentFeedStore.getSubContentPage();

  const { parentContentId, isSelected, sequence, newContent, needRefresh, quiet } = payload;


  if(!parentContentId || (parentContentId && parentContentId.indexOf('notification') === 0)){
    return callback();
  }

  const parentalLineage = payload.parentalLineage || 2;

  const storedSubContentPage = contentFeedStore.findContentPage(parentContentId);


  !newContent && !quiet && context.dispatch('CONTENT_PAGE_TOGGLE_LOADING', { isSubContent: true, isLoading: true, isLoadMore: !!sequence });

  if(storedSubContentPage && !sequence && !newContent && !needRefresh){
    //console.log('loading stored subcontentPage');
    context.dispatch('CONTENT_PAGE_SET_SUBCONTENT_PARENT', { subContentPage: storedSubContentPage, isSelected });
  }


  context.service
    .read('contentService_feed')
    .params({ parentId: parentContentId, parentalLineage, sequence })
    .end((error, apiResponse) => {
      if(error || apiResponse.error){
        if(isSelected){
          return callback(error || apiResponse.error);
        }

        context.dispatch('CONTENT_PAGE_READ_ERROR', { error: error || apiResponse.error, isSubContent: true });
        //context.executeAction(navigateAction, { method: 'GET', url: '/error'});
        return callback();
      }

      context.dispatch('CONTENT_PAGE_READ_SUCCESS', {
        newContentPage: apiResponse.result,
        isLoadMore: !!sequence,
        isSubContent: true,
        isSelected
      });

      callback(null, apiResponse.result);
    });
};

contentActions.getContentFeedData = (context, payload, callback) => {

  //console.log('SHOW', JSON.stringify([...payload]));

  const ContentFeedStore = context.getStore('ContentFeedStore');
  const currentContentPage = ContentFeedStore.getFeedContentPage();

  const parentId = payload.parentId;
  const parentalLineage = payload.parentalLineage || 1;
  const sequence = payload.sequence;

  if(currentContentPage && currentContentPage.parentContentId == parentId && !payload.needRefresh){
    if(!contentUtils.isContentPageDataStale(currentContentPage)){
      return callback(null, currentContentPage);
    }
  }
  !payload.quiet && context.dispatch('CONTENT_PAGE_TOGGLE_LOADING', { isSubContent: false, isLoading: true, isLoadMore: !!sequence });

  const storedContentPage = ContentFeedStore.findContentPage(parentId);

  if(storedContentPage && !payload.needRefresh){
    if(true /*!contentUtils.isContentPageDataStale(storedContentPage)*/){
      context.dispatch('CONTENT_PAGE_SET_FEED_PARENT', parentId);
      return callback(null, storedContentPage);
    }
  }

  context.service
    .read('contentService_feed')
    .params({ parentId, parentalLineage, sequence })
    .end((error, apiResponse) => {
      if(error || apiResponse.error){
        //context.executeAction(navigateAction, { method: 'GET', url: '/error'});
        return callback(error || apiResponse.error);
      }

      //console.log('fetched feed content from api')

      context.dispatch('CONTENT_PAGE_READ_SUCCESS', {
        newContentPage: apiResponse.result,
        isSubContent: false,
        isLoadMore: !!sequence,
        refreshOnly: payload.needRefresh
      });

      return callback(null, apiResponse.result);
    });

};

contentActions.getStreamData = (context, payload, callback) => {
  const { feedIdentifier, nextState, newContent, needRefresh } = payload;

  const ContentFeedStore = context.getStore('ContentFeedStore');
  const storedStream = ContentFeedStore.findContentPage(feedIdentifier);
  const previousFeedContentPage = ContentFeedStore.getPreviousFeedContentPage();

  if(storedStream && !nextState && !needRefresh && !newContent){
    if(feedIdentifier !== 'versynotifications' ||
      previousFeedContentPage && previousFeedContentPage.parentContentId === 'versynotifications'){
      context.dispatch('CONTENT_PAGE_SET_FEED_PARENT', feedIdentifier);
      return callback(null, storedStream);
    }
  }

  context.dispatch('CONTENT_PAGE_TOGGLE_LOADING', { isSubContent: false, isLoading: true, isLoadMore: !!nextState });

  let serviceName;
  //console.log(feedIdentifier);
  const isCategoryOrTag = lodash.includes(feedIdentifier, 'category:') || lodash.includes(feedIdentifier, 'tag:');
  const filter = !isCategoryOrTag && feedIdentifier.replace('versy', '');
  const serviceParams = { nextState, filter };

  if(!isCategoryOrTag && lodash.contains(feedIdentifier, 'versy')){
    serviceName = 'versyStreamService_versyStream';
  }
  else{
    serviceName = 'searchContent_bySearchType';
    serviceParams.query = feedIdentifier;
    serviceParams.searchType = lodash.startsWith(feedIdentifier, 'tag') ? 'tag' : 'category';
  }

  context.service
    .read(serviceName)
    .params(serviceParams)
    .end((error, apiResponse) => {
      if(error || apiResponse.error){
        context.executeAction(navigateAction, { method: 'GET', url: '/error'});
        return callback(error || apiResponse.error);
      }

      context.dispatch('STREAM_READ_SUCCESS', {
        feedIdentifier: feedIdentifier,
        newStreamPage: apiResponse.result,
        isNextState: !!nextState }
      );

      return callback(null, apiResponse.result);
    });
};

contentActions.getVersyStream = (context, payload, callback) => {

  //console.log(JSON.stringify([...payload]));
  const feedIdentifier = `versy${payload.params.filterName || ''}`;

  const contentFeedStore = context.getStore('ContentFeedStore');
  if(contentFeedStore.feedParentContentId !== feedIdentifier){
    context.executeAction(contentActions.clearFeeds);
  }

  context.executeAction(contentActions.getStreamData, { feedIdentifier }, (error, result) => {
    if(error){
      return callback(error);
    }

    console.log('versy stream result', result);

    const feedScrollPosition = contentFeedStore.getFeedScrollPosition();

    if(result && result.content && result.content.length){

      const contentInViewport = feedScrollPosition
        ? feedScrollPosition.contentInViewport
        : result.content[0];

      if(!contentInViewport || (contentInViewport && contentInViewport.isNotification && !contentInViewport.feedContentRepliesEnabled)  ){
        return callback();
      }

      const repliesParentContentId = contentInViewport.isNotification
        ? contentInViewport.feedContent.id
        : contentInViewport.id;

      return context.executeAction(contentActions.getSubcontentData, {parentContentId: repliesParentContentId }, callback);
    }

    return callback();
  });
};

contentActions.getContent = (context, payload, callback) => {
  let parentId = payload.params.parentId && decodeURIComponent(payload.params.parentId).replace('%3A', ':');
  const contentFeedStore = context.getStore('ContentFeedStore');

  if(contentFeedStore.feedParentContentId !== parentId){
    context.executeAction(contentActions.clearFeeds);
  }

  return context.executeAction(contentActions.getSubcontentData, {
    parentContentId: parentId,
    isSelected: true
  }, callback);
};

contentActions.getContentFeed = (context, payload, callback) => {
  const contentFeedStore = context.getStore('ContentFeedStore');
  const UserStore = context.getStore('UserStore');

  console.log('payload', payload);

  let parentId = payload.params.parentId
    && decodeURIComponent(payload.params.parentId).replace('%3A', ':');

  if(parentId === 'user'){
    parentId = UserStore.user.id;
  }

  if(contentFeedStore.feedParentContentId !== parentId){
    context.executeAction(contentActions.clearFeeds);
  }

  let getDataAction = contentActions.getContentFeedData;
  let actionParams = { parentId };

  if(lodash.startsWith(parentId, 'category') || lodash.startsWith(parentId, 'tag')){
    getDataAction = contentActions.getStreamData;

    actionParams = { feedIdentifier:  decodeURIComponent(parentId) };
  }

  context.executeAction(getDataAction, actionParams, (error, result) => {
    if(error){
      if(error.statusCode === 401 && getDataAction === contentActions.getStreamData){
        return context.executeAction(navigateAction, { method: 'GET', url: '/'});
      }

      return callback(error);
    }

    //console.log('result', result);

    const feedScrollPosition = contentFeedStore.getFeedScrollPosition();

    if(result && result.content.length){

      const contentInViewport = feedScrollPosition
        ? feedScrollPosition.contentInViewport
        : result.content[0];

      if(!contentInViewport || contentInViewport && contentInViewport.isTemp){
        return callback();
      }

      return context.executeAction(contentActions.getSubcontentData, {parentContentId: contentInViewport.id }, callback);
    }

    return callback();
  });
};


export default contentActions;
