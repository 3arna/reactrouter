import async from 'async';
import lodash from 'lodash';

import { navigateAction } from 'fluxible-router';
import { getStreamData, getSubcontentData } from './contentActions';
import { toastProcessSuccess, toastProcessError } from './applicationActions';

const privateContentActions = {};

privateContentActions.acceptInvite = (context, { content, feedIdentifier }, callback) => {
  context.dispatch('CONTENT_PAGE_TOGGLE_LOADING', { isLoading: true });

  context.service
    .create('privateConversationService_acceptInvite')
    .params({ objectId: content.id })
    .end((error, apiResponse) => {
      if(error || apiResponse.error){
        context.executeAction(toastProcessError, context.translate('phrases.joinFailed'));
        return callback();
      }

      context.executeAction(getStreamData, { feedIdentifier, needRefresh: true }, () => {
        context.executeAction(navigateAction, { method: 'GET', url: `/content/${content.id}/${content.slug}`});

        return callback();
      });
    });
};

privateContentActions.getRecipients = (context, { contentId }, callback) => {
  context.service
    .read('privateConversationService_getRecipients')
    .params({ contentId })
    .end((error, result) => {
      if(result){
        //console.log('new content', result);
        context.dispatch('CONTENT_PAGE_RECIPIENTS_READ_SUCCESS', payload.isSubContent);
      }

      return callback();
    });
};

privateContentActions.removeSelf = (context, payload, callback) => {
  context.service
    .delete('privateConversationService_removeSelf')
    .params({ contentId: payload.contentId })
    .end((error, result) => {
      if(error || result.error){
        context.executeAction(toastProcessError, context.translate('phrases.leavingConversationFailed'));
        return callback();
      }

      context.executeAction(toastProcessSuccess, context.translate('phrases.youLeftTheConversation'));

      context.executeAction(getStreamData, { feedIdentifier: 'versy', needRefresh: true }, () => {
        context.executeAction(navigateAction, { method: 'GET', url: '/feed/versy' });

        return callback();
      });
    });
};

privateContentActions.updateACL = (context, { contentId, currentAllowedUserIds, allowedUserIds }, callback) => {
  const userIdsToAdd = lodash.difference(allowedUserIds, currentAllowedUserIds);
  const userIdsToRemove = lodash.difference(currentAllowedUserIds, allowedUserIds);


  const addRemoveUsers = (userIds, remove, done) => {
    if(!userIds || !userIds.length){
      return done();
    }

    context.service
      .create('privateConversationService_addRemoveRecipients')
      .params({ contentId, remove })
      .body({ userIds })
      .end(done);
  };

  async.parallel([
    (done) => addRemoveUsers(userIdsToAdd, false, done),
    (done) => addRemoveUsers(userIdsToRemove, true, done),
  ], (error) => {
    if(error){
      context.executeAction(toastProcessError, context.translate('phrases.savingParticipantsFailed'));
      return callback();
    }

    context.executeAction(getSubcontentData, {
      parentContentId: content.id,
      needRefresh: true,
    }, callback);
  });
}

export default privateContentActions;
