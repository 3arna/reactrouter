import { navigateAction } from 'fluxible-router';
import routes from '../routes';
import { toastProcessSuccess, toastProcessError } from './applicationActions';

const authenticationActions = {};

authenticationActions.ensureAuthenticated = (context, payload, callback) => {
  const userStore = context.getStore('UserStore');

  if(!userStore.isLoggedIn() || !context.getCookie('token')){
    context.executeAction(navigateAction, { method: 'GET', url: '/'});
    return callback(null, true);
  }

  if(userStore.interests && userStore.interests.length < 3 && ['versyStream'].indexOf(payload.get('page')) !== -1){
    return context.redirectTo('/first');
  }

  return callback();
};

authenticationActions.ensureSpecial = (context, payload, callback) => {
  const userStore = context.getStore('UserStore');

  if(!userStore.user.specialType){
    context.executeAction(navigateAction, { method: 'GET', url: '/'});
    return callback(null, true);
  }

  return callback();
};

authenticationActions.notAuthenticated = (context, payload, callback) => {
  const isLoggedIn = context.getStore('UserStore').isLoggedIn() && !!context.getCookie('token');

  if(isLoggedIn && ['home', 'login'/*, 'forgotPassword', 'changePassword'*/].indexOf(payload.get('page') !== -1)){
    return context.redirectTo('/feed/versy');
  }
  return callback();
}

export default authenticationActions;
