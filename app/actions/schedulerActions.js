import { navigateAction } from 'fluxible-router';

import { toastProcessSuccess, toastProcessError } from './applicationActions';

const schedulerActions = {};

schedulerActions.getScheduledTasks = (context, payload, callback) => {
  context.service.read('schedulerService_getAllTasks')
    .end((error, apiResponse) => {
      if(error || apiResponse.error){
        return callback(error || apiResponse.error, true);
      }
      context.dispatch('SCHEDULER_GET_TASKS_SUCCESS', {tasks: apiResponse.result});
      callback();
    });
}

schedulerActions.updateScheduledTask = (context, payload, callback) => {

  context.service.create('schedulerService_task')
    .params(payload)
    .end((error, apiResponse) => {

      
      if(error){
        return context.redirectTo('/error');
      }

      if(apiResponse.error){
        context.executeAction(toastProcessError, apiResponse.error);
        return callback();
      }

      context.executeAction(toastProcessSuccess, context.translate('phrases.yourPostHasBeenScheduled'));
      schedulerActions.getScheduledTasks(context, payload, callback);
    });
}

schedulerActions.removeScheduledTask = (context, payload, callback) => {

  context.service.delete('schedulerService_task')
    .params(payload)
    .end((error, apiResponse) => {

      if(error){
        return context.redirectTo('/error');
      }

      if(apiResponse.error){
        context.executeAction(toastProcessError, apiResponse.error);
        return callback();
      }

      
      schedulerActions.getScheduledTasks(context, payload, () => {
        context.executeAction(toastProcessSuccess, context.translate('phrases.postRemoved'));
        callback();
      });

    });
}

export default schedulerActions;
