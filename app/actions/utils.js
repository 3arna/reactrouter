import async from 'async';

export default {
  executeActions(actions){
    return (context, payload, callback) => {
      let exit = false;

      async.eachSeries(actions, (action, callback) => {
          if(exit){
            return;
          }

          context.executeAction(action, payload, (error, redirected) => {
            if(redirected){
              exit = true;
            }

            return callback(error);
          });
        },callback);
    }
  }
}
