import BookmarkStreamType from '../enums/BookmarkStreamType';

const bookmarkStreamActions = {};

bookmarkStreamActions.getFollowersStream = (context, payload, callback) => {
  getBookmarkStream(context, getUserIdFromPayload(payload), BookmarkStreamType.FOLLOWERS, payload.nextState, callback);
};

bookmarkStreamActions.getFollowingStream = (context, payload, callback) => {
  getBookmarkStream(context, getUserIdFromPayload(payload), BookmarkStreamType.FOLLOWING, payload.nextState, callback);
};

bookmarkStreamActions.getInterestsStream = (context, payload, callback) => {
  getBookmarkStream(context, getUserIdFromPayload(payload), BookmarkStreamType.INTERESTS, payload.nextState, callback);
};

function getBookmarkStream(context, userId,  streamType, nextState, callback){
  let serviceName;
  switch(streamType){
    case BookmarkStreamType.FOLLOWERS:
      serviceName = 'versyStreamService_followersStream';
      break;
    case BookmarkStreamType.FOLLOWING:
      serviceName = 'versyStreamService_followingStream';
      break;
    case BookmarkStreamType.INTERESTS:
      serviceName = 'versyStreamService_interestsStream';
      break;
  }

  const params = {
    userId
  };

  if(nextState){
    params.nextState = nextState;
  }

  context.dispatch('BOOKMARK_STREAM_TOGGLE_LOADING', { isLoadingMore: !!nextState });

  context.service
    .read(serviceName)
    .params(params)
    .end((error, apiResponse) => {
      if(error || apiResponse.error){
        //context.executeAction(navigateAction, { method: 'GET', url: '/error'});
        return callback(error || apiResponse.error);
      }

      context.dispatch('BOOKMARK_STREAM_READ_SUCCESS', {
        isNextState: !!nextState,
        userId,
        streamType,
        streamPage: apiResponse.result,
      });

      return callback();
    });
}

function getUserIdFromPayload(payload){
  if(payload.userId){
    return payload.userId;
  }

  return payload.get('params').get('userId') && decodeURIComponent(payload.get('params').get('userId')).replace('%3A', ':');
}

export default bookmarkStreamActions;
