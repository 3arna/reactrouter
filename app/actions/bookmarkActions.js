import lodash from 'lodash';
import { navigateAction } from 'fluxible-router';
import { getContentFeedData } from './contentActions';
import { toastProcessSuccess, toastProcessError } from './applicationActions';

const bookmarkActions = {};

bookmarkActions.getBookmarks = (context, payload, callback) => {
  context.service.read('bookmarkService', {}, {}, (error, apiResponse) => {
    if(error || apiResponse.error){
      context.executeAction(navigateAction, { method: 'GET', url: '/error'});
      return callback(error || apiResponse.error, true);
    }

    context.dispatch('USER_BOOKMARKS_GET_SUCCESS', apiResponse.result.bookmarks);

    callback();
  });
};

bookmarkActions.getDefaultCategoryBookmarks = (context, payload, callback) => {
  context.service.read('categoryService_default')
    .end((error, apiResponse) => {
      if(error || apiResponse.error){
        context.executeAction(navigateAction, { method: 'GET', url: '/error'});
        return callback(error || apiResponse.error, true);
      }
      
      context.dispatch('USER_DEFAULT_CATEGORIES_GET_SUCCESS', apiResponse.result.categories);
      callback();
    })
}

bookmarkActions.toggleBookmark = (context, payload, callback) => {
  !payload.isBookmarked && bookmarkActions.addBookmark(context, payload, callback);
  payload.isBookmarked && bookmarkActions.removeBookmark(context, payload, callback);
};

bookmarkActions.addBookmark = (context, payload, callback) => {

  const userStore = context.getStore('UserStore');

  if(userStore.interests && userStore.interests.length >= context.apiConfig.maximumNumberOfCategories 
    && payload.objectId && payload.objectId.indexOf('category:') !== -1){
      context.executeAction(toastProcessError, context.translate('phrases.sorryYouveHitTheMaximumNumberOfInterests'));
      return callback();
  }

  context.service.create('bookmarkService')
    .params(payload)
    .end((error, apiResponse) => {
      
      if(error || apiResponse.error){
        context.executeAction(toastProcessError, context.translate('phrases.followingFailed'));
        return callback();
      }

      context.executeAction(bookmarkActions.getBookmarks);

      if(lodash.startsWith(payload.objectId, 'user') || lodash.startsWith(payload.objectId, 'content')){
        context.executeAction(getContentFeedData, { parentId: payload.objectId, needRefresh: true });
      }

      context.executeAction(getContentFeedData, { parentId: 'user', needRefresh: true });

      callback();
  });
};

bookmarkActions.removeBookmark = (context, payload, callback) => {
  context.service.delete('bookmarkService', payload, {}, (error, apiResponse) => {
    if(error || apiResponse.error){
      context.executeAction(toastProcessError, context.translate('phrases.unfollowingFailed'));
      return callback();
    }

    context.executeAction(bookmarkActions.getBookmarks);

    if(lodash.startsWith(payload.objectId, 'user') || lodash.startsWith(payload.objectId, 'content')){
      context.executeAction(getContentFeedData, { parentId: payload.objectId, needRefresh: true });
    }

    context.executeAction(getContentFeedData, { parentId: 'user', needRefresh: true });

    callback();
  });
};

export default bookmarkActions;
