import * as serviceUtils from '../utils/serviceUtils';

const analyticsActions = {};

analyticsActions.getUserAnalyticsData = (context, payload, callback) => {
  context.service
    .read('analyticsService_user')
    .end(serviceUtils.serviceResponseHandler({
      onError: callback,
      onSuccess: (userAnalytics) => {
        context.dispatch('USER_ANALYTICS_READ_SUCCESS', {
          userAnalytics: userAnalytics
        });

        callback();
      }
    }));
};

export default analyticsActions;
