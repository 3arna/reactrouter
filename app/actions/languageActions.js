import lodash from 'lodash';

export default {

  setLanguage(context, payload, done){
    const lang = payload.lang || context.getCookie('lang') || 'en';

    if(context.getCookie('lang') === payload.lang){
      return done();
    }
    
    context.setLanguage(lang);
    context.setCookie('lang', lang);
  	context.dispatch('LANGUAGE_SET');
  	return done();
  }

};