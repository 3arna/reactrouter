import { navigateAction } from 'fluxible-router';
import lodash from 'lodash';
import { toastProcessSuccess, toastProcessError } from './applicationActions';

export default {
  report: (context, payload, callback) => {

    let serviceName = 'reportService_content';

    if(lodash.contains(payload.objectId, 'user')){
      serviceName = 'reportService_user'
    }

    context.service.create(serviceName)
      .params(payload)
      .end((error, apiResponse) => {

        if(error || apiResponse.error){
          context.executeAction(toastProcessError, context.translate('phrases.reportingFailed'));
          return callback();
        }

        context.executeAction(toastProcessSuccess, context.translate('phrases.thankyouForYourReport'));

        callback();
      });
  },
}
