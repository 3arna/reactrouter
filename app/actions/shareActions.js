import { navigateAction } from 'fluxible-router';
import lodash from 'lodash';
import { getContentFeedData } from './contentActions';
import { toastProcessSuccess, toastProcessError } from './applicationActions';

export default {
  repostContent: (context, payload, callback) => {

    context.service.create('contentService_share')
      .params(payload)
      .end((error, apiResponse) => {

        if(error){
          context.executeAction(toastProcessError, context.translate('phrases.repostingContentFailed'));
          return callback();
        }

        if(apiResponse.error){
          context.executeAction(toastProcessError, apiResponse.error);
          return callback();
        }

        context.executeAction(toastProcessSuccess, context.translate('phrases.contentReposted'));
        context.executeAction(getContentFeedData, { parentId: 'user', needRefresh: true }, callback );
        //callback();

      });
  },
}
