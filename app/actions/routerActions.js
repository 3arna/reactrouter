const routerActions = {
  navigateAction: (context, payload) => {
    if(context.res){
      return res.redirect(payload.url);
    }

    return context.history.push(payload.url);
  }
};

export default routerActions;
