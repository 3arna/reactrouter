import { navigateAction } from 'fluxible-router';

export default {
  
  mediaSetStarted: (context, payload, callback) => {
    context.dispatch('MEDIA_PLAYER_SET_STARTED_MEDIA', payload.mediaId)
  },
}
