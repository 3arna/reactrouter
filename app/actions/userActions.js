import async from 'async';
import lodash from 'lodash';

import { navigateAction } from 'fluxible-router';
import { toastProcessSuccess, toastProcessError } from './applicationActions';

export default {
  
  getUser: (context, payload, callback) => {
    context.service.read('userService_user')
      .params({ userId: payload.userId })
      .end((error, apiResponse) => {
      if(error || apiResponse.error){
        context.executeAction(navigateAction, { method: 'GET', url: '/error'});
        return callback(error || apiResponse.error, true);
      }

      context.dispatch('USER_READ_SUCCESS', apiResponse.result);

      callback();
    });
  },

  getMe: (context, payload, callback) => {
    context.service.read('userService_me')
      .end((error, apiResponse) => {
      if(error || apiResponse.error){
        context.executeAction(navigateAction, { method: 'GET', url: '/error'});
        return callback(error || apiResponse.error, true);
      }

      context.dispatch('USER_DETAILS_UPDATE', apiResponse.result);

      callback();
    });
  },

  blockUser: (context, payload, callback) => {

    context.service.create('userService_block')
      .params({ userId: payload.userId })
      .end((error, apiResponse) => {
        
        if(error){
          return context.redirectTo('/error');
        }

        if(apiResponse.error){
          context.executeAction(toastProcessError, apiResponse.error);
          return callback();
        }

        context.executeAction(toastProcessSuccess, 'User was successfully blocked');
        callback();
      });
  },

  saveUserProfileBackground: (context, payload, callback) => {
    if(payload.error){
      context.dispatch('API_PROCESS_ERROR', {
        error: context.translate(payload.error),
        process: payload.process,
      });
      return callback();
    }

    payload.data = lodash.merge(payload.data, {attributes: {category: 'background'}});

    context.service.update('userService_background').params(payload.data).end((error, apiResponse) => {
      if(error){
        return context.redirectTo('/error');
      }

      if(apiResponse.error){
        context.dispatch('API_PROCESS_ERROR', {
          error: apiResponse.error,
          process: payload.process,
        });

        return callback();
      }
      context.dispatch('USER_DETAILS_UPDATE', apiResponse.result);
      context.dispatch('API_PROCESS_CLEAR', payload.process);
    });
  },

  saveUserProfileAvatar: (context, payload, callback) => {
    if(payload.error){
      context.dispatch('API_PROCESS_ERROR', {
        error: context.translate(payload.error),
        process: payload.process,
      });
      return callback();
    }

    payload.data = lodash.merge(payload.data, {attributes: {category: 'icon'}});

    context.service.update('userService_avatar').params(payload.data).end((error, apiResponse) => {
      if(error){
        return context.redirectTo('/error');
      }

      if(apiResponse.error){
        context.dispatch('API_PROCESS_ERROR', {
          error: apiResponse.error,
          process: payload.process,
        });

        return callback();
      }
      context.dispatch('USER_DETAILS_UPDATE', apiResponse.result);
      context.dispatch('API_PROCESS_CLEAR', payload.process);
    });
  },

  saveUserProfileChanges: (context, payload, callback) => {

    context.dispatch('API_PROCESS_START', payload.process);

    context.service.update('userService_me').params(payload.data).end((error, apiResponse) => {
      if(error){
        return context.redirectTo('/error');
      }

      if(apiResponse.error){
        context.dispatch('API_PROCESS_ERROR', {
          error: apiResponse.error,
          process: payload.process,
        });

        return callback();
      }

      context.dispatch('USER_DETAILS_UPDATE', apiResponse.result);
      context.dispatch('API_PROCESS_CLEAR', payload.process);
      return context.redirectTo('/feed/user');
      callback();

    });

  }
}
