import { navigateAction } from 'fluxible-router';

export default {
  getAlerts: (context, payload, callback) => {
    context.service.read('alertService_pull')
      .end((error, apiResponse) => {
        if(error || apiResponse.error){
          return callback(error || apiResponse.error, true);
        }
        
        context.dispatch('ALERTS_GET_SUCCESS', apiResponse.result.list);

        callback();
      });
  },

  clearAlerts: (context, payload, callback) => {

    if(payload.get('params').get('filterName') !== 'notifications'){
      return callback();
    }

    context.service.delete('alertService_pull')
      .end((error, apiResponse) => {
        if(error || apiResponse.error){
          return callback(error || apiResponse.error, true);
        }
        
        context.dispatch('ALERTS_GET_SUCCESS', apiResponse.result.list);

        callback();
      });
  }
}
