import { navigateAction } from 'fluxible-router';
import lodash from 'lodash';
import { getContentFeedData } from './contentActions';
import { toastProcessSuccess, toastProcessError } from './applicationActions';

export default {
  like: (context, payload, callback) => {

    context.service.create('likeService_like')
      .params(payload)
      .end((error, apiResponse) => {

        if(error || apiResponse.error){
          context.executeAction(toastProcessError, context.translate('phrases.somethingWentWrong'));
          return callback();
        }

        context.dispatch('CONTENT_LIKENDISLIKE', { parentId: payload.objectId, isLike: true });
        callback();
      });
  },

  dislike: (context, payload, callback) => {
    //payload.process && context.dispatch('API_PROCESS_START', payload.process);

    context.service.create('likeService_dislike')
      .params(payload)
      .end((error, apiResponse) => {

        if(error || apiResponse.error){
          context.executeAction(toastProcessError, context.translate('phrases.somethingWentWrong'));
          return callback();
        }

        context.dispatch('CONTENT_LIKENDISLIKE', { parentId: payload.objectId, isLike: false });
        callback();
        
      });
  },

  remove: (context, payload, callback) => {
    //payload.process && context.dispatch('API_PROCESS_START', payload.process);

    context.service.create('likeService_remove')
      .params(payload)
      .end((error, apiResponse) => {

        if(error || apiResponse.error){
          context.executeAction(toastProcessError, context.translate('phrases.somethingWentWrong'));
          return callback();
        }

        console.log(apiResponse.result);

        callback();
      });
  },
}
