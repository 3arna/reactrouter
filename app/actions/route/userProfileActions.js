import async from 'async';

import contentActions from '../contentActions';
import userActions from '../userActions';

export default {
  loadUserProfileData(context, payload, callback){

    const params = payload.get('params');
    const userId = params.get('userId') && decodeURIComponent(params.get('userId')).replace('%3A', ':');
    const selectedContentId = params.get('selectedContentId') && decodeURIComponent(params.get('selectedContentId')).replace('%3A', ':');

    async.parallel([
      (cb) => context.executeAction(userActions.getUser, {userId: userId}, cb)
    ], callback);
  }
};
