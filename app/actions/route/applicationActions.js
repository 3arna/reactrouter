import async from 'async';

export default {
  setBackRoute(context, payload, done){
    context.dispatch('NAVIGATE_SET_BACK_ROUTE', payload);
    done();
  },
  navigate(actionContext, payload, done) {
    actionContext.dispatch('CHANGE_ROUTE', payload);
    done();
  }
};
