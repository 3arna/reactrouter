import { navigateAction } from 'fluxible-router';

export default {
  getConfig: (context, payload, callback) => {
    context.service.read('configService_config', {}, {}, (error, apiResponse) => {
      if(error || apiResponse.error){
        context.executeAction(navigateAction, { method: 'GET', url: '/error'});
        return callback(error || apiResponse.error, true);
      }

      context.dispatch('CONFIG_GET_SUCCESS', apiResponse.result);

      callback(null, apiResponse.result);
    });
  }
}
