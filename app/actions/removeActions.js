import { navigateAction } from 'fluxible-router';
import lodash from 'lodash';
import { getContentFeedData } from './contentActions';
import { toastProcessSuccess, toastProcessError } from './applicationActions';

export default {
  removeContent: (context, payload, callback) => {

    context.service.delete('contentService_redact')
      .params(payload)
      .end((error, apiResponse) => {

        if(error){
          return context.redirectTo('/error');
        }

        if(!!apiResponse.error){
          context.executeAction(toastProcessError, apiResponse.error);
          return callback();
        }

        context.executeAction(toastProcessSuccess, context.translate('phrases.postRemoved'));
        context.executeAction(getContentFeedData, { parentId: 'user', needRefresh: true });
        callback();

      });
  },
}
