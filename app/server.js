/**
 * This leverages Express to create and run the http server.
 * A Fluxible context is created and executes the navigateAction
 * based on the URL. Once completed, the store state is dehydrated
 * and the application is rendered via React.
 */

import express from 'express';
import compression from 'compression';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import locale from 'locale';
import path from 'path';
import session from 'express-session';
import redisStore from 'connect-redis';

import app from './app';
import { serverConfigs } from './configs';
import loadApiConfig from './server/loadApiConfig';
import authenticate from './server/middleware/authenticate';
import browserCheck from './server/middleware/browserCheck';
import render from './server/render';
import router from './server/router';
import registerServices from './server/registerServices';

const server = express();
const RedisStore = redisStore(session);

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended: true}));
server.use(cookieParser());
server.use(express.static(path.join(__dirname, '../build')));
server.use(compression());
server.use(session({
  key: 'versy.sid',
  secret: serverConfigs.session.secret,
  resave: false,
  saveUninitialized: false,
  store: new RedisStore({
    host: serverConfigs.redis.host,
    port: serverConfigs.redis.port
  })
}));

server.use(browserCheck);

server.use(locale(serverConfigs.locales));

server.use(loadApiConfig);
//server.use(authenticate);

server.use(router);

registerServices(app, server);

server.use(render);


const port = process.env.PORT || 8080;
server.listen(port);
console.log('Application listening on port ' + port);

export default server;
