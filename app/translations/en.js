// jscs:disable disallowQuotedKeysInObjects
export default {
  name: 'English',
  prefix: 'en',

  word: {
    settings: 'Settings',
    name: 'Name',
    email: 'Email',
    password: 'Password',
    close: 'Close',
    change: 'Change',
    continue: 'Continue',
    save: 'Save',
  },

  phrase: {
    slogan: 'Versy connects you with people with similar interests',
    settingsDescription: 'Modify your interests and language',
    signUp: 'Sign up',
    signIn: 'Sign in',
    signOut: 'Sign out',
    orViaEmail: 'Or via email',
    signUpWith: 'Sign up with',
    signInWith: 'Sign in with',
    alreadyUseVersy: 'Already use versy?',
    newToVersy: 'New to versy?',
    byContinuingAgree: 'By continuing you agree to our Terms & Privacy Policy',
    signingUp: 'Signing up',
    signingIn: 'Signing in',
    sendingEmail: 'Sending email',
    changingPassword: 'Changing password',
    savingInterests: 'Saving interest',
    forgotPassword: 'Forgot password?',
    resetPassword: 'Reset password',
    changePassword: 'Change password?',
    rememberYourPassword: 'Remember your password?',
    resetPasswordEmailWasSent: 'Password reset link was sent to',
    gotIt: 'Got it',
    tokenOrEmailIsNotSpecified: 'Token or email is not specified',
    repeatPassword: 'Repeat password',
    pickInterests: 'Pick some things you like',
    whatDoYouLike: 'What do you like?',
    categoryDoesNotExists: 'does not exist - Select or click "Enter" to add it',
    categoryAlreadyAdded: 'This category is already added to your interest list',
    categoryMaxLimitReached: 'Max tag amount reached, tag length is incorrect or same tag was specified',

  },

  navigation: {
    home: 'Versy+ Sign Up Page',
    login: 'Versy+ Sign In Page',
    resetPassword: 'Versy+ Reset Password Page',
    forgotPassword: 'Versy+ Forgot Password Page',
    settings: 'User Settings Page',
    broadcast: 'User Profile Broadcast',
    testingContent: 'Post on this feed',
  },

  errorCode: {
    1000: 'A required field was missing',
    1001: 'The length or size of the supplied object was invalid',
    1002: 'The value supplied did not match the regex (see the clientErrorMessage for regex)',
    1003: 'The field should of been null or empty',
    1004: 'The field failed a boolean assertion',
    2000: 'The format of the supplied email address did not conform to the email specification',
    2001: 'The supplied email address did pass pass the MX record check',
    100001: 'No registration exists for the supplied email address',
    100002: 'User is already registered',
    100003: 'Failed to confirm registration - Registration request has expired',
    100004: 'Failed to confirm registration - Token does not match',
    100005: 'Failed to reset password - Reset Request does not exist',
    100006: 'Failed to reset password - Reset Request has expired',
    100007: 'Failed to reset password - Token does not match',
  },

  errorMessage: {
    timedOut: 'Connection timed out',
    emailLoginFailed: 'login details are incorrect or this user does not exist',
    inputEntryIncorrect: 'field value is incorrect',
    apiFailed: 'Something went wrong',
    emailDoesNotExist: 'Email does not exist',
    fileTypeNotAllowed: 'File type is not allowed',
  },

  messages: {

    welcome: "Welcome to Versy",
    greeting: "Versy connects you with people with similar interests",

    test: "I'm ${name} and I would like to ${hobby} ",

    "features": {
      "popular": "Most Popular",
      "highest_rated": "Highest Rated",
      "upcoming": "Upcoming",
      "editors": "Editors picks",
      "fresh_today": "Fresh Today",
      "fresh_yesterday": "Fresh Yesterday",
      "fresh_week": "Fresh This Week"
    },

    "home": "Welcome, In order to use Versy+ you need to login",
  }
};
