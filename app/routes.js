import {
  getFacebookCode, getFacebookToken, getGoogleCode, getGoogleToken, getTwitterCode, getTwitterToken, logout, login } from './actions/loginActions';
import { getContent, getContentFeed, getVersyStream } from './actions/contentActions';
import { executeActions } from './actions/utils';
import { ensureAuthenticated, ensureSpecial, notAuthenticated } from './actions/authenticationActions';
import { loadUserProfileData } from './actions/route/userProfileActions';
import { setLanguage } from './actions/languageActions';
import { confirmEmail } from './actions/emailActions';
import { getFollowingStream, getFollowersStream, getInterestsStream } from './actions/bookmarkStreamActions';
import { getResults } from './actions/searchActions';
import { clearAlerts } from './actions/alertActions';
import { getScheduledTasks } from './actions/schedulerActions';
import { getUserAnalyticsData } from './actions/analyticsActions';

if (typeof require.ensure !== 'function') {
  /**
   * polyfill require.ensure
   */
  require.ensure = function (deps, cb) {
    cb(require);
  };
}

let AnalyticsContainer;

export default {

  home: {
    path: '/',
    method: 'get',
    page: 'home',
    title: 'Versy',
    onNavigation: true,
    onAuthenticatedHide: true,
    iconClass: 'icon-versy-logo',
    navHide: true,
    mobNavHide: true,
    handler: require('./views/pages/home/HomeContainer'),
    action: executeActions([notAuthenticated]),
  },
  login: {
    path: '/login',
    method: 'get',
    page: 'login',
    title: 'phrases.logIn',
    navHide: true,
    mobNavHide: true,
    handler: require('./views/pages/login/LoginContainer'),
    action: executeActions([notAuthenticated]),
  },
  register: {
    path: '/register',
    method: 'get',
    page: 'register',
    title: 'words.register',
    navHide: true,
    mobNavHide: true,
    handler: require('./views/pages/register/RegisterContainer'),
    action: executeActions([notAuthenticated]),
  },
  forgotPassword: {
    path: '/forgot-password',
    method: 'get',
    page: 'forgotPassword',
    title: 'phrases.forgottenPassword',
    navHide: true,
    mobNavHide: true,
    handler: require('./views/pages/forgotPassword/ForgotPasswordContainer'),
    action: executeActions([notAuthenticated]),
  },
  changePassword: {
    path: '/reset-password',
    method: 'get',
    page: 'changePassword',
    title: 'phrases.changePassword',
    navHide: true,
    mobNavHide: true,
    handler: require('./views/pages/changePassword/ChangePasswordContainer'),
    action: executeActions([notAuthenticated]),
  },
  first: {
    path: '/first',
    method: 'get',
    page: 'first',
    title: 'phrases.selectInterests',
    navHide: true,
    mobNavHide: true,
    handler: require('./views/pages/first/FirstContainer'),
    action: executeActions([ensureAuthenticated]),
  },

  settings: {
    path: '/settings',
    method: 'get',
    page: 'settings',
    title: 'words.settings',
    mobNavHide: true,
    handler: require('./views/pages/settings/SettingsContainer'),
    action: executeActions([ensureAuthenticated]),
  },

  versyStream: {
    path: '/feed/versy/:filterName?',
    method: 'get',
    page: 'versyStream',
    onNavigation: true,
    onAuthenticated: true,
    title: 'phrases.versyStream',
    iconClass: 'icon-versy-logo',
    action: executeActions([ensureAuthenticated, clearAlerts, getVersyStream]),
    handler: require('./views/pages/versyStream/VersyStreamContainer'),
  },

  contentFeed: {
    method: 'get',
    page: 'contentFeed',
    path: '/feed/:parentId/:title?',
    action: executeActions([getContentFeed]),
    handler: require('./views/pages/contentFeed/ContentFeedContainer'),
  },
  content: {
    method: 'get',
    page: 'contentFeed',
    path: '/content/:parentId/:title?',
    action: executeActions([getContent]),
    handler: require('./views/pages/content/ContentContainer'),
  },
  broadcast: {
    path: '/feed/user',
    method: 'get',
    page: 'broadcast',
    iconClass: 'icon-profile',
    onAuthenticated: true,
    onNavigation: true,
  },

  notifications: {
    path: '/feed/versy/notifications',
    method: 'get',
    page: 'notifications',
    title: 'words.notifications',
    onNavigation: true,
    onAuthenticated: true,
    iconClass: 'icon-notifications'
  },

  search: {
    path: '/search',
    method: 'get',
    page: 'search',
    title: 'words.search',
    onNavigation: true,
    onAuthenticated: true,
    mobNavHide: true,
    iconClass: 'icon-search',
    handler: require('./views/pages/search/SearchContainer'),
    action: executeActions([ensureAuthenticated, getResults]),
  },

  scheduler: {
    path: '/scheduler/',
    method: 'get',
    page: 'scheduler',
    onNavigation: true,
    onAuthenticated: true,
    showLoadingPage: true,
    onSpecialUser: true,
    title: 'phrases.scheduledPosts',
    iconClass: 'icon-clock',
    action: executeActions([ensureAuthenticated, ensureSpecial, getScheduledTasks]),
    handler: require('./views/pages/scheduler/SchedulerContainer'),
  },

  analytics: {
    path: '/analytics',
    method: 'get',
    page: 'analytics',
    title: 'words.analytics',
    onNavigation: true,
    onAuthenticated: true,
    onSpecialUser: true,
    showLoadingPage: true,
    iconClass: 'icon-analytics',
    action: executeActions([ensureAuthenticated, ensureSpecial, getUserAnalyticsData]),
    handler: require('./views/pages/analytics/AnalyticsContainer')
  },

  followers: {
    path: '/:userId/followers',
    method: 'get',
    page: 'followers',
    title: 'words.followers',
    showLoadingPage: true,
    mobNavHide: true,
    handler: require('./views/pages/bookmarkStream/BookmarkStreamContainer'),
    action: executeActions([ensureAuthenticated, getFollowersStream]),
  },
  following: {
    path: '/:userId/following',
    method: 'get',
    page: 'following',
    title: 'words.following',
    showLoadingPage: true,
    mobNavHide: true,
    handler: require('./views/pages/bookmarkStream/BookmarkStreamContainer'),
    action: executeActions([ensureAuthenticated, getFollowingStream]),
  },
  interests: {
    path: '/:userId/interests',
    method: 'get',
    page: 'interests',
    title: 'words.interests',
    showLoadingPage: true,
    mobNavHide: true,
    handler: require('./views/pages/bookmarkStream/BookmarkStreamContainer'),
    action: executeActions([ensureAuthenticated, getInterestsStream]),
  },
  interestsSelector: {
    path: '/interests-selector',
    method: 'get',
    page: 'interestsSelector',
    title: 'phrases.selectInterests',
    showLoadingPage: true,
    mobNavHide: true,
    handler: require('./views/pages/interestsSelector/InterestsSelectorContainer'),
    action: executeActions([ensureAuthenticated]),
  },

  about: {
    path: '/about',
    method: 'get',
    page: 'about',
    title: 'words.about',
    onNavigation: true,
    isIndividual: true,
    handler: require('./views/pages/About'),
    iconClass: 'icon-info'
  },

  privacyPolicy: {
    path: '/privacy-policy',
    method: 'get',
    page: 'privacyPolicy',
    title: 'phrases.privacyPolicy',
    isIndividual: true,
    handler: require('./views/pages/privacyPolicy/PrivacyPolicy'),
  },

  terms: {
    path: '/terms',
    method: 'get',
    page: 'Terms',
    title: 'phrases.termsAndConditions',
    isIndividual: true,
    handler: require('./views/pages/terms/Terms'),
  },

  whatIsVersy: {
    path: '/what-is-versy',
    method: 'get',
    page: 'whatIsVersy',
    title: 'phrases.whatIsVersy',
    navHide: true,
    mobNavHide: true,
    handler: require('./views/pages/WhatIsVersy')
  },

  confirmEmail: {
    path: '/confirm-email',
    method: 'get',
    page: 'Confirm Email',
    title: 'phrases.confirmEmail',
    handler: require('./views/pages/ConfirmEmail'),
    navHide: true,
    action: confirmEmail,
  },

  loginPost: {
    path: '/login',
    method: 'post',
    page: 'login post',
    title: 'phrases.loggingIn',
    hanlder: require('./views/pages/Loading'),
    navHide: true,
    action: login,
  },
  logout: {
    path: '/logout',
    method: 'get',
    page: 'logout',
    title: 'phrases.loggingIn',
    mobNavHide: true,
    hanlder: require('./views/pages/Loading'),
    navHide: true,
    action: logout,
    iconClass: 'icon-sign-in-out',
    onNavigation: true,
    onAuthenticated: true,
  },
  loginFacebook: {
    path:'/login/facebook',
    method: 'get',
    page: 'loginFacebook',
    title: 'phrases.loggingIn',
    mobNavHide: true,
    handler: require('./views/pages/Loading'),
    navHide: true,
    action: getFacebookCode,
  },
  authFacebook: {
    path:'/auth/facebook',
    method: 'get',
    page: 'authFacebook',
    mobNavHide: true,
    title: 'phrases.loggingIn',
    handler: require('./views/pages/Loading'),
    navHide: true,
    action: getFacebookToken
  },
  loginGoogle: {
    path:'/login/google',
    method: 'get',
    page: 'loginGoogle',
    title: 'phrases.loggingIn',
    mobNavHide: true,
    handler: require('./views/pages/Loading'),
    navHide: true,
    action: getGoogleCode,
  },
  authGoogle: {
    path:'/auth/google',
    method: 'get',
    page: 'authGoogle',
    action: getGoogleToken
  },
  loginTwitter: {
    path:'/login/twitter',
    method: 'get',
    page: 'loginTwitter',
    title: 'phrases.loggingIn',
    navHide: true,
    mobNavHide: true,
    handler: require('./views/pages/Loading'),
    action: getTwitterCode,
  },
  authTwitter: {
    path:'/auth/twitter',
    method: 'get',
    page: 'authTwitter',
    navHide: true,
    mobNavHide: true,
    handler: require('./views/pages/Loading'),
    action: getTwitterToken,
    title: 'phrases.loggingIn',
  },
  error: {
    path:'/error',
    method: 'get',
    page: 'error',
    title: 'words.oops',
    handler: require('./views/pages/ErrorPage')
  },
  accessDenied: {
    path:'/access-denied',
    method: 'get',
    page: 'accessDenied',
    navHide: true,
    title: 'words.oops',
    handler: require('./views/pages/AccessDenied')
  },
  notFound: {
    path: '/not-found',
    method: 'get',
    page: 'notFound',
    navHide: true,
    title: 'words.oops',
    handler: require('./views/pages/NotFound')
  },
  browserNotSupported: {
    path: '/browser-not-supported',
    method: 'get',
    page: 'browserNotSupported',
    navHide: true,
    title: 'words.oops',
    handler: require('./views/pages/BrowserNotSupportedPage')
  }
};
