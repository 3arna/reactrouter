import lodash from 'lodash';

import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';

import AccessToken from '../../models/AccessToken';
import User from '../../models/User';

export const registerUser = {
  name: "identityService_user",

  create(req, resource, options, body, config, callback) {
    const requestBody = {
      language: options.language,
      provider: options.provider,
      providerIdentity: options.providerIdentity
    };

    if(req.session && req.session.campaignCode){
      requestBody.campaignCode = req.session.campaignCode;
    }

    apiClient.sendRequest(
      { url: '/identity/token/issue', method: 'POST' },
      { requestBody, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      (error, apiResponse) => {
        //console.log(error, apiResponse);
        if(error){
          return callback(error);
        }

        req.session.user = apiResponse.result && apiResponse.result.user && new User(apiResponse.result.user);
        callback(apiResponse.error, apiResponse);
      }
    );
  },

  read: (req, resource, options, config, callback) => {
    if(!req.session){
      return callback(new Error('Session is not set correctly'));
    }

    if(req.session.user){
      return callback(null, {result: options});
    }

    apiClient.sendRequest(
      { url: `/users/${options.userId}`, method: 'GET' },
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      User,
      (error, apiResponse) => {
        if(!error && !apiResponse.error && req.session){
          req.session.user = apiResponse.result;
        }

        apiResponse.result.token = options.token;

        callback(error || apiResponse.error, apiResponse);
      }
    );
  },

  //delete from session user data on logout
  delete: (req, resource, options, config, callback) => {
    if(req.session){
      delete req.session.user;
    }
    callback();
  },
};
