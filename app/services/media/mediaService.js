import lodash from 'lodash';
import fs from 'fs';
import request from 'request';
import path from 'path';
import async from 'async';

import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';

import Media from '../../models/Media';
import MediaUpload from '../../models/MediaUpload';

export const uploadMedia = {
  name: 'mediaService_upload',

  //request api to provide url of aws
  create: (req, res, fields, files, options, callback) => {

    const qs = { mimeType: options.mimeType };
    const requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    async.waterfall([

      (cb) => apiClient.sendRequest({ url: '/media/requestUpload' }, { qs, requestHeaders }, MediaUpload, cb),
      (apiResponse, cb) => {
        if(apiResponse.error){
          return cb(apiResponse.error);
        }

        const awsSignedUrl = apiResponse.result.url;
        let readStream = fs.createReadStream(options.path);


        const requestData = {
          url: awsSignedUrl,
          headers: { 'Content-Type': options.mimeType, 'Content-Length': options.size },
          timeout: 100000000,
        };

        readStream.pipe(
          request.put(requestData, (err, res, body) => {

            if(err){
              return cb(err);
            }

            if(res.statusCode !== 200){
              return cb(new Error(body));
            }

            let mediaUrl = apiResponse.result.downloadUrl;
            mediaUrl = mediaUrl.indexOf('https') < 0 && mediaUrl.replace('http', 'https') || mediaUrl;

            return cb(null, new Media({
              mimeType: apiResponse.result.mimeType,
              mediaUrl,
              id: apiResponse.result.id,
              attributes: options.attributes || false,
            }))

          })

        ).on('error', (err) => {
          console.log(err);
          return cb(err)
        })
      }

    ], callback)
  },

};
