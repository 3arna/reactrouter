import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';

import Config from '../../models/Config';

export const configService = {
  name: "configService_config",

  read(req, resource, options, config, callback){
    apiClient.sendRequest(
      { url: '/config/get'},
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      Config,
      callback
    );
  }
};
