import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';
import { ExternalContentShareEvent } from '../../enums/EventReportingType';

export const reportUser = {
  name: 'reportingService_catchAuthenticated',

  create: (req, resource, body, options, config, callback) => {

    const requestBody = {
      platform: body.platform.toString(),
      itemId: body.contentId.toString(),
      type: ExternalContentShareEvent,
    };

    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    apiClient.sendRequest(
      {url: '/reporting/catchEmitAuthenticated', method: 'POST'},
      {requestHeaders, requestBody},
      null,
      callback
    );
  },

}
