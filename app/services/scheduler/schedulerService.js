import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';
import lodash from 'lodash';

import ScheduledTask from '../../models/ScheduledTask';

export const schedulerService = {
  name: "schedulerService_getAllTasks",

  read(req, resource, options, config, callback){
    apiClient.sendRequest(
      { url: '/scheduler/get/all'},
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      ScheduledTask,
      (err, apiResponse) => {
        if(err){
          return callback(err);
        }

        callback(err, apiResponse);
      }
    );
  }
};

export const schedulerServiceUpdate = {
  name: "schedulerService_task",

  create(req, resource, body, options, config, callback){
    const requestBody = {
      fireTime: body.fireDate,
      id: body.id,
    }
    apiClient.sendRequest(
      { url: '/scheduler/update/firetime', method: 'POST'},
      { requestBody, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      callback
    );
  },

  delete: (req, resource, params, config, callback) => {
    const qs = {id: params.id};
    apiClient.sendRequest(
      { url: `/scheduler/delete/`, method: 'DELETE'},
      { qs, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      callback
    );
  }
};
