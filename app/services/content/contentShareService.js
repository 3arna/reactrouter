import lodash from 'lodash';
import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';
import * as contentUtils from '../../utils/contentUtils';

export const contentShare = {
  name: 'contentService_share',

  create: (req, resource, params, body, config, callback) => {

    const requestBody = {
      'parentId': params.parentId,
      'contentId': params.contentId,
    }

    apiClient.sendRequest(
      { url: '/content/share/', method: 'POST'},
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req), requestBody },
      null,
      callback
    );
  }
};
