import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';

import ACLUserList from '../../models/ACLUserList';

export const acceptInvite = {
  name: 'privateConversationService_acceptInvite',
  create: (req, resource, options, body, config, callback) => {
    const url = `/invite/accept/${options.objectId}`;

    apiClient.sendRequest(
      { url , method: 'POST' },
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      callback
    );
  }
};

export const addRemoveRecipients = {
  name: 'privateConversationService_addRemoveRecipients',
  create: (req, resource, options, body, config, callback) => {
    const url = `/private/recipients/${options.remove ? 'remove' : 'add'}/${options.contentId}`;

    apiClient.sendRequest(
      { url , method: 'POST' },
      { requestBody: { userIds: body.userIds }, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      callback
    );
  }
};

export const getRecipients = {
  name: "privateConversationService_getRecipients",

  read(req, resource, options, config, callback){
    const contentId = options.contentId;

    const apiUrl = `/private/recipients/get/${contentId}`;

    apiClient.sendRequest(
      { url: apiUrl, method: 'GET'},
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      ACLUserList,
      callback
    );
  }
};

export const removeSelf = {
  name: 'privateConversationService_removeSelf',
  delete: (req, resource, options, config, callback) => {
    const url = `/private/recipients/remove/self/${options.contentId}`;

    apiClient.sendRequest(
      { url , method: 'DELETE' },
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      callback
    );
  }
};
