import lodash from 'lodash';
import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';
import * as contentUtils from '../../utils/contentUtils';

export const publish = {
  name: 'contentService_publish',

  create: (req, resource, options, body, config, callback) => {
    const requestBody = body;
    const user = options.user;

    let postTo = options.postTo === 'user' ? '' : options.postTo;

    if(!requestBody.message){
      delete requestBody.message;
    }

    if(options.isPrivate){
      postTo = `private/${postTo}`;
    }

    const url = `/content/publish/${postTo}`;

    if(requestBody.media && !lodash.isArray(requestBody.media)){
      requestBody.media = [requestBody.media];
    }

    apiClient.sendRequest(
      { url , method: 'POST' },
      { requestBody, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      (error, response) => {
        if(error || (response && response.error)){
          return callback(error, response);
        }

        //console.log(response);

        //after posting to api, stick the new content in temp content
        //temp content is used to display the new content until it is processed and returned by the api
        if(!requestBody.publishDate){
          const tempContent = contentUtils.getTempContent(requestBody, response.result.identifier, user, options.currentPageMax);
          const tempContentKey = options.postTo === 'user' ? user.id : options.postTo;

          setTempContent(tempContent, tempContentKey, req);
        }

        return callback(null, response);
      }
    );
  }
};


function setTempContent(content, key, req){
  req.session.tempContent = req.session.tempContent || {};

  req.session.tempContent[key] = req.session.tempContent[key] || [];

  req.session.tempContent[key].push(content);
}
