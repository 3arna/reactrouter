import lodash from 'lodash';
import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';
import * as contentUtils from '../../utils/contentUtils';

import ContentPage from '../../models/ContentPage';

export const contentFeed = {
  name: "contentService_feed",

  read(req, resource, options, config, callback){
    const parentId = options && options.parentId || 'user';

    let apiUrl;

    apiUrl = `${!req.cookies.token ? '/anonymous' : ''}/content/feed/${parentId}${options.sequence ? ('/' + options.sequence) : ''}`;

    apiUrl += `?maxResults=${req.apiConfig.numberOfItemsPerPage}`;

    if(options.parentalLineage){
      apiUrl += '&parentalLineage=' + options.parentalLineage;
    }

    apiClient.sendRequest(
      { url: apiUrl, method: 'GET'},
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      ContentPage,
      (error, apiResponse) => {
        mergeTempContent(error, apiResponse, req, callback);
      }
    );
  }
};

export const poll = {
  name: 'contentFeedService_poll',
  read(req, resource, options, config, callback){
    const { parentContentId, pollToken } = options;
    const isAnonymous = !req.cookies.token;

    const apiUrl = `${isAnonymous ? '/anonymous' : ''}/content/feed/${parentContentId}/${isAnonymous ? 'poll': 'delta/poll'}/${pollToken}`;

    apiClient.sendRequest(
      { url: apiUrl, method: 'GET'},
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      (error, apiResponse) => {
        if(error){
          return callback(null, false);
        }

        if(isAnonymous || apiResponse.statusCode !== 200){
          return callback(null, { newContentAvailable: apiResponse.statusCode === 205 } );
        }

        const newContentIds = apiResponse.result.list;

        if(apiResponse.statusCode === 200 && newContentIds.length > 1){
          return callback(null, { newContentAvailable: true });
        }

        return callback(null, { newContentAvailable: true, forceReload: !!contentUtils.findTempContent(parentContentId, newContentIds[0], req.session.tempContent)});
      }
    );
  }
}

function mergeTempContent(error, apiResponse, req, callback){
  if(error){
    return callback(error);
  }

  if(apiResponse.error){
    return callback(null, apiResponse);
  }

  contentUtils.mergeTempContent(apiResponse.result, req.session.tempContent);
  return callback(null, apiResponse);
}
