import lodash from 'lodash';
import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';
import * as contentUtils from '../../utils/contentUtils';

export const contentModify = {
  name: 'contentService_redact',

  delete: (req, resource, params, config, callback) => {
    apiClient.sendRequest(
      { url: `/content/modify/redact/${params.contentId}`, method: 'DELETE'},
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      callback
    );
  }
};
