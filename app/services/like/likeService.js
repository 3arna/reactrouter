import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';

export const like = {
  name: 'likeService_like',
  create: (req, resource, params, body, config, callback) => {

    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    apiClient.sendRequest(
      {url: `/like/add/like/${params.objectId}`, method: 'POST'},
      { requestHeaders },
      null,
      callback
    );
  }
};

export const dislike = {
  name: 'likeService_dislike',
  create: (req, resource, params, body, config, callback) => {

    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    apiClient.sendRequest(
      {url: `/like/add/dislike/${params.objectId}`, method: 'POST'},
      { requestHeaders },
      null,
      callback
    );
  }
};

export const remove = {
  name: 'likeService_remove',
  create: (req, resource, params, body, config, callback) => {

    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    apiClient.sendRequest(
      {url: `/like/remove/${params.objectId}`, method: 'POST'},
      { requestHeaders },
      null,
      callback
    );
  }
};
