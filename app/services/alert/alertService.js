import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';
import lodash from 'lodash';

import AlertList from '../../models/AlertList';

export const alertsService = {
  name: "alertService_pull",

  read(req, resource, options, config, callback){
    apiClient.sendRequest(
      { url: '/alerts/pull'},
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      AlertList,
      (err, apiResponse) => {
        if(err){
          return callback(err);
        }

        if(req.session && apiResponse.result && req.session.user){
          if(!!apiResponse.result && !!apiResponse.result.list && !!apiResponse.result.list.length){
            req.session.alerts = !lodash.isArray(req.session.alerts) ? [] : req.session.alerts;
            req.session.alerts = req.session.alerts.concat(apiResponse.result.list); 
          }
          apiResponse.result.list = req.session.alerts;
        }

        callback(err, apiResponse);
      }
    );
  },

  delete(req, resource, options, config, callback){
    //console.log('DELETE', options);

    req.session && req.session.alerts && req.session.alerts.length
      ? req.session.alerts = []
      : false;

    callback(null, {result: {list: req.session.alerts}});
  },
};
