import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';

import User from '../../models/User';

export const userService = {
  name: "userService_user",

  read(req, resource, options, config, callback){
    const userId = options && options.userId || 'me';

    apiClient.sendRequest(
      { url: `/users/${userId}`, method: 'GET'},
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      User,
      callback
    );
  }
};

export const userBlockService = {
  name: "userService_block",

  create(req, resource, prams, body, config, callback){

    apiClient.sendRequest(
      { url: `/user/block/${prams.userId}`, method: 'POST'},
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      callback
    );
  }
};

export const userMeService = {
  name: "userService_me",

  read: (req, resource, params, config, callback) => {
    apiClient.sendRequest(
      { url: '/users/me', method: 'GET'},
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      User,
      callback
    );
  },

  update: (req, resource, params, body, config, callback) => {
    const requestBody = {
      displayName: params.displayName,
      description: params.description,
      location: params.location,
      webSite: params.webSite,
      emailAddress: params.emailAddress,
      gender: params.gender,
    }

    apiClient.sendRequest(
      { url: '/users/me', method: 'PUT'},
      { requestBody, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      User,
      callback
    );
  }
};

export const userAvatarService = {
  name: "userService_avatar",

  update: (req, resource, params, body, config, callback) => {
    if(!Object.keys(params).length){
      return callback(null, null);
    }

    const requestBody = {
      media: params,
    }

    apiClient.sendRequest(
      { url: '/users/me/avatar', method: 'PUT'},
      { requestBody, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      User,
      callback
    );
  }
};

export const userBackgroundService = {
  name: "userService_background",

  update: (req, resource, params, body, config, callback) => {
    if(!Object.keys(params).length){
      return callback(null, null);
    }

    const requestBody = {
      media: params,
    }

    apiClient.sendRequest(
      { url: '/users/me/background', method: 'PUT'},
      { requestBody, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      User,
      callback
    );
  }
}
