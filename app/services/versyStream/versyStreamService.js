import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';

import VersyStreamPage from '../../models/VersyStreamPage';

export const versyStream = {
  name: 'versyStreamService_versyStream',

  read(req, resource, options, config, callback){
    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    const maxResults = req.apiConfig.numberOfItemsPerPage;
    const filter = !!options.filter ? `&filter=${options.filter.toUpperCase()}`: '';
    const state = options.nextState ? `&state=${options.nextState}`: '';
    const apiUrl = `/versy/stream/?maxResults=${maxResults}${state}${filter}`;

    apiClient.sendRequest(
      { url: apiUrl, method: 'GET'},
      { requestHeaders },
      VersyStreamPage,
      callback
    );
  }
};

export const poll = {
  name: 'versyStreamService_poll',

  read(req, resource, options, config, callback){
    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    const apiUrl = `/versy/stream/poll/${options.pollToken}`;

    apiClient.sendRequest(
      { url: apiUrl, method: 'GET'},
      { requestHeaders },
      null,
      (error, apiResponse) => {
        if(error){
          return callback(null, false);
        }


        return callback(null, { newContentAvailable: apiResponse.statusCode === 205 });
      }
    );
  }
};

