import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';

import StreamPage from '../../models/StreamPage';

export const versyStream = {
  name: "versyStreamService_interestsStream",

  read(req, resource, options, config, callback){
    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    const qs = {
      maxResults: req.apiConfig.numberOfItemsPerPage,
    };

    if(options.nextState){
      qs.state = options.nextState;
    }

    apiClient.sendRequest(
      { url: `/versy/stream/categories/${options.userId}`, method: 'GET'},
      { qs, requestHeaders },
      StreamPage,
      callback
    );
  }
};
