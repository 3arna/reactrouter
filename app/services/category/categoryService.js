import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';

import Category from '../../models/Category';
import CategoryList from '../../models/CategoryList';

export const categories = {
  name: "categoryService_firstUse",

  read(req, resource, options, config, callback){
    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);
    const apiUrl = `/category/firstuse/get${options.language ? `?language=${options.language}` : ''}`;

    apiClient.sendRequest(
      { url: apiUrl, method: 'GET'},
      { requestHeaders },
      CategoryList,
      callback
    );
  }
};

export const categoriesDefault = {
  name: "categoryService_default",

  read(req, resource, options, config, callback){
    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);
    const apiUrl = `/default/category/get`;
    apiClient.sendRequest(
      { url: apiUrl, method: 'GET'},
      { requestHeaders },
      CategoryList,
      callback
    );
  }

}

export const categoriesSearch = {
  name: "categoryService_search",

  read(req, resource, options, config, callback){

    const qs = {
      query: options.query,
      offset: options.offset || 0,
      limit: options.limit || 10,
    }

    apiClient.sendRequest(
      { url: '/category/search'},
      { qs, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      CategoryList,
      callback
    );
  }
};

export const categoriesBookmark = {
  name: "categoryService_bookmark",

  create(req, resource, body, options, config, callback){

    const requestBody = {
      'categoryIds': [],
      'newCategoryNames': body.selectedCategories,
    }

    apiClient.sendRequest(
      { url: '/category/bookmarks/set', method: 'POST'},
      { requestBody, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      callback
    );
  }
}
