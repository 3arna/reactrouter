import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';

import SearchResult from '../../models/SearchResult';
import SearchSummaryResults from '../../models/SearchSummaryResults';

export const searchTrending = {
  name: 'searchService_trending',

  read: (req, resource, options, config, callback) => {
    const qs = {
      maxResults: options && options.maxResults || req.apiConfig.numberOfItemsPerPage,
      state: options && options.state,
    };

    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    apiClient.sendRequest(
      {url: '/search/trending', method: 'GET'},
      {qs, requestHeaders},
      SearchSummaryResults,
      callback
    );
  },
}

export const searchAll = {
  name: 'searchService_all',

  read: (req, resource, options, config, callback) => {
    const qs = {
      maxResults: options && options.maxResults || req.apiConfig.numberOfItemsPerPage,
      query: options.query || '',
      state: options && options.state,
    };

    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    apiClient.sendRequest(
      {url: '/search', method: 'GET'},
      {qs, requestHeaders},
      SearchSummaryResults,
      callback
    );
  },
}

export const searchBySearchType = {
  name: 'searchService_bySearchType',

  read: (req, resource, options, config, callback) => {

    const qs = {
      maxResults: options && options.maxResults || req.apiConfig.numberOfItemsPerPage,
      query: options.query && options.query.replace('#', '') || '',
      state: options && options.state,
    };

    const url = `/search/${options.typeOfSearch || ''}`

    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    apiClient.sendRequest(
      {url, method: 'GET'},
      {qs, requestHeaders},
      SearchSummaryResults,
      callback
    );
  },
}
