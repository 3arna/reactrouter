import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';

import SearchContentResult from '../../models/SearchContentResult';

export const searchContent = {
  name: 'searchContent_bySearchType',

  read: (req, resource, options, config, callback) => {
    const qs = {
      maxResults: options && options.maxResults || req.apiConfig.numberOfItemsPerPage,
      state: options && options.nextState,
      query: options.query && options.query.replace('tag:', '') || '',
    };

    const url = `/search/content/${options.searchType || ''}`;

    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    apiClient.sendRequest(
      {url, method: 'GET'},
      {qs, requestHeaders},
      SearchContentResult,
      (error, apiResponse) => {
        if(error){
          return callback(error);
        }

        if(apiResponse && apiResponse.result){
          apiResponse.result.parentContentId = options.query;
        }

        return callback(null, apiResponse);
      }
    );
  },

}

export const poll = {
  name: 'searchContentService_poll',
  read(req, resource, options, config, callback){
    const { pollToken } = options;

    const apiUrl = `/search/content/poll/${pollToken}`;

    apiClient.sendRequest(
      { url: apiUrl, method: 'GET'},
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      (error, apiResponse) => {
        if(error){
          return callback(null, false);
        }

        return callback(null, { newContentAvailable: apiResponse.statusCode === 205 });
      }
    );
  }
}
