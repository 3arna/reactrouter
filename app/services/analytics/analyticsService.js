import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';
import lodash from 'lodash';

import UserAnalytics from '../../models/UserAnalytics';

export const analyticsService = {
  name: "analyticsService_user",

  read(req, resource, options, config, callback){
    apiClient.sendRequest(
      { url: '/analytics/user' },
      { requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      UserAnalytics,
      callback
    );
  }
};
