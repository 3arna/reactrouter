import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';

import BookmarkList from '../../models/BookmarkList';

export const bookmarks = {
  name: 'bookmarkService',

  read: (req, resource, options, config, callback) => {
    let apiUrl = `/bookmarks?maxResults=${options.maxResults || 4096}`;

    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    apiClient.sendRequest(
      {url: apiUrl, method: 'GET'},
      {requestHeaders},
      BookmarkList,
      callback
    );
  },

  create: (req, resource, options, config, body, callback) => {
    const objectId = options.objectId || escape(options.objectId);

    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    apiClient.sendRequest(
      {url: `/bookmarks/add/${objectId}`, method: 'POST'},
      {requestHeaders},
      null,
      callback
    );
  },


  delete: (req, resource, options, config, callback) => {
    const objectId = options.objectId || escape(options.objectId);

    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    apiClient.sendRequest(
      {url: `/bookmarks/delete/${objectId}`, method: 'DELETE'},
      {requestHeaders},
      null,
      callback
    );
  },
}
