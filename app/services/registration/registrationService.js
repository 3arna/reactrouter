import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';

import User from '../../models/User';

export const registrationService = {
  name: "registrationService_user",

  create(req, resource, body, options, config, callback){

    const requestBody = {
      displayName: body.name,
      email: body.email,
      password: body.password,
      language: body.language,
    }

    if(req.session && req.session.campaignCode){
      requestBody.campaignCode = req.session.campaignCode;
    }

    apiClient.sendRequest(
      { url: '/registration/requestAndConfirm', method: 'POST'},
      { requestBody, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      (error, apiResponse) => {
        if(error){
          return callback(error);
        }

        /*if(req.session && req.session.campaignCode){
          delete req.session.campaignCode;
        }*/
        if(apiResponse.result && apiResponse.result.user){
          req.session.user = new User(apiResponse.result.user);
        }

        callback(null, apiResponse);
      }
    );
  },
  read(req, resource, options, config, callback){

    const requestBody = {
      email: options.email,
      password: options.password,
    }

    apiClient.sendRequest(
      { url: '/registration/login/email', method: 'POST'},
      { requestBody, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      (error, apiResponse) => {
        if(error){
          return callback(error);
        }
        if(apiResponse.result && apiResponse.result.user){
          req.session.user = new User(apiResponse.result.user);
        }

        callback(null, apiResponse);
      }
    );
  }
};

export const passwordService = {
  name: "passwordService_reset",

  read(req, resource, options, config, callback){

    const qs = {
      email: options.email,
      language: options.language,
    }

    apiClient.sendRequest(
      { url: '/registration/password/reset'},
      { qs, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      (error, apiResponse) => {
        if(error){
          return callback(error);
        }


        if(apiResponse.result && apiResponse.result.user){
          req.session.user = new User(apiResponse.result.user);
        }

        callback(null, apiResponse);
      }
    );
  },

  create(req, resource, body, options, config, callback){

    const requestBody = {
      email: body.email,
      token: body.token,
      password: body.password,
    }

    apiClient.sendRequest(
      { url: '/registration/password/reset', method: 'POST'},
      { requestBody, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      (error, apiResponse) => {
        if(error){
          return callback(error);
        }

        if(apiResponse.result && apiResponse.result.user){
          req.session.user = new User(apiResponse.result.user);
        }

        callback(null, apiResponse);
      }
    );
  },
};

export const emailService = {
  name: "emailService_confirm",

  read(req, resource, options, config, callback){

    const qs = {
      email: options && options.email,
      token: options && options.token,
    }

    apiClient.sendRequest(
      { url: '/registration/confirm/email'},
      { qs, requestHeaders: apiClientUtils.getApiRequestHeaders(req) },
      null,
      callback
    );
  },
}

