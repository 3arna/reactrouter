import lodash from 'lodash';
import querystring from 'querystring';
import request from 'request';
import { serverConfigs } from '../../configs';

export const getFacebookAccessToken = {
  name: 'authenticationService_facebookAccessToken',

  read(req, resource, options, config, callback) {

    const facebookConfig = lodash.clone(serverConfigs.facebook);
    facebookConfig.redirect_uri = serverConfigs.baseUrl + serverConfigs.facebook.redirect_uri;

    Object.assign(options, facebookConfig);

    request({
      qs: options,
      uri: serverConfigs.facebook.accessTokenUrl,
    }, (error, response, body) => {
      if(error){
        return callback(error);
      }

      if(response.statusCode !== 200){
        return callback(new Error('facebook returned status code ' + response.statusCode));
      }

      const accessToken = querystring.parse(body).access_token;
      return callback(null, accessToken);
    });

  }
};

export const getGoogleAccessToken = {
  name: 'authenticationService_googleAccessToken',

  read(req, resource, options, config, callback) {

    const { clientId, clientSecret, redirectUri } = serverConfigs.google;

    options.client_id = clientId;
    options.client_secret = clientSecret;
    options.grant_type = 'authorization_code';
    options.redirect_uri = serverConfigs.baseUrl + redirectUri;

    request({
      method: 'POST',
      form: options,
      uri: serverConfigs.google.accessTokenUrl,
    }, (error, response, body) => {
      if(error){
        return callback(error);
      }

      if(response.statusCode !== 200){
        return callback(new Error('google returned status Code ' + response.statusCode));
      }

      const accessToken = JSON.parse(body).access_token;
      return callback(null, accessToken);
    });
  }
};

export const getTwitterRequestToken = {
  name: 'authenticationService_twitterRequestToken',

  read(req, resource, options, config, callback) {
    //console.log(options, configs);
    const oauth = {
      callback: serverConfigs.baseUrl + serverConfigs.twitter.callback,
      consumer_key: serverConfigs.twitter.consumer_key,
      consumer_secret: serverConfigs.twitter.consumer_secret,
    };

    request({
      method: 'post',
      uri: serverConfigs.twitter.requestTokenUrl,
      oauth
    }, (error, response, body) => {

      if(error){
        return callback(error);
      }

      if(response.statusCode !== 200){
        return callback(new Error('twitter returned status code ' + response.statusCode));
      }

      body = querystring.parse(body);

      if(!body.oauth_callback_confirmed){
        return callback(new Error('twitter callback is not confirmed'));
      }

      if(!body.oauth_token || !body.oauth_token_secret){
        return callback(new Error('twitter request token data is incorrect'));
      }

      const requestTokenData = {
        requestToken: body.oauth_token,
        requestTokenSecret: body.oauth_token_secret,
      };
      return callback(null, requestTokenData);
    });

  }
};

export const getTwitterAccessToken = {
  name: 'authenticationService_twitterAccessToken',

  read(req, resource, options, config, callback) {
    const oauth = {
      token: options.token,
      verifier: options.verifier,
    };

    //console.log(oauth, serverConfigs.twitter.accessTokenUrl);

    request({
      method: 'post',
      uri: serverConfigs.twitter.accessTokenUrl,
      oauth
    }, (error, response, body) => {

      if(error){
        return callback(error);
      }

      if(response.statusCode !== 200){
        return callback(new Error('twitter returned status code ' + response.statusCode));
      }

      body = querystring.parse(body);

      return callback(null, `${body.oauth_token};${body.oauth_token_secret}`);
    });

  }
};
