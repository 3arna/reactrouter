import * as apiClient from '../../apiClient/client';
import * as apiClientUtils from '../../utils/apiClientUtils';

export const reportUser = {
  name: 'reportService_user',

  create: (req, resource, body, options, config, callback) => {

    const requestBody = {
      entityIdentifier: body.objectId,
      reason: body.reason,
    };

    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    apiClient.sendRequest(
      {url: '/report/user', method: 'POST'},
      {requestHeaders, requestBody},
      null,
      callback
    );
  },

}

export const reportContent = {
  name: 'reportService_content',

  create: (req, resource, body, options, config, callback) => {
    const requestBody = {
      entityIdentifier: body.objectId,
      reason: body.reason,
    };

    let requestHeaders = apiClientUtils.getApiRequestHeaders(req);

    apiClient.sendRequest(
      {url: '/report/content', method: 'POST'},
      {requestHeaders, requestBody},
      null,
      callback
    );
  },

}
