let config = {};

config.apiBaseUrl = 'http://api.versy.com';
config.resizeImagesBaseUrl = 'https://dymulno8a97tt.cloudfront.net';
config.baseUrl = 'https://versy.com';

config.facebook = {
  client_id: '1634850610088427',
  client_secret: '64ea2d64e5b4c4da8f220ec94c91acbf',
  redirect_uri: '/auth/facebook',
  accessTokenUrl: 'https://graph.facebook.com/oauth/access_token',
  codeUrl: 'https://www.facebook.com/dialog/oauth',
};

config.google = {
	clientId: '179100250617-d8j3o23j8utu43nta0tmvdod5mq2m43h.apps.googleusercontent.com',
  clientSecret: 'i_uaaesUe2CDB48OGZmcImeJ',
	redirectUri: '/auth/google',
	codeUrl: 'https://accounts.google.com/o/oauth2/auth',
  accessTokenUrl: 'https://www.googleapis.com/oauth2/v3/token',
};

config.twitter = {
	consumer_key: 'jRkML6nnvjuoXAodleHE474Co',
  consumer_secret: 'F9lZ3R6OARGkmXJLovyeWZEQVFg28ktXTybpDbEuvO2Ukf80Md',
  callback: '/auth/twitter',
  requestTokenUrl: 'https://api.twitter.com/oauth/request_token',
  authenticateUrl: 'https://api.twitter.com/oauth/authenticate',
  accessTokenUrl: 'https://api.twitter.com/oauth/access_token',
}

config.session = {
  secret: 'asdasdqwe12e324i2jqsamdi22e12ascbd',
}

config.redis = {
  host: '127.0.0.1',
  port: 6379
}

export default config;
