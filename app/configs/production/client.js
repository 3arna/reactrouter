let config = {};

config.resizeImagesBaseUrl = 'https://dymulno8a97tt.cloudfront.net';
config.baseUrl = 'https://versy.com';

config.facebook = {
  client_id: '1634850610088427',
  redirect_uri: '/auth/facebook',
  codeUrl: 'https://www.facebook.com/dialog/oauth',
};

config.google = {
	clientId: '179100250617-d8j3o23j8utu43nta0tmvdod5mq2m43h.apps.googleusercontent.com',
	redirectUri: '/auth/google',
	codeUrl: 'https://accounts.google.com/o/oauth2/auth',
};

config.twitter = {
	consumer_key: 'jRkML6nnvjuoXAodleHE474Co',
  callback: '/auth/twitter',
  requestTokenUrl: 'https://api.twitter.com/oauth/request_token',
  authenticateUrl: 'https://api.twitter.com/oauth/authenticate',
}

export default config;
