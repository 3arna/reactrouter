let config = {};

//config.apiBaseUrl = 'http://api-stage.versy.com:80';
config.apiBaseUrl = 'http://versyplus-api-dev.versy.com:8080';
config.resizeImagesBaseUrl = 'https://image.versy.com';
config.baseUrl = 'http://localhost:8080';

config.locales = [
  'en',
  'es',
  'pt'
];

config.facebook = {
  client_id: '1635147640058724',
  client_secret: '2f8593882645c2529dddaa98dff655dc',
  redirect_uri: '/auth/facebook',
  accessTokenUrl: 'https://graph.facebook.com/oauth/access_token',
  codeUrl: 'https://www.facebook.com/dialog/oauth',
};

config.google = {
  clientId: '179100250617-lhtobolcmlsv3vqnbhpqsr20db8odvmj.apps.googleusercontent.com',
  clientSecret: 'pB1cOnL_mB3bYq1gcUE9L1rv',
  redirectUri: '/auth/google',
  codeUrl: 'https://accounts.google.com/o/oauth2/auth',
  accessTokenUrl: 'https://www.googleapis.com/oauth2/v3/token',
};

config.twitter = {
  consumer_key: 'V1tDYjdUGC23Zjrc4Br3icEAm',
  consumer_secret: '5jdIQYUbq0W9nBuqM5zKwcR2Ube8Rx6AomF6cpZqZ3mfoXYGBV',
  callback: '/auth/twitter',
  requestTokenUrl: 'https://api.twitter.com/oauth/request_token',
  authenticateUrl: 'https://api.twitter.com/oauth/authenticate',
  accessTokenUrl: 'https://api.twitter.com/oauth/access_token',
}

config.session = {
  secret: 'asdasdqwe12e324i2jqsamdi22e12ascbd',
}

config.redis = {
  host: '127.0.0.1',
  port: 6379
}

export default config;
