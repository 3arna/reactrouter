let config = {};

config.resizeImagesBaseUrl = 'https://image.versy.com';
config.baseUrl = 'http://localhost:8080';

config.facebook = {
  client_id: '1635147640058724',
  redirect_uri: '/auth/facebook',
  codeUrl: 'https://www.facebook.com/dialog/oauth',
  shareUrl: 'https://www.facebook.com/dialog/share',
};

config.google = {
  clientId: '179100250617-lhtobolcmlsv3vqnbhpqsr20db8odvmj.apps.googleusercontent.com',
  redirectUri: '/auth/google',
  codeUrl: 'https://accounts.google.com/o/oauth2/auth',
  shareUrl: 'https://plus.google.com/u/0/share'
};

config.twitter = {
  consumer_key: 'V1tDYjdUGC23Zjrc4Br3icEAm',
  callback: '/auth/twitter',
  requestTokenUrl: 'https://api.twitter.com/oauth/request_token',
  authenticateUrl: 'https://api.twitter.com/oauth/authenticate',
  shareUrl: 'https://twitter.com/intent/tweet'
}


export default config;