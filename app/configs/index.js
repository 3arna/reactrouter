import lodash from 'lodash';
import globalClient from './globalClient';
import globalServer from './globalServer';

const generateClientConfigs = () => {
  const env = process.env.NODE_ENV;
  return env   
    ? lodash.merge(globalClient, require('./' + env + '/client.js'))
    : globalClient;
};

const generateServerConfigs = () => {
  const env = process.env.NODE_ENV;
  return env
    ? lodash.merge(globalServer, require('./' + env + '/server.js'))
    : globalServer;
};

export const clientConfigs = generateClientConfigs();
export const serverConfigs = generateServerConfigs();
