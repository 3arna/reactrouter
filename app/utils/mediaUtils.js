import lodash from 'lodash';

export function getMainMedia(content){
  return lodash.isArray(content.media) && lodash.first(content.media) || content.media;
}

export function getDimensions(media, maxMediaWidth, maxMediaHeight){
  const {height, width} = media.attributes;

  const isWidthBigger = width > maxMediaWidth;
  const isHeightBigger = height > maxMediaHeight;

  const wRatio = width / height;
  const hRatio = height / width;
  const maxRatio = maxMediaHeight / maxMediaWidth;

  if(!isWidthBigger && !isHeightBigger){
    return {
      width: width,
      height: height,
      padding: (hRatio*100)
    }
  }

  let scaledHeight;
  let scaledWidth;

  if(wRatio <= 1 && isHeightBigger){
    scaledHeight = maxMediaHeight;
    scaledWidth = wRatio * scaledHeight;
  } else
  if(wRatio > 1 && isWidthBigger){
    scaledWidth = maxMediaWidth;
    scaledHeight = hRatio * scaledWidth;
  } else {
    scaledWidth = maxMediaWidth;
    scaledHeight = maxMediaHeight;
  }


  return {
    width: parseInt(scaledWidth),
    height: parseInt(scaledHeight),
    padding: (hRatio*100)
  }
}

export function getMediaType(media){
  const mimeType = media && media.mimeType && lodash.first(media.mimeType.split('/'));
  return mimeType;
}

export function getMediaUrl(media, dimensions, imageResizeEndpoint){
  return !lodash.contains(media.mediaUrl, 'blob:')
    ? `${imageResizeEndpoint}${dimensions.width}x${dimensions.height}/${media.mediaUrl}`
    : media.mediaUrl;
}