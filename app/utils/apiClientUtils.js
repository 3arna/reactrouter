export function getApiRequestHeaders(req, token){
  let requestHeaders = {
    'User-Agent': req.headers['user-agent'] || '',
    'X-Forwarded-For': req.headers['x-forwarded-for'] || '',
    'Client-Type': 'WEB'
  };

  if(req.cookies.token){
    requestHeaders['Authorization'] = `Bearer ${req.cookies.token}`
  }

  return requestHeaders;
};
