import moment from 'moment';
import passwordStrength from 'password-strength';

export function validEmail(email){
  return (/[^\s@]+@[^\s@]+\.[^\s@]+/.test(email));
};

export function validPassword(password, minLength, maxLength){
  minLength = minLength || 8;
  maxLength = maxLength || 25;
  return password && password.length >= minLength && password.length <= maxLength;
}

export const validInput = (input, options) => {
  options.type = options.type || options.name;
  if(options.validEmpty && !input.length){
    return true;
  }

  if(options.minLength && input.length < options.minLength){
    return false;
  }

  if(options.type !== 'email' && options.maxLength && input.length > options.maxLength){
    return false;
  }

  let valid = false;

  switch(options.type){
    case 'password':
      const psw = passwordStrength(input);
      return psw.strength;
    break;
    case 'email':
      return (/[^\s@]+@[^\s@]+\.[^\s@]+/).test(input);
    break;
    case 'text':
      return (/^[a-zA-Z0-9\s\-\.\@\/\&\u00C0-\u017F]*$/).test(input);
    case 'name':
      if(input.split(' ').join('').length < options.minLength){
        return false;
      } 
      return (/^[a-zA-Z\s0-9]*$/).test(input);
    default:
      return true;
    break;
  }
}
