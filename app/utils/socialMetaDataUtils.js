

export function getFacebookMetaTags(pageTitle, pageDescription, pageImage, type){
  let tags = [
    { property: 'og:site_name', content: 'Versy' },
    { property: 'og:title', content: pageTitle },
    { property: 'og:type', content: type }
  ];

  if(pageDescription){
    tags.push({ property: 'og:description', content: pageDescription });
  }

  if(pageImage){
    tags.push({ property: 'og:image', content: pageImage});
  }

  return tags;
}

export function getGoogleMetaTags(pageTitle, pageDescription, pageImage){
  let tags = [
    { itemProp: 'name', content: pageTitle }
  ];

  if(pageDescription){
    tags.push({ itemProp: 'description', content: pageDescription });
  }

  if(pageImage){
    tags.push({ itemProp: 'image', content: pageImage});
  }

  return tags;
}

export function getTwitterMetaTags(pageTitle, pageDescription, pageImage){
  let tags = [
    { name: 'twitter:card', content: 'summary_large_image' },
    { name: 'twitter:site', content: 'Versy' },
    { name: 'twitter:title', content: pageTitle },
  ];

  if(pageDescription){
    tags.push({ name: 'twitter:description', content: pageDescription });
  }

  if(pageImage){
    tags.push({ name: 'twitter:image:src', content: pageImage});
  }

  return tags;
}
