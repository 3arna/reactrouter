import lodash from 'lodash';
import moment from 'moment';
import translations from '../translations';

export function getAgo(date){
  date = moment().format('DD/MM/YYYY') === moment(date).format('DD/MM/YYYY')
    ? 'Today'
    : moment(date).format('MMM DD');

  return date;
};

export function translatedFromNow(date, language){
  const translatedMoment = moment(date);
  const translationsForLanguage = lodash.find(translations, { prefix: language});

  if(translationsForLanguage && translationsForLanguage.relativeTime){
    translatedMoment.locale(language, translationsForLanguage.relativeTime);
  }

  return translatedMoment.fromNow();
};

export function translateWeekdays(language, isFull){
  const translationsForLanguage = lodash.find(translations, { prefix: language});
  return lodash.get(translationsForLanguage, 'weekdaysShort') || [];
}

export function translateTime(language, time){
  const translationsForLanguage = lodash.find(translations, { prefix: language});
  return lodash.get(translationsForLanguage, `time.${time}`) || time;
}

