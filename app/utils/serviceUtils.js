export function serviceResponseHandler({ onError, onSuccess }){
  return (error, apiResponse) => {
    if(error || apiResponse.error){
      return onError(error || apiResponse);
    }

    return onSuccess(apiResponse.result);
  }
}
