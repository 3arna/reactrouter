import lodash from 'lodash';
import * as mediaUtils from './mediaUtils';
import * as socialMetaDataUtils from './socialMetaDataUtils';

export function getPageTitle(feedContentPage, translate){
  if(!feedContentPage){
    return '';
  }

  if(feedContentPage.isProfile){
    return `${feedContentPage.ultimateParent.displayName} | Versy`
  }

  if(lodash.startsWith(feedContentPage.parentContentId, 'tag') || lodash.startsWith(feedContentPage.parentContentId, 'category')){
    return `${translate('phrases.postsAbout')} ${feedContentPage.parentContentId.split(':').pop()} | Versy`;
  }

  return 'Versy';
}

export function getPageDescription(feedContentPage){
  if(!feedContentPage || (feedContentPage && !feedContentPage.isProfile)){
    return '';
  }

  return feedContentPage.ultimateParent.description;
}

export function getPageImage(feedContentPage){
  const defaultImage = '/img/versy-rainbow.png';

  if(!feedContentPage.isProfile){
    return defaultImage;
  }

  return feedContentPage.ultimateParent.backgroundUrl;
}

export function getFeedMetaTags(feedContentPage, translate){
  if(!feedContentPage){
    return [];
  }

  const pageTitle = getPageTitle(feedContentPage, translate);
  const pageDescription = getPageDescription(feedContentPage);
  const pageImage = getPageImage(feedContentPage);

  let metaTags = [
    { name: 'description', content: pageDescription }
  ];

  const facebookTags = socialMetaDataUtils.getFacebookMetaTags(
    pageTitle,
    pageDescription,
    pageImage,
    feedContentPage.isProfile ? 'profile' : 'article'
  );

  const googleTags = socialMetaDataUtils.getGoogleMetaTags(pageTitle, pageDescription, pageImage);
  const twitterTags = socialMetaDataUtils.getTwitterMetaTags(pageTitle, pageDescription, pageImage);

  return metaTags.concat(facebookTags, googleTags, twitterTags);
}
