import lodash from 'lodash';
import Content from '../models/Content';
import moment from 'moment';

import * as mediaUtils from './mediaUtils';
import * as textFormattingUtils from './textFormatingUtils';
import * as socialMetaDataUtils from './socialMetaDataUtils';

export function getPageTitle(content){
  if(!content){
    return '';
  }

  return content.name && lodash.trunc(content.name, 60, '...') ||
    `${lodash.trunc(content.author.displayName, 60, '...')} | Versy`;
}

export function getPageDescription(content){
  if(!content){
    return '';
  }

  return content.message ? lodash.trunc(content.message, 150, '...') : '';
}

export function getPageImage(content, baseImageResizeUrl){
  const mainMedia = mediaUtils.getMainMedia(content);

  if(!mainMedia || (mainMedia && mediaUtils.getMediaType(mainMedia) !== 'image')){
    return '/img/versy-rainbow.png';
  }

  const dimensions = mediaUtils.getDimensions(mainMedia, 800, 600);

  return mediaUtils.getMediaUrl(mainMedia, dimensions, baseImageResizeUrl);
}


export function getContentMetaTags(content, baseImageResizeUrl){
  if(!content){
    return [];
  }

  const pageTitle = getPageTitle(content);
  const pageDescription = getPageDescription(content);
  const pageImage = getPageImage(content, baseImageResizeUrl);

  let metaTags = [
    { name: 'description', content: pageDescription }
  ];

  const facebookTags = socialMetaDataUtils.getFacebookMetaTags(pageTitle, pageDescription, pageImage, 'article');
  const googleTags = socialMetaDataUtils.getGoogleMetaTags(pageTitle, pageDescription, pageImage);
  const twitterTags = socialMetaDataUtils.getTwitterMetaTags(pageTitle, pageDescription, pageImage);

  return metaTags.concat(facebookTags, googleTags, twitterTags);
}

export function findTempContent(parentContentId, contentId, tempContent){
  if(!tempContent){
    return false;
  }

  const contentPageTempContent = tempContent[parentContentId];

  return contentPageTempContent && lodash.find(contentPageTempContent, { id: contentId });
}

export function getTempContent(publishedContent, newContentId, currentUser, currentPageMax){
  const newContentSequence = currentPageMax ? currentPageMax +1: 1;
  publishedContent.created = new Date();
  publishedContent.sequence = newContentSequence;
  publishedContent.isTemp = true;
  publishedContent.author = currentUser;
  publishedContent.id = newContentId;
  publishedContent.media = publishedContent.media ? publishedContent.media : [];

  return new Content(publishedContent);
}

export function mergeTempContent(contentPage, tempContent){
  if(!tempContent){
    return;
  }

  const contentPageTempContent = tempContent[contentPage.parentContentId];

  let newContentPageTempContent = [];

  if(contentPageTempContent && contentPageTempContent.length){
    contentPageTempContent.forEach((tempContent, index) => {
      if(!lodash.find(contentPage.content, { id: tempContent.id })){
        contentPage.content.unshift(tempContent);
        if(tempContent.sequence > contentPage.pageMax){
          contentPage.pageMax = tempContent.sequence;
        }
        newContentPageTempContent.push(tempContent);
      }
    })

    tempContent[contentPage.parentContentId] = newContentPageTempContent;
  }
}

export function generateSharingUrl(config, apiConfig, params){

  let campaignQuery = apiConfig.externalShareCampaignCodeFormat;
  campaignQuery = campaignQuery.replace('${clientType}', 'web');
  campaignQuery = campaignQuery.replace('${platform}', params.platform);
  campaignQuery = campaignQuery.replace('${contentId}', params.contentId);
  campaignQuery = campaignQuery.replace('${userId}', params.userId);

  const url = !params.url
    ? lodash.contains(params.contentId, 'user')
        && `${config.baseUrl}/feed/${params.contentId}${campaignQuery}`
        || `${config.baseUrl}/content/${params.contentId}${campaignQuery}`
    : params.url+campaignQuery;

  switch(params.platform){
    case 'facebook':
      return `${config.facebook.shareUrl}?app_id=${config.facebook.client_id}&href=${url}&display=popup`;
    case 'google':
      return `${config.google.shareUrl}?url=${url}`;
    case 'twitter':
      return `${config.twitter.shareUrl}?text=${params.subject}&via=VersySocial&url=${url}`;
    default:
      return `mailto:?subject=${params.subject}&body=${params.body} ${url}`;
  }
}

export function isContentPageDataStale(contentPage){
  if(!contentPage.fetchedAt){
    return true;
  }

  const now = moment();
  const fetchedAt = moment(contentPage.fetchedAt);
  return now.diff(fetchedAt, 'seconds') > 120;
}

export function getFirstVisibleIndex(contentArray){
  contentArray.forEach((content, index) => {
    if(!content.redacted || content.redacted && content.childCount){
      return index;
    }
  });
}

export function getFriendlyUrl(id, title, isFeed){
  return `/${isFeed ? 'feed' : 'content'}/${id}/${textFormattingUtils.convertToUrlSlug(title)}`;
}


