import moment from 'moment';

export function getChartData(userAnalytics, chartTypes){
  return {
    labels: userAnalytics.datapoints.map((datapoint, index) => {
      if(index === 0 || index === userAnalytics.datapoints.length - 1){
        return moment(datapoint.date).format('D MMM');
      }

      return moment(datapoint.date).format('D');
    }),
    datasets: chartTypes.map(chartType => {
      return {
        ...chartType.config,
        data: userAnalytics.datapoints.map(datapoint => chartType.config.aggregate
          ? Math.ceil(datapoint.totals[chartType.key])
          : Math.ceil(datapoint.deltas[chartType.key])
        )
      };
    })
  };
};
