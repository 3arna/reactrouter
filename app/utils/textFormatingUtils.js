import lodash from 'lodash';

export function generateHypertext(text, tags, urls){
  const lowerCaseText = text.toLowerCase();

  const hypertextArray = tags.concat(lodash.pluck(urls, 'originalUrl'));
  let hypertextPositions = [];


  hypertextArray && hypertextArray.forEach((hypertext) => lodash.contains(lowerCaseText, hypertext.toLowerCase()) && hypertextPositions.push({
    indexStart: lowerCaseText.indexOf(hypertext.toLowerCase()),
    indexEnd: (lowerCaseText.indexOf(hypertext.toLowerCase()) + hypertext.length),
    string: hypertext,
    type: lodash.contains(hypertext, 'http') ? 'link' : 'tag'
  }));

  if(!hypertextPositions.length){
    return text;
  }

  hypertextPositions = lodash.sortBy(hypertextPositions, 'indexStart').reverse();

  hypertextPositions.forEach((hypertextPosition) => {

    let tagHtml = '';

    switch(hypertextPosition.type){
      case 'link':
        tagHtml = `<a class="content-link" target="_blank" href="${hypertextPosition.string}">${hypertextPosition.string}</a>`;
      break;
      case 'tag':
        tagHtml = `<a class="content-tag" href="/feed/${hypertextPosition.string.replace('#', 'tag:')}">${hypertextPosition.string}</a>`;
    }

    text = text.substr(0, hypertextPosition.indexStart)
      + tagHtml
      + text.substr(hypertextPosition.indexEnd, text.length);
  });

  return text;


  /*urls && urls.forEach((url) => {

    url = url.originalUrl;
    const indexStart = text.indexOf(url);

    hypertextPositions[indexStart] = {
      indexStart: indexStart,
      indexEnd: (indexStart + url.length),
      string: url,
    };
    text = text.replace(url.originalUrl, `<a href="#" class="content-url">${url.originalUrl}</a>`)
  });*/

  /*urls && urls.forEach((url) => {
    text = text.replace(url.originalUrl, `<a href="#" class="content-url">${url.originalUrl}</a>`)
  });*/
};

export function getOverThousandString(number){
  return number > 999 && ((number/1000).toFixed(1) + 'k') || number;
}

export function convertToUrlSlug(text)
{
  return text && text
    .toLowerCase()
    .replace(/[^\w ]+/g,'')
    .replace(/ +/g,'-');
}
