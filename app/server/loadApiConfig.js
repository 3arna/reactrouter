import cache from 'memory-cache';

import { configService } from '../services/config/configService';

const loadApiConfig = (req, res, next) => {
  const configCacheKey = 'versy-api-config';
  const cachedConfig = cache.get(configCacheKey);

  if(cachedConfig){
    req.apiConfig = cachedConfig;
    return next();
  }

  configService.read(req, {}, {}, {}, (error, apiResponse) => {
    if (error || apiResponse.error){
      //dont error if we're on the debug url
      if(req.url === '/debug'){
        return next();
      }

      return next(new Error('error fetching api config'));
    }

    const config = apiResponse.result;

    cache.put(configCacheKey, config, config.configurationRefreshTime);
    req.apiConfig = config;

    return next();
  });
};

export default loadApiConfig;
