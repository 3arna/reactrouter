import lodash from 'lodash';
import cookie from 'cookie';
import { navigateAction } from 'fluxible-router';

export default () => {

	return {
    name: 'RedirectorPlugin',

    plugContext: (options) =>  {
      const expressResponse = options.res;

      return {
        plugActionContext: (actionContext) => {
          actionContext.req = options.req;

          actionContext.back = () => {
            if(!expressResponse){
              return window.history.back();
            }
          };

          actionContext.redirect = (url) => {
            if(!expressResponse){
              return window.location.href = url;
            } else {

              // Make sure the URL starts with a / so it isn't interpreted as a relative URL
              if(url.indexOf('http://') === -1 && url.indexOf('https://') === -1 && url.substring(0, 1) !== '/') {
                  url = '/' + url;
              }

              redirectOptions = redirectOptions || {};
              const status = redirectOptions.status || 303;

              //console.log('REDIRECTING IN SERVER SIDE', url);
              return expressResponse.redirect(status, url);
              //this.redirectedTo = url;
            }
          };
          actionContext.redirectTo = (url, redirectOptions) => {

            var forceRedirect = redirectOptions && redirectOptions.forceRedirect || false;

            /*if(history){
                history.replaceState({}, redirectOptions.title || 'error with title after redirect', url);
              }*/

            if(!url || !url.length) {
                return;
            }

            if(!expressResponse){
              if(!forceRedirect &&  url.substring(0, 1) === '/'){
                //alert('REDIRECTING WITHOUT REFRESH TO ' + url);
                //history.replaceState({}, 'error with title after redirect', url);
                return context.executeAction(navigateAction, { method: 'GET', url: url});
              } else {
                //alert('REDIRECTING CLIENT SIDE WITH REDIRECT ' + url);
                return window.location.href = url;
              }
            } else {

              // Make sure the URL starts with a / so it isn't interpreted as a relative URL
              if(url.indexOf('http://') === -1 && url.indexOf('https://') === -1 && url.substring(0, 1) !== '/') {
                  url = '/' + url;
              }

              redirectOptions = redirectOptions || {};
              const status = redirectOptions.status || 303;

              //console.log('REDIRECTING IN SERVER SIDE', url);
              return expressResponse.redirect(status, url);
              //this.redirectedTo = url;
            }
          };

          actionContext.hasRedirected = function () {
              return !!this.redirectedTo;
          };
        }
      };
    }
	}
};
