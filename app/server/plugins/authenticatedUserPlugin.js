export default () => {
  let authenticatedUser;

  return {
    name: 'AuthenticatedUserPlugin',
    plugContext: function plugContext(contextOptions){

      return {
        plugActionContext: (actionContext) => {
          const userStore = actionContext.getStore('UserStore');
          actionContext.authenticatedUser = userStore.user;
        },

        plugComponentContext: (componentContext) => {
          const userStore = componentContext.getStore('UserStore');
          componentContext.authenticatedUser = userStore.user;
        }
      };
    },
  };
}
