import lodash from 'lodash';
import cookie from 'cookie';
import template from 'es6-template-strings';

export default (translations, options) => {

	try {
    if(translations === void(0)) throw 'You have to specify translation files as an array of objects eg: [module.require("en.js")]';
	}
	catch(err){
	  throw new Error(err);
	}

  let defaultLanguage;

	//set default translation file prefix field name
	const defaultPrefixField = options && options.prefixField || 'prefix';
	const defaultNameField = options && options.nameField || 'name';

	//set available language list
	const languages = lodash.map(translations, (translation) => { 
		return {[defaultNameField]: translation[defaultNameField], [defaultPrefixField]: translation[defaultPrefixField]}
	});

	return {
	  name: 'TranslationPlugin',
	  plugContext: (options) => {
	    let req = options.req;
	    let cookies = req ? req.cookies : cookie.parse(document.cookie);

      if(req){
        defaultLanguage = req.query.lng || req.locale;
      }

      const validatedLanguage = lodash.find(languages, { prefix: cookies.lang || defaultLanguage });

	    //set language
	    let language = validatedLanguage ? validatedLanguage.prefix : 'en';

	    //set default translation object
			let translation = lodash.find(translations, {[defaultPrefixField]: language});


      const translate = (valuePath, opts, fallback) => {
				//gets translated string from selected language translation object or default translation object
				let translatedString = lodash.result(translation, valuePath);

        if(lodash.startsWith(translatedString, 'phrases.') || lodash.startsWith(translatedString, 'words.')){
          translatedString = lodash.result(translation, translatedString);
        }

        if(!translatedString && fallback){
          translatedString = lodash.result(translation, fallback);
        }

        translatedString = translatedString || valuePath;

				//returns plain or templated string 
				return !lodash.keys(opts).length ? translatedString : template(translatedString, opts);
			}

	    return {
	      
	      plugComponentContext: (componentContext) => {
	      	//add translate function to compontentContext
	        componentContext.translate = translate;
	        //return current language to componentContext
	        componentContext.getLanguage = () => language;
	        //return language list
	        componentContext.getLanguages = () => languages;


	      },
	      
	      plugActionContext: (actionContext) => {
	      	//add setlanguage function to actionContext
          actionContext.setLanguage = (lang) => {
          	language = lang;
          	translation = lodash.find(translations, {[defaultPrefixField]: lang});
          };

          actionContext.translate = translate;
          actionContext.getLanguage = () => language;
        },

        plugStoreContext: (storeContext) => {
        	storeContext.translate = translate;
        },

	      dehydrate: () => {
          return {
            defaultLanguage: defaultLanguage,
            translation: translation,
            language: language,
          };
        },

        rehydrate: (state) => {
          defaultLanguage: defaultLanguage,
        	translation = state.translation;
        	language = state.language;
        }
	    }
	  }
	}
};