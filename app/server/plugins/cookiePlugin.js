import cookie from 'cookie';
import lodash from 'lodash';

export default () => {
  return {
    name: 'CookiePlugin',
    plugContext: function (options) {
      const req = options.req;
      const res = options.res;

      return {
        plugActionContext: (actionContext) => {

          actionContext.setCookie = (name, value, options) => {
            //sets default cookie path as a root '/'
            options && !options.path ? options.path = '/' : false;
            !options ? options = {path: '/'} : false;

            if(!options.expires){
              let date = new Date();
              const days = 365;
              date.setTime(+ date + (days * 86400000)); //24 * 60 * 60 * 1000
              options.expires = date;
            }

            const cookieStr = cookie.serialize(name, value, options);

            res ? res.setHeader('Set-Cookie', cookieStr) : document.cookie = cookieStr;
          };

          actionContext.getCookie = (name) => {
            const cookies = req ? req.cookies : cookie.parse(document.cookie);
            return cookies[name];
          }

          actionContext.clearCookie = (name, options) => {
            //just sets cookie value to empty string
            const cookieStr = cookie.serialize(name, '', options);
            res ? res.setHeader('Set-Cookie', cookieStr) : document.cookie = cookieStr;
          };
        }
      };
    }
  };
}
