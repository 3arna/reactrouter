export default () => {
  let apiConfig;

  return {
    name: 'ApiConfigPlugin',
    plugContext: function plugContext(contextOptions){

      if(contextOptions.req){
        apiConfig = contextOptions.req.apiConfig;
      }

      return {
        plugActionContext: (actionContext) => {
          actionContext.apiConfig = apiConfig;
        },

        plugComponentContext: (componentContext) => {
          componentContext.apiConfig = apiConfig;
        },

        plugStoreContext: (storeContext) => {
          storeContext.apiConfig = apiConfig;
        },

        dehydrate: () => {
          return {
            apiConfig: apiConfig
          };
        },

        rehydrate: (state) => {
          apiConfig = state.apiConfig;
        }
      };
    },
  };
}
