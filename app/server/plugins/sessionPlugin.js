import cookie from 'cookie';
import lodash from 'lodash';

export default () => {
  let session;

  return {
    name: 'sessionPlugin',
    plugContext: function plugContext(contextOptions){

      if(contextOptions.req && contextOptions.req.session){
        session = contextOptions.req.session;
      }

      return {
        plugActionContext: (actionContext) => {
          actionContext.session = session;
        },

        plugComponentContext: (componentContext) => {
          componentContext.session = session;
        },

        plugStoreContext: (storeContext) => {
          storeContext.session = session;
        }
      };
    },

    dehydrate: function dehydrate() {
      return session
    },

    rehydrate: function rehydrate(state) {
      session = state
    }
  };
}
