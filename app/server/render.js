import React from 'react';
import async from 'async';
import ReactDomServer from 'react-dom/server';
import HtmlComponent from '../views/Html';
import FluxibleComponent from 'fluxible-addons-react/FluxibleComponent';
import ReactRouterFluxibleComponent from '../server/ReactRouterFluxibleComponent';
import { match, RouterContext } from 'react-router';

import app from '../app';
import debugLib from 'debug';
import serialize from 'serialize-javascript';

import { createElementWithContext } from 'fluxible-addons-react';
import { navigate } from '../actions/route/applicationActions';

import { getCategories } from '../actions/categoryActions';
import { getBookmarks, getDefaultCategoryBookmarks } from '../actions/bookmarkActions';
import { getAlerts } from '../actions/alertActions';
import { getMe } from '../actions/userActions';
import { getResults } from '../actions/searchActions';

import { version } from '../../package.json';

import getRoutes from '../views/Routes';

const debug = debugLib('versy');
const htmlComponent = React.createFactory(HtmlComponent);

// Render the app
function renderApp(req, res, context, next) {
  console.log(req.url);
  match({
    routes: getRoutes(context),
    location: req.url
  }, (error, redirectLocation, renderProps) => {
    if(error) {
      res.status(500).send(error.message);
    } else if (redirectLocation) {
      res.redirect(302, redirectLocation.pathname + redirectLocation.search);
    } else if (renderProps) {
      context.executeAction(navigate, {path: req.url}, () => {

        const exposed = 'window.App=' + serialize(app.dehydrate(context)) + ';';
        const env = process.env.NODE_ENV;

        const htmlComponent = React.createFactory(HtmlComponent);

        var markupElement = React.createElement(
          FluxibleComponent,
          { context: context.getComponentContext() },
          React.createElement(RouterContext, renderProps)
        );

        const html = ReactDomServer.renderToStaticMarkup(htmlComponent({
          scripts: env ? ['vendor.min.js', 'main.min.js'] : ['main.js'],
          styles: env ? ['main.min.css'] : [],
          context: context.getComponentContext(),
          state: exposed,
          version,
          markup: ReactDomServer.renderToString(markupElement)
        }));

        res.type('html');
        res.write('<!DOCTYPE html>' + html);
        res.end();
      });
    } else {
      next();
    }
  })
}

function render(req, res, next){
  let context = app.createContext({ req, res });

  const token = req.cookies.token;
  const user = req.session && req.session.user || {};

  context.getActionContext().dispatch('APP_INFO_SUCCESS', { version });

  /*if(!token){
    return context.getActionContext().executeAction(navigateAction, { url: req.url }, (err) => renderApp(res, context));
  }

  context.getActionContext().dispatch('USER_LOGIN_SUCCESS', {token, user});*/

  /*if(!token){
    return context.getActionContext().executeAction(navigateAction, { url: req.url }, (err) => renderApp(res, context));
  }*/

  context.getActionContext().dispatch('USER_LOGIN_SUCCESS', {token, user});

  async.parallel([
    (cb) => context.getActionContext().executeAction(getMe, {}, cb),
    (cb) => context.getActionContext().executeAction(getCategories, {}, cb),
    (cb) => context.getActionContext().executeAction(getAlerts, {}, cb),
    (cb) => context.getActionContext().executeAction(getBookmarks, {}, cb),
    (cb) => context.getActionContext().executeAction(getDefaultCategoryBookmarks, {}, cb),
    (cb) => context.getActionContext().executeAction(getResults, {}, cb),
  ], (err) => renderApp(req, res, context, next));

}

export default render;
