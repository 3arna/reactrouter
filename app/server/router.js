import express from 'express';
import packageJson from '../../package.json';
import { serverConfigs } from '../configs';
import formidable from 'formidable';
import ffmpeg from 'fluent-ffmpeg';
import ffprobe from 'node-ffprobe';
import path from 'path';
import fs from 'fs';
import lodash from 'lodash';
import { uploadMedia } from '../services/media/mediaService';
import gm from 'gm';
import bytes from 'bytes';

const imageMagick = gm.subClass({ imageMagick: true });

let router = express.Router();

router.get('*', (req, res, next) => {
  if(req.query.campaignCode && req.session && !req.session.campaignCode){
    req.session.campaignCode = req.query.campaignCode.toString();
  }
  next();
});

router.get('/login', (req, res, next) => {
  next();
});

router.get('/debug', (req, res, next) => {
  return res.send((`
    version: ${packageJson.version}<br>
    environment: ${process.env.NODE_ENV}<br>
    api connection: ${req.apiConfig ? 'success': 'failed'}<br>
      ${process.env.NODE_ENV != 'production' && '<h3>Server Config</h3>' + JSON.stringify(serverConfigs, null, 4)}
  `));
});

router.post('/media', (req, res, next) => {

  const form = new formidable.IncomingForm();

  form.uploadDir = path.join(__dirname, '../../build/tmp');
  form.parse(req, (err, fields, files) => {
    if(!files || !files.file){
      return next(new Error('File does not exist'));
    }
    if(!req.cookies.token){
      return callback(new Error('User must login before uploading the file'));
    }

    const file = files.file;

    const options = {
      size: file.size,
      mimeType: file.type,
      extension: file.name.split('.').pop().toLowerCase(),
      path: file.path,
      autoOrientImage: fields.autoOrientImage
    };

    convertMedia(options, (err, newOptions) => {
      if(err){
        return next(err);
      }

      req.on('close', (err) => {
        return fs.unlink(newOptions.path, ()=>{});
      });

      uploadMedia.create(req, res, fields, files, newOptions, (err, apiResponse) => {
        fs.unlink(newOptions.path, ()=>{});
        if(err){
          return next(err);
        }
        return res.json(apiResponse);
      })

    });
  });
});

const autoOrientImage = (options, cb) => {
  const newPath = options.path + 'auto-oriented';
  imageMagick(options.path)
    .autoOrient()
    .write(newPath, (error) => {
      fs.unlink(options.path, ()=>{});
      
      if(error){
        return cb(error);
      }

      fs.unlink(options.path, ()=>{});
      options.path = newPath;

      fs.stat(newPath, function(error, stat) {
        if(error){
          return cb(error);
        }

        options.size = stat.size;
        return cb(null, options);
      });
    });
};

const convertMedia = (options, cb) => {

  if(options.autoOrientImage){
    return autoOrientImage(options, cb);
  }

  if(['mov'].indexOf(options.extension) === -1){
    return cb(null, options);
  }

  let start = new Date().getTime();
  const path = options.path+'mp4';

  ffmpeg(options.path)
    .audioCodec('copy')
    .videoCodec('copy')
    .format('mp4')
    .output(path)
    .on('end', (err) => {
      if(err){
        return cb(err);
      }

      fs.unlink(options.path, ()=>{});

      ffprobe(path, (err, stats) => {
        const video = lodash.find(stats.streams, {codec_type: 'video'});

        return cb(err, {
          path,
          size: stats.format.size,
          mimeType: 'video/mp4',
          extension: 'mp4',
          attributes: {
            width: video.width,
            height: video.height,
            duration: video.duration*1000,
          },
        });
      });
    }).run()
}

export default router;
