import {userMeService} from '../../services/user/userService';

const authenticate = (req, res, next) => {
  if(!req.cookies.token){
    return next();
  }

  userMeService.read(req, {}, {}, {}, (error, apiResponse) => {
    if(error || apiResponse.error){
      return next(error || apiResponse.error);
    }

    req.authenticatedUser = apiResponse.result;
    next();
  });
};

export default authenticate;
