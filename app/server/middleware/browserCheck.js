import lodash from 'lodash';

export default function(req, res, next){
  if(req.url === '/browser-not-supported'){
    return next();
  }

  const userAgent = req.headers['user-agent'] && req.headers['user-agent'].toLowerCase()
  const ieVersion = userAgent && lodash.includes(userAgent, 'msie') && parseInt(lodash.get(userAgent.split('msie'), 1));
  if(ieVersion && ieVersion <= 10){
    return res.redirect('/browser-not-supported');
  }

  next();
};