import * as analyticsService from '../services/analytics/analyticsService';
import * as contentFeedService from '../services/content/contentFeedService';
import * as contentPublishService from '../services/content/contentPublishService';
import * as identityService from '../services/identity/identityService';
import * as mediaService from '../services/media/mediaService';
import * as authenticationService from '../services/external/authenticationService';
import * as userService from '../services/user/userService';
import * as categoryService from '../services/category/categoryService';
import * as bookmarkService from '../services/bookmark/bookmarkService';
import * as configService from '../services/config/configService';
import * as registrationService from '../services/registration/registrationService';
import * as followingStreamService from '../services/versyStream/followingStreamService';
import * as followersStreamService from '../services/versyStream/followersStreamService';
import * as interestsStreamService from '../services/versyStream/interestsStreamService';
import * as searchContentService from '../services/searchContent/searchContentService';
import * as reportService from '../services/report/reportService';
import * as searchService from '../services/search/searchService';
import * as privateConversationService from '../services/content/privateConversationService';
import * as versyStreamService from '../services/versyStream/versyStreamService';
import * as contentModifyService from '../services/content/contentModifyService';
import * as likeService from '../services/like/likeService';
import * as alertService from '../services/alert/alertService';
import * as contentShareService from '../services/content/contentShareService';
import * as reportingService from '../services/reporting/reportingService';
import * as schedulerService from '../services/scheduler/schedulerService';

const registerServices = (app, server) => {
  const fetchr = app.getPlugin('FetchrPlugin');

  //register fetchr services;
  registerService(fetchr, analyticsService);
  registerService(fetchr, contentFeedService);
  registerService(fetchr, contentPublishService);
  registerService(fetchr, identityService);
  registerService(fetchr, mediaService);
  registerService(fetchr, authenticationService);
  registerService(fetchr, userService);
  registerService(fetchr, categoryService);
  registerService(fetchr, bookmarkService);
  registerService(fetchr, configService);
  registerService(fetchr, registrationService);
  registerService(fetchr, followingStreamService);
  registerService(fetchr, followersStreamService);
  registerService(fetchr, interestsStreamService);
  registerService(fetchr, privateConversationService);
  registerService(fetchr, searchService);
  registerService(fetchr, versyStreamService);
  registerService(fetchr, searchContentService);
  registerService(fetchr, reportService);
  registerService(fetchr, contentModifyService);
  registerService(fetchr, likeService);
  registerService(fetchr, alertService);
  registerService(fetchr, contentShareService);
  registerService(fetchr, reportingService);
  registerService(fetchr, schedulerService);

  server.use(fetchr.getXhrPath(), fetchr.getMiddleware());
};

const registerService = (fetchr, service) => {
  for(let key in service){
    fetchr.registerService(service[key]);
  }
}

export default registerServices;
