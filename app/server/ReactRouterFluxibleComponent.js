var FluxibleComponent = require('fluxible-addons-react/FluxibleComponent');
import React from 'react';

FluxibleComponent.childContextTypes = {
  executeAction: React.PropTypes.func.isRequired,
  getStore: React.PropTypes.func.isRequired,
  apiConfig: React.PropTypes.object,
  authenticatedUser: React.PropTypes.object,
  config: React.PropTypes.object.isRequired,
  translate: React.PropTypes.func,
  getLanguage: React.PropTypes.func,
  getLanguages: React.PropTypes.func,
  userLoggedIn: React.PropTypes.bool,
  history: React.PropTypes.object
};

Object.assign(FluxibleComponent.prototype, {
  /**
   * Provides the current context as a child context
   * @method getChildContext
   */
  getChildContext: function () {
    return this.props.context;
  },
});

export default FluxibleComponent;
