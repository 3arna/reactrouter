import lodash from 'lodash';
import BaseStore from 'fluxible/addons/BaseStore';

export default class PrivacySelectorStore extends BaseStore {
  static storeName = 'PrivacySelectorStore';
  static handlers = {
    'PRIVACY_SELECTOR_RECIPIENTS_READ_SUCCESS': 'handleRecipientsReadSuccess',
    'PRIVACY_SELECTOR_TOGGLE_PRIVACY': 'handleTogglePrivacy',
    'PRIVACY_SELECTOR_FOLLOWING_STREAM_READ_SUCCESS': 'handleFollowingStreamReadSuccess',
    'PRIVACY_SELECTOR_TOGGLE_OPEN': 'handleToggleOpen',
    'PRIVACY_SELECTOR_TOGGLE_FOLLOWING': 'handleToggleFollowing'
  };

  constructor(dispatcher){
    super(dispatcher);
    this.isOpen = false;
    this.isPrivate = false;
    this.recipients = null;
    this.selectedRecipients = null;
    this.showFollowing = false;
  }

  handleRecipientsReadSuccess({ recipients, selectedOnly }){
    if(!selectedOnly){
      this.recipients = lodash.clone(recipients);
    }

    this.selectedRecipients = lodash.clone(recipients);

    this.emitChange();
  }

  handleFollowingStreamReadSuccess({ streamPage, isNextState }){
    if(isNextState){
      this.followingStreamPage.entries = this.followingStreamPage.entries.concat(streamPage.entries);
      this.followingStreamPage.nextState = streamPage.nextState;
    }
    else{
      this.followingStreamPage = streamPage;
    }

    this.emitChange();
  }

  handleToggleFollowing(){
    this.showFollowing = !this.showFollowing;
    this.emitChange();
  }

  handleToggleOpen({ contentId, currentUrl, recipients, isPrivate, isReadOnly, reset }){
    if(this.isOpen){
      this.contentId = null;
      this.followingStreamPage = null;
      this.isPrivate = false;
      this.recipients = null;
      this.selectedRecipients = null;
      this.showFollowing = null;
      this.isOpen = false;
      this.openedAtUrl = null;
    }
    else{
      this.contentId = contentId;
      this.isPrivate = isPrivate || !!contentId;
      this.recipients = lodash.clone(recipients);
      this.selectedRecipients = recipients ? lodash.clone(recipients) : null;
      this.showFollowing = !this.isPrivate || (this.isPrivate && !this.recipients && !contentId) ? true: false;
      this.isReadOnly = isReadOnly;
      this.isOpen = true;
      this.openedAtUrl = currentUrl;
    }

    this.emitChange();
  }

  handleTogglePrivacy(){
    this.isPrivate = !this.isPrivate;
    this.emitChange();
  }

  dehydrate () {
    return {
      contentId: this.contentId,
      followingStreamPage: this.followingStreamPage,
      isOpen: this.isOpen,
      isPrivate: this.isPrivate,
      openedAtUrl: this.openedAtUrl,
      recipients: this.recipients,
      selectedRecipients: this.selectedRecipients,
      showFollowing: this.showFollowing
    }
  }

  rehydrate (state){
    this.contentId = state.contentId;
    this.followingStreamPage = state.followingStreamPage;
    this.isOpen = state.isOpen;
    this.isPrivate = state.isPrivate;
    this.openedAtUrl = state.openedAtUrl;
    this.recipients = state.recipients;
    this.selectedRecipients = state.selectedRecipients;
    this.showFollowing = state.showFollowing;
  }
}
