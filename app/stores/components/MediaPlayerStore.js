import BaseStore from 'fluxible/addons/BaseStore';
import lodash from 'lodash';

export default class MediaPlayerStore extends BaseStore {
  static storeName = 'MediaPlayerStore';
  static handlers = {
    'MEDIA_PLAYER_SET_STARTED_MEDIA': 'handleMediaSetStarted',
  };

  constructor(dispatcher){
    super(dispatcher);
    this.startedMediaId = null;
  }

  handleMediaSetStarted(mediaId){
    this.startedMediaId = mediaId;
    this.emitChange();
  }

  dehydrate() {
    return {
      startedMediaId: this.startedMediaId
    }
  }

  rehydrate(state) {
    this.startedMediaId = state.startedMediaId;
  }
}
