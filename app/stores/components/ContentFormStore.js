import BaseStore from 'fluxible/addons/BaseStore';
import lodash from 'lodash';

export default class ContentFormStore extends BaseStore {
  static storeName = 'ContentFormStore';
  static handlers = {
    'CONTENT_FORM_ERROR_SET': 'formErrorSet',

    'CONTENT_FORM_TOGGLE': 'formToggle',
    'CONTENT_FORM_UPDATE_INPUT': 'formUpdateInput',

    'CONTENT_FORM_UPDATE_MEDIA': 'formUpdateMedia',
    'CONTENT_FORM_UPDATE_PRIVACY': 'formUpdatePrivacy',
    'CONTENT_FORM_UPDATE_CATEGORIES': 'formUpdateCategories',
    'CONTENT_FORM_UPDATE_PUBLISH_DATE': 'formUpdatePublishDate',

    'CONTENT_FORM_POSTING_START': 'formPostingStart',
    'CONTENT_FORM_POSTING_SUCCESS': 'formPostingSuccess',
  };

  constructor(dispatcher){
    super(dispatcher);
    this.errorMessage = null;
    this.formVisible = false;
    this.clearForm();
  }

  clearForm(){
    this.errorMessage = null;
    this.formData = {};
    this.formMedia = null;
    this.isContent = false;
    this.isPrivate = false;
    this.recipients = [];
    this.categories = [];
    this.posting = false;
    this.publishDate = null;
  }

  formToggle({ isContent, currentUrl, defaultCategories }){  

    if(this.formVisible){
      this.clearForm();
      this.formVisible = false;
    }
    else{
      this.formVisible = true;
      this.isContent = isContent;
      this.openedAtUrl = currentUrl;
      if(defaultCategories){
        this.categories = defaultCategories;
      }
    }

    if(isContent){
      let defaultUserCategories = this.dispatcher.getStore('UserStore').defaultCategories;

      defaultUserCategories = defaultUserCategories 
        && lodash.map(defaultUserCategories, (category) => category && category.id && category.id.split(':').pop());

      if(defaultUserCategories && !this.categories.length){
        this.categories = defaultUserCategories;
      }
    }

    this.emitChange();
  }

  formErrorSet(errorMessage){
    this.errorMessage = errorMessage;
    this.posting = false;
    setTimeout(() => {
      this.errorMessage = null;
      this.emitChange();
    }, 2500);
    this.emitChange();
  }

  formUpdateInput(field){
    this.formData[field.name] = field.value;
  }

  formUpdateMedia(mediaData){
    if(mediaData && lodash.isObject(mediaData.attributes)){
      mediaData.attributes.category = 'uploaded';
    }
    this.formMedia = mediaData;
    this.emitChange();
  }

  formUpdateCategories(categories){
    this.categories = categories;
    this.emitChange();
  }

  formUpdatePrivacy(privacyData){
    this.isPrivate = privacyData.isPrivate;
    this.recipients = privacyData.recipients || [];
    this.emitChange();
  }

  formUpdatePublishDate(publishDate){
    if(this.publishDate === publishDate){
      return;
    }
    
    this.isPrivate = false;
    this.publishDate = publishDate;
    this.emitChange();
  }

  returnFormData(){
    const formData = this.formData;
    lodash.isObject(this.formMedia) && Object.keys(this.formMedia).length
      ? formData.media = this.formMedia
      : false;

    if(this.recipients.length && this.isPrivate && !this.publishDate){
      formData.recipients = this.recipients.map((user) => user.id);
    }

    this.isPrivate = this.isPrivate && !!this.recipients.length && !this.publishDate;

    this.categories.length && !this.isPrivate ? formData.categoryIds = this.categories : false;
    this.publishDate ? formData.publishDate = this.publishDate : false;
    return formData;
  }

  formPostingStart(){
    this.posting = true;
    this.emitChange();
  }

  formPostingSuccess(){
    this.formVisible = null;
    this.clearForm();
    this.emitChange();
  }

  dehydrate() {
    return {
      formVisible: this.formVisible,
      formData: this.formData,
      formMedia: this.formMedia,
      isPrivate: this.isPrivate,
      categories: this.categories,
      recipients: this.recipients,
      posting: this.posting,
      publishDate: this.publishDate,
      errorMessage: this.errorMessage,
      isContent: this.isContent,
    }
  }

  rehydrate(state) {
    this.formVisible = state.formVisible;
    this.formData = state.formData;
    this.formMedia = state.formMedia;
    this.isPrivate = state.isPrivate;
    this.categories = state.categories;
    this.recipients = state.recipients;
    this.posting = state.posting;
    this.publishDate = state.publishDate;
    this.errorMessage = state.errorMessage;
    this.isContent = state.isContent;
  }
}
