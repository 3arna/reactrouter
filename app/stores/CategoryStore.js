import BaseStore from 'fluxible/addons/BaseStore';

export default class CategoryStore extends BaseStore {
  static storeName = 'CategoryStore';
  static handlers = {
    'CATEGORY_GET_ALL_SUCCESS': 'storeCategories',
    'CATEGORY_GET_SEARCHED_SUCCESS': 'storeSearchedCategories',
  };

  constructor(dispatcher){
    super(dispatcher);
    this.categories = [];
    this.searchedCategories = false;
  }

  storeCategories(categories){
    this.categories = categories;
    this.emitChange();
  }

  storeSearchedCategories(searchedCategories){
    if(searchedCategories === this.searchedCategories){
      return true;
    }
    this.searchedCategories = searchedCategories;
    this.emitChange();
  }

  dehydrate () {
    return {
      categories: this.categories,
      searchedCategories: this.searchedCategories,
    }
  }

  rehydrate (state){
    this.categories = state.categories;
    this.searchedCategories = state.searchedCategories;
  }
}
