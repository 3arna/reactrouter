import BaseStore from 'fluxible/addons/BaseStore';
import lodash from 'lodash';

class UserStore extends BaseStore {
  static storeName = 'UserStore';
  static handlers = {
    'USER_LOGIN_SUCCESS': 'handleLoginSuccess',
    'USER_LOGOUT_SUCCESS': 'handleLogoutSuccess',
    'USER_BOOKMARKS_GET_SUCCESS': 'handleBookmarks',
    'USER_DETAILS_UPDATE': 'handleUpdateDetails',
    'USER_DEFAULT_CATEGORIES_GET_SUCCESS': 'handleDefaultCategories',
  };

  constructor(dispatcher) {
    super(dispatcher);
    this.context = dispatcher.getContext();
    this.user = null;
    this.bookmarks = [];
    this.interests = [];
    this.following = [];
  }

  handleLoginSuccess(userData){
    this.user = userData.user;
    this.emitChange();
  }

  handleUpdateDetails(userDetails){
    this.user = lodash.merge(this.user, userDetails);
    this.emitChange();
  }

  handleLogoutSuccess(){
    this.user = {};
    this.emitChange();
  }

  handleBookmarks(bookmarks){
    this.bookmarks = bookmarks;
    this.interests = lodash.filter(bookmarks, (bookmark) => lodash.contains(bookmark.id, 'category'));
    this.following = lodash.filter(bookmarks, (bookmark) => lodash.contains(bookmark.id, 'user'));

    this.emitChange();
  }

  handleDefaultCategories(defaultCategories){
    this.defaultCategories = defaultCategories;
    this.emitChange();
  }


  getUser(){
    return this.user;
  }

  getAvatar(){
    return this.user && this.user.media && lodash.get(lodash.find(this.user.media, {
      attributes: {category: 'icon'}
    }), 'mediaUrl');
  }

  isLoggedIn(){
    return !!this.user;
  }

  dehydrate () {
    return {
      user: this.user,
      bookmarks: this.bookmarks,
      interests: this.interests,
      following: this.following,
      defaultCategories: this.defaultCategories,
    }
  }

  rehydrate (state) {
    this.user = state.user;
    this.bookmarks = state.bookmarks;
    this.interests = state.interests;
    this.following = state.following;
    this.defaultCategories = state.defaultCategories;
  }
}

export default UserStore;
