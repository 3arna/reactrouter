import BaseStore from 'fluxible/addons/BaseStore';

export default class BookmarkStreamStore extends BaseStore {
  static storeName = 'BookmarkStreamStore';
  static handlers = {
    'BOOKMARK_STREAM_READ_SUCCESS': 'handleBookmarkStreamReadSuccess',
    'BOOKMARK_STREAM_TOGGLE_LOADING': 'handleToggleLoading'
  };

  constructor(dispatcher){
    super(dispatcher);
    this.isLoading = false;
    this.isLoadingMore = false;
  }

  handleBookmarkStreamReadSuccess({ streamPage, streamType , isNextState }){
    if(isNextState){
      this.streamPage.entries = this.streamPage.entries.concat(streamPage.entries);
      this.streamPage.nextState = streamPage.nextState;
    }
    else{
      this.streamPage = streamPage;
      this.streamType = streamType;
    }

    this.handleToggleLoading({ isLoadingMore: isNextState });
  }

  handleToggleLoading({ isLoadingMore }){
    if(isLoadingMore){
      this.isLoadingMore = !this.isLoadingMore;
    }
    else{
      this.isLoading = !this.isLoading;
    }

    this.emitChange();
  }

  dehydrate () {
    return {
      streamPage: this.streamPage,
      streamType: this.streamType,
      isLoadingMore: this.isLoadingMore,
      isLoading: this.isLoading
    }
  }

  rehydrate (state){
    this.streamPage = state.streamPage;
    this.streamType = state.streamType;
    this.isLoadingMore = state.isLoadingMore;
    this.isLoading = state.isLoading;
  }
}
