import BaseStore from 'fluxible/addons/BaseStore';
import lodash from 'lodash';

export default class SchedulerStore extends BaseStore {
  static storeName = 'SchedulerStore';
  static handlers = {
    'SCHEDULER_GET_TASKS_SUCCESS': 'handleGetTasks',
  };

  constructor(dispatcher){
    super(dispatcher);
    this.tasks = [];
    this.nextState = null;
  }

  handleGetTasks({tasks, nextState}){
    this.tasks = lodash.sortBy(tasks, 'fireDate');;
    this.emitChange();
  }

  dehydrate () {
    return {
      tasks: this.tasks
    }
  }

  rehydrate (state){
    this.tasks = state.tasks;
  }
}
