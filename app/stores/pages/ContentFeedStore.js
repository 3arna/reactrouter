import lodash from 'lodash';

import BaseStore from 'fluxible/addons/BaseStore';

class ContentFeedStore extends BaseStore {

  static storeName = 'ContentFeedStore';
  static handlers = {
    'CONTENT_PAGE_READ_SUCCESS': 'handleContentPageReadSuccess',
    'CONTENT_PAGE_READ_ERROR': 'handleContentPageReadError',
    'STREAM_READ_SUCCESS': 'handleStreamReadSuccess',
    'CONTENT_FEED_CREATE_SUCCESS': 'handleFeedCreateSuccess',
    'CONTENT_PAGE_NEW_CONTENT_AVAILABLE': 'handleNewContentAvailable',
    'CONTENT_PAGE_SET_FEED_PARENT': 'setFeedParent',
    'CONTENT_PAGE_SET_FEED_SCROLL_POSITION': 'setFeedScrollPosition',
    'CONTENT_PAGE_SET_SUBCONTENT_PARENT': 'setSubContentParent',
    'CONTENT_PAGE_TOGGLE_LOADING': 'toggleLoading',
    'CONTENT_PAGE_FEED_CLEAR': 'clearFeed',
    'CONTENT_PAGE_SUBCONTENT_CLEAR': 'clearSubContent',
    'CONTENT_PAGE_SELECTED_CLEAR': 'clearSelected',
    'CONTENT_PAGE_REPLY': 'setReplyTo',
    'CONTENT_LIKENDISLIKE': 'handleLikeNDislike',
  };

  constructor(dispatcher){
    super(dispatcher);

    this.contentPages = [];
    this.contentPageScrollPositions = {};

    this.feedLoading = false;
    this.feedLoadingMore = false;
    this.feedParentContentId = null;
    this.filter = null;
    this.previousFeedParentContentId = null;
    this.newFeedContentAvailable = false;
    this.newSubContentAvailable = false;

    this.subContentLoading = false;
    this.subContentLoadingMore = false;
    this.subContentError = null;
    this.subContentParentContentId = null;
    this.allowSubContentPosting = false;
    this.subContentReplyTo = null;

    this.selectedContent = null;
  }

  handleContentPageReadSuccess(result){

    const { newContentPage, isSubContent, isLoadMore, isSelected, refreshOnly } = result;
    let isLoadingMore = false;

    if(newContentPage){
      if(isSubContent){
        this.subContentParentContentId = newContentPage.parentContentId;
        this.newSubContentAvailable = false;
      }
      else{
        if(!refreshOnly){
          this.feedParentContentId = newContentPage.parentContentId;
        }

        if(!newContentPage.parent){
          this.previousFeedParentContentId = newContentPage.parentContentId;
        }

        this.newFeedContentAvailable = false;
      }

      let existing = false;


      for(var i = 0; i < this.contentPages.length; i++){
        if(this.contentPages[i].parentContentId === newContentPage.parentContentId){

          if(isLoadMore){
            this.contentPages[i] = lodash.clone(this.contentPages[i]);
            this.contentPages[i].content =
              lodash.chain(this.contentPages[i].content)
              .concat(newContentPage.content)
              .uniq('sequence')
              .sortBy((content) => { return content.sequence })
              .reverse()
              .value();

            this.contentPages[i].pageMin = newContentPage.pageMin;
            this.contentPages[i].pageMax = newContentPage.pageMax;
          }
          else{
            this.contentPages[i] = newContentPage;
          }
          existing = true;
        }
      }

      if(!existing){
        this.contentPages.push(newContentPage);
      }

      if(isSelected){
        if(newContentPage.parent){
          this.selectedContentId = newContentPage.parent.id;
          this.selectedContent = newContentPage.parent;
        }
      }
    }

    this.toggleLoading({ isLoading: false, isSubContent, isLoadMore: isLoadingMore });
  }

  handleContentPageReadError(error){
    this.subContentError = error;
    this.subContentLoading = false;
    this.emitChange();
  }

  handleNewContentAvailable(isSubContent){
    if(isSubContent){
      this.newSubContentAvailable = true;
    }
    else{
      this.newFeedContentAvailable = true;
    }

    this.emitChange();
  }

  handleLikeNDislike(data){
    const { selectedContent } = this;
    if(selectedContent.id !== data.parentId){
      return;
    }

    if(selectedContent.hasLiked && !data.isLike && selectedContent.likeCount !== 0){
      selectedContent.dislikeCount += 1;
      selectedContent.likeCount -= 1;
    } else
    if(selectedContent.hasDisliked && data.isLike && selectedContent.dislikeCount !== 0){
      selectedContent.dislikeCount -= 1;
      selectedContent.likeCount += 1;
    } else
    if(!selectedContent.hasLiked && !selectedContent.hasDisliked){
      data.isLike
        ? selectedContent.likeCount += 1
        : selectedContent.dislikeCount += 1;
    }

    selectedContent.hasLiked = data.isLike;
    selectedContent.hasDisliked = !data.isLike;

    this.emitChange();
  }

  //used for versy stream, interest stream and tag stream
  //can be used for any stream that uses next state pagination
  handleStreamReadSuccess({ feedIdentifier, newStreamPage, isNextState}){
    console.log('got content in store', newStreamPage);
    this.feedParentContentId = feedIdentifier;
    this.previousFeedParentContentId = feedIdentifier;

    let stream = this.getFeedContentPage();

    if(stream){
      if(isNextState){
        stream.content = stream.content.concat(newStreamPage.content);
        stream.nextState = newStreamPage.nextState;
      }
      else{
        this.contentPages.remov
        stream.content = newStreamPage.content;
        stream.nextState = newStreamPage.nextState;
        stream.pollingURI = newStreamPage.pollingURI;

        this.contentPageScrollPositions[newStreamPage.parentContentId] = { scrollPosition: 0, contentInViewport: newStreamPage.content[0] };
      }
    }
    else{
      this.contentPages.push(newStreamPage);
    }

    this.newFeedContentAvailable = false;

    this.toggleLoading({ isLoading: false, isSubContent: false, isLoadMore: isNextState });
  }


  setFeedParent(parentContentId){
    this.feedParentContentId = parentContentId;
    this.feedLoading = false;

    const contentPage = this.getFeedContentPage();
    if(contentPage && !contentPage.parent){
      this.previousFeedParentContentId = contentPage && !contentPage.parent && contentPage.parentContentId;
    }

    this.emitChange();
  }

  setSubContentParent(payload){
    const { subContentPage, isSelected } = payload;

    if(subContentPage && subContentPage.parent){
      this.subContentParentContentId = subContentPage.parent.id;

      if(isSelected){
        this.selectedContent = subContentPage.parent;
        this.selectedContentId = subContentPage.parent.id;
      }
    }

    this.subContentLoading = false;

    //console.log('sub parent set');

    this.emitChange();
  }

  toggleLoading(payload){
    const { isLoading, isSubContent, isLoadMore } = payload;

    if(isSubContent){
      if(isLoadMore){
        this.subContentLoadingMore = isLoading;
      }
      else{
        this.subContentLoading = isLoading;
        this.subContentLoadingMore = false;
      }

      this.subContentError = null;
    }
    else{
      if(isLoadMore){
        this.feedLoadingMore = isLoading;
      }
      else{
        this.feedLoading = isLoading;
        this.feedLoadingMore = false;
      }
    }
    this.emitChange();
  }

  clearFeed(){
    this.feedParentContentId = null;
    this.emitChange();
  }

  clearSubContent(){
    this.subContentParentContentId = null;
    this.subContentError = null;
    this.emitChange();
  }

  clearSelected(){
    this.selectedContent = false;
    this.emitChange();
  }

  findContentPage(parentContentId){
    return lodash.find(this.contentPages, { parentContentId: parentContentId });
  }

  findContent(contentId){
    let foundContent = null;
    this.contentPages.forEach(contentPage => {
      const content = lodash.find(contentPage.content, { id: contentId });
      if(content){
        foundContent = content;
      }
    });

    return foundContent;
  }

  getFeedContentPage(){
    return this.findContentPage(this.feedParentContentId);
  }

  getPreviousFeedContentPage(){
    return this.findContentPage(this.previousFeedParentContentId);
  }

  getSubContentPage(){
    return this.findContentPage(this.subContentParentContentId);
  }

  setReplyTo(contentId){
    this.subContentReplyTo = contentId;
    this.emitChange();
  }

  setFeedScrollPosition(payload){
    const { scrollPosition, contentInViewport } = payload;

    let feedContentPage = this.getFeedContentPage();

    if(feedContentPage){
      //console.log('store scroll pos', payload);
      this.contentPageScrollPositions[feedContentPage.parentContentId] = { scrollPosition, contentInViewport };
    }
  }

  getFeedScrollPosition(){
    let feedContentPage = this.getFeedContentPage();

    return feedContentPage && this.contentPageScrollPositions[feedContentPage.parentContentId];
  }


  // For sending state to the client
  dehydrate () {
    return {
      contentPages: this.contentPages,
      contentPageScrollPositions: this.contentPageScrollPositions,
      feedParentContentId : this.feedParentContentId,
      previousFeedParentContentId: this.previousFeedParentContentId,
      newFeedContentAvailable: this.newFeedContentAvailable,
      newSubContentAvailable: this.newSubContentAvailable,
      subContentParentContentId: this.subContentParentContentId,
      selectedContent : this.selectedContent,
      feedLoading: this.feedLoading,
      feedLoadingMore: this.feedLoadingMore,
      subContentLoading: this.subContentLoading,
      subContentLoadingMore: this.subContentLoadingMore,
      subContentReplyTo: this.subContentReplyTo,
      subContentError: this.subContentError
    }
  }

  // For rehydrating server state
  rehydrate (state) {
    this.contentPages = state.contentPages;
    this.contentPageScrollPositions = state.contentPageScrollPositions;
    this.feedParentContentId = state.feedParentContentId;
    this.newFeedContentAvailable = state.newFeedContentAvailable;
    this.newSubContentAvailable = state.newSubContentAvailable;
    this.previousFeedParentContentId = state.previousFeedParentContentId;
    this.subContentParentContentId = state.subContentParentContentId;
    this.selectedContent = state.selectedContent;
    this.feedLoading = state.feedLoading;
    this.feedLoadingMore = state.feedLoadingMore;
    this.subContentLoading = state.subContentLoading;
    this.subContentLoadingMore = state.subContentLoadingMore;
    this.subContentReplyTo = state.subContentReplyTo;
    this.subContentError = state.subContentError;
  }
}

export default ContentFeedStore;

