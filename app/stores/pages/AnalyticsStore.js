import BaseStore from 'fluxible/addons/BaseStore';

export default class AnalyticsStore extends BaseStore {
  static storeName = 'AnalyticsStore';
  static handlers = {
    'USER_ANALYTICS_READ_SUCCESS': 'handleUserAnalyticsReadSuccess',
  };

  constructor(dispatcher){
    super(dispatcher);
  }

  handleUserAnalyticsReadSuccess({ userAnalytics }){
    this.userAnalytics = userAnalytics;
    this.emitChange();
  }

  dehydrate () {
    return {
      userAnalytics: this.userAnalytics
    }
  }

  rehydrate (state){
    this.userAnalytics = state.userAnalytics;
  }
}
