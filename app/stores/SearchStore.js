import BaseStore from 'fluxible/addons/BaseStore';
import lodash from 'lodash';

export default class SearchStore extends BaseStore {
  static storeName = 'SearchStore';
  static handlers = {
    'SEARCH_GET_RESULTS_SUCCESS': 'handleStoreSearchResults',
    'SEARCH_CLEAR_RESULTS': 'handleClearSearchResults',
  };

  constructor(dispatcher){
    super(dispatcher);

    this.searchQuery = null;
    this.typeOfSearch = null;
    this.searchResults = null;
    this.trendingResults = null;
    this.nextState = null;
  }

  handleStoreSearchResults({searchResults, typeOfSearch, searchQuery, nextState}){

    if(!typeOfSearch){
      this.trendingResults = searchResults;
    }
    else{
      this.searchResults = this.searchResults && nextState !== this.nextState 
        && this.typeOfSearch === typeOfSearch && this.searchQuery === searchQuery
          ? this.searchResults.concat(searchResults) 
          : searchResults;
    }
    
    this.searchQuery = searchQuery || '';
    this.nextState = nextState;
    this.typeOfSearch = typeOfSearch;
    this.emitChange();
  }

  handleClearSearchResults(){
    this.searchQuery = null;
    this.typeOfSearch = null;
    this.searchResults = null;
    this.nextState = null;
    this.emitChange();
  }

  dehydrate () {
    return {
      typeOfSearch: this.typeOfSearch,
      searchResults: this.searchResults,
      searchQuery: this.searchQuery,
      trendingResults: this.trendingResults,
      nextState: this.nextState,
    }
  }

  rehydrate (state){
    this.typeOfSearch = state.typeOfSearch;
    this.searchResults = state.searchResults;
    this.searchQuery = state.searchQuery;
    this.trendingResults = state.trendingResults;
    this.nextState = state.nextState;
  }
}
