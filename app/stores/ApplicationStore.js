import BaseStore from 'fluxible/addons/BaseStore';
import lodash from 'lodash';
import RouteStore from './RouteStore';

class ApplicationStore extends BaseStore {
  static storeName = 'ApplicationStore';
  static handlers = {
    'CHANGE_ROUTE': 'handleNavigate',
    'NAVIGATE_SUCCESS': 'handleNavigateSuccess',
    'LANGUAGE_SET': 'handleLanguage',
    'SET_METADATA': 'handleSetMetaData',
    'CONFIG_GET_SUCCESS': 'storeConfig',
    'ALERTS_GET_SUCCESS': 'storeAlerts',
    'APP_INFO_SUCCESS': 'storeAppInfo',

    'API_PROCESS_START': 'processStart',
    'API_PROCESS_ERROR': 'processError',
    'API_PROCESS_SUCCESS': 'processSuccess',
    'API_PROCESS_CLEAR': 'processClear',
    //'NAVIGATE_START': 'handlePageLoading',
  };

  constructor(dispatcher) {
    super(dispatcher);
    this.alerts = [];
    this.currentPageName = null;
    this.currentPage = null;
    this.previousPage = null;
    this.pageTitle = '';
    this.currentRoute = null;
    this.previousRoute = null;
    this.config = false;
    this.facebookMetadata = {};

    this.process = {
      default: {
        name: 'default',
        inProgress: false,
        error: false,
        success: false,
        timer: false,
      }
    };
  }

  handleNavigate(route) {

    //console.log(route);
    if(this.currentRoute && route.path === this.currentRoute.path) {
        return;
    }

    this.currentRoute = route;
    this.emitChange();
  }

  handleNavigateSuccess(route){
    this.dispatcher.waitFor(RouteStore, () => {

      if(route && route.get('page')) {
        const pageTitle = route.get('title');

        if(pageTitle) {
          this.pageTitle = this.dispatcher.getContext().translate(pageTitle);
          this.facebookMetadata = {};
        }

        //console.log(this.currentRoute, this.previousRoute, route.get('page'));
        if(this.currentPage){
          this.previousPage = this.currentPage;
        }

        this.currentPage = route.get('url');

        if(this.currentRoute){
          this.previousRoute = this.currentRoute;
        }

        this.currentRoute = route;

        this.emitChange();
      }
    });
  }

  handleSetMetaData({ pageTitle, facebookMetadata }){
    //console.log('set metadata', facebookMetadata);
    this.pageTitle = pageTitle;
    this.facebookMetadata = facebookMetadata;

    this.emitChange();
  }

  handleLanguage(){
    this.emitChange();
  }

  storeConfig(config){
    this.config = config;
    this.emitChange();
  }
  storeAlerts(alerts){
    if(alerts && this.alerts &&  alerts.length !== this.alerts.length){
      this.alerts = alerts;
      this.emitChange();
    }
  }

  storeAppInfo(appInfo){
    this.dispatcher.waitFor(RouteStore, () => {
      this.appInfo = appInfo;
      this.emitChange();
    });
  }

  getCurrentPageName() {
    return this.currentPageName;
  }

  getPageTitle() {
    return this.pageTitle;
  }

  getCurrentUrl(){
    const route = this.dispatcher.getStore("RouteStore").getCurrentRoute();
    if (!route) {
      return '/'
    }
    return route.get("url");
  }

  getPreviousUrl(){
    return this.previousRoute ? this.previousRoute.get('url') : '';
  }
  processGet(processName, maxTime){
    maxTime = maxTime === 0 ? 0 : maxTime || 6000;
    processName = processName || 'default';
    return lodash.get(this.process, processName) || {name: processName, maxTime};
  }
  processStart(proc){
    proc.inProgress = true;

    proc.maxTime
      ? proc.timer = setTimeout(() => {
          this.process[proc.name] = {
            name: proc.name,
            inProgress: false,
            error: this.dispatcher.getContext().translate('phrases.timedOut'),
            success: false,
            timer: false,
          };
          this.emitChange();
        }, proc.maxTime)
      : false;

    this.process[proc.name] = proc;
    this.emitChange();
  }
  processError(object){
    const proc = object.process || object;

    const error = object.error && object.error.errorCode
      ? this.dispatcher.getContext().translate(`apiErrorCodes.${object.error.errorCode}`, {}, 'phrases.somethingWentWrong')
      : lodash.isString(object.error)
        ? object.error
        : this.dispatcher.getContext().translate('phrases.somethingWentWrong');

    proc.error = error;
    clearTimeout(proc.timer);
    delete proc.inProgress;
    delete proc.success;
    if(object.maxTime || proc.maxTime){
      proc.timer = setTimeout(() => this.processClear(proc), object.maxTime || proc.maxTime)
    }

    this.process[proc.name] = proc;
    this.emitChange();
  }
  processSuccess(object){
    const proc = object.process || object;
    clearTimeout(proc.timer);
    proc.success = object.success || true;
    delete proc.inProgress;
    delete proc.timer;
    delete proc.error;
    this.process[proc.name] = proc;

    if(proc.maxTime){
      proc.timer = setTimeout(() => this.processClear(proc), proc.maxTime)
    }

    this.emitChange();
  }
  processClear(proc){
    proc = lodash.isString(proc) && this.process[proc] || proc;
    clearTimeout(proc.timer);
    delete this.process[proc && proc.name || 'default'];
    this.emitChange();
  }
  dehydrate() {
    return {
      alerts: this.alerts,
      currentPageName: this.currentPageName,
      currentPage: this.currentPage,
      previousPage: this.previousPage,
      facebookMetadata: this.facebookMetadata,

      pageTitle: this.pageTitle,
      config: this.config,
      process: this.process,
      appInfo: this.appInfo
    };
  }
  rehydrate(state) {
    this.alerts = state.alerts;
    this.currentPageName = state.currentPageName;
    this.currentPage = state.currentPage;
    this.previousPage = state.previousPage;
    this.facebookMetadata = state.facebookMetadata;
    this.pageTitle = state.pageTitle;
    this.config = state.config;
    this.process = this.process;
    this.appInfo = state.appInfo;
  }
}
export default ApplicationStore;
