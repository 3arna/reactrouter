const ContentType = {
  FeedBookmark: 'com.myriadgroup.versy.model.bookmark.FeedBookmark',
  UserFeedContent: 'com.myriadgroup.versy.model.content.user.UserFeedContent',
  UserInvitedToPrivateConversation: 'com.myriadgroup.versy.notification.types.UserInvitedToPrivateConversationNotification',
  UserStartedPrivateConversation: 'com.myriadgroup.versy.notification.types.UserStartedPrivateConversationNotification',
  PrivateContentActivityNotification: 'com.myriadgroup.versy.service.activity.notification.type.PrivateContentActivityNotification',
  PublicContentActivityNotification: 'com.myriadgroup.versy.service.activity.notification.type.PublicContentActivityNotification',
  UserFollowedContentNotification: 'com.myriadgroup.versy.notification.types.UserFollowedContentNotification'
};

export default ContentType;
