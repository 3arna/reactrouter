const EventReportingType = {
  ExternalContentShareEvent: 'com.myriadgroup.versy.service.content.reports.ExternalContentShareEvent',
};

export default EventReportingType;
