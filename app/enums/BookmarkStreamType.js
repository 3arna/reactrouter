const BookmarkStreamType = {
  FOLLOWERS: 1,
  FOLLOWING: 2,
  INTERESTS: 3
};

export default BookmarkStreamType;
