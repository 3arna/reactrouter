/*global document, window */

import 'babel-polyfill';
import ReactDOM from 'react-dom';
import React from 'react';
import {Router, browserHistory} from 'react-router';
import debug from 'debug';
import { createElementWithContext } from 'fluxible-addons-react';
import FluxibleComponent from 'fluxible-addons-react/FluxibleComponent';
import ReactRouterFluxibleComponent from './server/ReactRouterFluxibleComponent';
import createBrowserHistory from 'history/lib/createBrowserHistory';

import app from './app';
import history from './history';
import './styles/style.scss';

import getRoutes from './views/Routes';

const debugClient = debug('versy');
const dehydratedState = window.App; // Sent from the server

window.React = ReactDOM; // For chrome dev tool support

window.fluxibleDebug = debug;

// pass in the dehydrated server state from server.js
app.rehydrate(dehydratedState, (err, context) => {
  if (err) {
    throw err;
  }
  window.context = context;
  const mountNode = document.getElementById('app');

  app.getPlugin('ReactRouterPlugin').setHistory(browserHistory);

  ReactDOM.render(
    React.createElement(
      FluxibleComponent,
      { context: context.getComponentContext() },
      React.createElement(Router, {
          routes: getRoutes(context),
          history: browserHistory
      })
    ), mountNode
  );

});
