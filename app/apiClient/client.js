import debugLib from 'debug';
import request from 'request';
import lodash from 'lodash';
import querystring from 'querystring';

import { serverConfigs } from '../configs';
import ApiErrorInfo from '../models/ApiErrorInfo';

export function sendRequest(apiService, params, responseModel, callback){
  const requestHeaders = {};

  Object.assign(requestHeaders, params.requestHeaders);


  const requestParams = {
    headers: requestHeaders,
    json: true,
    method: apiService.method || 'GET',
    uri: serverConfigs.apiBaseUrl + encodeApiUrl(apiService, params),
  };
  
  params.requestForm ? requestParams.form = params.requestForm : false;
  params.requestBody ? requestParams.body = params.requestBody : false;

  request(requestParams, (error, response, body) => {
    if(error){
      return callback(error);
    }

    //console.log(requestHeaders);

    if(response.statusCode !== 200 &&
      response.statusCode !== 201 &&
      response.statusCode !== 204 &&
      response.statusCode != 205){
        error = new ApiErrorInfo(body, response.statusCode);
        console.log(`[ API ERROR ] | ${response.statusCode} | ${serverConfigs.apiBaseUrl}${apiService.url}`, error /*requestParams, params.requestBody*/);
        return callback(null, {error});
    }

    var result = inflateResponseModel(body, responseModel);
    console.log(`[ API SUCCESS ] | ${response.statusCode} | ${requestParams.method} | ${serverConfigs.apiBaseUrl}${apiService.url}`, requestParams.body || requestParams.qs || '');

    return callback(null, { result, statusCode: response.statusCode });
  });
}


function inflateResponseModel(responseBody, responseModel){
  //if we didn't pass in a response model then just return the api response body
  if(!responseModel){
    return responseBody;
  }

  //converts object to array if object has only one key and only value is array
  /*if(Object.keys(responseBody).length === 1){
    const key = Object.keys(responseBody).shift();
    Array.isArray(responseBody[key]) ? responseBody = responseBody[key] : false;
  }*/

  if(!Array.isArray(responseBody)){
    return new responseModel(responseBody);
  }

  return responseBody.map((item) => new responseModel(item) );
}

function encodeApiUrl(apiService, params){
  const urlString = apiService.url.split('?');
  const url = urlString.shift();

  let qs = urlString.shift();

  qs = querystring.parse(qs) || {};
  qs = params.qs ? lodash.merge(qs, params.qs) : qs;
  
  apiService.url = lodash.map(url.split('/'), (urlPart) => encodeURIComponent(urlPart)).join('/');
  return apiService.url += `?${querystring.stringify(qs)}`;
}

