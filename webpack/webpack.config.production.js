var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var SplitByPathPlugin = require('webpack-split-by-path');
var env = process.env.NODE_ENV || 'dev';

var webpackConfig = {
  resolve: {
    extensions: ['', '.js']
  },
  entry: [
    './app/client.js'
  ],
  output: {
    path: path.resolve('build'),
    filename: '/js/[name].min.js',
    chunkFilename: '/js/[name].min.js'
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loaders: [
          require.resolve('babel-loader')
        ]
      },
      { test: /\.json$/, loader: 'json-loader'},
      { test: /\.scss$/, loader: ExtractTextPlugin.extract('css!sass?outputStyle=expanded') },
    ]
  },
  plugins: [
    new SplitByPathPlugin([
      {
        name: 'vendor',
        path: path.resolve('node_modules')
      }
    ]),
    new ExtractTextPlugin('/css/main.min.css', {
      allChunks: true
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(env)
      }
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })
  ]
};

module.exports = webpackConfig;
