var webpack = require('webpack');
var path = require('path');

var webpackConfig = {
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  entry: [
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server',
    './app/client.js'
  ],
  output: {
    path: path.resolve('./build/js'),
    publicPath: '/js/',
    filename: 'main.js'
  },
  module: {
    loaders: [
      {
        loader: 'babel',
        test: /\.(js|jsx)$/,
        exclude: [/node_modules/, /tmp/],
        query: {
          "stage": 0,
          "plugins": ["react-transform"],
          "extra": {
            "react-transform": {
              "transforms": [{
                "transform": "react-transform-hmr",
                "imports": ["react"],
                "locals": ["module"]
              }, {
                "transform": "react-transform-catch-errors",
                "imports": ["react", "redbox-react"]
              }]
            }
          }
        }
      },
      { test: /\.json$/, loader: 'json'},
      { test: /\.scss$/, loader: 'style!css!sass'}
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    })
  ],
  devtool: 'eval,source-map'
};

module.exports = webpackConfig;
