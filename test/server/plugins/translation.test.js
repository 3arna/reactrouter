import translationPlugin from '../../../app/server/plugins/translationPlugin';

describe('server/plugins/translation', () => {
  const englishTranslations = {
    name: 'English',
    prefix: 'en',
    messages: {
      test: 'english text'
    }
  };

  const spanishTranslations = {
    name: 'Spanish',
    prefix: 'es',
    messages: {
      test: 'spanish text'
    }
  };

  const translations = [
    englishTranslations,
    spanishTranslations
  ];

  let translator;

  beforeEach(() => {
    translator = translationPlugin(translations).plugContext({ req: { cookies: [] } });
  });

  describe('componentContext', () => {
    let componentContext = {};

    beforeEach(() => {
      translator.plugComponentContext(componentContext);
    });

    describe('translate', () => {
      it('should provide the correct translation for a translation key', () => {
        componentContext.translate('messages.test').should.eql(englishTranslations.messages.test);
      });

      it('should return the translation key if there is no matching translation', () => {
        const translationKey = 'messages.thisKeyDoesNotExist';
        componentContext.translate(translationKey).should.eql(translationKey);
      });
    });

    describe('getLanguage', () => {
      it('should return the correct language', () => {
        componentContext.getLanguage().should.eql('en');
      });
    });

    describe('getLanguages', () => {
      it('should list all languages and translations', () => {
        const expectedLanguages = [
          { name: englishTranslations.name, prefix: englishTranslations.prefix },
          { name: spanishTranslations.name, prefix: spanishTranslations.prefix }
        ];

        componentContext.getLanguages().should.eql(expectedLanguages);
      });
    })
  });

  describe('actionContext', () => {
    let componentContext = {};
    let actionContext = {};

    beforeEach(() => {
      translator.plugComponentContext(componentContext);
      translator.plugActionContext(actionContext);
    });

    describe('changeLanguage', () => {
      it('should change the language', () => {
        componentContext.translate('messages.test').should.eql(englishTranslations.messages.test);
        actionContext.setLanguage('es');
        componentContext.translate('messages.test').should.eql(spanishTranslations.messages.test);
      });
    });
  });

  describe('storeContext', () => {
    let storeContext = {};

    beforeEach(() => {
      translator.plugComponentContext(storeContext);
    });

    describe('translate', () => {
      it('should provide the correct translation for a translation key', () => {
        storeContext.translate('messages.test').should.eql(englishTranslations.messages.test);
      });
    });
  });
});
