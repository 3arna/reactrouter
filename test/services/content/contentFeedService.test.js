import sinon from 'sinon';

import * as apiClient from '../../../app/apiClient/client';
import { broadcastFeed } from '../../../app/services/content/contentFeedService';

describe('services/content/contentFeedService', () => {
  describe('create', () => {
    
    let sendRequestStub;
    const expectedApiUrl = { url: '/content/publish', method: 'POST' };
    const expectedBody = { title: 'Test title', message: 'Test message ...' };
    const expectedOptions = {requestHeaders: 'Test Headers'};
    let fakeApiResponse = {id: 'testID123'};
    const fakeRequest = {}, fakeResource = {}, fakeConfigs = {};

    beforeEach(() => {
      sendRequestStub = sinon.stub(apiClient, 'sendRequest');
      sendRequestStub.withArgs(
        expectedApiUrl, 
        {requestBody: expectedBody, requestHeaders: expectedOptions.requestHeaders}, 
        null
      ).callsArgWith(3, null, fakeApiResponse);
    });

    it('should call API client with the correct parameters', (done) => {
      broadcastFeed.create(fakeRequest, fakeResource, expectedOptions, expectedBody, fakeConfigs, (error, apiResponse) => {
        apiResponse.should.eql(fakeApiResponse);
        done();
      });
    });
  });
});